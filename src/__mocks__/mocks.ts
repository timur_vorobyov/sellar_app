import {
  SubOrderHistoryProduct,
  Product,
  RefundPageProduct,
  SubOrderHistoryProductCustomisation,
} from 'types/models/product';
import FairPrice from 'components/assets/images/SubOrderPage/fairprice-logo.png';
import FairPriceLogo from 'components/assets/images/SubOrderPage/fairprice-logo.png';
import { SubOrderType } from '../constants/subOrder';
import { SubOrder, SubOrderHistory, SubOrderState, Platform } from 'types/models/subOrder';
import Deliveroo from 'components/assets/images/SubOrderPage/deliveroo-logo.png';
import DeliverooLogo from 'components/assets/images/SubOrderPage/deliveroo-logo.png';
import moment from 'moment-timezone';
import { PaymentMethod } from '../constants/payment';
import { ZReportData } from 'types/ZReportData';
import { FoodSupplierPlatform } from 'types/models/stall';
import { CustomisationOption, MenuItem } from 'types/models/menu';
import { SubOrdersHistoryFilter } from 'types/pages';

export const product: Product = {
  id: 6986,
  menuItemId: 12,
  name: 'Fishball Soup Noodle',
  nameCn: '',
  customerComments: '',
  customisations: [],
  fullProductSum: 16,
  price: 4.5,
  quantity: 1,
  containerFee: 0.3,
};

export const productCustomisation: SubOrderHistoryProductCustomisation = {
  price: 10,
  name: 'Curry',
  nameCn: '',
  quantity: 1,
  id: 10,
  optionId: 10,
  refundedQuantity: 0,
  actualQuantity: 1,
};

export const subOrderHistoryProductCustomisations: SubOrderHistoryProductCustomisation[] = [
  {
    price: 10,
    name: 'Curry',
    nameCn: 'Curry',
    quantity: 1,
    id: 10,
    optionId: 10,
    refundedQuantity: 0,
    actualQuantity: 1,
  },
  {
    price: 20,
    name: 'Cocoa milk',
    nameCn: 'Cocoa milk',
    quantity: 1,
    id: 20,
    optionId: 20,
    refundedQuantity: 0,
    actualQuantity: 1,
  },
];

export const refundPageProduct: RefundPageProduct = {
  containerFee: 0.4,
  customerComments: '',
  customisations: [],
  discountedPrice: 13.5,
  id: 7002,
  menuItemId: 300,
  name: 'Tropic Crab with tartar',
  nameCn: '',
  price: 15,
  quantity: 1,
  fullProductSum: 16,
  actualQuantity: 1,
  refundedQuantity: 0,
  returnedContainerFeeWithDiscount: 0,
};

export const products: Product[] = [
  {
    id: 6986,
    menuItemId: 12,
    name: 'Fishball Soup Noodle',
    nameCn: '',
    customerComments: '',
    price: 4.5,
    quantity: 1,
    containerFee: 0.3,
    fullProductSum: 16,
    customisations: [
      {
        price: 0,
        nameCn: '',
        name: 'Custom 1',
        quantity: 1,
        id: 10,
        optionId: 10,
        actualQuantity: 2,
      },
    ],
  },
];

export const subOrder: SubOrder = {
  id: 1,
  fees: 0,
  isCutleryNeeded: false,
  logo: FairPrice,
  state: 'SUBMITTED',
  type: SubOrderType.SelfCollect,
  createdAt: new Date(),
  graceTime: 300,
  subTotalPrice: 900,
  totalPrice: 900,
  netSales: 0,
  productsAmount: 1,
  totalContainerFeesAmount: 0,
  products: [
    {
      menuItemId: 1,
      name: 'Meatball Noodle',
      nameCn: '',
      quantity: 2,
      price: 900,
      id: 1,
      customerComments: '',
      containerFee: 0.3,
      fullProductSum: 16,
      customisations: [
        {
          price: 0,
          name: '',
          nameCn: '',
          quantity: 1,
          id: 10,
          optionId: 10,
          actualQuantity: 2,
        },
      ],
    },
  ],
  productsGroupedByCategories: [
    {
      category: {
        id: 1,
        name: '',
        nameCn: '',
      },
      list: [
        {
          menuItemId: 1,
          name: 'Meatball Noodle',
          nameCn: '',
          quantity: 2,
          price: 900,
          id: 1,
          customerComments: '',
          fullProductSum: 16,
          containerFee: 0.3,
          customisations: [
            {
              price: 10,
              name: '',
              nameCn: '',
              quantity: 1,
              id: 10,
              optionId: 10,
              actualQuantity: 2,
            },
          ],
        },
      ],
    },
  ],
  deliveryPrice: 0,
  orderId: '##MISSING##',
  customer: {
    id: 1,
    name: "DOESN'T_HAVE_FIELD",
    phoneNumber: "DOESN'T_HAVE_FIELD",
  },
  driver: {
    id: 1,
    name: "DOESN'T_HAVE_FIELD",
    phoneNumber: "DOESN'T_HAVE_FIELD",
    note: "DOESN'T_HAVE_FIELD",
  },
  payments: [],
  foodSupplierId: 1,
  location: { address: '', available: true, coordinates: '', disabled: false, id: -1, isOutlet: false, name: '' },
  pickUpTime: null,
  platform: Platform.Fairprice,
  order: {
    id: 1,
    discounts: 25,
  },
  isCancellationReceiptPrinted: false,
  isAutoAccepted: false,
  isReceiptPrinted: false,
};

export const subOrders: SubOrder[] = [
  {
    id: 1,
    fees: 0,
    isCutleryNeeded: false,
    logo: FairPrice,
    state: 'SUBMITTED',
    type: SubOrderType.SelfCollect,
    createdAt: new Date(),
    graceTime: 300,
    subTotalPrice: 900,
    totalPrice: 900,
    netSales: 0,
    productsAmount: 1,
    totalContainerFeesAmount: 0,
    products: [
      {
        menuItemId: 1,
        name: 'Meatball Noodle',
        nameCn: '',
        quantity: 2,
        price: 900,
        id: 1,
        customerComments: '',
        containerFee: 0.3,
        fullProductSum: 16,
        customisations: [
          {
            price: 0,
            name: '',
            nameCn: '',
            quantity: 1,
            id: 10,
            optionId: 10,
            actualQuantity: 2,
          },
        ],
      },
    ],
    productsGroupedByCategories: [
      {
        category: {
          id: 1,
          name: '',
          nameCn: '',
        },
        list: [
          {
            menuItemId: 1,
            name: 'Meatball Noodle',
            nameCn: '',
            quantity: 2,
            price: 900,
            id: 1,
            customerComments: '',
            fullProductSum: 16,
            containerFee: 0.3,
            customisations: [
              {
                price: 10,
                name: '',
                nameCn: '',
                quantity: 1,
                id: 10,
                optionId: 10,
                actualQuantity: 2,
              },
            ],
          },
        ],
      },
    ],
    deliveryPrice: 0,
    orderId: '##MISSING##',
    customer: {
      id: 1,
      name: "DOESN'T_HAVE_FIELD",
      phoneNumber: "DOESN'T_HAVE_FIELD",
    },
    driver: {
      id: 1,
      name: "DOESN'T_HAVE_FIELD",
      phoneNumber: "DOESN'T_HAVE_FIELD",
      note: "DOESN'T_HAVE_FIELD",
    },
    payments: [],
    foodSupplierId: 1,
    location: { address: '', available: true, coordinates: '', disabled: false, id: -1, isOutlet: false, name: '' },
    pickUpTime: null,
    platform: Platform.Fairprice,
    order: {
      id: 1,
      discounts: 25,
    },
    isCancellationReceiptPrinted: false,
    isAutoAccepted: false,
    isReceiptPrinted: false,
  },

  {
    id: 2,
    fees: 0,
    isCutleryNeeded: false,
    logo: Deliveroo,
    state: 'SUBMITTED',
    type: SubOrderType.HavingHere,
    createdAt: new Date(),
    graceTime: 300,
    subTotalPrice: 900,
    totalPrice: 900,
    productsAmount: 1,
    netSales: 0,
    totalContainerFeesAmount: 0,
    products: [
      {
        menuItemId: 1,
        name: 'Meatball Noodle',
        nameCn: '',
        quantity: 2,
        price: 900,
        id: 1,
        customerComments: '',
        containerFee: 0.3,
        fullProductSum: 16,
        customisations: [
          {
            price: 0,
            name: '',
            nameCn: '',
            quantity: 1,
            id: 10,
            optionId: 10,
            actualQuantity: 2,
          },
        ],
      },
    ],
    productsGroupedByCategories: [
      {
        category: {
          id: 1,
          name: '',
          nameCn: '',
        },
        list: [
          {
            menuItemId: 1,
            name: 'Fishball Soup Noodle',
            nameCn: '',
            quantity: 2,
            price: 900,
            id: 1,
            containerFee: 0.3,
            fullProductSum: 16,
            customerComments: '',
            customisations: [
              {
                price: 10,
                name: '',
                nameCn: '',
                quantity: 1,
                id: 10,
                optionId: 10,
                actualQuantity: 2,
              },
            ],
          },
        ],
      },
    ],
    deliveryPrice: 0,
    orderId: '##MISSING##',
    customer: {
      id: 1,
      name: "DOESN'T_HAVE_FIELD",
      phoneNumber: "DOESN'T_HAVE_FIELD",
    },
    driver: {
      id: 1,
      name: "DOESN'T_HAVE_FIELD",
      phoneNumber: "DOESN'T_HAVE_FIELD",
      note: "DOESN'T_HAVE_FIELD",
    },
    payments: [],
    foodSupplierId: 1,
    location: { address: '', available: true, coordinates: '', disabled: false, id: -1, isOutlet: false, name: '' },
    pickUpTime: null,
    platform: Platform.Deliveroo,
    order: {
      id: 1,
      discounts: 25,
    },
    isCancellationReceiptPrinted: false,
    isAutoAccepted: false,
    isReceiptPrinted: false,
  },
];

export const subOrderHistoryProduct: SubOrderHistoryProduct = {
  customerComments: '',
  menuItemId: 1,
  name: 'Fishball Soup Noodle',
  nameCn: 'Fishball Soup Noodle',
  quantity: 2,
  price: 900,
  id: 1,
  customisations: [],
  discountedPrice: 0,
  refundedQuantity: 0,
  fullProductSum: 16,
  containerFee: 0.3,
  returnedContainerFeeWithDiscount: 0,
};

export const subOrderHistoryProducts: SubOrderHistoryProduct[] = [
  {
    customerComments: '',
    menuItemId: 1,
    name: 'Fishball Soup Noodle',
    nameCn: 'Fishball Soup Noodle',
    quantity: 1,
    price: 900,
    id: 1,
    customisations: [],
    fullProductSum: 16,
    discountedPrice: 0,
    refundedQuantity: 0,
    containerFee: 0.4,
    returnedContainerFeeWithDiscount: 0,
  },
  {
    customerComments: '',
    menuItemId: 1,
    name: 'Carbonara',
    fullProductSum: 16,
    nameCn: '',
    quantity: 3,
    price: 1200,
    id: 1,
    customisations: [],
    discountedPrice: 0,
    refundedQuantity: 0,
    containerFee: 0.3,
    returnedContainerFeeWithDiscount: 0,
  },
];

export const subOrderHistory: SubOrderHistory = {
  id: 1,
  isCutleryNeeded: false,
  logo: FairPrice,
  state: SubOrderState.Incoming,
  refundState: '',
  type: SubOrderType.SelfCollect,
  createdAt: new Date(),
  subTotalPrice: 900,
  totalPrice: 900,
  totalContainerFeesAmount: 20,
  numberOfProducts: 5,
  originNetSales: 5,
  products: [
    {
      customerComments: '',
      menuItemId: 1,
      name: 'Fishball Soup Noodle',
      nameCn: '',
      quantity: 2,
      price: 900,
      id: 1,
      customisations: [],
      fullProductSum: 16,
      discountedPrice: 0,
      refundedQuantity: 0,
      containerFee: 5,
      returnedContainerFeeWithDiscount: 0,
    },
  ],
  order: {
    id: 1,
    discounts: 0,
    refunds: [],
  },
  deliveryPrice: 0,
  orderId: '##MISSING##',
  customer: {
    id: 1,
    name: "DOESN'T_HAVE_FIELD",
    phoneNumber: "DOESN'T_HAVE_FIELD",
  },
  driver: {
    id: 1,
    name: "DOESN'T_HAVE_FIELD",
    phoneNumber: "DOESN'T_HAVE_FIELD",
    note: "DOESN'T_HAVE_FIELD",
  },
  location: { address: '', available: true, coordinates: '', disabled: false, id: -1, isOutlet: false, name: '' },
  pickUpTime: null,
  platform: Platform.Fairprice,
  isCancellationReceiptPrinted: false,
  paymentsMethod: PaymentMethod.Card,
  completedTime: new Date(),
  collectionTime: new Date(),
  cancelledTime: new Date(),
  readyForCollectionTime: new Date(),
  netSales: 0,
  grossSales: 0,
  refundedPrice: 0,
  totalReturnedContainerFeesAmountWithDiscount: 0,
};

export const subOrderHistoryList: SubOrderHistory[] = [
  {
    id: 1,
    isCutleryNeeded: false,
    logo: FairPrice,
    state: 'SUBMITTED',
    type: SubOrderType.SelfCollect,
    createdAt: new Date(),
    originNetSales: 5,
    totalPrice: 500,
    numberOfProducts: 0,
    products: [
      {
        customerComments: '',
        menuItemId: 1,
        name: 'Chicken',
        nameCn: '',
        fullProductSum: 16,
        quantity: 3,
        price: 800,
        id: 1,
        customisations: [],
        discountedPrice: 0,
        refundedQuantity: 0,
        containerFee: 3,
        returnedContainerFeeWithDiscount: 0,
      },
    ],
    order: {
      id: 1,
      refunds: [],
      discounts: 25,
    },
    deliveryPrice: 0,
    orderId: '3229S',
    customer: {
      id: 1,
      name: "DOESN'T_HAVE_FIELD",
      phoneNumber: "DOESN'T_HAVE_FIELD",
    },
    driver: {
      id: 1,
      name: "DOESN'T_HAVE_FIELD",
      phoneNumber: "DOESN'T_HAVE_FIELD",
      note: "DOESN'T_HAVE_FIELD",
    },
    location: { address: '', available: true, coordinates: '', disabled: false, id: -1, isOutlet: false, name: '' },
    pickUpTime: null,
    platform: Platform.Deliveroo,
    isCancellationReceiptPrinted: true,
    completedTime: new Date(),
    collectionTime: new Date(),
    cancelledTime: new Date(),
    readyForCollectionTime: new Date(),
    subTotalPrice: 900,
    paymentsMethod: PaymentMethod.Card,
    refundState: '',
    netSales: 900,
    grossSales: 900,
    totalReturnedContainerFeesAmountWithDiscount: 0,
    refundedPrice: 0,
    totalContainerFeesAmount: 0,
  },
  {
    id: 2,
    isCutleryNeeded: false,
    logo: FairPrice,
    state: 'SUBMITTED',
    type: SubOrderType.SelfCollect,
    createdAt: new Date(),
    subTotalPrice: 900,
    originNetSales: 5,
    totalPrice: 900,
    numberOfProducts: 0,
    products: [
      {
        customerComments: '',
        menuItemId: 1,
        name: 'Fishball Soup Noodle',
        nameCn: '',
        quantity: 2,
        price: 900,
        id: 1,
        customisations: [],
        discountedPrice: 0,
        fullProductSum: 16,
        containerFee: 3,
        refundedQuantity: 0,
        returnedContainerFeeWithDiscount: 0,
      },
    ],
    order: {
      id: 1,
      refunds: [],
      discounts: 25,
    },
    deliveryPrice: 0,
    orderId: '7822S',
    customer: {
      id: 1,
      name: "DOESN'T_HAVE_FIELD",
      phoneNumber: "DOESN'T_HAVE_FIELD",
    },
    driver: {
      id: 1,
      name: "DOESN'T_HAVE_FIELD",
      phoneNumber: "DOESN'T_HAVE_FIELD",
      note: "DOESN'T_HAVE_FIELD",
    },
    location: { address: '', available: true, coordinates: '', disabled: false, id: -1, isOutlet: false, name: '' },
    pickUpTime: null,
    platform: Platform.Deliveroo,
    isCancellationReceiptPrinted: true,
    completedTime: new Date(),
    collectionTime: new Date(),
    cancelledTime: new Date(),
    readyForCollectionTime: new Date(),
    paymentsMethod: PaymentMethod.Card,
    refundState: '',
    netSales: 900,
    grossSales: 900,
    totalReturnedContainerFeesAmountWithDiscount: 0,
    refundedPrice: 0,
    totalContainerFeesAmount: 0,
  },
];

export const filters: SubOrdersHistoryFilter = {
  subOrderStatuses: [1, 2, 3, 4],
  date: moment(),
  platformsIds: [],
  searchTerm: '',
};

export const report: ZReportData = {
  businessDate: '',
  discountsSum: 900,
  grossTotalSum: 900,
  gst: 900,
  qc: { FAIRPRICE: 56 },
  netTotalSum: 900,
  foodSupplierId: 1,
  refundSum: 0,
  qcFunded: 0,
  amountReceived: 0,
  tenantFunded: 0,
  containerFees: 0,
};

export const foodSupplierPlatforms: FoodSupplierPlatform[] = [
  {
    id: '1',
    name: Platform.Fairprice,
    logo: FairPriceLogo,
    status: 'OPEN',
    autoAccept: false,
    autoPrint: false,
    prepTime: '0',
    manualAcceptOnly: false,
    autoAcceptOnly: false,
    isLongRingtone: false,
  },
  {
    id: '2',
    name: Platform.Deliveroo,
    logo: DeliverooLogo,
    status: 'OPEN',
    autoAccept: false,
    autoPrint: false,
    prepTime: '0',
    manualAcceptOnly: false,
    isLongRingtone: false,
  },
];

export const filteredMenuItems: MenuItem[] = [
  {
    id: 1603,
    category: {
      name: 'Add On',
      nameCn: 'Add On',
    },
    name: 'Bee hoon',
    nameCn: 'Bee hoon 米粉',
    available: 1,
    unavailableUntil: null,
    activeHours: {
      friday: {
        timings: [
          {
            openTime: '14:00:00',
            closeTime: '16:00:00',
          },
        ],
      },
      monday: {
        timings: [
          {
            openTime: '14:00:00',
            closeTime: '16:00:00',
          },
        ],
      },
      sunday: {
        timings: [
          {
            openTime: '12:00:00',
            closeTime: '16:00:00',
          },
        ],
      },
      tuesday: {
        timings: [
          {
            openTime: '14:00:00',
            closeTime: '16:00:00',
          },
        ],
      },
      saturday: {
        timings: [
          {
            openTime: '14:00:00',
            closeTime: '16:00:00',
          },
        ],
      },
      thursday: {
        timings: [
          {
            openTime: '14:00:00',
            closeTime: '16:00:00',
          },
        ],
      },
      wednesday: {
        timings: [
          {
            openTime: '14:00:00',
            closeTime: '16:00:00',
          },
        ],
      },
    },
  },
  {
    id: 1604,
    category: {
      name: 'Add On',
      nameCn: 'Add On',
    },
    name: 'Kuey Chap',
    nameCn: 'Kuey Chap 粿什',
    available: 1,
    unavailableUntil: null,
    activeHours: {
      friday: {
        timings: [
          {
            openTime: '14:00:00',
            closeTime: '16:00:00',
          },
        ],
      },
      monday: {
        timings: [
          {
            openTime: '14:00:00',
            closeTime: '16:00:00',
          },
        ],
      },
      sunday: {
        timings: [
          {
            openTime: '12:00:00',
            closeTime: '15:00:00',
          },
        ],
      },
      tuesday: {
        timings: [
          {
            openTime: '14:00:00',
            closeTime: '19:00:00',
          },
        ],
      },
      saturday: {
        timings: [
          {
            openTime: '14:00:00',
            closeTime: '16:00:00',
          },
        ],
      },
      thursday: {
        timings: [
          {
            openTime: '13:00:00',
            closeTime: '16:00:00',
          },
        ],
      },
      wednesday: {
        timings: [
          {
            openTime: '13:00:00',
            closeTime: '16:00:00',
          },
        ],
      },
    },
  },
  {
    id: 1628,
    category: {
      name: 'Add On',
      nameCn: 'Add On',
    },
    name: 'White Rice',
    nameCn: 'White Rice 白米',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 2152,
    category: {
      name: 'Click & Collect Exclusive',
      nameCn: 'Click & Collect Exclusive',
    },
    name: 'Braised egg (App exclusive)',
    nameCn: 'Salted vegetable (App exclusive)',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 2153,
    category: {
      name: 'Click & Collect Exclusive',
      nameCn: 'Click & Collect Exclusive',
    },
    name: 'Salted vegetable (App exclusive)',
    nameCn: 'Salted vegetable (App exclusive)',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1589,
    category: {
      name: 'Mains',
      nameCn: 'Mains',
    },
    name: '(A3) Pork Leg Fried beehoon ',
    nameCn: '(A3) Pork Leg Fried beehoon  (A3) 猪腿炒米粉',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1590,
    category: {
      name: 'Mains',
      nameCn: 'Mains',
    },
    name: 'kway chap set (beancurd, beancurd puff,fish cake,kai lan,kway chap)(HEALTHY CHOICE)',
    nameCn:
      'kway chap set (beancurd, beancurd puff,fish cake,kai lan, kway chap)(HEALTHY CHOICE) 粿什(豆腐, 豆腐泡芙, 鱼糕, kai lan, kway chap)(HEALTHY CHOICE)',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1591,
    category: {
      name: 'Mains',
      nameCn: 'Mains',
    },
    name: 'Crispy Pork belly (10 CUT PCS)',
    nameCn: 'Crispy Pork belly (10 CUT PCS) 脆皮五花肉（10 个切块）',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1592,
    category: {
      name: 'Mains',
      nameCn: 'Mains',
    },
    name: 'Mixed Platter ',
    nameCn: 'Mixed Platter  混合拼盘',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1596,
    category: {
      name: 'Mains',
      nameCn: 'Mains',
    },
    name: '(A2) Pork Belly Rice Set (w Egg & Veg)',
    nameCn: '(A2) Pork Belly Rice Set (w Egg & Veg) (A2) 猪肚饭套餐（蛋和蔬菜）',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1600,
    category: {
      name: 'Mains',
      nameCn: 'Mains',
    },
    name: 'Crispy Pork Belly Set',
    nameCn: 'Crispy Pork Belly Set 炸三层肉套餐',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1626,
    category: {
      name: 'Mains',
      nameCn: 'Mains',
    },
    name: 'Braised Pork Leg (half)',
    nameCn: 'Braised Pork Leg (half) 红烧猪腿（半只）',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1627,
    category: {
      name: 'Mains',
      nameCn: 'Mains',
    },
    name: 'Braised Pork Leg (Whole)',
    nameCn: 'Braised Pork Leg (Whole) 红烧猪腿（整条）',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1634,
    category: {
      name: 'Mains',
      nameCn: 'Mains',
    },
    name: '(A1) Thai Pork Leg Rice Sets',
    nameCn: '(A1) Thai Pork Leg Rice Sets (A1) 猪腿饭套餐',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1593,
    category: {
      name: 'Sides',
      nameCn: 'Sides',
    },
    name: 'Fried pig intestine',
    nameCn: 'Fried pig intestine',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1594,
    category: {
      name: 'Sides',
      nameCn: 'Sides',
    },
    name: 'Fish Sausage',
    nameCn: 'Fish Sausage',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1595,
    category: {
      name: 'Sides',
      nameCn: 'Sides',
    },
    name: 'Braised egg ',
    nameCn: 'Braised egg  蛋',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1597,
    category: {
      name: 'Sides',
      nameCn: 'Sides',
    },
    name: 'Braised Pig Intestine',
    nameCn: 'Braised Pig Intestine 猪肠',
    available: 0,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1598,
    category: {
      name: 'Sides',
      nameCn: 'Sides',
    },
    name: 'salted vegetable',
    nameCn: 'salted vegetable 咸菜',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1599,
    category: {
      name: 'Sides',
      nameCn: 'Sides',
    },
    name: 'Fish Cake',
    nameCn: 'Fish Cake 鱼饼',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1601,
    category: {
      name: 'Sides',
      nameCn: 'Sides',
    },
    name: 'Tao Po/Dried Beancurd',
    nameCn: 'Tao Po/Dried Beancurd 豆粕/豆腐干',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
  {
    id: 1602,
    category: {
      name: 'Sides',
      nameCn: 'Sides',
    },
    name: 'Kai Lan',
    nameCn: 'Kai Lan 芥兰',
    available: 1,
    unavailableUntil: null,
    activeHours: {},
  },
];

export const filteredCustomisationOptions: CustomisationOption[] = [
  {
    id: 4,
    name: 'Egg',
    nameCn: '蛋',
    available: 1,
    customisationId: 2,
    customisation: {
      name: 'Add-ons',
      nameCn: 'Add-ons',
    },
  },
  {
    id: 3,
    name: 'Rice',
    nameCn: '米',
    available: 1,
    customisationId: 2,
    customisation: {
      name: 'Add-ons',
      nameCn: 'Add-ons',
    },
  },
  {
    id: 59,
    name: 'free option',
    nameCn: 'free option',
    available: 1,
    customisationId: 25,
    customisation: {
      name: 'Free Customisations',
      nameCn: 'Free Customisationss',
    },
  },
];
