import { getDeviceDataForZReport } from 'apis/rest/catalog';
import { getFoodSupplierForZReport } from 'apis/graphql/hasura/queries';
import config from 'config';
import { toast } from 'react-toastify';
import { AcceptedSubOrder, CancelledSubOrder, SubOrderHistory, Platform } from 'types/models/subOrder';
import { Product } from 'types/models/product';
import { ZReportData } from 'types/ZReportData';
import { sliceTwoMoreDecimals, сonvertFromCentsToDollars } from 'utils/formater';
import { receiptTypes } from 'constants/receiptType';
import { convertToSingaporeTime } from './datetime';
import { getSubOrderById } from 'apis/rest/subOrders';
import axios from 'axios';
import { SubOrderType, SubOrderTypeTranslations } from 'constants/subOrder';
/**
 
Mobile:
  body - font-size: 24 charLength: 32
  header - font-size: 40 charLength: 19

Tablet:
  body - font-size: 30 charLength: 34
  header - font-size: 38 charLength: 26

 */
interface FormatReceipResult {
  header?: {
    image_url: string;
    text: string;
    font_size: number;
  };
  body: string;
  font_size: number;
}

const getReceiptImageUrl = (platform: Platform): string => {
  switch (platform) {
    case Platform.Foodpanda:
      return `${process.env.PUBLIC_URL}/Receipt/receipt-logo-foodpanda.jpg`;
    case Platform.Grab:
      return `${process.env.PUBLIC_URL}/Receipt/receipt-logo-grabfood.jpg`;
    case Platform.Fairprice:
    default:
      return `${process.env.PUBLIC_URL}/Receipt/receipt-logo-fairprice.jpg`;
  }
};

const allowedLength = (fontSize: number, isTablet: boolean) => {
  const allowedMobileLineLengthInHeader = 19;
  const allowedMobileLineLengthInBody = 32;
  const allowedTabletLineLengthInHeader = 26;
  const allowedTabletLineLengthInBody = 34;

  const mobileHeaderFontSize = 40;
  const tabletHeaderFontSize = 38;
  if (isTablet) {
    return fontSize === tabletHeaderFontSize ? allowedTabletLineLengthInHeader : allowedTabletLineLengthInBody;
  }
  return fontSize === mobileHeaderFontSize ? allowedMobileLineLengthInHeader : allowedMobileLineLengthInBody;
};

const moveExtrawordsToTheNextLine = (phrase: string, fontSize: number, isTablet: boolean): string => {
  if (phrase.length >= allowedLength(fontSize, isTablet)) {
    const newArr = [] as string[];
    const wordsArr = phrase.split(' ');
    let i = 0;
    for (const word of wordsArr) {
      i += word.length + 1;
      if (i <= allowedLength(fontSize, isTablet)) {
        newArr.push(word + ' ');
      } else {
        newArr.push('\n', word + ' ');
        i = word.length + 1;
      }
    }

    return newArr.join('');
  }

  return phrase;
};

const spaceCreatorBetweenWords = (existedLength: number, wordLength: number, fontSize: number, isTablet: boolean) => {
  const fullLength = existedLength + wordLength;
  if (fullLength < allowedLength(fontSize, isTablet)) {
    const neededLength = allowedLength(fontSize, isTablet) - fullLength;

    return Array(neededLength).fill(' ').join('');
  }

  return '\n';
};

const centralizeWord = (word: string, fontSize: number, isTablet: boolean) => {
  const freeSpace = 42 - word.length;
  return spaceCreatorBetweenWords(freeSpace / 2, word.length, fontSize, isTablet) + word;
};

const serializePlatforms = (platforms: { [key: string]: number }, fontSize: number, isTablet: boolean) => {
  const lines = [];
  const spaceLength = 1;
  for (const name in platforms) {
    const platformName = name + ':';
    const productLine = `${
      platformName +
      spaceCreatorBetweenWords(
        platformName.length + spaceLength,
        `${сonvertFromCentsToDollars(platforms[name])}`.length + spaceLength,
        fontSize,
        isTablet,
      ) +
      '$' +
      sliceTwoMoreDecimals(сonvertFromCentsToDollars(platforms[name]))
    }`;
    lines.push(productLine);
  }

  return lines;
};

const alignThreeColumns = (
  firstColumn: string,
  secondColumn: string,
  thirdColumn: string,
  fontSize: number,
  isTablet: boolean,
): string => {
  const firstColumnLength = firstColumn.length;
  const thirdColumnLength = thirdColumn.length;
  const spaceLengthBetweenData = 2;
  const spaceBetweenData = Array(spaceLengthBetweenData).fill(' ').join('');
  const spaceLengthAllowedForSecondColumn =
    spaceCreatorBetweenWords(firstColumnLength, thirdColumnLength, fontSize, isTablet).length -
    spaceLengthBetweenData * 2;
  const productNameArr = secondColumn.split(' ');
  let i = 0;
  const secondColumnNameWithinAllowedLength = [];
  const secondColumnNameOutsideAllowedLength = [];

  for (const word of productNameArr) {
    i += word.length + 1;
    if (i < spaceLengthAllowedForSecondColumn) {
      secondColumnNameWithinAllowedLength.push(word);
    } else {
      if (productNameArr.length === 1) {
        secondColumnNameWithinAllowedLength.push(word.slice(0, spaceLengthAllowedForSecondColumn));
        secondColumnNameOutsideAllowedLength.push(word.slice(spaceLengthAllowedForSecondColumn));
      } else {
        secondColumnNameOutsideAllowedLength.push(word);
      }
    }
  }

  const isSecondColumnLengthWhithinAllowedValue = !secondColumnNameOutsideAllowedLength.length;
  let line = `${
    firstColumn +
    spaceBetweenData +
    secondColumnNameWithinAllowedLength.join(' ') +
    spaceCreatorBetweenWords(
      firstColumnLength + secondColumnNameWithinAllowedLength.join(' ').length + spaceLengthBetweenData,
      thirdColumnLength,
      fontSize,
      isTablet,
    ) +
    thirdColumn
  }`;

  if (!isSecondColumnLengthWhithinAllowedValue) {
    line +=
      '\n' +
      alignThreeColumns(
        Array(firstColumnLength).fill(' ').join(''),
        secondColumnNameOutsideAllowedLength.join(' '),
        Array(thirdColumnLength).fill(' ').join(''),
        fontSize,
        isTablet,
      );
  }

  return line;
};

export const formatAcceptedReceipt = (
  appPaymentDiscount: number,
  subOrder: AcceptedSubOrder | SubOrderHistory,
  locale: string,
  receiptType: string,
  isQRQodeShown: string,
  isTablet: boolean,
): FormatReceipResult => {
  const isCustomerReceipt = receiptType === receiptTypes.CUSTOMER;
  const bodySeparator = isTablet ? '----------------------------------' : '--------------------------------';
  const headerSeparator = isTablet ? '-------------------------' : '-------------------';
  const palcedTime = convertToSingaporeTime(subOrder.createdAt, 'DD/MM/YYYY hh:mm a');
  const pickeUpTime = subOrder.pickUpTime ? convertToSingaporeTime(subOrder.pickUpTime, 'DD/MM/YYYY hh:mm a') : '??:??';
  const productsAmount = `${subOrder.products.length} ${subOrder.products.length > 1 ? 'items' : 'item'}`;
  const subtotalPrice = `$${sliceTwoMoreDecimals(subOrder.subTotalPrice)}`;
  const deliveryPrice = `$${sliceTwoMoreDecimals(subOrder.deliveryPrice)}`;
  const appPaymentdiscounts = `-$${sliceTwoMoreDecimals(appPaymentDiscount)}`;
  const containerFee = `$${sliceTwoMoreDecimals(subOrder.totalContainerFeesAmount)}`;
  const totalPrice = `$${sliceTwoMoreDecimals(subOrder.netSales)}`;
  const subOrderType = SubOrderTypeTranslations(locale)[subOrder.type];
  const cutleryOption = () => {
    if (subOrder.isCutleryNeeded) {
      return locale === 'en' || isCustomerReceipt ? 'Cutlery needed' : 'Cutlery needed 需要餐具';
    }

    return locale === 'en' || isCustomerReceipt ? 'No cutlery needed' : 'No cutlery needed 不需要餐具';
  };
  const pickUpTimeText = locale === 'en' || isCustomerReceipt ? 'Pick up time' : 'Pick up time 接取时间';

  const serializeProducts = (
    products: Product[],
    isCustomerReceipt: boolean,
    fontSize: number,
    isTablet: boolean,
  ): string[] => {
    return products.reduce<string[]>((lines, product) => {
      const productPrice = '$' + sliceTwoMoreDecimals(product.fullProductSum);
      const productQuantity = 'x' + product.quantity;
      let productName = locale === 'en' || isCustomerReceipt ? product.name : product.nameCn;
      if (!productName) {
        productName = product.name;
      }
      const spaceEqualToProductPriceLength = Array(productPrice.length).fill(' ').join('');
      const spaceEqualToProductQuantityLength = Array(productQuantity.length).fill(' ').join('');
      const productLine = alignThreeColumns(productQuantity, productName, productPrice, fontSize, isTablet);
      const customizationsLines = product.customisations.map((cust) => {
        let custName = locale === 'en' ? cust.name : cust.nameCn;
        if (!custName) {
          custName = cust.name;
        }
        return alignThreeColumns(
          spaceEqualToProductQuantityLength,
          `${custName + ' ' + 'x' + cust.quantity}`,
          spaceEqualToProductPriceLength,
          fontSize,
          isTablet,
        );
      });
      const comments = alignThreeColumns(
        spaceEqualToProductQuantityLength,
        `Remarks: ${product.customerComments}`,
        spaceEqualToProductPriceLength,
        fontSize,
        isTablet,
      );
      lines.push(productLine);
      lines.push(...customizationsLines);
      product.customerComments ? lines.push(comments) : null;

      return lines;
    }, []);
  };

  const headerFontSize = isTablet ? 38 : 40;
  const bodyFontSize = isTablet ? 30 : 24;
  const headerText = [
    subOrder.orderId,
    headerSeparator,
    moveExtrawordsToTheNextLine(subOrderType, headerFontSize, isTablet),
    headerSeparator,
    moveExtrawordsToTheNextLine(pickUpTimeText, headerFontSize, isTablet),
    moveExtrawordsToTheNextLine(pickeUpTime, headerFontSize, isTablet),
    headerSeparator,
    `${productsAmount}`,
    'PAID',
    headerSeparator,
    moveExtrawordsToTheNextLine(cutleryOption(), headerFontSize, isTablet),
    headerSeparator,
  ];

  const body = [
    subOrder.location?.name,
    `Placed on ${spaceCreatorBetweenWords(10, palcedTime.length, bodyFontSize, isTablet) + palcedTime}`,
    bodySeparator,
    ...serializeProducts(subOrder.products, isCustomerReceipt, bodyFontSize, isTablet),
    bodySeparator,
    `Subtotal ${spaceCreatorBetweenWords(9, subtotalPrice.length, bodyFontSize, isTablet) + subtotalPrice}`,
    `Container Fee ${spaceCreatorBetweenWords(14, containerFee.length, bodyFontSize, isTablet) + containerFee}`,
    `App Payment Discount(10%) ${
      spaceCreatorBetweenWords(26, appPaymentdiscounts.length, bodyFontSize, isTablet) + appPaymentdiscounts
    }`,
  ];

  if (subOrder.type === SubOrderType.Delivery) {
    body.push(
      `Delivery fee ${spaceCreatorBetweenWords(13, deliveryPrice.length, bodyFontSize, isTablet) + deliveryPrice}`,
    );
  }

  body.push(`Total ${spaceCreatorBetweenWords(6, totalPrice.length, bodyFontSize, isTablet) + totalPrice}`);

  if (isCustomerReceipt && isQRQodeShown === 'on') {
    body.push('<qr>https://www.fairprice.com.sg/</qr>');
  }

  return {
    header: {
      image_url: window.location.origin + getReceiptImageUrl(subOrder.platform),
      text: headerText.join('\n'),
      font_size: headerFontSize,
    },
    body: body.join('\n'),
    font_size: bodyFontSize,
  };
};

export const formatCancelledReceipt = (
  subOrder: CancelledSubOrder | SubOrderHistory,
  isTablet: boolean,
): FormatReceipResult => {
  const bodySeparator = isTablet ? '----------------------------------' : '--------------------------------';
  const headerFontSize = isTablet ? 38 : 40;
  const bodyFontSize = isTablet ? 30 : 24;

  return {
    header: {
      image_url: window.location.origin + getReceiptImageUrl(subOrder.platform),
      text: subOrder.orderId,
      font_size: headerFontSize,
    },
    body: [
      bodySeparator,
      subOrder.location?.name,
      `${convertToSingaporeTime(subOrder.createdAt, 'DD/MM/YYYY hh:mm a')}`,
      bodySeparator,
      `Order Cancelled`,
    ].join('\n'),
    font_size: bodyFontSize,
  };
};

export const formatZReportReceipt = (
  ZReportData: ZReportData,
  stallNumber: string,
  machineNumber: string,
  isTablet: boolean,
) => {
  const asteriskSeparator = isTablet ? '**********************************' : '********************************';
  const hyphenSeparator = isTablet ? '----------------------------------' : '--------------------------------';
  const grossTotalSum = `$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(ZReportData.grossTotalSum))}`;
  const netTotalSum = `$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(ZReportData.netTotalSum))}`;
  const refundSum = `$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(ZReportData.refundSum))}`;
  const gst = `$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(ZReportData.gst))}`;
  const bodyFontSize = isTablet ? 30 : 24;
  const qcFunded = `$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(ZReportData.qcFunded))}`;
  const amountReceived = `$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(ZReportData.amountReceived))}`;
  const tenantFunded = `$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(ZReportData.tenantFunded))}`;
  const containerFees = `$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(ZReportData.containerFees))}`;

  return {
    body: [
      `Stall ID: ${stallNumber}`,
      `Machine number: ${machineNumber}`,
      `Business Date fee: ${convertToSingaporeTime(ZReportData.businessDate, 'DD/MM/YYYY')}\n`,
      `${asteriskSeparator}`,
      `${centralizeWord('Z - REPORT', bodyFontSize, isTablet)}`,
      `${asteriskSeparator}\n\n`,
      `${centralizeWord('Sales Overview', bodyFontSize, isTablet)}\n`,
      `${hyphenSeparator}`,
      `Gross Sales : ${spaceCreatorBetweenWords(14, grossTotalSum.length, bodyFontSize, isTablet) + grossTotalSum}`,
      `QC-funded : ${spaceCreatorBetweenWords(12, qcFunded.length, bodyFontSize, isTablet) + qcFunded}`,
      `Tenant-funded : ${spaceCreatorBetweenWords(16, tenantFunded.length, bodyFontSize, isTablet) + tenantFunded}`,
      `Container fees : ${spaceCreatorBetweenWords(17, containerFees.length, bodyFontSize, isTablet) + containerFees}`,
      `Refunds : ${spaceCreatorBetweenWords(10, refundSum.length, bodyFontSize, isTablet) + refundSum}`,
      `Net Sales : ${spaceCreatorBetweenWords(12, netTotalSum.length, bodyFontSize, isTablet) + netTotalSum}`,
      `Inclusive GST 7% : ${spaceCreatorBetweenWords(19, gst.length, bodyFontSize, isTablet) + gst}`,
      `Actual Sales : ${spaceCreatorBetweenWords(15, grossTotalSum.length, bodyFontSize, isTablet) + grossTotalSum}\n`,
      `Channels:\n`,
      ...serializePlatforms(ZReportData.qc, bodyFontSize, isTablet),
      `Amount received : ${
        spaceCreatorBetweenWords(18, amountReceived.length, bodyFontSize, isTablet) + amountReceived
      }`,
    ].join('\n'),
  };
};

export const printAcceptedReceipt = async (
  subOrder: AcceptedSubOrder | SubOrderHistory,
  locale: string,
  receiptType: string,
  isQRQodeShown: string,
  isTablet: boolean,
): Promise<number | undefined> => {
  const subOrderData = await getSubOrderById(subOrder.id);
  const payload = formatAcceptedReceipt(
    сonvertFromCentsToDollars(subOrderData.appPaymentDiscount),
    subOrder,
    locale,
    receiptType,
    isQRQodeShown,
    isTablet,
  );

  try {
    const response = await axios.post(`${config.printServerUrl}/print`, payload);
    return response.status;
  } catch (error) {
    console.log('unexpected error: ', error);
    return 500;
  }
};

export const printCancelledReceipt = async (
  subOrder: CancelledSubOrder | SubOrderHistory,
  isTablet: boolean,
): Promise<void> => {
  const payload = formatCancelledReceipt(subOrder, isTablet);

  await axios
    .post(`${config.printServerUrl}/print`, payload)
    .then((response) => console.log(response.data))
    .catch((error) => {
      console.error(error);
    })
    .finally(() => {
      toast.info(`Cancellation receipt for order ${subOrder.orderId} printed`);
    });
};

export const printZReportReceipt = async (
  zReportData: ZReportData | null,
  serialNumber: string | null,
  isTablet: boolean,
): Promise<void> => {
  const foodSupplierId = zReportData?.foodSupplierId ?? 0;
  const foodSuplier = await getFoodSupplierForZReport(foodSupplierId.toString());
  const deviceResponse = serialNumber
    ? await getDeviceDataForZReport(serialNumber)
    : { name: 'SERIAL NUMBER IS MISSING' };
  const payload = zReportData
    ? formatZReportReceipt(zReportData, foodSuplier.stallNumber, deviceResponse?.name, isTablet)
    : null;
  await axios
    .post(`${config.printServerUrl}/print`, payload)
    .then((response) => console.log(response.data))
    .catch((error) => {
      console.error(error);
      // Accoprding to Hangky it is valid situation when server return 415
    })
    .finally(() => {
      toast.info(`Receipt for Z-REPORT printed`);
    });
};
