import '@testing-library/jest-dom';
import { cleanup } from '@testing-library/react';
import { formatAcceptedReceipt, formatCancelledReceipt, formatZReportReceipt } from './receipt';
import { report, subOrder } from '../__mocks__/mocks';
import moment from 'moment-timezone';
import { sliceTwoMoreDecimals, сonvertFromCentsToDollars } from './formater';

afterEach(cleanup);
const receiptLogoUrl = 'http://localhost/Receipt/receipt-logo-fairprice.jpg';
const receiptText =
  '##MISSING##\n' +
  '-------------------\n' +
  'Takeaway 打包\n' +
  '-------------------\n' +
  'Pick up time 接取时间\n' +
  '??:??\n' +
  '-------------------\n' +
  '1 item\n' +
  'PAID\n' +
  '-------------------\n' +
  'No cutlery needed \n' +
  '不需要餐具 \n' +
  '-------------------';

const bodyText =
  '\n' +
  `Placed on    ${moment.tz('Asia/Singapore').format('DD/MM/YYYY hh:mm a')}\n` +
  '--------------------------------\n' +
  'x2  Meatball Noodle       $16.00\n' +
  '     x1                         \n' +
  '--------------------------------\n' +
  'Subtotal                 $900.00\n' +
  'Container Fee              $0.00\n' +
  'App Payment Discount(10%) \n' +
  '-$4.08\n' +
  'Total                      $0.00';

describe('Receipt Util functions', () => {
  it('should run formatAcceptedReceipt', () => {
    const returnValue = formatAcceptedReceipt(4.08, subOrder, 'cn', 'tennant', 'off', false);
    expect(returnValue.header?.image_url).toBe(receiptLogoUrl);
    expect(returnValue.header?.text).toBe(receiptText);
    expect(returnValue.header?.font_size).toBe(40);
    expect(returnValue.body).toBe(bodyText);
    expect(returnValue.font_size).toBe(24);
  });

  it('should run formatCancelledReceipt', () => {
    const returnValue = formatCancelledReceipt(subOrder, false);
    expect(returnValue.header?.image_url).toBe(receiptLogoUrl);
    expect(returnValue.header?.text).toBe(subOrder.orderId);
    expect(returnValue.header?.font_size).toBe(40);
    expect(returnValue.body.includes('Order Cancelled')).toBeTruthy();
    expect(returnValue.font_size).toBe(24);
  });

  it('should run formatZReportReceipt ', () => {
    const stallNumber = '10';
    const machineNumber = '1121122';
    const grossTotalSum = `Gross Sales :              $${sliceTwoMoreDecimals(
      сonvertFromCentsToDollars(report.grossTotalSum),
    )}`;
    const netTotalSum = `Net Sales :                $${sliceTwoMoreDecimals(
      сonvertFromCentsToDollars(report.netTotalSum),
    )}`;
    const refundSum = `Refunds :                  $${sliceTwoMoreDecimals(
      сonvertFromCentsToDollars(report.refundSum),
    )}`;
    const gst = `Inclusive GST 7% :         $${sliceTwoMoreDecimals(сonvertFromCentsToDollars(report.gst))}`;
    const qcFunded = `QC-funded :                $${sliceTwoMoreDecimals(сonvertFromCentsToDollars(report.qcFunded))}`;
    const tenantFunded = `Tenant-funded :            $${sliceTwoMoreDecimals(
      сonvertFromCentsToDollars(report.tenantFunded),
    )}`;
    const containerFees = `Container fees :           $${sliceTwoMoreDecimals(
      сonvertFromCentsToDollars(report.containerFees),
    )}`;
    const actualSales = `Actual Sales :             $${sliceTwoMoreDecimals(
      сonvertFromCentsToDollars(report.grossTotalSum),
    )}`;
    const returnValue = formatZReportReceipt(report, stallNumber, machineNumber, false);
    expect(returnValue.body.includes(`Stall ID: ${stallNumber}`)).toBeTruthy();
    expect(returnValue.body.includes(`Machine number: ${machineNumber}`)).toBeTruthy();
    expect(returnValue.body.includes('Z - REPORT')).toBeTruthy();
    expect(returnValue.body.includes('Sales Overview')).toBeTruthy();
    expect(returnValue.body.includes(grossTotalSum)).toBeTruthy();
    expect(returnValue.body.includes(netTotalSum)).toBeTruthy();
    expect(returnValue.body.includes(refundSum)).toBeTruthy();
    expect(returnValue.body.includes(gst)).toBeTruthy();
    expect(returnValue.body.includes(qcFunded)).toBeTruthy();
    expect(returnValue.body.includes(tenantFunded)).toBeTruthy();
    expect(returnValue.body.includes(containerFees)).toBeTruthy();
    expect(returnValue.body.includes(actualSales)).toBeTruthy();
  });
});
