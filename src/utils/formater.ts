export const сonvertFromCentsToDollars = (price: number): number => {
  return price / 100;
};

export const sliceTwoMoreDecimals = (price: number): string => {
  return price.toFixed(2);
};
