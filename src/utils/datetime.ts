import { DEFAULT_ORDER_ACCEPT_GRACE_TIME } from 'constants/subOrder';
import { SubOrder, SubOrderState } from 'types/models/subOrder';
import moment from 'moment-timezone';

export const toDate = (value: string): Date => {
  if (!value.includes('Z')) {
    value = `${value}Z`;
  }

  return new Date(value);
};

export const convertToSingaporeTime = (value: string | Date, format: string) => {
  return moment.tz(value, 'Asia/Singapore').format(format);
};

export const getExpiresInSeconds = (
  createdAt: Date,
  graceTimeSec: string | number = DEFAULT_ORDER_ACCEPT_GRACE_TIME,
): number => {
  const created = moment.utc(createdAt);
  const now = moment.utc(Date.now());
  const difference = now.diff(created, 'second');

  return parseInt(`${graceTimeSec}`) - difference;
};

export const isSubOrderExpired = (subOrder: SubOrder): boolean =>
  getExpiresInSeconds(subOrder.createdAt, subOrder.graceTime) > 0;

export const getSubOrdersTimers = (subOrders: SubOrder[]) => {
  return subOrders
    .filter((subOrder) => {
      if (subOrder.state === SubOrderState.Accepted) {
        return true;
      }

      return isSubOrderExpired(subOrder);
    })
    .map((subOrder) => {
      return {
        subOrderId: subOrder.id,
        expireIn: getExpiresInSeconds(subOrder.createdAt, subOrder.graceTime),
      };
    });
};
