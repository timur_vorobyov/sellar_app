import { useStateContext } from 'contexts/store';
import { useState, useEffect, useCallback } from 'react';
import { toast } from 'react-toastify';
import { ApiCallOptions, defaultApiCallOptions } from 'types/async-hooks';
import { Anything, GenericAsyncFunction } from 'types/generics';
import { ActionTypes } from 'types/store';

const useExplicitApiCall = <T>(
  asyncApiFunction: GenericAsyncFunction,
  options: ApiCallOptions = defaultApiCallOptions,
) => {
  const { args, notifyOnSuccess, notifyOnFail, onSuccess, onFail, useLoader } = options || {};
  const [data, setData] = useState<T | null>(null);
  const [error, setError] = useState<unknown>(null);
  const [isLoading, setIsLoading] = useState(false);
  const [runExecution, setRunExecution] = useState(false);
  const [fnArgs, setFnArgs] = useState<Anything[]>(args || []);

  const { dispatch } = useStateContext();

  useEffect(() => {
    if (useLoader) {
      dispatch({
        type: ActionTypes.SET_LOADER_STATE,
        payload: { loading: isLoading },
      });
    }
  }, [isLoading]);

  const execute = (...execArgs: Anything[]) => {
    if (execArgs && execArgs.length) {
      setFnArgs(execArgs);
    }

    setRunExecution(true);
  };

  const makeCall = useCallback(() => {
    setIsLoading(true);

    asyncApiFunction(...fnArgs)
      .then((response: Anything) => {
        if (notifyOnSuccess) {
          toast(notifyOnSuccess || 'Operation finished successfuly.');
        }

        if (onSuccess) {
          onSuccess(response, fnArgs || []);
        }

        setData(response || null);
      })
      .catch((error: unknown) => {
        if (notifyOnFail) {
          toast.error(notifyOnFail || `Unable to perform operation: ${error}`);
        }

        if (onFail) {
          onFail(error, fnArgs || []);
        }

        console.error('useExplicitApiCall error', error);
        setError(error);
      })
      .finally(() => setIsLoading(false));
  }, [asyncApiFunction, fnArgs, notifyOnSuccess, notifyOnFail, onSuccess, onFail]);

  useEffect(() => {
    if (runExecution && !isLoading) {
      setRunExecution(false);
      makeCall();
    }
  }, [isLoading, makeCall, runExecution, args, notifyOnSuccess, notifyOnFail, onSuccess, onFail]);

  return { data, error, isLoading, execute };
};

export default useExplicitApiCall;
