import { useStateContext } from 'contexts/store';
import { useEffect, useState } from 'react';
import { Anything, GenericAsyncFunction } from 'types/generics';
import { toast } from 'react-toastify';
import { ApiCallOptions } from 'types/async-hooks';
import { ActionTypes } from 'types/store';

const useApiCall = <T>(asyncApiFunction: GenericAsyncFunction, options: ApiCallOptions = {}) => {
  const { args, notifyOnSuccess, notifyOnFail, onSuccess, onFail } = options;
  const [data, setData] = useState<T | null>(null);
  const [error, setError] = useState<unknown>(null);
  const [isLoading, setIsLoading] = useState(false);

  const argsAsString = JSON.stringify(args);

  const { dispatch } = useStateContext();

  useEffect(() => {
    dispatch({
      type: ActionTypes.SET_LOADER_STATE,
      payload: { loading: isLoading },
    });
  }, [isLoading]);

  useEffect(() => {
    (async () => {
      setIsLoading(true);

      let response: Anything | null = null;

      try {
        response = await asyncApiFunction(...(args || []));

        if (notifyOnSuccess) {
          toast(notifyOnSuccess || 'Operation finished successfuly.');
        }

        if (onSuccess) {
          onSuccess(response, args || []);
        }

        setData(response || null);
      } catch (error) {
        if (notifyOnFail) {
          toast.error(notifyOnFail || `Unable to perform operation: ${error}`);
        }

        if (onFail) {
          onFail(error, args || []);
        }

        console.error('useApiCall error', error);
        setError(error);
      }

      setIsLoading(false);
    })();
  }, [asyncApiFunction, argsAsString, notifyOnSuccess, notifyOnFail, onSuccess, onFail]);

  return { data, error, isLoading };
};

export default useApiCall;
