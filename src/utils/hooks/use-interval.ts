import { useEffect, useLayoutEffect, useRef } from 'react';

function useInterval(callback: () => void, delay: number | null) {
  const _callback = useRef(callback);

  useLayoutEffect(() => {
    _callback.current = callback;
  }, [callback]);

  useEffect(() => {
    if (!delay && delay !== 0) {
      return;
    }

    const intervalId = setInterval(() => _callback.current(), delay);

    return () => clearInterval(intervalId);
  }, [delay]);
}

export default useInterval;
