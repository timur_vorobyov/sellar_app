import { useEuiI18n } from '@elastic/eui';
import { SubOrderType } from 'constants/subOrder';
import { PaymentMethod } from 'constants/payment';
import { RefundReasonKey } from 'constants/refund';

const useCommonTranslations = () => {
  const [
    havingHere,
    selfCollect,
    takeaway,
    delivery,
    paymentMethodCard,
    paymentMethodLinkPoints,

    orderStatusReadyForCollection,
    orderStatusCancelled,
    orderStatusCollected,
    orderStatusRejected,

    refundReasonKCardDiscount,
    refundReasonQualityOfFood,
    refundReasonMissingItems,
    refundReasonFoodPoisoning,
    refundReasonDamagedItems,
    refundReasonWrongItems,
    refundReasonNoShow,
    refundReasonLateDelivery,
    refundReasonAppPaymentDiscount,
  ] = useEuiI18n(
    [
      'subOrders.subOrderType.havingHere',
      'subOrders.subOrderType.selfCollect',
      'subOrders.subOrderType.takeaway',
      'subOrders.subOrderType.delivery',
      'subOrders.paymentMethod.card',
      'subOrders.paymentMethod.linkPoints',
      'subOrders.subOrderState.readyForCollection',
      'subOrders.subOrderState.cancelled',
      'subOrders.subOrderState.collected',
      'subOrders.subOrderState.rejected',

      'refundSubOrder.reason.KCARD_DISCOUNT',
      'refundSubOrder.reason.QUALITY_OF_FOOD',
      'refundSubOrder.reason.MISSING_ITEMS',
      'refundSubOrder.reason.FOOD_POISONING',
      'refundSubOrder.reason.DAMAGED_ITEMS',
      'refundSubOrder.reason.WRONG_ITEMS',
      'refundSubOrder.reason.NO_SHOW',
      'refundSubOrder.reason.LATE_DELIVERY',
      'refundSubOrder.reason.APP_PAYMENT_DISCOUNT',
    ],
    [],
  );

  return {
    orderTypes: {
      [SubOrderType.HavingHere]: havingHere,
      [SubOrderType.SelfCollect]: selfCollect,
      [SubOrderType.Takeaway]: takeaway,
      [SubOrderType.Delivery]: delivery,
    },
    paymentMethods: {
      [PaymentMethod.Card]: paymentMethodCard,
      [PaymentMethod.LinkPoints]: paymentMethodLinkPoints,
    },
    orderStatuses: [
      { value: 1, text: orderStatusReadyForCollection },
      { value: 2, text: orderStatusRejected },
      { value: 3, text: orderStatusCancelled },
      { value: 4, text: orderStatusCollected },
    ],
    refundReasons: {
      [RefundReasonKey.KCardDiscount]: refundReasonKCardDiscount,
      [RefundReasonKey.QualityOfFood]: refundReasonQualityOfFood,
      [RefundReasonKey.FoodPoisoning]: refundReasonFoodPoisoning,
      [RefundReasonKey.MissingItems]: refundReasonMissingItems,
      [RefundReasonKey.DamagedItems]: refundReasonDamagedItems,
      [RefundReasonKey.WrongItems]: refundReasonWrongItems,
      [RefundReasonKey.NoShow]: refundReasonNoShow,
      [RefundReasonKey.LateDelivery]: refundReasonLateDelivery,
      [RefundReasonKey.AppPaymentDiscount]: refundReasonAppPaymentDiscount,
    },
  };
};

export default useCommonTranslations;
