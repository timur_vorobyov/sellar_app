import { StateProvider } from 'contexts/store';
import TenantIntegration from 'wrappers/SappWrapper/TenantIntegration';
import { render } from '@testing-library/react';
export const kposIntegratedRender = (ui: JSX.Element) => {
  return render(
    <StateProvider>
      <TenantIntegration>{ui}</TenantIntegration>
    </StateProvider>,
  );
};
