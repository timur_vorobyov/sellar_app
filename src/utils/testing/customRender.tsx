import { render } from '@testing-library/react';
import { Locale } from 'types/translation';
import { LanguageProvider } from 'contexts/languageContextProvider/provider';
import { I18n } from 'components/i18n/I18n';
import { setStorageLanguage } from 'storage/adapters/language';
import { TenantDataProvider } from 'contexts/tenant/provider';

export type ProviderProps = {
  locale: Locale;
  setLocale?: (_value: Locale) => void;
};

export const customI18nRender = (ui: JSX.Element, _providerProps?: ProviderProps) => {
  setStorageLanguage(_providerProps?.locale || '');

  return render(
    <LanguageProvider>
      <TenantDataProvider>
        <I18n>{ui}</I18n>
      </TenantDataProvider>
    </LanguageProvider>,
  );
};

export const customI18nRetailRender = (ui: JSX.Element, _providerProps?: ProviderProps) => {
  setStorageLanguage(_providerProps?.locale || '');
  return render(
    <LanguageProvider>
      <TenantDataProvider>
        <I18n>{ui}</I18n>
      </TenantDataProvider>
    </LanguageProvider>,
  );
};
