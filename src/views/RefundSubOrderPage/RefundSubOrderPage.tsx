import { useState, useEffect, useCallback } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import Sidebar from 'components/RefundSubOrder/Containers/Sidebar/Sidebar';
import RefundEmpty from 'components/RefundSubOrder/Containers/RefundEmpty/RefundEmpty';
import RefundTotal from 'components/RefundSubOrder/Containers/RefundTotal/RefundTotal';
import RefundReasons from 'components/RefundSubOrder/Containers/RefundReasons/RefundReasons';
import ErrorMessage from 'components/ErrorMessage/ErrorMessage';
import { SubOrderHistory } from 'types/models/subOrder';
import { SubOrderHistoryProduct, RefundPageProduct, RefundPageProductCustomisation } from 'types/models/product';
import useExplicitApiCall from 'utils/hooks/use-explicit-api-call';
import { refundOrder } from 'apis/rest/subOrders';
import './RefundSubOrderPage.scss';
import { toast } from 'react-toastify';
import { refundReasons } from 'constants/refund';
import { sliceTwoMoreDecimals } from 'utils/formater';
import { useDeviceContext } from '../..//contexts/deviceContextProvider/deviceContextProvider';
import RefundSubOrderMobilePage from './RefundSubOrderMobilePage';
import SuccessMessage from 'components/SuccessMessage/SuccessMessage';

interface LocationState {
  pathname: string;
  state: {
    subOrder: SubOrderHistory;
  };
  key: string;
  subOrder: SubOrderHistory;
}

export function RefundSubOrderPage() {
  const location = useLocation<LocationState>();
  const history = useHistory();
  const subOrderHistory = location?.state?.subOrder || null;
  const { isMobile } = useDeviceContext();

  if (!subOrderHistory) {
    history.push('/history');
  }
  const products: RefundPageProduct[] = (subOrderHistory.products || []).map((product: SubOrderHistoryProduct) => ({
    ...product,
    quantity: 0,
    actualQuantity: product.quantity,
    customisations: product.customisations.map((cust: RefundPageProductCustomisation) => ({
      ...cust,
      actualQuantity: 0,
    })),
  }));

  const [reason, setReason] = useState('');
  const [selectedProducts, setSelectedProducts] = useState<RefundPageProduct[]>(products || []);
  const [isRefunded, setIsRefunded] = useState(false);
  const [refundWithDiscountIncluded, setRefundWithDiscountIncluded] = useState(0);
  const [feesRefunded, setFeesRefunded] = useState(0);
  const [refundTotal, setRefundTotal] = useState(0);
  const [showConfirmMessage, setShowConfirmMessage] = useState(false);
  const [showErrorMessage, setShowErrorMessage] = useState(false);
  const [totalQuantity, setTotalQuantity] = useState(0);
  const { execute, data, isLoading, error } = useExplicitApiCall(refundOrder, {
    onSuccess: (_response, [subOrderHistory, reason]) => {
      toast.warn(`Order ${subOrderHistory.orderId} refunded with reason: "${reason}"`);
    },
    onFail: (_response) => {
      setShowErrorMessage(true);
    },
    useLoader: true,
  });

  useEffect(() => {
    if (!isLoading && data && !error) {
      setShowConfirmMessage(true);

      setTimeout(() => {
        setShowConfirmMessage(false);
        history.push('/history');
      }, 1500);
    }
  }, [isLoading, data, error]);

  useEffect(() => {
    const amountRefunded = selectedProducts.reduce((acc, product: RefundPageProduct) => {
      const price = product.discountedPrice * product.quantity;
      acc = acc + price;

      product.customisations.map((customisation: RefundPageProductCustomisation) => {
        const customisationPrice = customisation.price * customisation.actualQuantity * 0.9;
        acc = acc + customisationPrice;
      });

      return acc;
    }, 0);

    const productContainerFeesRefundedWithDiscount =
      selectedProducts.reduce(
        (acc: number, product: RefundPageProduct) =>
          acc + (product.containerFee / product.actualQuantity) * product.quantity,
        0,
      ) * 0.9;

    const totalQuantity = selectedProducts.reduce((acc, product: RefundPageProduct) => {
      acc = acc + product.quantity;
      product.customisations.map((customisation: RefundPageProductCustomisation) => {
        acc = acc + customisation.actualQuantity;
      });
      return acc;
    }, 0);

    setRefundWithDiscountIncluded(amountRefunded);
    setFeesRefunded(productContainerFeesRefundedWithDiscount);
    setRefundTotal(amountRefunded + productContainerFeesRefundedWithDiscount);
    setTotalQuantity(totalQuantity);
  }, [selectedProducts]);

  const handleUpdateProducts = useCallback(
    (product: RefundPageProduct, checked: boolean) => {
      const updatedSelectedProducts = selectedProducts.map((prod: RefundPageProduct) => {
        if (prod.id === product.id) {
          checked ? (prod.quantity = 1) : (prod.quantity = 0);
        }
        return prod;
      });

      setSelectedProducts(updatedSelectedProducts);
    },
    [selectedProducts],
  );

  const handleUpdateCustomisations = useCallback(
    (product: RefundPageProduct, customisation: RefundPageProductCustomisation, checked: boolean) => {
      const updatedSelectedProducts = selectedProducts.map((prod: RefundPageProduct) => {
        if (prod.id === product.id) {
          prod.customisations.map((cust: RefundPageProductCustomisation) => {
            if (cust.id === customisation.id) {
              checked ? (cust.actualQuantity = 1) : (cust.actualQuantity = 0);
            }

            return cust;
          });
        }

        return prod;
      });

      setSelectedProducts(updatedSelectedProducts);
    },
    [selectedProducts],
  );

  const handleProductQuantity = useCallback(
    (product: RefundPageProduct, option: string) => {
      const restSelectedProducts = selectedProducts.filter((prod) => prod.id !== product.id);
      if (option === 'plus') {
        setSelectedProducts([...restSelectedProducts, { ...product, quantity: product.quantity + 1 }]);
      }

      if (option === 'minus') {
        setSelectedProducts([...restSelectedProducts, { ...product, quantity: product.quantity - 1 }]);
      }
    },
    [selectedProducts],
  );

  const handleCustomisationQuantity = useCallback(
    (product: RefundPageProduct, customisation: RefundPageProductCustomisation, option: string) => {
      const updatedSelectedProducts = selectedProducts.map((prod: RefundPageProduct) => {
        if (prod.id === product.id) {
          prod.customisations.map((cust: RefundPageProductCustomisation) => {
            if (cust.id === customisation.id) {
              if (option === 'plus') {
                cust.actualQuantity++;
              }

              if (option === 'minus') {
                cust.actualQuantity--;
              }
            }

            return cust;
          });
        }

        return prod;
      });

      setSelectedProducts(updatedSelectedProducts);
    },
    [selectedProducts],
  );

  const handleSelectAll = useCallback(() => {
    const productsToSelect = subOrderHistory.products.map((product: SubOrderHistoryProduct) => ({
      ...product,
      actualQuantity: product.quantity,
      quantity: product.quantity - product.refundedQuantity,
      customisations: product.customisations.map((cust: RefundPageProductCustomisation) => ({
        ...cust,
        actualQuantity: cust.actualQuantity - cust.refundedQuantity,
      })),
    }));

    setSelectedProducts(productsToSelect);
  }, []);

  const handleConfirm = () => {
    const products = selectedProducts.map((product) => ({
      id: product.id,
      quantity: product.quantity,
      customisationOptions: product.customisations.map((customisation) => ({
        id: customisation.id,
        quantity: customisation.actualQuantity,
      })),
    }));

    execute(subOrderHistory.order.id, reason, [{ id: subOrderHistory.id, items: products }]);
  };

  if (showConfirmMessage) {
    return <SuccessMessage refund={true} refundPrice={refundTotal} />;
  }

  if (showErrorMessage) {
    return <ErrorMessage refund={true} />;
  }

  if (isMobile) {
    return (
      <RefundSubOrderMobilePage
        refundTotal={sliceTwoMoreDecimals(refundTotal)}
        refundReasonsProps={{
          refundReasons: refundReasons,
          selectedReason: reason,
          handleRefundReason: setReason,
          handleConfirm: handleConfirm,
        }}
        orderItemMenuProps={{
          subOrderHistory: subOrderHistory,
          selectedProducts: selectedProducts,
          handleUpdateProducts: (product, checked) => {
            handleUpdateProducts(product, checked);
          },
          handleUpdateCustomisations: (product, customisation, checked) => {
            handleUpdateCustomisations(product, customisation, checked);
          },
          onSelectAll: () => handleSelectAll(),
          handleProductQuantity: handleProductQuantity,
          handleCustomisationQuantity: handleCustomisationQuantity,
          isRefunded: isRefunded,
        }}
      />
    );
  }
  return (
    <div className="RefundSubOrderPage__container">
      {totalQuantity === 0 && <RefundEmpty />}
      {totalQuantity !== 0 && !isRefunded && (
        <RefundTotal
          returnedAmountFee={sliceTwoMoreDecimals(feesRefunded)}
          refundedPrice={sliceTwoMoreDecimals(refundWithDiscountIncluded)}
          refundTotal={sliceTwoMoreDecimals(refundTotal)}
          onClick={() => setIsRefunded(true)}
          itemsToRefund={totalQuantity}
        />
      )}
      {isRefunded && (
        <RefundReasons
          refundReasons={refundReasons}
          selectedReason={reason}
          handleRefundReason={setReason}
          handleConfirm={handleConfirm}
        />
      )}
      <Sidebar
        subOrderHistory={subOrderHistory}
        selectedProducts={selectedProducts}
        handleUpdateProducts={(product, checked) => handleUpdateProducts(product, checked)}
        handleUpdateCustomisations={(product, customisation, checked) =>
          handleUpdateCustomisations(product, customisation, checked)
        }
        onSelectAll={() => handleSelectAll()}
        handleProductQuantity={handleProductQuantity}
        handleCustomisationQuantity={handleCustomisationQuantity}
        isRefunded={isRefunded}
      />
    </div>
  );
}
