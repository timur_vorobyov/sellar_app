import '@testing-library/jest-dom';
import { cleanup, fireEvent, screen } from '@testing-library/react';
import { subOrderHistory } from '../../__mocks__/mocks';
import { customI18nRender } from 'utils/testing/customRender';
import { BrowserRouter, Redirect, Route } from 'react-router-dom';
import { StateProvider } from '../..//contexts/store';
import { DeviceContextProvider } from '../..//contexts/deviceContextProvider/deviceContextProvider';
import { RefundSubOrderPage } from './RefundSubOrderPage';

afterEach(cleanup);

describe('RefundSubOrderMobile page', function () {
  it('should render without error', function () {
    const { getByText } = customI18nRender(
      <BrowserRouter>
        <Route path="/refund">
          <StateProvider>
            <DeviceContextProvider width={500}>
              <RefundSubOrderPage />
            </DeviceContextProvider>
          </StateProvider>
        </Route>
        <Redirect
          to={{
            pathname: '/refund',
            state: {
              subOrder: subOrderHistory,
            },
          }}
        />
      </BrowserRouter>,
    );
    expect(getByText('Refund')).toBeInTheDocument();
    expect(getByText('Refund Amount')).toBeInTheDocument();
    expect(getByText('(inclusive of discount)')).toBeInTheDocument();
  });

  it('should select all items and update refund order', function () {
    const { getByText } = customI18nRender(
      <BrowserRouter>
        <Route path="/refund">
          <StateProvider>
            <DeviceContextProvider width={500}>
              <RefundSubOrderPage />
            </DeviceContextProvider>
          </StateProvider>
        </Route>
        <Redirect
          to={{
            pathname: '/refund',
            state: {
              subOrder: subOrderHistory,
            },
          }}
        />
      </BrowserRouter>,
    );
    const selectAll = screen.getByText('Select all');
    fireEvent.click(selectAll);
    expect(getByText('$ 4.50')).toBeInTheDocument();
  });

  it('should open refund reason screen', function () {
    const { container, getByText } = customI18nRender(
      <BrowserRouter>
        <Route path="/refund">
          <StateProvider>
            <DeviceContextProvider width={500}>
              <RefundSubOrderPage />
            </DeviceContextProvider>
          </StateProvider>
        </Route>
        <Redirect
          to={{
            pathname: '/refund',
            state: {
              subOrder: subOrderHistory,
            },
          }}
        />
      </BrowserRouter>,
    );
    expect(container.getElementsByClassName('CustomButton')[0]).toBeDisabled();

    const selectAll = screen.getByText('Select all');
    fireEvent.click(selectAll);
    expect(getByText('$ 4.50')).toBeInTheDocument();
    expect(container.getElementsByClassName('CustomButton')[0]).toBeEnabled();

    const showRefundReason = container.getElementsByClassName('CustomButton')[0];
    fireEvent.click(showRefundReason);

    expect(getByText('What is the reason for refunding this order?')).toBeInTheDocument();
    expect(container.getElementsByClassName('RefundReasonButton__container').length).toBe(9);
  });

  it('should show refund success message', function () {
    const { container, getByText } = customI18nRender(
      <BrowserRouter>
        <Route path="/history">In Order History Page</Route>
        <Route path="/refund">
          <StateProvider>
            <DeviceContextProvider width={500}>
              <RefundSubOrderPage />
            </DeviceContextProvider>
          </StateProvider>
        </Route>
        <Redirect
          to={{
            pathname: '/refund',
            state: {
              subOrder: subOrderHistory,
            },
          }}
        />
      </BrowserRouter>,
    );

    const selectAll = screen.getByText('Select all');
    fireEvent.click(selectAll);
    const showRefundReason = container.getElementsByClassName('CustomButton')[0];
    fireEvent.click(showRefundReason);
    expect(container.getElementsByClassName('CustomButton')[0]).toBeDisabled();

    const refundReasonDom = container.getElementsByClassName('RefundReasonButton__container')[2];
    fireEvent.click(refundReasonDom);
    expect(container.getElementsByClassName('RefundReasonButton__checkbox-checked').length).toBe(1);
    expect(container.getElementsByClassName('CustomButton')[0]).toBeEnabled();

    const ConfirmRefund = container.getElementsByClassName('CustomButton')[0];
    fireEvent.click(ConfirmRefund);

    const goBack = container.getElementsByClassName('HeaderMobile__container')[0];
    fireEvent.click(goBack);

    expect(getByText('Select all')).toBeInTheDocument();

    fireEvent.click(goBack);
    expect(getByText('In Order History Page')).toBeInTheDocument();
  });
});
