import '@testing-library/jest-dom';
import { cleanup, render } from '@testing-library/react';
import { RefundSubOrderPage } from './RefundSubOrderPage';
import { BrowserRouter, Redirect, Route } from 'react-router-dom';
import { subOrder } from '../../__mocks__/mocks';
import { DeviceContextProvider } from '../..//contexts/deviceContextProvider/deviceContextProvider';
import { StateProvider } from 'contexts/store';

afterEach(cleanup);

jest.mock('components/RefundSubOrder/Containers/RefundEmpty/RefundEmpty', () => () => <div>Refund</div>);
jest.mock('components/RefundSubOrder/Containers/Sidebar/Sidebar', () => () => <div>Sidebar</div>);

describe('<RefundPage> page', () => {
  it('should render without crashing', () => {
    const { container } = render(
      <BrowserRouter>
        <Route path="/reject">
          <RefundSubOrderPage />
        </Route>
        <Redirect
          to={{
            pathname: '/reject',
            state: {
              subOrder: subOrder,
            },
          }}
        />
      </BrowserRouter>,
    );

    expect(container.textContent).toMatch('Refund');
    expect(container.textContent).toMatch('Sidebar');
  });
  it('should render mobile page without crashing', () => {
    const { container } = render(
      <BrowserRouter>
        <Route path="/reject">
          <StateProvider>
            <DeviceContextProvider width={500}>
              <RefundSubOrderPage />
            </DeviceContextProvider>
          </StateProvider>
        </Route>
        <Redirect
          to={{
            pathname: '/reject',
            state: {
              subOrder: subOrder,
            },
          }}
        />
      </BrowserRouter>,
    );
    expect(container.getElementsByClassName('RefundSubOrderMobilePage__container').length).toBe(1);
  });

  /*
   it('should send correct refund reason to the backend after refund was confirmed', () => {
    const mockedRefundReasons = {
      [RefundReasonKey.KCardDiscount]: 'K-Card discount not reflected K-Card 折扣未反映',
      [RefundReasonKey.QualityOfFood]: 'Quality of food 食品质量',
      [RefundReasonKey.FoodPoisoning]: 'Food poisoning 食物中毒',
      [RefundReasonKey.MissingItems]: 'Missing item / ingredients 缺失项目/成分',
      [RefundReasonKey.DamagedItems]: 'Damaged item / packaging spillage 损坏的物品/包装溢出',
      [RefundReasonKey.WrongItems]: 'Wrong / additional item(s) 错误/附加项目',
      [RefundReasonKey.NoShow]: 'No show 不显示',
      [RefundReasonKey.LateDelivery]: 'Late delivery 逾期送达',
      [RefundReasonKey.AppPaymentDiscount]: 'App Payment Discount Not Reflected 付款折扣未反映'
    };

    jest.mock('../utils/hooks/user-common-translations', () => {
      return {
        refundReasons: mockedRefundReasons,
      };
    });

    const mockedExecuteFn = jest.fn();
    jest.mock('../utils/hooks/use-explicit-api-call', () => {
      return {
        execute: mockedExecuteFn
      }
    })

    React.useState = jest.fn()
      .mockReturnValue([true, jest.fn()])
      .mockReturnValueOnce([products, jest.fn()])

    customI18nRender(
      <BrowserRouter>
      <Route path="/reject">
        <RefundPage />
      </Route>
      <Redirect
        to={{
          pathname: '/reject',
          state: {
            order: order,
          },
        }}
      />
    </BrowserRouter>, { locale: Locale.CHINESE });

    const refundReasonKeys = Object.keys(refundReasons);
    refundReasonKeys.forEach((refundReason) => {
      const button = screen.getByText(mockedRefundReasons[refundReason as RefundReasonKey]);
      expect(button).toBeInTheDocument();

      fireEvent.click(button);
      expect(mockedExecuteFn).toBeCalledWith(refundReason);
    });
  })
  */
});
