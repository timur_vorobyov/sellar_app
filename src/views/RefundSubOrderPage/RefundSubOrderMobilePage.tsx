import React, { useState } from 'react';
import { RefundReasonKey } from 'constants/refund';
import { useHistory } from 'react-router-dom';
import { useEuiI18n } from '@elastic/eui';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import RefundReasonButton from 'components/RefundSubOrder/RefundReasonButton/RefundReasonButton';
import useCommonTranslations from '../../utils/hooks/user-common-translations';
import Sidebar from 'components/RefundSubOrder/Containers/Sidebar/Sidebar';
import { SubOrderHistory } from 'types/models/subOrder';
import { RefundPageProduct, RefundPageProductCustomisation } from 'types/models/product';
import './RefundSuborderMobilePage.scss';
import Typography from 'components/ui-kit/Typography';
import HeaderMobile from 'components/PDA/HeaderMobile/HeaderMobile';

type RefundReasonsType = {
  [key in RefundReasonKey]: string;
};

interface RefundReasonsProps {
  refundReasons: RefundReasonsType;
  selectedReason: string;
  handleRefundReason: (reason: string) => void;
  handleConfirm: () => void;
}

interface SidebarProps {
  subOrderHistory: SubOrderHistory;
  selectedProducts: RefundPageProduct[];
  handleUpdateProducts: (product: RefundPageProduct, checked: boolean) => void;
  handleUpdateCustomisations: (
    product: RefundPageProduct,
    customisation: RefundPageProductCustomisation,
    checked: boolean,
  ) => void;
  onSelectAll: () => void;
  handleProductQuantity: (product: RefundPageProduct, option: string) => void;
  handleCustomisationQuantity: (
    product: RefundPageProduct,
    customisation: RefundPageProductCustomisation,
    option: string,
  ) => void;
  isRefunded: boolean;
}

interface RefundSubOrderMobilePageProps {
  refundReasonsProps: RefundReasonsProps;
  orderItemMenuProps: SidebarProps;
  refundTotal: string;
}

const RefundSubOrderMobilePage = ({
  refundReasonsProps,
  orderItemMenuProps,
  refundTotal,
}: RefundSubOrderMobilePageProps) => {
  const { refundReasons, selectedReason, handleRefundReason, handleConfirm } = refundReasonsProps;
  const {
    subOrderHistory,
    selectedProducts,
    handleUpdateProducts,
    handleUpdateCustomisations,
    onSelectAll,
    handleProductQuantity,
    handleCustomisationQuantity,
    isRefunded,
  } = orderItemMenuProps;
  const [showRefundReason, toggleShowRefundReason] = useState(false);

  const history = useHistory();
  const [refundTitleText, titleText, confirmButtonText, refundAmountText, refundInclusiveDiscountText] = useEuiI18n(
    [
      'subOrdersHistory.details.refund',
      'refundSubOrder.reasonsList.title',
      'refundSubOrder.reasonsList.confirm',
      'refundMobile.refundAmount',
      'refundMobile.refundInclusiveDiscount',
    ],
    [],
  );
  const { refundReasons: refundReasonTranslations } = useCommonTranslations();

  const onConfirmClick = () => {
    if (!showRefundReason) {
      toggleShowRefundReason(true);
    } else {
      handleConfirm();
    }
  };

  const onBack = () => {
    if (showRefundReason) {
      handleRefundReason('');
      toggleShowRefundReason(false);
    } else {
      history.push('/history', {
        subOrder: subOrderHistory,
      });
    }
  };

  return (
    <div className="RefundSubOrderMobilePage__container">
      <HeaderMobile text={refundTitleText} onButtonClick={onBack} />
      <div className="RefundSubOrderMobilePage__body">
        {showRefundReason ? (
          <div className="RefundSubOrderMobilePage__body--refundReasons">
            <div className="RefundSubOrderMobilePage__body--refundQuestionTitle">
              <Typography variant="label">{titleText}</Typography>
            </div>
            {Object.entries(refundReasons).map(([key]) => (
              <RefundReasonButton
                key={key}
                label={refundReasonTranslations[key as RefundReasonKey]}
                checked={key === selectedReason}
                onClick={() => handleRefundReason(key)}
              />
            ))}
          </div>
        ) : (
          <Sidebar
            subOrderHistory={subOrderHistory}
            selectedProducts={selectedProducts}
            handleUpdateProducts={(product, checked) => handleUpdateProducts(product, checked)}
            handleUpdateCustomisations={(product, customisation, checked) =>
              handleUpdateCustomisations(product, customisation, checked)
            }
            onSelectAll={() => onSelectAll()}
            handleProductQuantity={handleProductQuantity}
            handleCustomisationQuantity={handleCustomisationQuantity}
            isRefunded={isRefunded}
          />
        )}
      </div>
      <div className="RefundSubOrderMobilePage__footer">
        <div className="RefundSubOrderMobilePage__footer--textSection">
          <div className="RefundSubOrderMobilePage__footer--text">
            <Typography variant="label"> {refundAmountText}</Typography>
            <Typography variant="span" fontWeight="normal" style={{ fontSize: 16 }}>
              {refundInclusiveDiscountText}
            </Typography>
          </div>
          <div className="RefundSubOrderMobilePage__footer--amount">
            <Typography variant="h2">{parseFloat(refundTotal) ? `$ ${refundTotal}` : '-'}</Typography>
          </div>
        </div>
        <div>
          <CustomButton
            color="green"
            size="xl"
            disabled={(showRefundReason && selectedReason == '') || !parseFloat(refundTotal)}
            onClick={onConfirmClick}
            text={confirmButtonText}
          />
        </div>
      </div>
    </div>
  );
};

export default RefundSubOrderMobilePage;
