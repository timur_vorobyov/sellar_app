import { useState, useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import { EuiPage, EuiPageBody, EuiPageContent, EuiPageContentBody, EuiButton, EuiText } from '@elastic/eui';
import QRCode from 'qrcode.react';
import moment from 'moment-timezone';
import { AxiosResponse, AxiosError } from 'axios';
import './LoginPage.scss';
import { datadogRum } from '@datadog/browser-rum';
import { getSerialNumber, requestAuthToken, requestDeviceCodeFlow } from 'apis/rest/auth/auth';
import { routes } from 'config/routes';
import { setStorageSerialNumber } from 'storage/adapters/serialNumber';
import { setStorageAuth } from 'storage/adapters/auth';
import { setStorageAccessToken } from 'storage/adapters/accessToken';
import { setStorageRefreshToken } from 'storage/adapters/refreshToken';
import { getStorageExpireIn, setStorageExpireIn } from 'storage/adapters/expireIn';
import { useAuth } from 'contexts/authContextProvider/authContextProvider';

const LoginPage = (): JSX.Element => {
  const { isAuthenticated, setIsAuthenticated } = useAuth();
  const [isShowQR, setIsShowQR] = useState<boolean>(false);
  const [isShowSerialNumberRetryBtn, setIsShowSerialNumberRetryBtn] = useState<boolean>(true);
  const [isShowDeviceCodeRetryBtn, setIsShowDeviceCodeRetryBtn] = useState<boolean>(false);
  const [qrLink, setQRLink] = useState<string>('');
  const intervalRequestAuth = useRef<NodeJS.Timeout | null>(null);
  const intervalGetSerialNumber = useRef<NodeJS.Timeout | null>(null);
  const history = useHistory();

  useEffect(() => {
    if (isAuthenticated) {
      history.push('/');
    }
  }, []);

  const handleGetSerialNumber = (): void => {
    getSerialNumber()
      .then((response) => {
        if (intervalGetSerialNumber.current) {
          clearInterval(intervalGetSerialNumber.current);
        }

        datadogRum.addRumGlobalContext('serial_number', response.data.serialNumber);
        setStorageSerialNumber(response.data.serialNumber);
        setIsShowSerialNumberRetryBtn(false);
      })
      .catch((err: AxiosError): void => {
        //unable to verify device
        console.log(err);
        setIsShowSerialNumberRetryBtn(true);
      });
  };

  const handleRequestAuthToken = (device_code: string): void => {
    requestAuthToken(device_code, '')
      .then((resp: AxiosResponse): void => {
        if (intervalRequestAuth.current) {
          clearInterval(intervalRequestAuth.current);
        }
        if (resp.data.access_token) {
          setStorageAuth(JSON.stringify(resp.data));
          setStorageAccessToken(resp.data.access_token);
          setStorageRefreshToken(resp.data.refresh_token);
          setIsShowQR(false);
          setIsAuthenticated(true);
          history.push(routes.orders.path);
        }
      })
      .catch((err: AxiosError): void => {
        console.log(err);
      });
  };

  const handleLogin = (): void => {
    requestDeviceCodeFlow()
      .then((response: AxiosResponse): void => {
        setQRLink(response.data.verification_uri_complete);
        setIsShowQR(true);
        setIsShowDeviceCodeRetryBtn(false);

        const expires_in: number = response.data.expires_in;
        const interval: number = response.data.interval;
        setStorageExpireIn(moment().add(expires_in, 'seconds').toISOString());
        handleRequestAuthToken(response.data.device_code);

        intervalRequestAuth.current = setInterval((): void => {
          handleRequestAuthToken(response.data.device_code);

          // Check if the token is expired
          if (moment().isAfter(moment(getStorageExpireIn()))) {
            if (intervalRequestAuth.current) {
              clearInterval(intervalRequestAuth.current);
            }
            setIsShowDeviceCodeRetryBtn(true);
          }
        }, interval * 1000);
      })
      .catch((err: AxiosError): void => {
        console.error(err);
      });
  };

  const handleStartAuthProcess = (): void => {
    if (intervalRequestAuth.current) {
      clearInterval(intervalRequestAuth.current);
    }
    handleLogin();
    handleGetSerialNumber();

    intervalGetSerialNumber.current = setInterval((): void => {
      handleGetSerialNumber();
    }, 5000);
  };

  useEffect(() => {
    handleStartAuthProcess();

    return (): void => {
      if (intervalRequestAuth.current) {
        clearInterval(intervalRequestAuth.current);
      }
    };
  }, []);

  return (
    <div>
      <EuiPage paddingSize="none">
        <EuiPageBody style={{ height: 'calc(100vh - 50px)' }} component="div">
          <EuiPageContent verticalPosition="center" horizontalPosition="center">
            <EuiPageContentBody>
              {isShowQR && (
                <>
                  {!isShowDeviceCodeRetryBtn && (
                    <>
                      <EuiText className="scan-text">
                        <h1>Scan QR code using your Tenant mobile app for authentication</h1>
                      </EuiText>
                      <QRCode value={qrLink} size={200} />
                    </>
                  )}
                </>
              )}
              {isShowSerialNumberRetryBtn && (
                <div className="retry-block">
                  <EuiText className="retry-error">
                    <h1>Please ensure that Peripheral App is turned on</h1>
                  </EuiText>
                  <EuiButton fill id="retry-btn" onClick={(): void => handleStartAuthProcess()}>
                    Retry
                  </EuiButton>
                </div>
              )}
              {isShowDeviceCodeRetryBtn && (
                <div className="retry-block">
                  <EuiText className="retry-error">
                    <h1>Code expire</h1>
                  </EuiText>
                  <EuiButton fill id="retry-qr-btn" onClick={() => handleLogin()}>
                    Retry
                  </EuiButton>
                </div>
              )}
            </EuiPageContentBody>
          </EuiPageContent>
        </EuiPageBody>
      </EuiPage>
    </div>
  );
};
export default LoginPage;
