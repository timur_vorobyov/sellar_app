import { SubOrder } from 'types/models/subOrder';
import Tabs from 'components/Tabs/Tabs';
import NavBarTablet from 'components/Tablet/NavBarTablet/NavBarTablet';
import { useEuiI18n } from '@elastic/eui';
import './SubOrdersPage.scss';
import { IncomingSubOrders } from 'components/SubOrder/Containers/IncomingSubOrders/IncomingSubOrders';
import { AcceptedSubOrders } from 'components/SubOrder/Containers/AcceptedSubOrders/AcceptedSubOrders';
import NavBarMobile from 'components/PDA/NavBarMobile/NavBarMobile';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import { IncomingSubOrdersMobile } from 'components/SubOrder/Containers/IncomingSubOrdersMobile/IncomingSubOrdersMobile';
import { AcceptedSubOrdersMobile } from 'components/SubOrder/Containers/AcceptedSubOrdersMobile/AcceptedSubOrdersMobile';

interface SubOrdersPageProps {
  handleExpire: (subOrder: SubOrder) => void;
  handlePickup: (subOrder: SubOrder) => void;
  handleAccept: (subOrder: SubOrder) => void;
  incomingSubOrdersNumber: string | null;
  acceptedSubOrdersNumber: string | null;
}
const SubOrdersPage = ({
  handleExpire,
  handlePickup,
  handleAccept,
  incomingSubOrdersNumber,
  acceptedSubOrdersNumber,
}: SubOrdersPageProps): JSX.Element => {
  const [incomingTabText, acceptedTabText] = useEuiI18n(['subOrdersPage.tabIncoming', 'subOrdersPage.tabAccepted'], []);
  const { isTablet } = useDeviceContext();
  return (
    <div className="SubOrdersPage__container">
      {isTablet ? <NavBarTablet /> : <NavBarMobile />}
      <Tabs
        firstTabText={incomingTabText}
        firstTabContent={
          isTablet ? (
            <IncomingSubOrders onSubOrderAccept={handleAccept} onSubOrderExpire={handleExpire} />
          ) : (
            <IncomingSubOrdersMobile onSubOrderAccept={handleAccept} onSubOrderExpire={handleExpire} />
          )
        }
        firstTabAppend={incomingSubOrdersNumber}
        secondTabText={acceptedTabText}
        secondTabContent={
          isTablet ? (
            <AcceptedSubOrders onSubOrderPickup={handlePickup} />
          ) : (
            <AcceptedSubOrdersMobile onSubOrderPickup={handlePickup} />
          )
        }
        secondTabAppend={acceptedSubOrdersNumber}
      />
    </div>
  );
};

export default SubOrdersPage;
