import { cleanup } from '@testing-library/react';
import '@testing-library/jest-dom';
import { MenuPage } from './MenuPage';
import { customI18nRender } from 'utils/testing/customRender';
import { FoodSupplierProvider } from 'contexts/foodSupplierProvider/provider';

afterEach(cleanup);

jest.mock('components/Tablet/NavBarTablet/NavBarTablet', () => () => <div>Top nav bar</div>);
jest.mock('components/Menu/MenuTabs/MenuTabs', () => () => <div>Tabs</div>);
jest.mock('components/BottomButton/BottomButton', () => () => <div>Bottom button</div>);

describe('<MenuPage> page', () => {
  it('should render without crashing', () => {
    const { container } = customI18nRender(
      <FoodSupplierProvider>
        <MenuPage />
      </FoodSupplierProvider>,
    );
    expect(container.textContent).toMatch('Top nav bar');
    expect(container.textContent).toMatch('Tabs');
    expect(container.textContent).toMatch('Bottom button');
  });
});
