import { PropsWithChildren, useEffect, useState } from 'react';
import { Anything } from 'types/generics';
import BottomButton from 'components/BottomButton/BottomButton';
import MenuTabs from 'components/Menu/MenuTabs/MenuTabs';
import { CustomisationOptionFilter, MenuFilter } from 'types/pages';
import { CustomisationOption, MenuItem } from 'types/models/menu';
import { MenuTable } from 'components/Menu/MenuTable/MenuTable';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import { MenuTableMobile } from 'components/Menu/MenuTableMobile/MenuTableMobile';
import classnames from 'clsx';
import NavBarMobile from 'components/PDA/NavBarMobile/NavBarMobile';
import NavBarTablet from 'components/Tablet/NavBarTablet/NavBarTablet';
import useContainerData from './hooks/use-container-data-hook';
import { useLanguageContext } from 'contexts/languageContextProvider/context';

export const MenuPage = (_props: PropsWithChildren<Anything>): JSX.Element => {
  const { isMobile, isTablet } = useDeviceContext();
  const { getLocale } = useLanguageContext();
  const [showFilterMobile, setShowFilterMobile] = useState(false);
  const [filteredMenuItems, setFilteredMenuItems] = useState<MenuItem[]>([]);
  const [filteredCustomisationOptions, setFilteredCustomisationOptions] = useState<CustomisationOption[]>([]);
  const [menuFilters, setMenuFilters] = useState<MenuFilter>({
    category: [],
    availability: [],
    searchTerm: '',
  });
  const [customisationOptionFilters, setCustomisationOptionFilters] = useState<CustomisationOptionFilter>({
    customisationIds: [],
    availability: [],
    searchTerm: '',
  });

  const { menuItems, customisationOptions, menuCategories, customisations, updateMenuItem, updateCustomisationOption } =
    useContainerData();

  useEffect(() => {
    if (!menuItems || !menuItems.length) {
      return;
    }
    let result: MenuItem[] = menuItems;

    if (menuFilters.category.length) {
      const selectedCategories: string[] = menuCategories
        .filter((cat) => menuFilters.category.includes(cat.value))
        .map((cat) => cat.text);

      result = result.filter((item) =>
        selectedCategories.includes(getLocale() === 'en' ? item.category.name : item.category.nameCn),
      );
    } else {
      const selectedCategories: string[] = menuCategories.filter(() => menuFilters.category).map((cat) => cat.text);

      result = result.filter((item) =>
        selectedCategories.includes(getLocale() === 'en' ? item.category.name : item.category.nameCn),
      );
    }

    if (menuFilters.availability.length) {
      result = result.filter((item) => menuFilters.availability.includes(item.available));
    }

    if (menuFilters.searchTerm) {
      result = result.filter((item) => item.name.toLowerCase().includes(menuFilters.searchTerm.toLowerCase()));
    }

    setFilteredMenuItems(result);
  }, [menuItems, menuFilters.category, menuFilters.availability, menuFilters.searchTerm]);

  useEffect(() => {
    if (!customisationOptions || !customisationOptions.length) {
      return;
    }

    let result: CustomisationOption[] = customisationOptions;

    if (customisationOptionFilters.customisationIds.length) {
      const selectedCustomisations: string[] = customisations
        .filter((customisation) => customisationOptionFilters.customisationIds.includes(customisation.value))
        .map((customisation) => customisation.text);

      result = result.filter((item) =>
        selectedCustomisations.includes(getLocale() === 'en' ? item.customisation.name : item.customisation.nameCn),
      );
    } else {
      const selectedCustomisations: string[] = customisations
        .filter(() => customisationOptionFilters.customisationIds)
        .map((customisation) => customisation.text);

      result = result.filter((item) =>
        selectedCustomisations.includes(getLocale() === 'en' ? item.customisation.name : item.customisation.nameCn),
      );
    }

    if (customisationOptionFilters.availability.length) {
      result = result.filter((item) => customisationOptionFilters.availability.includes(item.available));
    }

    if (customisationOptionFilters.searchTerm) {
      result = result.filter((item) =>
        item.name.toLowerCase().includes(customisationOptionFilters.searchTerm.toLowerCase()),
      );
    }
    setFilteredCustomisationOptions(result);
  }, [
    customisationOptions,
    customisationOptionFilters.customisationIds,
    customisationOptionFilters.availability,
    customisationOptionFilters.searchTerm,
  ]);

  const menuAvailabilityComponent = menuItems.length ? (
    <>
      {isMobile && (
        <div className="MenuAvailabilityMobile__container">
          <MenuTableMobile
            updateMenuItemState={updateMenuItem}
            updateCustomisationOptionState={updateCustomisationOption}
            items={filteredMenuItems}
            isMainTab={true}
          />
        </div>
      )}
      {isTablet && (
        <div className="MenuAvailability__container">
          <div className="MenuAvailability__table-container">
            <MenuTable
              updateMenuItemState={updateMenuItem}
              updateCustomisationOptionState={updateCustomisationOption}
              items={filteredMenuItems}
              isMainTab={true}
            />
          </div>
        </div>
      )}
    </>
  ) : null;

  const customisationOptionsComponent = customisationOptions.length ? (
    <>
      {isMobile && (
        <div className="MenuAvailabilityMobile__container">
          <MenuTableMobile
            updateMenuItemState={updateMenuItem}
            updateCustomisationOptionState={updateCustomisationOption}
            items={filteredCustomisationOptions}
            isMainTab={false}
          />
        </div>
      )}
      {isTablet && (
        <div className="MenuAvailability__container">
          <div className="MenuAvailability__table-container">
            <MenuTable
              updateMenuItemState={updateMenuItem}
              updateCustomisationOptionState={updateCustomisationOption}
              items={filteredCustomisationOptions}
              isMainTab={false}
            />
          </div>
        </div>
      )}
    </>
  ) : null;

  return (
    <div className={classnames('MenuPage__container', { filters: showFilterMobile })}>
      {isTablet ? <NavBarTablet /> : <NavBarMobile />}
      <MenuTabs
        menuCategories={menuCategories}
        customisations={customisations}
        firstTabContent={menuAvailabilityComponent}
        secondTabContent={customisationOptionsComponent}
        onMenuFiltersChange={(filters) => setMenuFilters(filters)}
        onCustomisationOptionFiltersChange={(filters) => setCustomisationOptionFilters(filters)}
        onShowFilterMobile={setShowFilterMobile}
      />
      {isTablet && <BottomButton />}
    </div>
  );
};
