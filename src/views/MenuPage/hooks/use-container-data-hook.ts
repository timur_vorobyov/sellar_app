import { getCategoriesList, getMenuDataList } from 'apis/graphql/hasura/queries';
import { useFoodSupplierContext } from 'contexts/foodSupplierProvider/context';
import { useLanguageContext } from 'contexts/languageContextProvider/context';
import { useEffect, useState } from 'react';
import { Option } from 'types/generics';
import { Customisation, CustomisationOption, MenuItem } from 'types/models/menu';
import { MenuCategory } from 'types/models/menuCaregory';
import useApiCall from 'utils/hooks/use-api-call';

interface MenuCategoriesResponse {
  data: MenuCategory[] | null;
}

interface MenuDataListResponse {
  data: {
    menuItemsResponse: MenuItem[];
    customisationsResponse: Customisation[];
    customisationOptionsResponse: CustomisationOption[];
  } | null;
}

const useContainerData = () => {
  const { getFoodSupplierId } = useFoodSupplierContext();
  const { getLocale } = useLanguageContext();

  const { data: menuCategoriesResponse }: MenuCategoriesResponse = useApiCall(getCategoriesList, {
    args: [getFoodSupplierId()],
  });

  const { data }: MenuDataListResponse = useApiCall(getMenuDataList, {
    args: [getFoodSupplierId()],
  });

  const [menuCategories, setMenuCategories] = useState<Option[]>([]);
  const [menuItems, setMenuItems] = useState<MenuItem[]>([]);
  const [customisations, setCustomisations] = useState<Option[]>([]);
  const [customisationOptions, setCustomisationOptions] = useState<CustomisationOption[]>([]);

  useEffect(() => {
    if (menuCategoriesResponse) {
      const menuCategoryOptions = menuCategoriesResponse.map((menuCategory) => {
        return { value: menuCategory.id, text: getLocale() === 'en' ? menuCategory.name : menuCategory.nameCn };
      });
      setMenuCategories(menuCategoryOptions);
    }
  }, [menuCategoriesResponse, getLocale()]);

  useEffect(() => {
    if (data) {
      setMenuItems(data.menuItemsResponse);
      setCustomisationOptions(data.customisationOptionsResponse);
      const customisations = data.customisationsResponse.map((customisation) => {
        return { value: customisation.id, text: getLocale() === 'en' ? customisation.name : customisation.nameCn };
      });

      setCustomisations(customisations);
    }
  }, [data, getLocale()]);

  const updateMenuItem = (menuItem: MenuItem) => {
    const newMenuItems = [...menuItems];
    const menuItemIndex = newMenuItems.findIndex((item) => item.id === menuItem.id);
    newMenuItems[menuItemIndex].available = menuItem.available;
    setMenuItems([...newMenuItems]);
  };

  const updateCustomisationOption = (customisationOption: CustomisationOption) => {
    const newCustomisationOptions = [...customisationOptions];
    const customisationOptionIndex = newCustomisationOptions.findIndex((item) => item.id === customisationOption.id);
    newCustomisationOptions[customisationOptionIndex].available = customisationOption.available;
    setCustomisationOptions([...newCustomisationOptions]);
  };

  return {
    menuCategories,
    menuItems,
    customisations,
    customisationOptions,
    updateMenuItem,
    updateCustomisationOption,
  };
};

export default useContainerData;
