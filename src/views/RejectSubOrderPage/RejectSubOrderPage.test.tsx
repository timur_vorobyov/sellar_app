import '@testing-library/jest-dom';
import { cleanup, fireEvent, screen } from '@testing-library/react';
import { FoodSupplierProvider } from '../../contexts/foodSupplierProvider/provider';
import { RejectSubOrderPage } from './RejectSubOrderPage';
import { BrowserRouter, Redirect, Route } from 'react-router-dom';
import { StateProvider } from '../../contexts/store';
import { DeviceContextProvider } from '../../contexts/deviceContextProvider/deviceContextProvider';
import { subOrder } from '../../__mocks__/mocks';
import { customI18nRender } from '../../utils/testing/customRender';
import { TOO_BUSY_INTERVALS } from '../../constants/subOrder';
import { act } from 'react-dom/test-utils';

afterEach(cleanup);

const REJECTION_REASONS = ['Item unavailable', 'Stall closed', 'Too busy'];

const renderRejectSubOrderComponent = () => {
  return customI18nRender(
    <BrowserRouter>
      <Route path="/reject">
        <StateProvider>
          <DeviceContextProvider width={500}>
            <FoodSupplierProvider>
              <RejectSubOrderPage />
            </FoodSupplierProvider>
          </DeviceContextProvider>
        </StateProvider>
      </Route>
      <Redirect
        to={{
          pathname: '/reject',
          state: {
            subOrder,
          },
        }}
      />
    </BrowserRouter>,
  );
};

describe('Reject suborder page', () => {
  it('should render without crashing', () => {
    renderRejectSubOrderComponent();
    expect(screen.getByText('What is the reason for rejecting this order?')).toBeInTheDocument();
    REJECTION_REASONS.forEach((reason) => {
      expect(screen.getByText(reason)).toBeInTheDocument();
    });
  });

  it('should render stall closed option selected', () => {
    const { container } = renderRejectSubOrderComponent();
    const stallClosed = screen.getByText('Stall closed');
    expect(stallClosed).toBeInTheDocument();

    // Stall closed option selected test
    fireEvent.click(stallClosed);
    const selectedOptionStallClosed = container.getElementsByClassName('RejectionReasonButton__checkbox checked');
    expect(selectedOptionStallClosed.length).toBe(1);
  });

  it('should render stall Too busy option selected', () => {
    const { container } = renderRejectSubOrderComponent();
    const tooBusy = screen.getByText('Too busy');
    expect(tooBusy).toBeInTheDocument();
    fireEvent.click(tooBusy);
    const TooBusyRadioOptions = container.getElementsByClassName('TooBusyMobile__radio');
    expect(TooBusyRadioOptions.length).toBe(12);
    TOO_BUSY_INTERVALS.forEach((interval) => {
      expect(screen.getByText(`${interval} mins`)).toBeInTheDocument();
    });
  });

  it('should render item unavailable option selected', () => {
    renderRejectSubOrderComponent();
    const itemUnavailable = screen.getByText('Item unavailable');
    expect(itemUnavailable).toBeInTheDocument();
    fireEvent.click(itemUnavailable);
    expect(screen.getByText('Which item(s) are unavailable?')).toBeInTheDocument();
    subOrder.products.forEach((product) => {
      expect(screen.getByTestId(product.id).textContent).toBe(`x${product.quantity} ${product.name} `);
    });
  });

  it('should render options on back of item unavailable', () => {
    renderRejectSubOrderComponent();
    const itemUnavailable = screen.getByText('Item unavailable');
    const backButton = screen.getByAltText('back navigation');
    expect(itemUnavailable).toBeInTheDocument();
    fireEvent.click(itemUnavailable);
    expect(screen.getByText('Which item(s) are unavailable?')).toBeInTheDocument();
    fireEvent.click(backButton);
    REJECTION_REASONS.forEach((reason) => {
      expect(screen.getByText(reason)).toBeInTheDocument();
    });
  });

  it('should reject by stall closed option', () => {
    renderRejectSubOrderComponent();
    const stallClosed = screen.getByText('Stall closed');
    expect(stallClosed).toBeInTheDocument();
    fireEvent.click(stallClosed);
    fireEvent.click(screen.getByText('Confirm'));
  });

  it('should reject by too busy option', () => {
    renderRejectSubOrderComponent();
    const tooBusy = screen.getByText('Too busy');
    expect(tooBusy).toBeInTheDocument();
    fireEvent.click(tooBusy);
    fireEvent.click(screen.getByText('Confirm'));
  });

  it('should select all items in unavailable option', () => {
    renderRejectSubOrderComponent();
    const itemUnavailable = screen.getByText('Item unavailable');
    expect(itemUnavailable).toBeInTheDocument();
    fireEvent.click(itemUnavailable);
    fireEvent.click(screen.getByText('Select all'));
  });

  it('should handle update product quantity', () => {
    const { container } = renderRejectSubOrderComponent();

    const itemUnavailable = screen.getByText('Item unavailable');
    expect(itemUnavailable).toBeInTheDocument();

    fireEvent.click(itemUnavailable);
    const confirmButton = container.getElementsByClassName('CustomButton green max')[0];
    expect(confirmButton.getAttribute('disabled')).toBe('');

    const uncheckedButton = container.getElementsByClassName('RejectSubOrder__checkbox__icon')[0];
    fireEvent.click(uncheckedButton);
    expect(confirmButton.getAttribute('disabled')).toBe(null);

    const checkedButton = container.getElementsByClassName('RejectSubOrder__checkbox__icon-checked')[0];
    fireEvent.click(checkedButton);
    expect(confirmButton.getAttribute('disabled')).toBe('');

    const customisationList = container.getElementsByClassName('ItemUnavailable_customisationItems__list')[0];
    const customUncheckedButton = customisationList.getElementsByClassName('RejectSubOrder__checkbox__icon')[0];
    fireEvent.click(customUncheckedButton);
    expect(confirmButton.getAttribute('disabled')).toBe(null);

    const customCheckedButton = customisationList.getElementsByClassName('RejectSubOrder__checkbox__icon-checked')[0];
    fireEvent.click(customCheckedButton);
    expect(confirmButton.getAttribute('disabled')).toBe(null);
  });
});
