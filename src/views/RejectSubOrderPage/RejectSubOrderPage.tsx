import { PropsWithChildren, useCallback, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useEuiI18n } from '@elastic/eui';
import RejectionReasonButton from 'components/RejectSubOrder/RejectionReasonButton/RejectionReasonButton';
import ItemUnavailable from 'components/RejectSubOrder/Containers/ItemUnavailable/ItemUnavailable';
import TooBusy from 'components/RejectSubOrder/Containers/TooBusy/TooBusy';
import StallClosed from 'components/RejectSubOrder/Containers/StallClosed/StallClosed';
import ErrorMessage from 'components/ErrorMessage/ErrorMessage';
import { Product, ProductCustomisation } from 'types/models/product';
import { SubOrder } from 'types/models/subOrder';
import { getExpiresInSeconds } from 'utils/datetime';
import { rejectSubOrder } from 'apis/rest/subOrders';
import './RejectSubOrderPage.scss';
import { Anything } from 'types/generics';
import { toast } from 'react-toastify';
import { SubOrderRejectionReason, REASONS_TOO_BUSY } from 'constants/subOrder';
import useExplicitApiCall from 'utils/hooks/use-explicit-api-call';
import { updateCustomisationOption, updateMenuItem, updateStallSettings } from 'apis/rest/catalog';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import { useFoodSupplierContext } from 'contexts/foodSupplierProvider/context';
import SuccessMessage from 'components/SuccessMessage/SuccessMessage';
import HeaderMobile from 'components/PDA/HeaderMobile/HeaderMobile';

interface LocationState {
  pathname: string;
  state: {
    subOrder: SubOrder;
  };
  key: string;
  subOrder: SubOrder;
  products: Product[];
  createdAt: number | string;
  graceTime: string;
}

export const RejectSubOrderPage = (_props: PropsWithChildren<Anything>): JSX.Element => {
  const { getFoodSupplierId } = useFoodSupplierContext();
  const foodSupplierId = getFoodSupplierId();

  const { isMobile } = useDeviceContext();

  const { execute, data, isLoading, error } = useExplicitApiCall(rejectSubOrder, {
    onSuccess: (_response, [subOrder, reason]) => {
      toast.warn(`Order ${subOrder.orderId} rejected with reason: "${reason}"`);
    },
    onFail: (_response, [subOrder, reason]) => {
      toast.warn(`Order ${subOrder.orderId} rejected with reason: "${reason}"`);
    },
    useLoader: true,
  });
  const { execute: executeUpdateStallSettings } = useExplicitApiCall(updateStallSettings, {
    onSuccess: (_response, [[platform]]) => {
      toast.warn(`Platform ${platform.id} settings updated`);
    },
  });

  const [rejectionReason, setRejectionReason] = useState<SubOrderRejectionReason>();
  const [selectedProducts, setSelectedProducts] = useState<Product[]>([]);
  const [selectedCustomizations, setSelectedCustomizations] = useState<ProductCustomisation[]>([]);
  const [busyTime, setBusyTime] = useState<number>(REASONS_TOO_BUSY[0].value);

  const [confirmationEnabled, setConfirmationEnabled] = useState(false);
  const [showConfirmMessage, setShowConfirmMessage] = useState(false);
  const [showAutoRejectMessage, setShowAutoRejectMessage] = useState(false);
  const [hideHeader, toggleHeader] = useState(false);

  const location = useLocation<LocationState>();
  const history = useHistory();

  const subOrder = location?.state?.subOrder || null;

  const [
    reasonForRejectionText,
    confirmText,
    backToOrderDetailsText,
    merchantBusyText,
    itemUnavailableText,
    merchantClosedText,
    rejectOrderText,
  ] = useEuiI18n(
    [
      'subOrderRejectPage.whatIsTheReasonForRejection',
      'subOrderRejectPage.confirm',
      'subOrderRejectPage.backToSubOrderDetails',
      'subOrderRejectPage.rejectionReason.merchantBusy',
      'subOrderRejectPage.rejectionReason.itemUnavailable',
      'subOrderRejectPage.rejectionReason.merchantClosed',
      'menu.header.rejectSubOrder',
    ],
    [],
  );

  const rejectionReasons = [
    { key: SubOrderRejectionReason.ItemUnavailable, text: itemUnavailableText },
    { key: SubOrderRejectionReason.MerchantClosed, text: merchantClosedText },
    { key: SubOrderRejectionReason.MerchantBusy, text: merchantBusyText },
  ];

  if (!subOrder) {
    return <ErrorMessage />;
  }

  useEffect(() => {
    if (!isLoading && (data || error)) {
      setShowConfirmMessage(true);

      setTimeout(() => {
        setShowConfirmMessage(false);
        history.push('/');
      }, 1500);
    }
  }, [isLoading, data]);

  useEffect(() => {
    if (
      rejectionReason === SubOrderRejectionReason.MerchantBusy ||
      rejectionReason === SubOrderRejectionReason.MerchantClosed
    ) {
      setConfirmationEnabled(true);
    } else {
      setConfirmationEnabled(selectedProducts.length > 0);
    }
  }, [rejectionReason, selectedProducts, selectedCustomizations]);

  const handleUpdateProducts = useCallback(
    (product: Product, checked: boolean) => {
      if (checked) {
        if (!selectedProducts.includes(product)) {
          setSelectedProducts([...selectedProducts, product]);
        }
      } else {
        setSelectedProducts(selectedProducts.filter((p) => p.id !== product.id));
      }
    },
    [selectedProducts],
  );

  const handleUpdateCustomizations = useCallback(
    (cust: ProductCustomisation, checked: boolean) => {
      if (checked) {
        if (!selectedCustomizations.includes(cust)) {
          setSelectedCustomizations([...selectedCustomizations, cust]);
        }
      } else {
        setSelectedCustomizations(selectedCustomizations.filter((c) => c.id !== cust.id));
      }
    },
    [selectedCustomizations],
  );

  const handleSelectAll = useCallback(() => {
    setSelectedProducts(subOrder.products);
    setSelectedCustomizations(
      subOrder.products.reduce((acc: ProductCustomisation[], product: Product) => {
        acc.push(...(product.customisations || []));

        return acc;
      }, []),
    );
  }, []);

  const handleConfirm = () => {
    const sec = getExpiresInSeconds(subOrder.createdAt, subOrder.graceTime);

    if (sec <= 0) {
      setShowAutoRejectMessage(true);
      return;
    }

    // TODO: Use this reasons as key instead of changeable text

    execute(subOrder, rejectionReason);

    if (rejectionReason === SubOrderRejectionReason.MerchantBusy) {
      executeUpdateStallSettings({ id: subOrder.platform, prepTime: busyTime }, foodSupplierId);
    } else if (rejectionReason === SubOrderRejectionReason.ItemUnavailable) {
      // NOTE: It is generally incorrect to call API directly but useExplicitApiCall doesn't support
      //       multiple calls in parallel. Also it is border case and thus it would be acceptable.
      const promises = [];

      if (selectedProducts.length) {
        promises.push(...selectedProducts.map((product) => updateMenuItem({ id: product.menuItemId, available: 0 })));
      }

      if (selectedCustomizations.length) {
        promises.push(
          ...selectedCustomizations.map((cust) => updateCustomisationOption({ id: cust.optionId, available: 0 })),
        );
      }

      Promise.all(promises);
    } else {
      executeUpdateStallSettings({ id: subOrder.platform, status: 'CLOSED' }, foodSupplierId);
    }
  };

  if (showAutoRejectMessage) {
    return <ErrorMessage />;
  }

  if (showConfirmMessage) {
    return (
      <SuccessMessage
        option={rejectionReason}
        products={selectedProducts}
        customisations={selectedCustomizations}
        time={busyTime}
      />
    );
  }
  const onBack = () => {
    if (rejectionReason === SubOrderRejectionReason.ItemUnavailable) {
      setRejectionReason(undefined);
      toggleHeader(false);
    } else {
      history.push('/', { orderId: subOrder.id });
    }
  };

  const onRejectSelection = (reasonKey: SubOrderRejectionReason) => {
    if (reasonKey === SubOrderRejectionReason.ItemUnavailable) {
      toggleHeader(true);
    }
    setRejectionReason(reasonKey);
  };

  return (
    <div className="RejectSubOrder__container">
      {isMobile && <HeaderMobile text={rejectOrderText} onButtonClick={onBack} />}
      {!hideHeader && (
        <>
          <div className="RejectSubOrder__header">
            <h1 className="RejectSubOrder__header__title">{reasonForRejectionText}</h1>
          </div>

          <div className="OrderRejectPage__selectors__container">
            {rejectionReasons.map((reason) => {
              return (
                <RejectionReasonButton
                  key={reason.key}
                  label={reason.text}
                  checked={reason.key === rejectionReason}
                  onClick={() => onRejectSelection(reason.key)}
                />
              );
            })}
          </div>
        </>
      )}

      <div className="RejectSubOrder__body__container">
        {rejectionReason === SubOrderRejectionReason.ItemUnavailable && (
          <ItemUnavailable
            products={subOrder.products}
            selectedProducts={selectedProducts}
            selectedCustomizations={selectedCustomizations}
            onProductChange={(product, checked) => handleUpdateProducts(product, checked)}
            onCustomizationChange={(custs, checked) => handleUpdateCustomizations(custs, checked)}
            onSelectAll={() => handleSelectAll()}
          />
        )}
        {rejectionReason === SubOrderRejectionReason.MerchantBusy && (
          <TooBusy selected={`${busyTime}`} onChange={(time) => setBusyTime(time)} />
        )}
        {rejectionReason === SubOrderRejectionReason.MerchantClosed && <StallClosed />}
      </div>
      <div>
        <div className="RejectSubOrder__buttonsContainer">
          {!isMobile && <CustomButton size="xl" onClick={() => history.push('/')} text={backToOrderDetailsText} />}
          <CustomButton
            color="green"
            size={isMobile ? 'max' : 'xl'}
            disabled={!confirmationEnabled}
            onClick={() => handleConfirm()}
            text={confirmText}
          />
        </div>
      </div>
    </div>
  );
};
