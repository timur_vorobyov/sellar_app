import OrdersHistoryList from 'components/SubOrdersHistory/SubOrdersHistoryList/SubOrdersHistoryList';
import SubOrderHistoryDetails from 'components/SubOrdersHistory/SubOrderHistoryDetails/SubOrderHistoryDetails';
import { PropsWithChildren, useEffect, useState } from 'react';
import { Anything, Option } from 'types/generics';
import { SubOrderHistory, SubOrderState } from 'types/models/subOrder';
import ReportBtn from 'components/SubOrdersHistory/ReportBtn/ReportBtn';
import { SubOrdersHistoryFilter } from 'types/pages';
import useApiCall from 'utils/hooks/use-api-call';
import { FoodSupplierPlatform } from 'types/models/stall';
import useExplicitApiCall from 'utils/hooks/use-explicit-api-call';
import { getFoodSupplierPlatforms, getSubOrdersHistory } from 'apis/graphql/hasura/queries';
import { getZReportData } from 'apis/rest/subOrders';
import useCommonTranslations from 'utils/hooks/user-common-translations';
import { useEuiI18n } from '@elastic/eui';
import { FilterMobile } from 'components/PDA/FilterMobile/FilterMobile';
import PrintIcon from 'components/assets/images/HistoryPage/printIcon.svg';
import { printAcceptedReceipt, printCancelledReceipt } from 'utils/receipt';
import { receiptTypes } from 'constants/receiptType';
import { FeatureFlags } from '../../constants/featureFlags';
import { useTreatments } from '@splitsoftware/splitio-react';
import BackIcon from 'components/assets/images/HeaderMobile/arrow-back.svg';
import moment from 'moment-timezone';
import { useLocation } from 'react-router-dom';
import './SubOrdersHistoryPage.scss';
import Typography from 'components/ui-kit/Typography';
import { useLanguageContext } from 'contexts/languageContextProvider/context';
import NavBarMobile from 'components/PDA/NavBarMobile/NavBarMobile';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import { useFoodSupplierContext } from 'contexts/foodSupplierProvider/context';
import { ZReportData } from 'types/ZReportData';
import HeaderMobile from 'components/PDA/HeaderMobile/HeaderMobile';
import NavBarTablet from 'components/Tablet/NavBarTablet/NavBarTablet';
interface LocationState {
  pathname: string;
  state: {
    subOrder: SubOrderHistory;
  };
  key: string;
  subOrder: SubOrderHistory;
}

export const SubOrdersHistoryPage = (_props: PropsWithChildren<Anything>): JSX.Element => {
  const [filterText, platformText, orderStatusText, orderDetailsText] = useEuiI18n(
    [
      'menu.filters.filters',
      'subOrdersHistory.filters.platform',
      'subOrdersHistory.filters.subOrderStatus',
      'menu.header.subOrderDetails',
    ],
    [],
  );
  const location = useLocation<LocationState>();
  const subOrderHistory = location?.state?.subOrder || null;

  const { isMobile, isTablet } = useDeviceContext();
  const [showFilterMobile, setShowFilterMobile] = useState(false);

  const [selectedSubOrderHistory, setSelectedSubOrderHistory] = useState<SubOrderHistory | null>(null);

  type PlatformsProps = { data: FoodSupplierPlatform[] | null; error: unknown; isLoading: boolean };
  const { getFoodSupplierId } = useFoodSupplierContext();
  const foodSupplierId = getFoodSupplierId();

  const { data: platforms }: PlatformsProps = useApiCall(getFoodSupplierPlatforms, { args: [foodSupplierId] });
  const [platformsOptionsState, setPlatformsOptionsState] = useState<Option[]>([]);
  const [filters, setFilters] = useState<SubOrdersHistoryFilter>({
    subOrderStatuses: [1, 2, 3, 4],
    date: moment().tz('Asia/Singapore'),
    platformsIds: [],
    searchTerm: '',
  });

  const { execute: executeSubOrdersHistory, data: ordersHistory } =
    useExplicitApiCall<SubOrderHistory[]>(getSubOrdersHistory);
  const { execute: executeZReportData, data: zReportData } = useExplicitApiCall<ZReportData>(getZReportData);
  const [subOrdersHistoryState, setSubOrdersHistoryState] = useState<SubOrderHistory[]>([]);
  const { getLocale } = useLanguageContext();
  const feat = useTreatments([FeatureFlags.QR_CODE]);

  const netTotalSum = zReportData ? zReportData.netTotalSum : 0;
  const grossTotalSum = zReportData ? zReportData.grossTotalSum : 0;
  const refundSum = zReportData ? zReportData.refundSum : 0;

  useEffect(
    function () {
      if (subOrderHistory) {
        setSelectedSubOrderHistory(subOrderHistory);
      }
    },
    [subOrderHistory],
  );

  useEffect(() => {
    if (platforms) {
      const platformsOptions = platforms.map((platform: FoodSupplierPlatform) => {
        //this part of code should be modified when filter comonent will be adapted
        let i = 0;
        return { value: ++i, text: platform.name };
      });
      setPlatformsOptionsState(platformsOptions);
    }
  }, [platforms]);

  useEffect(() => {
    setFilters({
      ...filters,
      platformsIds: platformsOptionsState.map((option) => option.value),
    });
  }, [platformsOptionsState]);

  useEffect(() => {
    executeZReportData(foodSupplierId, [{ date: filters.date }]);
    executeSubOrdersHistory(foodSupplierId, [filters]);
  }, [filters]);

  useEffect(() => {
    setSubOrdersHistoryState(ordersHistory || []);
  }, [ordersHistory]);

  const { orderStatuses } = useCommonTranslations();

  const onPrint = (ev: React.MouseEvent) => {
    ev.preventDefault();
    if (selectedSubOrderHistory) {
      selectedSubOrderHistory?.state === SubOrderState.Cancelled
        ? printCancelledReceipt(selectedSubOrderHistory, isTablet)
        : printAcceptedReceipt(
            selectedSubOrderHistory,
            getLocale(),
            receiptTypes.TENNANT,
            feat[FeatureFlags.QR_CODE].treatment,
            isTablet,
          );
    }
  };

  const MobileHeaderWithPrint = () => (
    <div className="HeaderMobile__container">
      <img src={BackIcon} onClick={() => setSelectedSubOrderHistory(null)} alt="Back" />
      <div className="SubOrdersHistory__headerTitle">
        <Typography color="black" variant="span" fontWeight="bold">
          {orderDetailsText}
        </Typography>
        <img src={PrintIcon} onClick={onPrint} alt="Print" />
      </div>
    </div>
  );

  return (
    <div className="SubOrdersHistory__container">
      {selectedSubOrderHistory ? (
        <>
          {isMobile && <MobileHeaderWithPrint />}
          <SubOrderHistoryDetails
            selectedSubOrderHistory={selectedSubOrderHistory}
            setSelectedSubOrderHistory={(subOrderHistory: SubOrderHistory | null) =>
              setSelectedSubOrderHistory(subOrderHistory)
            }
          />
        </>
      ) : (
        <>
          {showFilterMobile && (
            <>
              <HeaderMobile text={filterText} onButtonClick={() => setShowFilterMobile(false)} />
              <FilterMobile
                label={`${platformText}:`}
                secondLabel={`${orderStatusText}:`}
                options={platformsOptionsState}
                selected={filters.platformsIds}
                secondOptions={orderStatuses}
                secondSelected={filters.subOrderStatuses}
                onApply={(selected: number[], secondSelected: number[]) =>
                  setFilters({ ...filters, platformsIds: selected, subOrderStatuses: secondSelected })
                }
                backToMenu={() => setShowFilterMobile(false)}
              />
            </>
          )}
          {!showFilterMobile && (
            <>
              {isTablet ? (
                <NavBarTablet append={<ReportBtn zReportData={zReportData} />} />
              ) : (
                <NavBarMobile zReportData={zReportData} />
              )}
              <OrdersHistoryList
                refundSum={refundSum}
                netTotalSum={netTotalSum}
                grossTotalSum={grossTotalSum}
                setSelectedSubOrderHistory={(subOrderHistory: SubOrderHistory) =>
                  setSelectedSubOrderHistory(subOrderHistory)
                }
                platformsOptions={platformsOptionsState}
                subOrdersStatusesOptions={orderStatuses}
                filters={filters}
                setFilters={setFilters}
                subOrdersHistoryState={subOrdersHistoryState}
                setShowFilterMobile={setShowFilterMobile}
              />
            </>
          )}
        </>
      )}
    </div>
  );
};
