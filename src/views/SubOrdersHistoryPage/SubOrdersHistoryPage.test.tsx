import '@testing-library/jest-dom';
import { cleanup } from '@testing-library/react';
import { SubOrdersHistoryPage } from './SubOrdersHistoryPage';
import { BrowserRouter, Redirect } from 'react-router-dom';
import { StateProvider } from '../..//contexts/store';
import { DeviceContextProvider } from '../..//contexts/deviceContextProvider/deviceContextProvider';
import { subOrderHistory } from '../../__mocks__/mocks';
import { customI18nRender } from 'utils/testing/customRender';
import { FoodSupplierProvider } from 'contexts/foodSupplierProvider/provider';

afterEach(cleanup);

describe('<SubOrdersHistoryPage> page', () => {
  it('should render without crashing', () => {
    const { container } = customI18nRender(
      <FoodSupplierProvider>
        <BrowserRouter>
          <StateProvider>
            <DeviceContextProvider width={500}>
              <SubOrdersHistoryPage />
            </DeviceContextProvider>
          </StateProvider>
          <Redirect
            to={{
              pathname: '/history',
              state: {
                subOrder: subOrderHistory,
              },
            }}
          />
        </BrowserRouter>
      </FoodSupplierProvider>,
    );
    expect(container.textContent).toMatch('SUBMITTED');
    expect(container.textContent).toMatch('Fishball Soup Noodle');
  });
});
