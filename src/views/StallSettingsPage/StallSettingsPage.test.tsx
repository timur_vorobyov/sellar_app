import '@testing-library/jest-dom';
import { cleanup, render } from '@testing-library/react';
import { FoodSupplierProvider } from 'contexts/foodSupplierProvider/provider';
import { StallSettingsPage } from './StallSettingsPage';

afterEach(cleanup);
jest.mock('components/Tablet/NavBarTablet/NavBarTablet', () => () => <div>Top nav bar</div>);
jest.mock('components/StallSettings/StallTable/StallTable', () => () => <div>Stall Table</div>);
jest.mock('components/BottomButton/BottomButton', () => () => <div>Bottom button</div>);

describe('<StallSettingsPage> page', () => {
  it('should render without crashing', () => {
    const { container } = render(
      <FoodSupplierProvider>
        <StallSettingsPage />
      </FoodSupplierProvider>,
    );
    expect(container.textContent).toMatch('Top nav bar');
    expect(container.textContent).toMatch('Stall Table');
    expect(container.textContent).toMatch('Bottom button');
  });
});
