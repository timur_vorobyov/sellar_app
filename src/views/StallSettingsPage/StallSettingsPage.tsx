import { PropsWithChildren } from 'react';
import { Anything } from 'types/generics';
import StallTable from 'components/StallSettings/StallTable/StallTable';
import BottomButton from 'components/BottomButton/BottomButton';
import { useStateContext } from 'contexts/store';
import { Dictionary } from 'types/generics';
import { ActionTypes } from 'types/store';
import { updateStallSettings } from 'apis/rest/catalog';
import useExplicitAsyncCall from 'utils/hooks/use-explicit-api-call';
import { EuiButton } from '@elastic/eui';
import config from 'config';
import StallTableMobile from 'components/StallSettings/StallTableMobile/StallTableMobile';
import './StallSettingsPage.scss';
import { useFoodSupplierContext } from 'contexts/foodSupplierProvider/context';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import NavBarMobile from 'components/PDA/NavBarMobile/NavBarMobile';
import NavBarTablet from 'components/Tablet/NavBarTablet/NavBarTablet';

export const StallSettingsPage = (_props: PropsWithChildren<Anything>): JSX.Element => {
  const { execute: executeUpdateStallSettings } = useExplicitAsyncCall(updateStallSettings);
  const {
    state: { foodSupplierPlatforms },
    dispatch,
  } = useStateContext();
  const { getFoodSupplierId } = useFoodSupplierContext();
  const foodSupplierId = getFoodSupplierId();

  const { isMobile, isTablet } = useDeviceContext();

  const onChange = (platform: Dictionary) => {
    executeUpdateStallSettings(platform, foodSupplierId);

    dispatch({
      type: ActionTypes.UPDATE_FOOD_SUPPLIER_PLATFORMS,
      payload: {
        platforms: [...foodSupplierPlatforms.map((el) => (el.id === platform.id ? { ...el, ...platform } : el))],
      },
    });
  };

  const onChangeAll = (platforms: Dictionary[]) => {
    Promise.all(
      platforms.map(async (platform) => {
        executeUpdateStallSettings(platform, foodSupplierId);
      }),
    );

    dispatch({
      type: ActionTypes.UPDATE_FOOD_SUPPLIER_PLATFORMS,
      payload: {
        platforms: [
          ...foodSupplierPlatforms.map((el) => {
            const newPlatform = platforms.find((plat) => el.id === plat.id);
            if (newPlatform) {
              return { ...el, ...newPlatform };
            }
            return el;
          }),
        ],
      },
    });
  };

  const buildVersionAppend = (
    <div className="build-version">
      Seller app version: {config.buildVersion}
      <EuiButton size="s" fill onClick={() => window.location.reload()}>
        ↻
      </EuiButton>
    </div>
  );

  return (
    <div className="StallSettingsPage__container">
      {isTablet ? <NavBarTablet append={buildVersionAppend} /> : <NavBarMobile />}

      {isMobile ? (
        <StallTableMobile onChange={onChange} onChangeAll={onChangeAll} foodSuppliers={foodSupplierPlatforms} />
      ) : (
        <StallTable onChange={onChange} onChangeAll={onChangeAll} foodSuppliers={foodSupplierPlatforms} />
      )}
      {isTablet && <BottomButton />}
    </div>
  );
};
