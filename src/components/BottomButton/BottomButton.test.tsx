import { cleanup, fireEvent } from '@testing-library/react';
import { screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';

import '@testing-library/jest-dom';
import BottomButton from './BottomButton';
import { RouteId, routes } from 'config/routes';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<BottomButton> component', () => {
  it('should redirect to order page if button has been clicked', () => {
    const button = (
      <BrowserRouter>
        <BottomButton />
      </BrowserRouter>
    );
    customI18nRender(button, { locale: Locale.CHINESE });

    fireEvent.click(screen.getByText('Back to Orders 返回订单页面'));
    expect(window.location.href).toBe('http://localhost' + routes[RouteId.Orders].path);
  });
});
