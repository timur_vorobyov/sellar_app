import { useEuiI18n } from '@elastic/eui';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import { RouteId, routes } from 'config/routes';
import { useHistory } from 'react-router-dom';
import './BottomButton.scss';

const BottomButton = () => {
  const history = useHistory();
  const [backToOrdersButtonText] = useEuiI18n(['menu.footer.backToSubOrdersButton'], []);

  const onBackToOrderClick = () => {
    history.push(routes[RouteId.Orders].path);
  };

  return (
    <div className="BottomButton__container">
      <CustomButton padding="xl" onClick={onBackToOrderClick} text={backToOrdersButtonText} />
    </div>
  );
};

export default BottomButton;
