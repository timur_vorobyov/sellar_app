import WarningIcon from 'components/assets/images/SubOrderPage/warningIcon.svg';

import './Splash.scss';

interface SplashProps {
  message: string;
}

export const Splash = ({ message }: SplashProps): JSX.Element => {
  return (
    <div className="Splash__overlay">
      <div className="Splash__container">
        <img src={WarningIcon} width="70" />
        <h1 className="Splash__title">
          <p>{message}</p>
        </h1>
      </div>
    </div>
  );
};
