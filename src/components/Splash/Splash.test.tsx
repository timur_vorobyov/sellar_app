import '@testing-library/jest-dom';
import { cleanup, render, screen } from '@testing-library/react';
import { Splash } from './Splash';

afterEach(cleanup);

describe('<Splash> component', () => {
  it('should render component with correct message', () => {
    const splashMessage = 'splashComponentMessage';

    render(<Splash message={splashMessage} />);
    expect(screen.getByText(splashMessage)).toBeInTheDocument();
  });
});
