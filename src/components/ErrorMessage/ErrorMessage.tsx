import { useHistory } from 'react-router-dom';

import { useEuiI18n } from '@elastic/eui';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import WarningIcon from 'components/assets/images/SubOrderPage/warningIcon.svg';
import './ErrorMessage.scss';

interface ErrorMessageProps {
  refund?: boolean;
}

export default function ErrorMessage({ refund }: ErrorMessageProps) {
  const history = useHistory();

  const [
    refundUnsuccessfulTitleText,
    refundUnsuccessfulDescriptionText,
    refundUnsuccessfulButtonText,
    autoRejectedTitleText,
    autoRejectedDescriptionText,
    autoRejectedButtonText,
  ] = useEuiI18n(
    [
      'errorMessage.refundUnsuccessful.title',
      'errorMessage.refundUnsuccessful.text',
      'errorMessage.refundUnsuccessful.backToRefundScreen',
      'errorMessage.autoRejected.title',
      'errorMessage.autoRejected.text',
      'errorMessage.autoRejected.backToSubOrderDetails',
    ],
    [],
  );

  return (
    <div className="ErrorMessage__overlay">
      <div className="ErrorMessage__container">
        <img src={WarningIcon} width="70" />
        {refund ? (
          <>
            <h1 className="ErrorMessage__title">
              <p>{refundUnsuccessfulTitleText}</p>
            </h1>
            <p className="ErrorMessage__text">{refundUnsuccessfulDescriptionText}</p>
            <CustomButton size="xl" onClick={() => history.push('/history')} text={refundUnsuccessfulButtonText} />
          </>
        ) : (
          <>
            <h1 className="ErrorMessage__title">
              <p>{autoRejectedTitleText}</p>
            </h1>
            <p className="ErrorMessage__text">{autoRejectedDescriptionText}</p>
            <CustomButton size="xl" onClick={() => history.push('/')} text={autoRejectedButtonText} />
          </>
        )}
      </div>
    </div>
  );
}
