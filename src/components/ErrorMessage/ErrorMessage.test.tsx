import '@testing-library/jest-dom';
import { screen, cleanup, fireEvent } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import ErrorMessage from './ErrorMessage';
import WarningIcon from 'components/assets/images/SubOrderPage/warningIcon.svg';
import { RouteId, routes } from 'config/routes';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<ErrorMessage> component', () => {
  it('should render component for reject', () => {
    customI18nRender(<ErrorMessage />, { locale: Locale.CHINESE });
    expect(screen.getByRole('img')).toHaveAttribute('src', WarningIcon);
    expect(screen.getByText('Order is already auto-rejected 订单已被自动拒绝')).toBeInTheDocument();
    expect(
      screen.getByText(
        'This order has been auto-rejected as it has passed the grace period. 此订单已超过宽限期，因此已被自动拒绝。',
      ),
    ).toBeInTheDocument();
  });

  it('should render component for refund', () => {
    customI18nRender(<ErrorMessage refund={true} />, { locale: Locale.CHINESE });
    expect(screen.getByRole('img')).toHaveAttribute('src', WarningIcon);
    expect(screen.getByText('Refund unsuccessful 退款失败')).toBeInTheDocument();
    expect(
      screen.getByText('Refund unsuccessful due to system failure. Please try again. 系统故障导致退款失败。请再试一次'),
    ).toBeInTheDocument();
  });

  it('should redirect to OrdersPage after back button clicked', () => {
    customI18nRender(
      <BrowserRouter>
        <ErrorMessage />
      </BrowserRouter>,
      { locale: Locale.CHINESE },
    );
    const backBtn = screen.getByText('Back to Order Details 返回订单详细');
    fireEvent.click(backBtn);
    expect(window.location.href).toBe('http://localhost' + routes[RouteId.Orders].path);
  });
});
