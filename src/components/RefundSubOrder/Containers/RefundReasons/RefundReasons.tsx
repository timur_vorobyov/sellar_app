import { useHistory } from 'react-router-dom';
import { useEuiI18n } from '@elastic/eui';
import RefundReasonButton from '../../RefundReasonButton/RefundReasonButton';
import { RefundReasonKey } from 'constants/refund';

import './RefundReasons.scss';
import useCommonTranslations from 'utils/hooks/user-common-translations';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import { BoxTypography } from '../../../ui-kit/Typography';

type RefundReasons = {
  [key in RefundReasonKey]: string;
};

interface RefundReasonsProps {
  refundReasons: RefundReasons;
  selectedReason: string;
  handleRefundReason: (reason: string) => void;
  handleConfirm: () => void;
}

export default function RefundReasons({
  refundReasons,
  selectedReason,
  handleRefundReason,
  handleConfirm,
}: RefundReasonsProps) {
  const history = useHistory();

  const [titleText, cancelButtonText, confirmButtonText] = useEuiI18n(
    ['refundSubOrder.reasonsList.title', 'refundSubOrder.reasonsList.cancel', 'refundSubOrder.reasonsList.confirm'],
    [],
  );
  const { refundReasons: refundReasonTranslations } = useCommonTranslations();

  return (
    <div className="RefundReasons__container">
      <div className="RefundReasons__header">
        <BoxTypography
          typographyProps={{ variant: 'h1', fontWeight: 'bold', lh: '34', style: { fontSize: 26 } }}
          boxProps={{ pt: 8, pb: 8, pl: 20, pr: 20 }}
        >
          {titleText}
        </BoxTypography>
      </div>
      <div className="RefundReasons__body">
        <div className="RefundReasons__body__reason__btns">
          {Object.entries(refundReasons).map(([key]) => (
            <RefundReasonButton
              key={key}
              label={refundReasonTranslations[key as RefundReasonKey]}
              checked={key === selectedReason}
              onClick={() => handleRefundReason(key)}
            />
          ))}
        </div>
      </div>
      <div className="RefundReasons__buttonsContainer">
        <CustomButton color="red" size="xl" onClick={() => history.push('/history')} text={cancelButtonText} />
        <CustomButton
          color="green"
          size="xl"
          disabled={selectedReason === ''}
          onClick={handleConfirm}
          text={confirmButtonText}
        />
      </div>
    </div>
  );
}
