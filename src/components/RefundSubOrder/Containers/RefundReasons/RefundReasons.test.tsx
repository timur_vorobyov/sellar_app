import '@testing-library/jest-dom';
import { cleanup, fireEvent, screen } from '@testing-library/react';
import RefundReasons from './RefundReasons';
import { RefundReasonKey, refundReasons } from 'constants/refund';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

const reasons = {
  damagedItem: 'Damaged item / packaging spillage 损坏的物品/包装溢出',
  discount: 'K-Card discount not reflected K-Card 折扣未反映',
  missingItems: 'Missing item / ingredients 缺失项目/成分',
  wrongItems: 'Wrong / additional item(s) 错误/附加项目',
};

describe('<RefundReasons> component', () => {
  it('should render RefundTotal component with data', () => {
    const mockedReasons = {
      [RefundReasonKey.KCardDiscount]: reasons.discount,
    };

    jest.mock('utils/hooks/user-common-translations', () => {
      return {
        refundReasons: mockedReasons,
      };
    });

    const handleRefundReasonMock = jest.fn();

    customI18nRender(
      <RefundReasons
        refundReasons={refundReasons}
        selectedReason={''}
        handleConfirm={() => null}
        handleRefundReason={handleRefundReasonMock}
      />,
      { locale: Locale.CHINESE },
    );

    const title = screen.getByText('What is the reason for refunding this order? 为什么要推这份订单？');
    const cancelBtn = screen.getByText('Cancel 取消');
    const confirmBtn = screen.getByText('Confirm 确定');
    const damagedItemReason = screen.getByText(reasons.damagedItem);
    const discountReason = screen.getByText(reasons.discount);
    const missingItemsReason = screen.getByText(reasons.missingItems);
    const wrongItemsReason = screen.getByText(reasons.wrongItems);

    expect(title).toBeInTheDocument();
    expect(cancelBtn).toBeInTheDocument();
    expect(confirmBtn).toBeInTheDocument();
    expect(damagedItemReason).toBeInTheDocument();
    expect(discountReason).toBeInTheDocument();
    expect(missingItemsReason).toBeInTheDocument();
    expect(wrongItemsReason).toBeInTheDocument();

    fireEvent.click(discountReason);
    expect(handleRefundReasonMock).toBeCalledWith(RefundReasonKey.KCardDiscount);
  });
});
