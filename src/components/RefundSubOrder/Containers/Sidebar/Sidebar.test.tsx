import '@testing-library/jest-dom';
import { cleanup, screen } from '@testing-library/react';
import Sidebar from './Sidebar';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';
import { subOrderHistory, subOrderHistoryProduct } from '__mocks__/mocks';

afterEach(cleanup);

describe('<Sidebar> component', () => {
  it('should render Sidebar component with product data unfreeezed', () => {
    customI18nRender(
      <Sidebar
        subOrderHistory={subOrderHistory}
        selectedProducts={subOrderHistory.products}
        onSelectAll={() => null}
        handleProductQuantity={() => null}
      />,
      { locale: Locale.CHINESE },
    );

    const productQuantity = screen.getByText('x2');
    const productName = screen.getByText('Fishball Soup Noodle');
    const selectAll = screen.getByText('Select all 全选');

    expect(productQuantity).toHaveTextContent(`x${subOrderHistoryProduct.quantity}`);
    expect(productName).toHaveTextContent(subOrderHistoryProduct.name);
    expect(selectAll).toBeInTheDocument();
  });
});
