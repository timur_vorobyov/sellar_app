import '@testing-library/jest-dom';
import { cleanup, fireEvent, screen } from '@testing-library/react';
import ProductDataRaw from './ProductDataRaw';
import { customI18nRender } from 'utils/testing/customRender';
import { subOrderHistoryProduct, subOrderHistoryProductCustomisations, refundPageProduct } from '__mocks__/mocks';
import { SubOrderHistoryProduct, SubOrderHistoryProductCustomisation, RefundPageProduct } from 'types/models/product';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<ProductDataRaw> component', () => {
  it('should render component with correct product name', () => {
    const getProductForCounterMocked = jest.fn((): RefundPageProduct => refundPageProduct);

    customI18nRender(
      <ProductDataRaw
        product={refundPageProduct}
        handleUpdateProducts={jest.fn()}
        handleUpdateCustomisations={jest.fn()}
        isRefunded={false}
        getProductForCounter={getProductForCounterMocked}
        getCustomisationForCounter={jest.fn()}
      />,
      { locale: Locale.CHINESE },
    );

    const productName = screen.getByText(refundPageProduct.name);
    expect(productName).toBeInTheDocument();
  });

  it('should render correct text for refunded product', () => {
    const orderHistoryProductRefunded = { ...subOrderHistoryProduct, refundedQuantity: 2, actualQuantity: 3 };
    const getProductForCounterMocked = jest.fn((): RefundPageProduct => orderHistoryProductRefunded);

    customI18nRender(
      <ProductDataRaw
        product={orderHistoryProductRefunded}
        handleUpdateProducts={jest.fn()}
        handleUpdateCustomisations={jest.fn()}
        isRefunded={false}
        getProductForCounter={getProductForCounterMocked}
        getCustomisationForCounter={jest.fn()}
      />,
      { locale: Locale.CHINESE },
    );

    const productRefundedQuantityText = screen.getByText(`${orderHistoryProductRefunded.quantity} refunded 已退款个`);
    expect(productRefundedQuantityText).toBeInTheDocument();
  });

  it('should render product customisations if they are available', () => {
    const orderHistoryProductWithCustomisations = {
      ...subOrderHistoryProduct,
      actualQuantity: subOrderHistoryProduct.quantity,
      customisations: subOrderHistoryProductCustomisations,
    };

    const getCustomisationForCounter = jest.fn(
      (
        product: SubOrderHistoryProduct,
        customisation: SubOrderHistoryProductCustomisation,
      ): SubOrderHistoryProductCustomisation => {
        return { ...customisation };
      },
    );
    const getProductForCounterMocked = jest.fn((): RefundPageProduct => orderHistoryProductWithCustomisations);

    customI18nRender(
      <ProductDataRaw
        product={orderHistoryProductWithCustomisations}
        handleUpdateProducts={jest.fn()}
        handleUpdateCustomisations={jest.fn()}
        isRefunded={false}
        getProductForCounter={getProductForCounterMocked}
        getCustomisationForCounter={getCustomisationForCounter}
      />,
      { locale: Locale.CHINESE },
    );

    orderHistoryProductWithCustomisations.customisations.forEach((customisation) => {
      const expectedCustomisationText = customisation.nameCn + ' ' + 'x' + customisation.actualQuantity;
      const customisationText = screen.getByText(expectedCustomisationText);
      expect(customisationText).toBeInTheDocument();
    });
  });

  it('should update products after checkbox has been clicked', () => {
    const orderHistoryProductRefunded = { ...subOrderHistoryProduct, actualQuantity: 2 };
    const getProductForCounterMocked = jest.fn((): RefundPageProduct => orderHistoryProductRefunded);
    const handleUpdateProductsMocked = jest.fn();

    customI18nRender(
      <ProductDataRaw
        product={orderHistoryProductRefunded}
        handleUpdateProducts={handleUpdateProductsMocked}
        handleUpdateCustomisations={jest.fn()}
        isRefunded={false}
        getProductForCounter={getProductForCounterMocked}
        getCustomisationForCounter={jest.fn()}
      />,
      { locale: Locale.CHINESE },
    );

    const checkBox = screen.getByTestId('Sidebar__checkbox');
    fireEvent.click(checkBox);

    expect(handleUpdateProductsMocked).toHaveBeenCalledTimes(1);
    expect(handleUpdateProductsMocked).toBeCalledWith(orderHistoryProductRefunded, false);
  });

  it('should properly increase and decrease product quantity', () => {
    let optionPlusHasBeenCalled = false;
    let optionMinusHasBeenCalled = false;
    let productCounterHasBeenCalledWith: SubOrderHistoryProduct | undefined;

    const customQuantityProduct = { ...subOrderHistoryProduct, quantity: 6, actualQuantity: 3 };

    const getProductForCounterMocked = jest.fn(
      (product: SubOrderHistoryProduct, option?: string | undefined): RefundPageProduct => {
        if (option === 'minus') {
          optionMinusHasBeenCalled = true;
        }
        if (option === 'plus') {
          optionPlusHasBeenCalled = true;
        }

        productCounterHasBeenCalledWith = { ...productCounterHasBeenCalledWith, ...product };

        return { ...subOrderHistoryProduct, quantity: 3, refundedQuantity: 0, actualQuantity: 3 };
      },
    );

    customI18nRender(
      <ProductDataRaw
        product={customQuantityProduct}
        handleUpdateProducts={jest.fn()}
        handleUpdateCustomisations={jest.fn()}
        isRefunded={false}
        getProductForCounter={getProductForCounterMocked}
        getCustomisationForCounter={jest.fn()}
      />,
      { locale: Locale.CHINESE },
    );

    const counterMinus = screen.getByTestId('Sidebar__counterMinus');
    fireEvent.click(counterMinus);
    expect(optionMinusHasBeenCalled).toBe(true);
    expect(productCounterHasBeenCalledWith).toMatchObject(customQuantityProduct);

    const counterPlus = screen.getByTestId('Sidebar__counterPlus');
    fireEvent.click(counterPlus);
    expect(optionPlusHasBeenCalled).toBe(true);
    expect(productCounterHasBeenCalledWith).toMatchObject(customQuantityProduct);
  });
});
