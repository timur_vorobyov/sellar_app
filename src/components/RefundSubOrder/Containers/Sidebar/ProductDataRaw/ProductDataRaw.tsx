import { RefundPageProduct, RefundPageProductCustomisation } from 'types/models/product';
import { sliceTwoMoreDecimals } from 'utils/formater';
import Checkbox from '../Checkbox/Checkbox';
import Counter from '../Counter/Counter';
import CustomisationDataRaw from './CustomisationDataRaw/CustomisationDataRaw';
import { useEuiI18n } from '@elastic/eui';
import { useLanguageContext } from 'contexts/languageContextProvider/context';
import { BoxTypography } from '../../../../ui-kit/Typography';
import Box from '../../../../ui-kit/Box';
import './ProductDataRaw.scss';

interface ProductDataRawProps {
  product: RefundPageProduct;
  handleUpdateProducts: (product: RefundPageProduct, checked: boolean) => void;
  handleUpdateCustomisations: (
    product: RefundPageProduct,
    customisation: RefundPageProductCustomisation,
    checked: boolean,
  ) => void;
  isRefunded: boolean;
  getProductForCounter: (product: RefundPageProduct, option?: string | undefined) => RefundPageProduct;
  getCustomisationForCounter: (
    product: RefundPageProduct,
    customisation: RefundPageProductCustomisation,
    option?: string,
  ) => RefundPageProductCustomisation;
}

const ProductDataRaw = ({
  isRefunded,
  product,
  handleUpdateProducts,
  handleUpdateCustomisations,
  getProductForCounter,
  getCustomisationForCounter,
}: ProductDataRawProps) => {
  const [refundedText] = useEuiI18n(['subOrdersHistory.details.refunded'], []);
  const { getLocale } = useLanguageContext();

  let productName = getLocale() === 'en' ? product.name : product.nameCn;

  if (!productName) {
    productName = product.name;
  }

  return (
    <div className="ProductDataRaw__container">
      <div className="ProductDataRaw__product">
        <div className="ProductDataRaw__firstColumn">
          <Checkbox
            isDisabled={isRefunded || product.quantity === product.refundedQuantity}
            checked={!!getProductForCounter(product).quantity}
            onChange={(checked) => {
              handleUpdateProducts(product, checked);
            }}
          />
          <BoxTypography
            typographyProps={{ variant: 'span', fontWeight: 'bold' }}
            boxProps={{ addClass: 'ProductDataRaw__productQuantity', mr: 16, ml: 16 }}
          >
            {'x' + product.quantity}
          </BoxTypography>
        </div>
        <div className="ProductDataRaw__secondColumn">
          <div className="ProductDataRaw__productNameContainer">
            <BoxTypography
              typographyProps={{ variant: 'span', fontWeight: 'bold' }}
              boxProps={{ addClass: 'ProductDataRaw__name' }}
            >
              {productName}
            </BoxTypography>
            <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'ProductDataRaw__quantity' }}>
              ${sliceTwoMoreDecimals(product.fullProductSum)}
            </BoxTypography>
          </div>
          {product.refundedQuantity > 0 ? (
            <Box>
              <BoxTypography
                typographyProps={{ variant: 'span' }}
                boxProps={{ addClass: 'ProductDataRaw__refundedAmount', mt: 8 }}
              >
                {product.refundedQuantity + ' ' + refundedText}
              </BoxTypography>
            </Box>
          ) : null}
          {getProductForCounter(product).quantity !== 0 && product.quantity > 1 && (
            <Counter
              isProduct={true}
              isRefunded={isRefunded}
              selectedItemsQuantity={getProductForCounter(product).quantity}
              generalItemsQuantity={product.quantity}
              refundedQuantity={product.refundedQuantity}
              decreaseAmount={() => getProductForCounter(product, 'minus')}
              increaseAmount={() => getProductForCounter(product, 'plus')}
            />
          )}
          {product.customisations.length
            ? product.customisations.map((customisation: RefundPageProductCustomisation) => (
                <CustomisationDataRaw
                  locale={getLocale()}
                  key={customisation.id}
                  handleUpdateCustomisations={handleUpdateCustomisations}
                  customisation={customisation}
                  product={product}
                  isRefund={isRefunded}
                  getCustomisationForCounter={getCustomisationForCounter}
                />
              ))
            : null}
        </div>
      </div>
    </div>
  );
};

export default ProductDataRaw;
