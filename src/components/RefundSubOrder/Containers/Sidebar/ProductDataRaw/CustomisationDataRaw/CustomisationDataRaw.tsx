import { RefundPageProduct, RefundPageProductCustomisation } from 'types/models/product';
import { sliceTwoMoreDecimals } from 'utils/formater';
import { useEuiI18n } from '@elastic/eui';
import Checkbox from '../../Checkbox/Checkbox';
import Counter from '../../Counter/Counter';
import { BoxTypography } from 'components/ui-kit/Typography';
import Box from '../../../../../ui-kit/Box';
import './CustomisationDataRaw.scss';

interface CustomisationDataRawProps {
  locale: string;
  customisation: RefundPageProductCustomisation;
  product: RefundPageProduct;
  handleUpdateCustomisations: (
    product: RefundPageProduct,
    customisation: RefundPageProductCustomisation,
    checked: boolean,
  ) => void;
  isRefund: boolean;
  getCustomisationForCounter: (
    product: RefundPageProduct,
    customisation: RefundPageProductCustomisation,
    option?: string,
  ) => RefundPageProductCustomisation;
}
const CustomisationDataRaw = ({
  locale,
  product,
  customisation,
  isRefund,
  handleUpdateCustomisations,
  getCustomisationForCounter,
}: CustomisationDataRawProps) => {
  const [refundedText] = useEuiI18n(['subOrdersHistory.details.refunded'], []);
  const isDisabled = isRefund || customisation.actualQuantity === customisation.refundedQuantity;
  const customisatioName = customisation.name
    ? locale === 'en'
      ? customisation.name
      : customisation.nameCn
    : customisation.nameCn;

  return (
    <div className="CustomisationDataRaw__container">
      <div className="CustomisationDataRaw__customisation">
        <div className="CustomisationDataRaw__firstColumn">
          <Checkbox
            isDisabled={isDisabled}
            checked={!!getCustomisationForCounter(product, customisation).actualQuantity}
            onChange={(checked) => {
              handleUpdateCustomisations(product, customisation, checked);
            }}
          />
          <Box ml={16}>
            <BoxTypography typographyProps={{ variant: 'span' }}>
              {customisatioName + ' ' + 'x' + customisation.actualQuantity}
            </BoxTypography>
            {customisation.refundedQuantity ? (
              <BoxTypography
                typographyProps={{ variant: 'span', color: 'red', fontWeight: 'normal' }}
                boxProps={{ addClass: 'CustomisationDataRaw__refundedAmount', mt: 8 }}
              >
                {customisation.refundedQuantity + ' ' + refundedText}
              </BoxTypography>
            ) : null}
          </Box>
        </div>
        <div className="CustomisationDataRaw__secondColumn">
          {customisation.price ? (
            <BoxTypography
              typographyProps={{ variant: 'span' }}
              boxProps={{ addClass: 'CustomisationDataRaw__customisationPrice' }}
            >
              ${sliceTwoMoreDecimals(customisation.price * customisation.actualQuantity)}
            </BoxTypography>
          ) : null}
        </div>
      </div>
      {getCustomisationForCounter(product, customisation).actualQuantity !== 0 && customisation.actualQuantity > 1 && (
        <Counter
          isProduct={false}
          isRefunded={isDisabled}
          selectedItemsQuantity={getCustomisationForCounter(product, customisation).actualQuantity}
          generalItemsQuantity={customisation.actualQuantity}
          refundedQuantity={customisation.refundedQuantity}
          decreaseAmount={() => getCustomisationForCounter(product, customisation, 'minus')}
          increaseAmount={() => getCustomisationForCounter(product, customisation, 'plus')}
        />
      )}
    </div>
  );
};

export default CustomisationDataRaw;
