import '@testing-library/jest-dom';
import { cleanup, fireEvent, screen } from '@testing-library/react';
import { customI18nRender } from 'utils/testing/customRender';
import CustomisationDataRaw from './CustomisationDataRaw';
import { Locale } from 'types/translation';
import { productCustomisation, refundPageProduct } from '__mocks__/mocks';

afterEach(cleanup);

describe('<CustomisationDataRaw> component', () => {
  it('should render component with correct data', () => {
    const mockedCustomisation = {
      ...productCustomisation,
      quantity: 2,
      actualQuantity: 2,
    };
    const handleUpdateCustomisationsMocked = jest.fn();
    const getCustomisationForCounterMocked = jest.fn(() => mockedCustomisation);

    customI18nRender(
      <CustomisationDataRaw
        customisation={productCustomisation}
        product={refundPageProduct}
        handleUpdateCustomisations={handleUpdateCustomisationsMocked}
        isRefund={false}
        getCustomisationForCounter={getCustomisationForCounterMocked}
        locale={Locale.CHINESE}
      />,
    );
  });

  it('should render correct text for refunded customisation', () => {
    const productCustomisationRefunded = {
      ...productCustomisation,
      actualQuantity: 6,
      quantity: 6,
      refundedQuantity: 1,
    };

    const handleUpdateCustomisationsMocked = jest.fn();
    const getCustomisationForCounterMocked = jest.fn(() => productCustomisationRefunded);

    customI18nRender(
      <CustomisationDataRaw
        customisation={productCustomisationRefunded}
        product={refundPageProduct}
        handleUpdateCustomisations={handleUpdateCustomisationsMocked}
        isRefund={false}
        getCustomisationForCounter={getCustomisationForCounterMocked}
        locale={Locale.CHINESE}
      />,
      { locale: Locale.CHINESE },
    );

    const customisationRefundedQuantityText = screen.getByText(
      `${productCustomisationRefunded.refundedQuantity} refunded 已退款个`,
    );
    expect(customisationRefundedQuantityText).toBeInTheDocument();
  });

  it('should call a handler if checkbox has been clicked', () => {
    const productCustomisationRefunded = {
      ...productCustomisation,
      actualQuantity: 6,
      quantity: 6,
      refundedQuantity: 1,
    };

    const handleUpdateCustomisationsMocked = jest.fn();
    const getCustomisationForCounterMocked = jest.fn(() => productCustomisationRefunded);

    customI18nRender(
      <CustomisationDataRaw
        customisation={productCustomisationRefunded}
        product={refundPageProduct}
        handleUpdateCustomisations={handleUpdateCustomisationsMocked}
        isRefund={false}
        getCustomisationForCounter={getCustomisationForCounterMocked}
        locale={Locale.CHINESE}
      />,
      { locale: Locale.CHINESE },
    );

    const checkBox = screen.getByTestId('Sidebar__checkbox');
    fireEvent.click(checkBox);

    expect(handleUpdateCustomisationsMocked).toBeCalledTimes(1);
  });
});
