import { EuiLink, useEuiI18n } from '@elastic/eui';
import { SubOrderHistoryProduct, RefundPageProduct, RefundPageProductCustomisation } from 'types/models/product';
import { SubOrderHistory } from 'types/models/subOrder';
import './Sidebar.scss';
import useCommonTranslations from 'utils/hooks/user-common-translations';
import SubOrderStatus from 'components/SubOrdersHistory/SubOrderStatus/SubOrderStatus';
import SubOrderUpdatedTime from 'components/SubOrdersHistory/SubOrderUpdatedTime/SubOrderUpdatedTime';
import ProductDataRaw from './ProductDataRaw/ProductDataRaw';
import Typography from 'components/ui-kit/Typography';

interface SidebarProps {
  subOrderHistory: SubOrderHistory;
  selectedProducts: RefundPageProduct[];
  handleUpdateProducts: (product: RefundPageProduct, checked: boolean) => void;
  handleUpdateCustomisations: (
    product: RefundPageProduct,
    customisation: RefundPageProductCustomisation,
    checked: boolean,
  ) => void;
  onSelectAll: () => void;
  handleProductQuantity: (product: RefundPageProduct, option: string) => void;
  handleCustomisationQuantity: (
    product: RefundPageProduct,
    customisation: RefundPageProductCustomisation,
    option: string,
  ) => void;
  isRefunded: boolean;
}
export default function Sidebar({
  subOrderHistory,
  selectedProducts,
  handleUpdateProducts,
  handleUpdateCustomisations,
  onSelectAll,
  handleProductQuantity,
  handleCustomisationQuantity,
  isRefunded,
}: SidebarProps) {
  const getProductForCounter = (product: RefundPageProduct, option?: string): RefundPageProduct => {
    const checkedProduct = selectedProducts.filter((prod) => prod.id === product.id);

    if (option) {
      handleProductQuantity(checkedProduct[0], option);
    }
    return checkedProduct[0];
  };

  const getCustomisationForCounter = (
    product: RefundPageProduct,
    customisation: RefundPageProductCustomisation,
    option?: string,
  ): RefundPageProductCustomisation => {
    const checkedProduct = selectedProducts.filter((prod) => prod.id === product.id);
    const checkedCustomisation = checkedProduct[0].customisations.filter(
      (cust: RefundPageProductCustomisation) => cust.id === customisation.id,
    );

    if (option) {
      handleCustomisationQuantity(checkedProduct[0], checkedCustomisation[0], option);
    }

    return checkedCustomisation[0];
  };

  const [selectAllText] = useEuiI18n(['refundSubOrder.selectAll'], []);

  const { orderTypes: orderTypeTranslations, paymentMethods } = useCommonTranslations();

  return (
    <div className="Sidebar__container">
      <div className="Sidebar__header">
        <div>
          <img src={subOrderHistory.logo} />
          <span className="Sidebar__header__orderNumber">{subOrderHistory.orderId}</span>
        </div>
        <div className="Sidebar__header__orderData">
          {subOrderHistory.order.refunds.length ? (
            <SubOrderStatus status={subOrderHistory.refundState} />
          ) : (
            <SubOrderUpdatedTime subOrderHistory={subOrderHistory} />
          )}
        </div>
        <div className="Sidebar__header__orderDetails">
          <span>{orderTypeTranslations[subOrderHistory.type]}</span>
          <span> | {paymentMethods[subOrderHistory.paymentsMethod]}</span>
        </div>
      </div>
      <div className="Sidebar__body">
        {!isRefunded && (
          <EuiLink className="Sidebar__selectAll__button" onClick={() => onSelectAll()}>
            <Typography variant="span">{selectAllText}</Typography>
          </EuiLink>
        )}
        {subOrderHistory.products.length ? (
          <div>
            {subOrderHistory.products.map((product: SubOrderHistoryProduct) => (
              <div key={product.id}>
                <ProductDataRaw
                  key={product.id}
                  product={{ ...product, actualQuantity: product.quantity }}
                  isRefunded={isRefunded}
                  handleUpdateProducts={handleUpdateProducts}
                  handleUpdateCustomisations={handleUpdateCustomisations}
                  getProductForCounter={getProductForCounter}
                  getCustomisationForCounter={getCustomisationForCounter}
                />
              </div>
            ))}
          </div>
        ) : null}
      </div>
    </div>
  );
}
