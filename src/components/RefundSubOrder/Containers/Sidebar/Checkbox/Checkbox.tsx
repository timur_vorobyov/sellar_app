import { EuiIcon } from '@elastic/eui';
import './Checkbox.scss';

interface CheckboxProps {
  checked: boolean;
  onChange: (checked: boolean) => void;
  isDisabled: boolean;
}

const getCheckboxIconColor = (isDisabled: boolean, checked: boolean) => {
  if (isDisabled) {
    return checked ? '#FFFFFF' : '#CCCCCC';
  }
  return '#FFFFFF';
};

const Checkbox = ({ checked, isDisabled, onChange }: CheckboxProps) => {
  return (
    <div className="Checkbox__container">
      <div
        data-testid="Sidebar__checkbox"
        className={
          checked
            ? `Checkbox__icon-checked${isDisabled ? '-disabled' : ''}`
            : `Checkbox__icon${isDisabled ? '-disabled' : ''}`
        }
        onClick={() => !isDisabled && onChange(!checked)}
      >
        <EuiIcon color={getCheckboxIconColor(isDisabled, checked)} type="check" size="l" />
      </div>
    </div>
  );
};

export default Checkbox;
