import { useEuiI18n } from '@elastic/eui';
import { ReactComponent as MinusIcon } from 'components/assets/images/RefundPage/minus.svg';
import { ReactComponent as PlusIcon } from 'components/assets/images/RefundPage/plus.svg';
import './Counter.scss';

interface CounterProps {
  isProduct: boolean;
  selectedItemsQuantity: number;
  generalItemsQuantity: number;
  refundedQuantity: number;
  isRefunded: boolean;
  decreaseAmount: () => void;
  increaseAmount: () => void;
}

const Counter = ({
  isProduct,
  selectedItemsQuantity,
  generalItemsQuantity,
  refundedQuantity,
  isRefunded,
  decreaseAmount,
  increaseAmount,
}: CounterProps) => {
  const [productsRefundText, customisationsRefundText] = useEuiI18n(
    [
      'subOrdersHistory.details.counter.refundProductsTitle',
      'subOrdersHistory.details.counter.refundCustomisationsTitle',
    ],
    [],
  );

  return (
    <div className={`Sidebar__counterContainer ${isProduct ? 'Sidebar__counterContainer__product' : ''}`}>
      <h2 className="Sidebar__counterTitle">{isProduct ? productsRefundText : customisationsRefundText}</h2>
      <div className="Sidebar__counter__body">
        <MinusIcon
          data-testid="Sidebar__counterMinus"
          className={
            selectedItemsQuantity > 1 && !isRefunded ? 'Sidebar__counterMinus--active' : 'Sidebar__counterMinus'
          }
          onClick={() => selectedItemsQuantity > 1 && !isRefunded && decreaseAmount()}
        />
        <input className="Sidebar__counter" type="text" value={selectedItemsQuantity} />
        <PlusIcon
          data-testid="Sidebar__counterPlus"
          className={
            generalItemsQuantity > selectedItemsQuantity &&
            !isRefunded &&
            generalItemsQuantity !== refundedQuantity + selectedItemsQuantity
              ? 'Sidebar__counterPlus--active'
              : 'Sidebar__counterPlus'
          }
          onClick={() =>
            generalItemsQuantity > selectedItemsQuantity &&
            !isRefunded &&
            generalItemsQuantity !== refundedQuantity + selectedItemsQuantity &&
            increaseAmount()
          }
        />
      </div>
    </div>
  );
};

export default Counter;
