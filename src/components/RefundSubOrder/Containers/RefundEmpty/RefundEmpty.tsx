import { useHistory } from 'react-router-dom';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import { useEuiI18n } from '@elastic/eui';
import './RefundEmpty.scss';

export default function RefundEmpty() {
  const history = useHistory();
  const [refundTitleText, cancelButtonText, refundButtonText] = useEuiI18n(
    ['refundEmpty.title', 'refundEmpty.cancelButton', 'refundEmpty.refundButton'],
    [],
  );

  return (
    <div className="RefundEmpty__container">
      <div className="RefundEmpty__body">
        <h1 className="RefundEmpty__title">{refundTitleText}</h1>
      </div>
      <div className="RefundEmpty__buttonsContainer">
        <CustomButton color="red" size="xl" onClick={() => history.push('/history')} text={cancelButtonText} />
        <CustomButton
          color="green"
          size="xl"
          disabled={true}
          typoColor="gray"
          text={refundButtonText}
          onClick={() => null}
        />
      </div>
    </div>
  );
}
