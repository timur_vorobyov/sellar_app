import '@testing-library/jest-dom';
import { screen, cleanup, fireEvent } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { RouteId, routes } from 'config/routes';
import RefundEmpty from './RefundEmpty';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<RefundEmpty> component', () => {
  it('should render RefundTotal component with data', () => {
    customI18nRender(<RefundEmpty />, { locale: Locale.CHINESE });

    const title = screen.getByText('Refund 退款');
    const cancelBtn = screen.getByText('Cancel 取消');
    const refundBtn = screen.getByText('Refund item(s) 退食品');

    expect(title).toBeInTheDocument();
    expect(cancelBtn).toBeInTheDocument();
    expect(refundBtn).toBeInTheDocument();
  });

  it('should render RefundTotal component and move history page after cancel click', () => {
    customI18nRender(
      <BrowserRouter>
        <RefundEmpty />
      </BrowserRouter>,
      { locale: Locale.CHINESE },
    );

    const title = screen.getByText('Refund 退款');
    const cancelBtn = screen.getByText('Cancel 取消');
    expect(title).toBeInTheDocument();
    fireEvent.click(cancelBtn);
    expect(window.location.href).toBe('http://localhost' + routes[RouteId.History].path);
  });
});
