import { useHistory } from 'react-router-dom';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import { useEuiI18n } from '@elastic/eui';
import './RefundTotal.scss';
import Typography from 'components/ui-kit/Typography';
import { BoxTypography } from '../../../ui-kit/Typography';

interface RefundTotalProps {
  refundTotal: string;
  refundedPrice: string;
  returnedAmountFee: string;
  onClick: () => void;
  itemsToRefund: number;
}

export default function RefundTotal({
  onClick,
  itemsToRefund,
  refundTotal,
  refundedPrice,
  returnedAmountFee,
}: RefundTotalProps) {
  const history = useHistory();

  const [refundTitleText, amountRefundedText, feesRefundedText, totalAmountToRefundText, cancelButtonText] = useEuiI18n(
    [
      'subOrdersHistory.details.refund',
      'subOrdersHistory.details.amountRefunded',
      'subOrdersHistory.details.feesRefunded',
      'subOrdersHistory.details.totalAmountToRefund',
      'subOrdersHistory.details.cancelButton',
    ],
    [],
  );

  const refundButtonText = useEuiI18n('subOrdersHistory.details.refundButton', '', {
    count: itemsToRefund,
    refundTotal,
  });

  return (
    <div className="RefundTotal__container">
      <BoxTypography
        typographyProps={{ variant: 'h1', className: 'RefundTotal__title' }}
        boxProps={{ pt: 8, pb: 8, pl: 20, pr: 20 }}
      >
        {refundTitleText}
      </BoxTypography>
      <div className="RefundTotal__body">
        <div className="RefundTotal__body__item_container">
          <Typography variant="span">{amountRefundedText}</Typography>
          <Typography variant="span">${refundedPrice}</Typography>
        </div>
        <div className="RefundTotal__body__item_container">
          <Typography variant="span">{feesRefundedText}</Typography>
          <Typography variant="span">${returnedAmountFee}</Typography>
        </div>
        <div className="RefundTotal__body__total_container">
          <Typography variant="span">{totalAmountToRefundText}</Typography>
          <Typography variant="span">${refundTotal}</Typography>
        </div>
      </div>
      <div className="RefundTotal__buttonsContainer">
        <CustomButton color="red" size="xl" onClick={() => history.push('/history')} text={cancelButtonText} />
        <CustomButton color="green" size="xl" onClick={onClick} text={refundButtonText} />
      </div>
    </div>
  );
}
