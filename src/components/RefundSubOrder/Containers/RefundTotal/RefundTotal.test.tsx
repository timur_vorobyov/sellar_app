import '@testing-library/jest-dom';
import { cleanup, screen } from '@testing-library/react';
import RefundTotal from './RefundTotal';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<RefundTotal> component', () => {
  it('should render RefundTotal component with data', () => {
    customI18nRender(
      <RefundTotal
        refundedPrice="3"
        returnedAmountFee="0.60"
        onClick={() => null}
        refundTotal={'3.60'}
        itemsToRefund={1}
      />,
      { locale: Locale.CHINESE },
    );

    const title = screen.getByText('Refund 退款');
    const total = screen.getByText('Total Amount to Refund 退款总额');
    const amount = screen.getAllByText('$3.60');
    const button = screen.getByText('Refund 1 item(s) 退1个食品 - $3.60');

    expect(title).toBeInTheDocument();
    expect(total).toBeInTheDocument();
    expect(button).toBeInTheDocument();
    expect(amount[0]).toBeInTheDocument();
  });
});
