import { EuiIcon } from '@elastic/eui';
import './RefundReasonButton.scss';

interface RefundReasonButtonProps {
  label: string;
  checked: boolean;
  onClick: () => void;
}

export default function RefundReasonButton({ label, checked, onClick }: RefundReasonButtonProps) {
  return (
    <div
      className={checked ? 'RefundReasonButton__container-checked' : 'RefundReasonButton__container'}
      onClick={onClick}
    >
      <label className="RefundReasonButton__label">{label}</label>
      <div className={checked ? 'RefundReasonButton__checkbox-checked' : 'RefundReasonButton__checkbox'}>
        <EuiIcon color="#FFFFFF" type="check" size="l" />
      </div>
    </div>
  );
}
