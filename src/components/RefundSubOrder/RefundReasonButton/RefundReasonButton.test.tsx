import '@testing-library/jest-dom';
import { render, screen, cleanup } from '@testing-library/react';
import RefundReasonButton from './RefundReasonButton';

afterEach(cleanup);

describe('<RefundReasonButton> component', () => {
  it('should render RefundReasonButton component unchecked', () => {
    const { container } = render(<RefundReasonButton label={'refund'} checked={false} onClick={() => null} />);
    const label = screen.getByText('refund');
    const btnContainer = container.getElementsByClassName('RefundReasonButton__container')[0];
    expect(label).toBeInTheDocument();
    expect(btnContainer).toHaveClass('RefundReasonButton__container');
  });

  it('should render RefundReasonButton component checked', () => {
    const { container } = render(<RefundReasonButton label={'refund'} checked={true} onClick={() => null} />);
    const btnContainer = container.getElementsByClassName('RefundReasonButton__container-checked')[0];
    expect(btnContainer).toHaveClass('RefundReasonButton__container-checked');
  });
});
