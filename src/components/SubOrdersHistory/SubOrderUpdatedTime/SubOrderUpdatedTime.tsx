import Box from 'components/ui-kit/Box';
import Typography from 'components/ui-kit/Typography';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import { SubOrderHistory, SubOrderState } from 'types/models/subOrder';
import { convertToSingaporeTime } from 'utils/datetime';
import SubOrderStatus from '../SubOrderStatus/SubOrderStatus';
import SubOrderStatusMobile from '../SubOrderStatusMobile/SubOrderStatusMobile';
import './SubOrderUpdatedTime.scss';

interface SubOrderUpdatedTimeProps {
  subOrderHistory: SubOrderHistory;
}

const SubOrderUpdatedTime = ({ subOrderHistory }: SubOrderUpdatedTimeProps) => {
  const { isMobile, isTablet } = useDeviceContext();

  switch (subOrderHistory.state) {
    case SubOrderState.ReadyForCollection: {
      return (
        <Box addClass="SubOrderStatus__updatedTime" mt={6}>
          <Typography variant="span">
            {subOrderHistory.readyForCollectionTime
              ? convertToSingaporeTime(subOrderHistory.readyForCollectionTime, 'hh:mm A')
              : '-----'}{' '}
            |{' '}
          </Typography>
          {isMobile && <SubOrderStatusMobile status={subOrderHistory.state} />}
          {isTablet && <SubOrderStatus status={subOrderHistory.state} />}
        </Box>
      );
    }
    case SubOrderState.Rejected: {
      return (
        <Box addClass="SubOrderStatus__updatedTime" mt={6}>
          <Typography variant="span">
            {subOrderHistory.cancelledTime ? convertToSingaporeTime(subOrderHistory.cancelledTime, 'hh:mm A') : '-----'}{' '}
            |{' '}
          </Typography>
          {isMobile && <SubOrderStatusMobile status={subOrderHistory.state} />}
          {isTablet && <SubOrderStatus status={subOrderHistory.state} />}
        </Box>
      );
    }
    case SubOrderState.Cancelled: {
      return (
        <Box addClass="SubOrderStatus__updatedTime" mt={6}>
          <Typography variant="span">
            {subOrderHistory.cancelledTime ? convertToSingaporeTime(subOrderHistory.cancelledTime, 'hh:mm A') : '-----'}{' '}
            |{' '}
          </Typography>
          {isMobile && <SubOrderStatusMobile status={subOrderHistory.state} />}
          {isTablet && <SubOrderStatus status={subOrderHistory.state} />}
        </Box>
      );
    }
    case SubOrderState.Collected: {
      return (
        <Box addClass="SubOrderStatus__updatedTime" mt={6}>
          <Typography variant="span">
            {subOrderHistory.collectionTime
              ? convertToSingaporeTime(subOrderHistory.collectionTime, 'hh:mm A')
              : '-----'}{' '}
            |{' '}
          </Typography>
          {isMobile && <SubOrderStatusMobile status={subOrderHistory.state} />}
          {isTablet && <SubOrderStatus status={subOrderHistory.state} />}
        </Box>
      );
    }
    default:
      return (
        <Box>
          <Typography variant="span">{subOrderHistory.state}</Typography>
        </Box>
      );
  }
};

export default SubOrderUpdatedTime;
