import '@testing-library/jest-dom';
import { render, screen, cleanup } from '@testing-library/react';
import { SubOrderHistory } from 'types/models/subOrder';
import { subOrderHistoryList } from '__mocks__/mocks';
import { SUB_ORDERS_STATUSES_OPTIONS } from '../consts';
import moment from 'moment-timezone';
import { SubOrdersHistoryFilter } from 'types/pages';
import SubOrdersHistoryList from './SubOrdersHistoryList';

afterEach(cleanup);

const platformsOptionsState = [
  {
    value: 1,
    text: 'FAIRPRICE',
  },
  {
    value: 2,
    text: 'DELIVEROO',
  },
];

let selectedFilters = {
  subOrderStatuses: [],
  date: moment(),
  platformsIds: [],
  searchTerm: '',
} as SubOrdersHistoryFilter;

const setFilters = (filters: SubOrdersHistoryFilter) => {
  selectedFilters = filters;
};

const setSelectedSubOrderHistory = (subOrderHistory: null | SubOrderHistory) => {
  return subOrderHistory;
};

describe('<OrderHistoryList> component', () => {
  it('should render component with data', () => {
    render(
      <SubOrdersHistoryList
        refundSum={0}
        netTotalSum={150}
        grossTotalSum={35}
        setSelectedSubOrderHistory={(subOrderHistory: SubOrderHistory) => setSelectedSubOrderHistory(subOrderHistory)}
        platformsOptions={platformsOptionsState}
        subOrdersStatusesOptions={SUB_ORDERS_STATUSES_OPTIONS}
        filters={selectedFilters}
        setFilters={setFilters}
        subOrdersHistoryState={subOrderHistoryList}
        setShowFilterMobile={jest.fn()}
      />,
    );

    expect(screen.getByText('3229S')).toBeInTheDocument();
    expect(screen.getByText('7822S')).toBeInTheDocument();
  });
});
