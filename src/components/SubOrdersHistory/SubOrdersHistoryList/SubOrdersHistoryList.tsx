import { SubOrderHistory } from 'types/models/subOrder';
import { SubOrdersHistoryFilter } from 'types/pages';
import Filters from '../Filters/Filters';
import Table from '../Table/Table';
import './SubOrdersHistoryList.scss';
import { Option } from 'types/generics';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import TableMobile from '../TableMobile/TableMobile';
import FiltersMobile from '../FiltersMobile/FiltersMobile';

interface SubOrdersHistoryListProps {
  setSelectedSubOrderHistory: (subOrderHistory: SubOrderHistory) => void;
  subOrdersHistoryState: SubOrderHistory[];
  filters: SubOrdersHistoryFilter;
  setFilters: (filters: SubOrdersHistoryFilter) => void;
  subOrdersStatusesOptions: Option[];
  platformsOptions: Option[];
  grossTotalSum: number;
  netTotalSum: number;
  refundSum: number;
  setShowFilterMobile: (value: boolean) => void;
}
const SubOrdersHistoryList = ({
  setSelectedSubOrderHistory,
  setFilters,
  subOrdersStatusesOptions,
  platformsOptions,
  filters,
  subOrdersHistoryState,
  grossTotalSum,
  netTotalSum,
  refundSum,
  setShowFilterMobile,
}: SubOrdersHistoryListProps) => {
  const { isMobile, isTablet } = useDeviceContext();

  return (
    <div className="MainContent__container">
      {isMobile && (
        <>
          <FiltersMobile
            filters={filters}
            setFilters={setFilters}
            onShowFilterMobile={setShowFilterMobile}
            totalFilters={platformsOptions.length + subOrdersStatusesOptions.length}
          />
          <TableMobile
            refundSum={refundSum}
            netTotalSum={netTotalSum}
            grossTotalSum={grossTotalSum}
            subOrdersHistory={subOrdersHistoryState}
            setSelectedSubOrderHistory={(subOrderHistory: SubOrderHistory) =>
              setSelectedSubOrderHistory(subOrderHistory)
            }
          />
        </>
      )}
      {isTablet && (
        <>
          <Filters
            platformsOptions={platformsOptions}
            subOrdersStatusesOptions={subOrdersStatusesOptions}
            filters={filters}
            setFilters={setFilters}
            netTotalSum={netTotalSum}
            refundSum={refundSum}
            grossTotalSum={grossTotalSum}
          />
          <Table
            subOrdersHistory={subOrdersHistoryState}
            setSelectedSubOrderHistory={(subOrderHistory: SubOrderHistory) =>
              setSelectedSubOrderHistory(subOrderHistory)
            }
          />
        </>
      )}
    </div>
  );
};

export default SubOrdersHistoryList;
