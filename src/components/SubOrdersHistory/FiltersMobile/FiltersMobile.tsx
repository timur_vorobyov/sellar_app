import { EuiFieldSearch, useEuiI18n } from '@elastic/eui';
import './FiltersMobile.scss';
import { SubOrdersHistoryFilter } from 'types/pages';
import FilterIcon from 'components/assets/images/MenuPage/filterIcon.svg';
import { useEffect, useState } from 'react';
import DatePickerMobile from './DatePickerMobile/DatePickerMobile';

interface FiltersMobileProps {
  filters: SubOrdersHistoryFilter;
  setFilters: (filters: SubOrdersHistoryFilter) => void;
  onShowFilterMobile: (value: boolean) => void;
  totalFilters: number;
}

const FiltersMobile = ({ filters, setFilters, onShowFilterMobile, totalFilters }: FiltersMobileProps) => {
  const searchOrderNumberText = useEuiI18n('subOrdersHistory.filters.searchOrderNumber', '');

  const [showFilterMobile, setShowFilterMobile] = useState(false);
  const [filterItemsNumber, setFilterItemsNumber] = useState(0);

  useEffect(() => {
    onShowFilterMobile(showFilterMobile);
    const appliedFilters = filters.platformsIds.length + filters.subOrderStatuses.length;
    setFilterItemsNumber(appliedFilters === totalFilters ? 0 : appliedFilters);
  }, [showFilterMobile]);

  return (
    <div className="SubOrdersHistory__FiltersMobile__container">
      <div className="SubOrdersHistory__FiltersMobile__date">
        <DatePickerMobile filters={filters} setFilters={(newFilters) => setFilters(newFilters)} />
      </div>
      <div className="SubOrdersHistory__FiltersMobile__search">
        <EuiFieldSearch
          placeholder={searchOrderNumberText}
          value={filters.searchTerm}
          onChange={(event) =>
            setFilters({
              ...filters,
              searchTerm: event?.target.value.toString().toUpperCase(),
            })
          }
          isClearable={true}
        />
        <div className="SubOrdersHistory__FiltersMobile__button" onClick={() => setShowFilterMobile(true)}>
          {!!filterItemsNumber && <div className="SubOrdersHistory__FiltersMobile__number">{filterItemsNumber}</div>}
          <img src={FilterIcon} width="24" />
        </div>
      </div>
    </div>
  );
};

export default FiltersMobile;
