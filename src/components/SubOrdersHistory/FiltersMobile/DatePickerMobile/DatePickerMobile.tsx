import { useState } from 'react';
import { EuiDatePicker, useEuiI18n } from '@elastic/eui';
import moment from 'moment-timezone';
import CalendarIcon from 'components/assets/images/HistoryPage/calendarIcon.svg';
import { SubOrdersHistoryFilter } from 'types/pages';
import './DatePickerMobile.scss';
import { BoxTypography } from 'components/ui-kit/Typography';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';

interface DatePickerMobileProps {
  filters: SubOrdersHistoryFilter;
  setFilters: (filters: SubOrdersHistoryFilter) => void;
}
const DatePickerMobile = ({ filters, setFilters }: DatePickerMobileProps) => {
  const [date, setDate] = useState(filters.date);
  const [isPopoverOpen, setIsPopoverOpen] = useState(false);
  const [dateFilter, setDateFilter] = useState(filters.date);

  const onApply = () => setFilters({ ...filters, date: dateFilter || date });

  const onDateChange = (selectedDate: moment.Moment) => {
    setDateFilter(selectedDate ? selectedDate : moment());
  };

  const applyDateFilter = () => {
    setDate(dateFilter);
    onApply();
    setIsPopoverOpen(false);
  };

  const [searchOrderNumberText, buttonCancelText, buttonApplyText] = useEuiI18n(
    ['subOrdersHistory.filters.selectDate', 'subOrdersHistory.datePicker.cancel', 'subOrdersHistory.datePicker.apply'],
    [],
  );

  return (
    <>
      <div className="DatePickerMobile__button" onClick={() => setIsPopoverOpen(!isPopoverOpen)}>
        <div className="DatePickerMobile__icon">
          <img src={CalendarIcon} width="24" height="24" />
        </div>
        <div>{`${date.format('Do MMMM YYYY')}`}</div>
      </div>
      {isPopoverOpen && (
        <div className="DatePickerMobile__container">
          <div className="DatePickerMobile__content" onClick={(event) => event.stopPropagation()}>
            <BoxTypography
              typographyProps={{ variant: 'span', fontWeight: 'bold' }}
              boxProps={{ pr: 12, pb: 12, pl: 12 }}
            >
              {searchOrderNumberText}
            </BoxTypography>
            <EuiDatePicker inline shadow={false} selected={date} locale="zh-cn" onChange={onDateChange} />
            <div className="DatePickerMobile__footer">
              <CustomButton
                size="sm"
                color="transparent"
                padding="no"
                onClick={() => {
                  setDateFilter(filters.date);
                  setIsPopoverOpen(false);
                }}
                text={buttonCancelText}
                typoColor="blue"
              />
              <CustomButton
                size="sm"
                color="light"
                padding="no"
                onClick={applyDateFilter}
                text={buttonApplyText}
                typoColor="blue"
              />
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default DatePickerMobile;
