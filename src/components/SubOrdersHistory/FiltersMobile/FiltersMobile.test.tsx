import '@testing-library/jest-dom';
import { screen, cleanup, fireEvent } from '@testing-library/react';
import FiltersMobile from './FiltersMobile';
import moment from 'moment-timezone';
import { SubOrdersHistoryFilter } from 'types/pages';
import { customI18nRender } from 'utils/testing/customRender';
import { filters } from '__mocks__/mocks';

afterEach(cleanup);

let selectedFilters = { ...filters };

const setFilters = (filters: SubOrdersHistoryFilter) => {
  selectedFilters = filters;
};

describe('<FiltersMobile> component', () => {
  it('should open popover after business date button has been pushed', () => {
    customI18nRender(
      <FiltersMobile
        filters={selectedFilters}
        setFilters={setFilters}
        onShowFilterMobile={() => null}
        totalFilters={5}
      />,
    );

    const date = moment();
    const button = screen.getByText(`${date.format('Do MMMM YYYY')}`);
    fireEvent.click(button);
  });
});
