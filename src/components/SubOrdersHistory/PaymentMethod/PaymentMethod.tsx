import { BoxTypography } from 'components/ui-kit/Typography';
import { PaymentMethod as PaymentMethodConst } from 'constants/payment';
import useCommonTranslations from 'utils/hooks/user-common-translations';

interface PaymentMehodProps {
  paymentMehod: string;
}

const PaymentMethod = ({ paymentMehod }: PaymentMehodProps) => {
  const { paymentMethods } = useCommonTranslations();

  return (
    <BoxTypography typographyProps={{ variant: 'span' }}>
      {paymentMethods[paymentMehod as PaymentMethodConst]}
    </BoxTypography>
  );
};

export default PaymentMethod;
