import '@testing-library/jest-dom';
import { cleanup, screen } from '@testing-library/react';
import PaymentMethod from './PaymentMethod';
import { PaymentMethod as PaymentMethodConst, PaymentMethodTranslations } from 'constants/payment';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<PaymentMethod> component', () => {
  it('should show card sub order payment method', () => {
    customI18nRender(<PaymentMethod paymentMehod={PaymentMethodConst.Card} />, { locale: Locale.CHINESE });

    expect(screen.getByText(PaymentMethodTranslations.CARD)).toBeInTheDocument();
  });

  it('should show linkpoints sub order payment method', () => {
    customI18nRender(<PaymentMethod paymentMehod={PaymentMethodConst.LinkPoints} />, { locale: Locale.CHINESE });

    expect(screen.getByText(PaymentMethodTranslations.LINKPOINTS)).toBeInTheDocument();
  });
});
