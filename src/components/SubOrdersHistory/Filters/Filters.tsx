import { EuiFieldSearch, useEuiI18n } from '@elastic/eui';
import { SubOrdersHistoryFilter } from 'types/pages';
import { Filter } from 'components/Menu/Filter/Filter';
import { Option } from 'types/generics';
import DatePicker from './DatePicker/DatePicker';
import Statistics from '../Table/Statistics/Statistics';
import './Filters.scss';
import Typography from 'components/ui-kit/Typography';
import Box from 'components/ui-kit/Box';

interface FiltersProps {
  filters: SubOrdersHistoryFilter;
  setFilters: (filters: SubOrdersHistoryFilter) => void;
  subOrdersStatusesOptions: Option[];
  platformsOptions: Option[];
  grossTotalSum: number;
  netTotalSum: number;
  refundSum: number;
}

const Filters = ({
  platformsOptions,
  subOrdersStatusesOptions,
  filters,
  setFilters,
  refundSum,
  grossTotalSum,
  netTotalSum,
}: FiltersProps) => {
  const [businessDateText, searchOrderNumberText, platformText, orderStatusText] = useEuiI18n(
    [
      'subOrdersHistory.filters.businessDate',
      'subOrdersHistory.filters.searchOrderNumber',
      'subOrdersHistory.filters.platform',
      'subOrdersHistory.filters.subOrderStatus',
    ],
    [],
  );

  return (
    <div className="SubOrdersHistories__Filters__container">
      <Box pt={16} pb={16} pl={20} pr={20} addClass="bg-white">
        <div className="Filters__first-filter-raw">
          <div className="Filters__filter-raw">
            <Typography variant="span" fontWeight="bold">
              {businessDateText}:
            </Typography>
            <DatePicker filters={filters} setFilters={(filters) => setFilters(filters)} />
          </div>
          <Box>
            <EuiFieldSearch
              placeholder={searchOrderNumberText}
              value={filters.searchTerm}
              onChange={(event) =>
                setFilters({
                  ...filters,
                  searchTerm: event?.target.value.toString().toUpperCase(),
                })
              }
              isClearable={true}
            />
          </Box>
        </div>
        <Box mt={16}>
          <Statistics refundSum={refundSum} netTotalSum={netTotalSum} grossTotalSum={grossTotalSum} />
        </Box>
      </Box>

      <Box pt={16} pb={16} pl={20} pr={20} addClass="Filters__second-filter-raw">
        <Filter
          label={platformText}
          options={platformsOptions}
          selected={filters.platformsIds}
          onApply={(selected: number[]) => setFilters({ ...filters, platformsIds: selected })}
        />
        <Filter
          label={orderStatusText}
          options={subOrdersStatusesOptions}
          selected={filters.subOrderStatuses}
          onApply={(selected: number[]) => setFilters({ ...filters, subOrderStatuses: selected })}
        />
      </Box>
    </div>
  );
};

export default Filters;
