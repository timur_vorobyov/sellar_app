import { EuiButton, EuiDatePicker, EuiFormRow, EuiPopover, EuiPopoverFooter, useEuiI18n } from '@elastic/eui';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import moment from 'moment-timezone';
import { useState } from 'react';
import { SubOrdersHistoryFilter } from 'types/pages';
import './DatePicker.scss';

interface DatePickerProps {
  filters: SubOrdersHistoryFilter;
  setFilters: (filters: SubOrdersHistoryFilter) => void;
}
const DatePicker = ({ filters, setFilters }: DatePickerProps) => {
  const [date, setDate] = useState(filters.date);
  const [dateFilter, setDateFilter] = useState(filters.date);
  const [isPopoverOpen, setIsPopoverOpen] = useState(false);

  const onButtonClick = () => setIsPopoverOpen((isPopoverOpen) => !isPopoverOpen);
  const closePopover = () => {
    setDateFilter(filters.date);
    setIsPopoverOpen(false);
  };
  const onApply = () => setFilters({ ...filters, date: dateFilter || date });

  const [buttonCancelText, buttonApplyText] = useEuiI18n(
    ['subOrdersHistory.datePicker.cancel', 'subOrdersHistory.datePicker.apply'],
    [],
  );

  const button = (
    <EuiButton
      className="SubOrdersHistory__datePickerPopover__button"
      iconType="arrowDown"
      iconSide="right"
      onClick={onButtonClick}
    >
      {`${date.format('Do MMMM YYYY')}`}
    </EuiButton>
  );

  const onDateChange = (selectedDate: moment.Moment) => {
    setDateFilter(selectedDate ? selectedDate : moment().tz('Asia/Singapore'));
  };

  const applyDateFilter = () => {
    setDate(dateFilter);
    onApply();
    setIsPopoverOpen(false);
  };

  return (
    <EuiPopover
      className="SubOrdersHistory__datePickerPopover"
      ownFocus={false}
      button={button}
      isOpen={isPopoverOpen}
      closePopover={closePopover}
    >
      <EuiFormRow hasChildLabel={false}>
        <div>
          <EuiDatePicker inline shadow={false} selected={dateFilter} onChange={onDateChange} />
          <EuiPopoverFooter className="SubOrdersHistory__datePickerPopover__footer">
            <CustomButton
              size="sm"
              color="transparent"
              padding="no"
              onClick={() => {
                setDateFilter(filters.date);
                setIsPopoverOpen(false);
              }}
              text={buttonCancelText}
              typoColor="blue"
            />
            <CustomButton
              size="sm"
              color="light"
              padding="no"
              onClick={applyDateFilter}
              text={buttonApplyText}
              typoColor="blue"
            />
          </EuiPopoverFooter>
        </div>
      </EuiFormRow>
    </EuiPopover>
  );
};

export default DatePicker;
