import '@testing-library/jest-dom';
import { screen, cleanup, fireEvent } from '@testing-library/react';
import Filters from './Filters';
import moment from 'moment-timezone';
import { SubOrdersHistoryFilter } from 'types/pages';
import { SUB_ORDERS_STATUSES_OPTIONS } from '../consts';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';
import { filters } from '__mocks__/mocks';

afterEach(cleanup);

let selectedFilters = { ...filters };

const setFilters = (filters: SubOrdersHistoryFilter) => {
  selectedFilters = filters;
};

const platformsOptions = [
  {
    value: 1,
    text: 'FAIRPRICE',
  },
  {
    value: 2,
    text: 'DELIVEROO',
  },
];

describe('<Filters> component', () => {
  it('should open popover after business date button has been pushed', () => {
    customI18nRender(
      <Filters
        filters={selectedFilters}
        setFilters={setFilters}
        subOrdersStatusesOptions={SUB_ORDERS_STATUSES_OPTIONS}
        platformsOptions={platformsOptions}
        grossTotalSum={0}
        netTotalSum={0}
        refundSum={0}
      />,
    );

    const date = moment();
    const button = screen.getByText(`${date.format('Do MMMM YYYY')}`);
    fireEvent.click(button);

    expect(screen.getByText('Apply')).toBeTruthy();
    expect(screen.getByText('Cancel')).toBeTruthy();
  });

  it('should update date value after it was chosen inside opened calendar', () => {
    customI18nRender(
      <Filters
        filters={selectedFilters}
        setFilters={setFilters}
        subOrdersStatusesOptions={SUB_ORDERS_STATUSES_OPTIONS}
        platformsOptions={platformsOptions}
        grossTotalSum={0}
        netTotalSum={0}
        refundSum={0}
      />,
    );

    const date = moment();
    const button = screen.getByText(`${date.format('Do MMMM YYYY')}`);
    fireEvent.click(button);
    const currentDay = date.format('D');
    const updatedDate = date.add(Number(currentDay) > 20 ? -2 : 2, 'days');
    const day = updatedDate.format('D');
    const datePickerButton = screen.getAllByText(day).slice(-1)[0];
    fireEvent.click(datePickerButton, { date: updatedDate });
    const applyButton = screen.getByText('Apply');
    fireEvent.click(applyButton);

    expect(selectedFilters.date.format('Do MMMM YYYY')).toBe(updatedDate.format('Do MMMM YYYY'));
  });

  it('should update search value after letters were written in input', () => {
    customI18nRender(
      <Filters
        filters={selectedFilters}
        setFilters={setFilters}
        subOrdersStatusesOptions={SUB_ORDERS_STATUSES_OPTIONS}
        platformsOptions={platformsOptions}
        grossTotalSum={0}
        netTotalSum={0}
        refundSum={0}
      />,
      { locale: Locale.CHINESE },
    );

    const input = screen.getByPlaceholderText('Search order no. 搜寻订单号');
    fireEvent.change(input, { target: { value: 'l9Ots' } });
    expect(selectedFilters.searchTerm).toBe('L9OTS');
  });
});
