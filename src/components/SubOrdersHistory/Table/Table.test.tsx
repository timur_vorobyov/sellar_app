import '@testing-library/jest-dom';
import { screen, cleanup } from '@testing-library/react';
import { SubOrderHistory } from 'types/models/subOrder';
import Table from './Table';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';
import { subOrderHistoryList } from '__mocks__/mocks';

afterEach(cleanup);

const setSelectedSubOrderHistory = (subOrderHistory: null | SubOrderHistory) => {
  return subOrderHistory;
};

describe('<Table> component', () => {
  it('should render component with data', () => {
    customI18nRender(
      <Table
        subOrdersHistory={subOrderHistoryList}
        setSelectedSubOrderHistory={(subOrderHistory: SubOrderHistory) => setSelectedSubOrderHistory(subOrderHistory)}
      />,
      { locale: Locale.CHINESE },
    );

    expect(screen.getAllByText('No. of Items 食品数量')[0]).toBeInTheDocument();
    expect(screen.getAllByText('S/N 编号')[0]).toBeInTheDocument();
    expect(screen.getAllByText('Platform & Order No. 平台及订单号')[0]).toBeInTheDocument();
    expect(screen.getAllByText('Order Status 订单状态')[0]).toBeInTheDocument();
    expect(screen.getAllByText('Total Amount 总额')[0]).toBeInTheDocument();
    expect(screen.getByText(subOrderHistoryList[0].orderId)).toBeInTheDocument();
    expect(screen.getByText(subOrderHistoryList[1].orderId)).toBeInTheDocument();
  });
});
