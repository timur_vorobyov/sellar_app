import { EuiBasicTable, CriteriaWithPagination, useEuiI18n } from '@elastic/eui';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import { useState } from 'react';
import { SubOrderHistory, SubOrderState } from 'types/models/subOrder';
import SubOrderStatus from '../SubOrderStatus/SubOrderStatus';
import { printAcceptedReceipt, printCancelledReceipt } from 'utils/receipt';
import { sliceTwoMoreDecimals } from 'utils/formater';
import { receiptTypes } from 'constants/receiptType';
import { useTreatments } from '@splitsoftware/splitio-react';
import { FeatureFlags } from 'constants/featureFlags';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import './Table.scss';
import Typography, { BoxTypography } from 'components/ui-kit/Typography';
import Box from 'components/ui-kit/Box';
import { useLanguageContext } from 'contexts/languageContextProvider/context';

interface TableProps {
  subOrdersHistory: SubOrderHistory[];
  setSelectedSubOrderHistory: (subOrderHistory: SubOrderHistory) => void;
}

const Table = ({ subOrdersHistory, setSelectedSubOrderHistory }: TableProps) => {
  const [pageIndex, setPageIndex] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const { getLocale } = useLanguageContext();
  const feat = useTreatments([FeatureFlags.QR_CODE]);
  const { isTablet } = useDeviceContext();

  const onChange = (criteria: CriteriaWithPagination<SubOrderHistory>) => {
    const { index: pageIndex, size: pageSize } = criteria?.page || {};
    setPageIndex(pageIndex);
    setPageSize(pageSize);
  };

  const [
    serialNumberColumnText,
    platformNumberColumnText,
    numberOfItemsColumnText,
    totalAmountColumnText,
    orderStatusColumnText,
    reprintColumnText,
    noItemsFoundText,
  ] = useEuiI18n(
    [
      'subOrdersHistory.table.serialNumberColumn',
      'subOrdersHistory.table.platformOrderNumberColumn',
      'subOrdersHistory.table.numberOfItemsColumn',
      'subOrdersHistory.table.totalAmountColumn',
      'subOrdersHistory.table.orderStatusColumn',
      'subOrdersHistory.table.reprintColumn',
      'menu.table.noItemsFound',
    ],
    [],
  );

  const fromItem = pageIndex ? pageIndex * pageSize : 0;
  const toItem = pageSize * (pageIndex + 1);
  const pageOfItems = subOrdersHistory.slice(fromItem, toItem);
  const totalItemCount = subOrdersHistory.length;

  const pagination = {
    pageIndex,
    pageSize,
    totalItemCount,
    pageSizeOptions: [5, 10, 15],
  };
  const columns = [
    {
      field: 'id',
      name: (
        <Typography fontWeight="bold" variant="span">
          {serialNumberColumnText}
        </Typography>
      ),
      width: '10%',
      render: (id: number) => <Typography variant="span">{id}</Typography>,
    },
    {
      field: 'logo',
      name: (
        <Typography fontWeight="bold" variant="span">
          {platformNumberColumnText}
        </Typography>
      ),
      width: '20%',
      render: (logo: string, record: SubOrderHistory) => (
        <Box addClass="SubOrdersHistoryTable__orderNumber">
          <img src={logo} />
          <Typography variant="span"> {record.orderId}</Typography>
        </Box>
      ),
    },
    {
      field: 'numberOfProducts',
      name: (
        <Typography fontWeight="bold" variant="span">
          {numberOfItemsColumnText}
        </Typography>
      ),
      width: '15%',
      render: (numberOfProducts: number) => <Typography variant="span">{numberOfProducts}</Typography>,
    },
    {
      field: 'originNetSales',
      name: (
        <Typography fontWeight="bold" variant="span">
          {totalAmountColumnText}
        </Typography>
      ),
      width: '20%',
      render: (originNetSales: number) => (
        <Typography variant="span">{sliceTwoMoreDecimals(originNetSales)}</Typography>
      ),
    },
    {
      field: 'state',
      name: (
        <Typography fontWeight="bold" variant="span">
          {orderStatusColumnText}
        </Typography>
      ),
      width: '20%',
      render: (state: string, record: SubOrderHistory) => {
        if (record.order.refunds.length) {
          return <SubOrderStatus status={record.refundState} />;
        } else {
          return <SubOrderStatus status={state} />;
        }
      },
    },
    {
      name: '',
      width: '15%',
      render: (record: SubOrderHistory) => (
        <CustomButton
          onClick={() => {
            record.state === SubOrderState.Cancelled
              ? printCancelledReceipt(record, isTablet)
              : printAcceptedReceipt(
                  record,
                  getLocale(),
                  receiptTypes.TENNANT,
                  feat[FeatureFlags.QR_CODE].treatment,
                  isTablet,
                );
          }}
          text={reprintColumnText}
        />
      ),
    },
  ];

  const getRowProps = (subOrderHistory: SubOrderHistory) => {
    return {
      className: 'SubOrdersHistory__tableCell',
      onClick: () => setSelectedSubOrderHistory(subOrderHistory),
    };
  };

  const getCellProps = () => {
    return {
      className: 'SubOrdersHistory__tableCell',
      textOnly: true,
    };
  };

  return (
    <Box pl={20} pr={20} addClass="SubOrdersHistoryTable__container">
      <BoxTypography typographyProps={{ variant: 'span', style: { fontSize: 16 } }} boxProps={{ mt: 16, mb: 8 }}>
        {`Showing ${fromItem + 1} - ${toItem > totalItemCount ? totalItemCount : toItem} out of ${totalItemCount}`}
      </BoxTypography>
      <EuiBasicTable<SubOrderHistory>
        items={pageOfItems}
        rowHeader="firstName"
        columns={columns}
        rowProps={getRowProps}
        cellProps={getCellProps}
        onChange={onChange}
        pagination={pagination}
        tableLayout="auto"
        noItemsMessage={noItemsFoundText}
      />
    </Box>
  );
};

export default Table;
