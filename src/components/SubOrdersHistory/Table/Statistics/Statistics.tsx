import { useMemo } from 'react';
import { EuiFlexGrid, EuiFlexGroup, EuiFlexItem, useEuiI18n } from '@elastic/eui';
import { sliceTwoMoreDecimals, сonvertFromCentsToDollars } from 'utils/formater';
import './Statistics.scss';
import { BoxTypography } from 'components/ui-kit/Typography';

interface StatisticsProps {
  refundSum: number;
  grossTotalSum: number;
  netTotalSum: number;
}

const Statistics = ({ refundSum, grossTotalSum, netTotalSum }: StatisticsProps) => {
  const [refundsTotalText, netSalesText, excludeDiscountsText, grossSalesText] = useEuiI18n(
    [
      'subOrdersHistory.statistics.refundsTotal',
      'subOrdersHistory.statistics.netSales',
      'subOrdersHistory.statistics.excludeDiscounts',
      'subOrdersHistory.statistics.grossSales',
    ],
    [],
  );

  return useMemo(() => {
    return (
      <div className="SubOrdersHistoryTable__statisticsContainer">
        <EuiFlexGroup>
          <EuiFlexItem grow={6}>
            <EuiFlexGrid columns={1} direction="column">
              <EuiFlexItem>
                <BoxTypography typographyProps={{ variant: 'span', fontWeight: 'bold' }}>
                  {refundsTotalText}
                </BoxTypography>
              </EuiFlexItem>
              <EuiFlexItem>
                <BoxTypography typographyProps={{ variant: 'span', fontWeight: 'bold' }}>
                  {`$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(refundSum))}`}
                </BoxTypography>
              </EuiFlexItem>
            </EuiFlexGrid>
          </EuiFlexItem>
          <EuiFlexItem grow={6}>
            <EuiFlexGrid columns={1} direction="column">
              <EuiFlexItem>
                <BoxTypography typographyProps={{ variant: 'span', fontWeight: 'bold' }}>{netSalesText}</BoxTypography>
              </EuiFlexItem>
              <EuiFlexItem>
                <BoxTypography typographyProps={{ variant: 'span', fontWeight: 'bold' }}>
                  {`$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(netTotalSum))}`}
                </BoxTypography>
              </EuiFlexItem>
              <EuiFlexItem>
                <BoxTypography typographyProps={{ variant: 'span', style: { fontSize: 16 } }}>
                  {excludeDiscountsText}
                </BoxTypography>
              </EuiFlexItem>
            </EuiFlexGrid>
          </EuiFlexItem>
          <EuiFlexItem grow={6}>
            <EuiFlexGrid columns={1} direction="column">
              <EuiFlexItem>
                <BoxTypography typographyProps={{ variant: 'span', fontWeight: 'bold' }}>
                  {grossSalesText}
                </BoxTypography>
              </EuiFlexItem>
              <EuiFlexItem>
                <BoxTypography typographyProps={{ variant: 'span', fontWeight: 'bold' }}>
                  {`$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(grossTotalSum))}`}
                </BoxTypography>
              </EuiFlexItem>
            </EuiFlexGrid>
          </EuiFlexItem>
        </EuiFlexGroup>
      </div>
    );
  }, [grossTotalSum, netTotalSum, refundSum, refundsTotalText, netSalesText, excludeDiscountsText, grossSalesText]);
};

export default Statistics;
