import { useEuiI18n } from '@elastic/eui';
import { BoxTypography } from 'components/ui-kit/Typography';
import { SubOrderState } from 'types/models/subOrder';
import './SubOrderStatusMobile.scss';

interface SubOrderStatusMobileProps {
  status: string;
}

const SubOrderStatusMobile = ({ status }: SubOrderStatusMobileProps) => {
  const [
    statusRefundedText,
    statusPartialRefundText,
    statusReadyForCollectionText,
    statusRejectedText,
    statusCancelledText,
    statusCollectedText,
    statusPendingRefundText,
  ] = useEuiI18n(
    [
      'subOrdersHistory.status.refunded',
      'subOrdersHistory.status.partialRefund',
      'subOrdersHistory.status.readyForCollection',
      'subOrdersHistory.status.rejected',
      'subOrdersHistory.status.cancelled',
      'subOrdersHistory.status.collected',
      'subOrdersHistory.status.pendingRefund',
    ],
    [],
  );

  switch (status) {
    case SubOrderState.Refunded: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatusMobile gray' }}>
          {statusRefundedText}
        </BoxTypography>
      );
    }

    case SubOrderState.PartialRefund: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatusMobile gray' }}>
          {statusPartialRefundText}
        </BoxTypography>
      );
    }

    case SubOrderState.PendingRefund: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatusMobile gray' }}>
          {statusPendingRefundText}
        </BoxTypography>
      );
    }

    case SubOrderState.ReadyForCollection: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatusMobile green' }}>
          {statusReadyForCollectionText}
        </BoxTypography>
      );
    }

    case SubOrderState.Rejected: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatusMobile red' }}>
          {statusRejectedText}
        </BoxTypography>
      );
    }

    case SubOrderState.Cancelled: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatusMobile red' }}>
          {statusCancelledText}
        </BoxTypography>
      );
    }

    case SubOrderState.Collected: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatusMobile green' }}>
          {statusCollectedText}
        </BoxTypography>
      );
    }

    default:
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatusMobile gray' }}>
          {status}
        </BoxTypography>
      );
  }
};

export default SubOrderStatusMobile;
