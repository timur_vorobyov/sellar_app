import { SubOrderStateTranslations } from 'types/models/subOrder';

export const SUB_ORDERS_STATUSES_OPTIONS = [
  //this part of code should be modified when filter comonent will be adapted
  { value: 1, text: SubOrderStateTranslations.READY_FOR_COLLECTION },
  { value: 2, text: SubOrderStateTranslations.REJECTED },
  { value: 3, text: SubOrderStateTranslations.CANCELLED },
  { value: 4, text: SubOrderStateTranslations.COLLECTED },
];
