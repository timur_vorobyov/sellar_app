import { EuiFlexGroup, EuiFlexItem, useEuiI18n } from '@elastic/eui';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { SubOrderData, SubOrderHistory, SubOrderState } from 'types/models/subOrder';
import { SubOrderHistoryProduct, SubOrderHistoryProductCustomisation } from 'types/models/product';
import { printAcceptedReceipt, printCancelledReceipt } from 'utils/receipt';
import SubOrderType from '../SubOrderType/SubOrderType';
import SubOrderUpdatedTime from '../SubOrderUpdatedTime/SubOrderUpdatedTime';
import PaymentMethod from '../PaymentMethod/PaymentMethod';
import SubOrderStatus from '../SubOrderStatus/SubOrderStatus';
import { sliceTwoMoreDecimals, сonvertFromCentsToDollars } from 'utils/formater';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import SubOrderStatusMobile from '../SubOrderStatusMobile/SubOrderStatusMobile';
import { receiptTypes } from 'constants/receiptType';
import { useTreatments } from '@splitsoftware/splitio-react';
import { FeatureFlags } from 'constants/featureFlags';
import { getSubOrderById } from 'apis/rest/subOrders';
import useApiCall from 'utils/hooks/use-api-call';
import './SubOrderHistoryDetails.scss';
import Typography, { BoxTypography } from 'components/ui-kit/Typography';
import Box from 'components/ui-kit/Box';
import { useLanguageContext } from 'contexts/languageContextProvider/context';

interface SubOrderHistoryDetailsProps {
  selectedSubOrderHistory: SubOrderHistory;
  setSelectedSubOrderHistory: (subOrderHistory: null | SubOrderHistory) => void;
}

const SubOrderHistoryDetails = ({
  selectedSubOrderHistory,
  setSelectedSubOrderHistory,
}: SubOrderHistoryDetailsProps) => {
  const { getLocale } = useLanguageContext();
  const feat = useTreatments([FeatureFlags.QR_CODE]);
  const { data: subOrderData }: { data: SubOrderData | null; error: unknown; isLoading: boolean } = useApiCall(
    getSubOrderById,
    { args: [selectedSubOrderHistory.id] },
  );
  const [
    actionQuestionText,
    reprintText,
    backToOrderHistoryText,
    subtotalPriceText,
    refundText,
    appPaymentDiscountText,
    originalTotalText,
    amountRefundedText,
    feesRefundedText,
    finalTotalText,
    refundedText,
    containerFeeText,
  ] = useEuiI18n(
    [
      'subOrdersHistory.details.actionQuestion',
      'subOrdersHistory.details.reprint',
      'subOrdersHistory.details.backToOrderHistory',
      'subOrdersHistory.details.subtotal',
      'subOrdersHistory.details.refund',
      'subOrdersHistory.details.appPaymentDiscount',
      'subOrdersHistory.details.originalTotal',
      'subOrdersHistory.details.amountRefunded',
      'subOrdersHistory.details.feesRefunded',
      'subOrdersHistory.details.finalTotal',
      'subOrdersHistory.details.refunded',
      'subOrders.subOrderDetails.containerFee',
    ],
    [],
  );

  const { isMobile, isTablet } = useDeviceContext();

  const [refundButtonClicked, setRefundButtonClicked] = useState(false);
  if (refundButtonClicked) {
    return (
      <Redirect
        to={{
          pathname: '/refund',
          state: {
            subOrder: selectedSubOrderHistory,
          },
        }}
      />
    );
  }

  const getCustomisations = (customisations: SubOrderHistoryProductCustomisation[]) => {
    return customisations.map((customisation) => {
      let customisatioName = getLocale() === 'en' ? customisation.name : customisation.nameCn;

      if (!customisatioName) {
        customisatioName = customisation.name;
      }

      return (
        <div key={customisation.id} className="SubOrderHistoryDetails__customisationInfo">
          <BoxTypography typographyProps={{ variant: 'span', style: { fontSize: 18 } }} boxProps={{ mt: 8 }}>
            {customisatioName + ' ' + 'x' + customisation.actualQuantity}
          </BoxTypography>
          {customisation.refundedQuantity ? (
            <BoxTypography typographyProps={{ variant: 'span', color: 'red' }} boxProps={{ mt: 8, mb: 8 }}>
              {customisation.refundedQuantity + ' ' + refundedText}
            </BoxTypography>
          ) : null}
        </div>
      );
    });
  };

  const getProductsList = () => {
    return selectedSubOrderHistory.products.map((product: SubOrderHistoryProduct) => {
      let productName = getLocale() === 'en' ? product.name : product.nameCn;

      if (!productName) {
        productName = product.name;
      }

      return (
        <EuiFlexGroup key={product.id} className="SubOrderHistoryDetails__listItem">
          <EuiFlexItem grow={1}>
            <BoxTypography typographyProps={{ variant: 'span', fontWeight: 'bold' }}>
              {'x' + product.quantity}
            </BoxTypography>
          </EuiFlexItem>
          <EuiFlexItem grow={10}>
            <BoxTypography typographyProps={{ variant: 'span', fontWeight: 'bold' }}>{productName}</BoxTypography>
            <BoxTypography
              typographyProps={{ variant: 'span' }}
              boxProps={{ addClass: 'SubOrderHistoryDetails__customerComments' }}
            >
              {product.customerComments}
            </BoxTypography>
            {product.refundedQuantity ? (
              <BoxTypography
                typographyProps={{ variant: 'span' }}
                boxProps={{ addClass: 'SubOrderHistoryDetails__refundedProductsAmount', mt: 8, mb: 8 }}
              >
                {product.refundedQuantity + ' ' + refundedText}
              </BoxTypography>
            ) : null}
            <div></div>
            {getCustomisations(product.customisations)}
          </EuiFlexItem>
          <EuiFlexItem grow={1}>
            <BoxTypography
              typographyProps={{ variant: 'span' }}
              boxProps={{ addClass: 'SubOrderHistoryDetails__price' }}
            >
              {'$' + sliceTwoMoreDecimals(product.fullProductSum)}
            </BoxTypography>
          </EuiFlexItem>
        </EuiFlexGroup>
      );
    });
  };

  const shouldDisplayRefundButton = (): boolean => {
    const { state, refundState } = selectedSubOrderHistory;

    return (
      state !== SubOrderState.ReadyForCollection &&
      state !== SubOrderState.Cancelled &&
      state !== SubOrderState.Rejected &&
      state !== SubOrderState.Refunded &&
      refundState !== SubOrderState.Refunded
    );
  };

  return (
    <div className="SubOrderHistoryDetails__container">
      <EuiFlexGroup className="SubOrderHistoryDetails__flexContainer">
        {isTablet && (
          <EuiFlexItem grow={7}>
            <EuiFlexGroup className="leftSide__container" direction="column">
              <EuiFlexItem grow={9}>
                <EuiFlexGroup alignItems="center" justifyContent="center">
                  <EuiFlexItem grow={false}>
                    <div className="actionsContainer">
                      <BoxTypography typographyProps={{ variant: 'span', style: { fontSize: 24 } }}>
                        {actionQuestionText}
                      </BoxTypography>
                      <div className="buttonsContainer">
                        {shouldDisplayRefundButton() && (
                          <CustomButton
                            padding="xl"
                            color="red"
                            onClick={() => setRefundButtonClicked(true)}
                            text={refundText}
                          />
                        )}
                        <CustomButton
                          padding="xl"
                          onClick={() =>
                            selectedSubOrderHistory.state === SubOrderState.Cancelled
                              ? printCancelledReceipt(selectedSubOrderHistory, isTablet)
                              : printAcceptedReceipt(
                                  selectedSubOrderHistory,
                                  getLocale(),
                                  receiptTypes.TENNANT,
                                  feat[FeatureFlags.QR_CODE].treatment,
                                  isTablet,
                                )
                          }
                          text={reprintText}
                        />
                      </div>
                    </div>
                  </EuiFlexItem>
                </EuiFlexGroup>
              </EuiFlexItem>
              <EuiFlexItem grow={1}>
                <EuiFlexGroup className="redirectButton__container" alignItems="center" justifyContent="center">
                  <EuiFlexItem grow={false}>
                    <CustomButton
                      padding="xl"
                      onClick={() => setSelectedSubOrderHistory(null)}
                      text={backToOrderHistoryText}
                    />
                  </EuiFlexItem>
                </EuiFlexGroup>
              </EuiFlexItem>
            </EuiFlexGroup>
          </EuiFlexItem>
        )}
        <EuiFlexItem
          className={`SubOrderHistoryDetails__content${shouldDisplayRefundButton() ? '--withFooter' : ''}`}
          grow={3}
        >
          <EuiFlexGroup className="SubOrderHistoryDetails__rightSide__container" direction="column">
            <EuiFlexItem className="SubOrderHistoryDetails__generalInfo" grow={isTablet ? 1 : undefined}>
              <EuiFlexGroup direction="column" justifyContent="spaceBetween">
                <EuiFlexItem className="SubOrderHistoryDetails__header">
                  <div className="SubOrderHistoryDetails__index">
                    <img src={selectedSubOrderHistory.logo} />
                    <BoxTypography typographyProps={{ variant: 'span', fontWeight: 'bold' }}>
                      {' '}
                      {selectedSubOrderHistory.orderId}
                    </BoxTypography>
                  </div>
                  {selectedSubOrderHistory.order.refunds.length ? (
                    <>
                      {isMobile && <SubOrderStatusMobile status={selectedSubOrderHistory.refundState} />}
                      {isTablet && <SubOrderStatus status={selectedSubOrderHistory.refundState} />}
                    </>
                  ) : (
                    <SubOrderUpdatedTime subOrderHistory={selectedSubOrderHistory} />
                  )}
                </EuiFlexItem>
                <EuiFlexItem className="SubOrderHistoryDetails__type__payment_container">
                  <SubOrderType type={selectedSubOrderHistory.type} />
                  <BoxTypography typographyProps={{ variant: 'span' }}>|</BoxTypography>
                  <PaymentMethod paymentMehod={selectedSubOrderHistory.paymentsMethod} />
                </EuiFlexItem>
              </EuiFlexGroup>
            </EuiFlexItem>
            <EuiFlexItem className="SubOrderHistoryDetails__productsInfo" grow={9}>
              <div className="SubOrderHistoryDetails__prices">{getProductsList()}</div>
              <div>
                <div className="SubOrderHistoryDetails__prices">
                  <Box addClass="SubOrderHistoryDetails__priceContainer" mt={16} mb={16}>
                    <Typography variant="span">{subtotalPriceText}</Typography>
                    <Typography variant="span">
                      {'$' + sliceTwoMoreDecimals(selectedSubOrderHistory.subTotalPrice)}
                    </Typography>
                  </Box>
                  <Box addClass="SubOrderHistoryDetails__priceContainer" mt={16} mb={16}>
                    <Typography variant="span">{containerFeeText}</Typography>
                    <Typography variant="span">
                      {'$' + sliceTwoMoreDecimals(selectedSubOrderHistory.totalContainerFeesAmount)}
                    </Typography>
                  </Box>
                  <Box addClass="SubOrderHistoryDetails__priceContainer" mt={16} mb={16}>
                    <Typography variant="span">{appPaymentDiscountText} (10%)</Typography>
                    <Typography variant="span">
                      {subOrderData
                        ? '-$' + sliceTwoMoreDecimals(сonvertFromCentsToDollars(subOrderData.appPaymentDiscount))
                        : ''}
                    </Typography>
                  </Box>
                  <Box addClass="SubOrderHistoryDetails__priceContainer" mt={16} mb={16}>
                    <Typography variant="span" fontWeight="bold">
                      {originalTotalText} (+7% GST)
                    </Typography>
                    <Typography variant="span" fontWeight="bold">
                      {'$' + sliceTwoMoreDecimals(selectedSubOrderHistory.originNetSales)}
                    </Typography>
                  </Box>
                </div>
                {selectedSubOrderHistory.refundState ? (
                  <div className="SubOrderHistoryDetails__refundPrices SubOrderHistoryDetails__prices">
                    <Box addClass="SubOrderHistoryDetails__priceContainer" mt={16} mb={16}>
                      <Typography variant="span">{amountRefundedText}</Typography>
                      <Typography variant="span">
                        {'$' + sliceTwoMoreDecimals(selectedSubOrderHistory.refundedPrice)}
                      </Typography>
                    </Box>
                    <Box addClass="SubOrderHistoryDetails__priceContainer" mt={16} mb={16}>
                      <Typography variant="span">{feesRefundedText}</Typography>
                      <Typography variant="span">
                        {'-$' +
                          sliceTwoMoreDecimals(selectedSubOrderHistory.totalReturnedContainerFeesAmountWithDiscount)}
                      </Typography>
                    </Box>
                    <Box addClass="SubOrderHistoryDetails__priceContainer" mt={16} mb={16}>
                      <Typography variant="span" fontWeight="bold">
                        {finalTotalText} (+7% GST)
                      </Typography>
                      <Typography variant="span" fontWeight="bold">
                        {'$' + sliceTwoMoreDecimals(selectedSubOrderHistory.netSales)}
                      </Typography>
                    </Box>
                  </div>
                ) : null}
              </div>
            </EuiFlexItem>
          </EuiFlexGroup>
        </EuiFlexItem>
        {isMobile && shouldDisplayRefundButton() && (
          <EuiFlexItem grow={1}>
            <EuiFlexGroup className="SubOrderHistoryDetails__footer" alignItems="center" justifyContent="center">
              <EuiFlexItem grow={false}>
                <CustomButton padding="xl" color="red" onClick={() => setRefundButtonClicked(true)} text={refundText} />
              </EuiFlexItem>
            </EuiFlexGroup>
          </EuiFlexItem>
        )}
      </EuiFlexGroup>
    </div>
  );
};

export default SubOrderHistoryDetails;
