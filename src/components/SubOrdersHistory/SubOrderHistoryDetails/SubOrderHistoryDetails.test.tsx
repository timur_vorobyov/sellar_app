import '@testing-library/jest-dom';
import { screen, cleanup } from '@testing-library/react';
import { SubOrderHistory, SubOrderState } from 'types/models/subOrder';
import SubOrderHistoryDetails from './SubOrderHistoryDetails';
import { customI18nRender } from 'utils/testing/customRender';
import { subOrderHistory } from '__mocks__/mocks';
import { Locale } from 'types/translation';

afterEach(cleanup);

const setSelectedSubOrderHistory = (subOrderHistory: null | SubOrderHistory) => {
  return subOrderHistory;
};

describe('<SubOrderHistoryDetails> component', () => {
  it('should render component with data', () => {
    customI18nRender(
      <SubOrderHistoryDetails
        selectedSubOrderHistory={subOrderHistory}
        setSelectedSubOrderHistory={setSelectedSubOrderHistory}
      />,
      { locale: Locale.CHINESE },
    );

    expect(screen.getByText('Reprint 重印')).toBeInTheDocument();
    expect(screen.getByText('Back to Order History 返回订单历史')).toBeInTheDocument();
    expect(screen.getByText('Subtotal 小计')).toBeInTheDocument();
    expect(screen.getByText('Original Total 原始总额 (+7% GST)')).toBeInTheDocument();
  });

  it('should not render refund button for cancelled/reject/refunded statuses', () => {
    const customOrderHistory = { ...subOrderHistory };

    customOrderHistory.state = SubOrderState.Rejected;
    customI18nRender(
      <SubOrderHistoryDetails
        selectedSubOrderHistory={customOrderHistory}
        setSelectedSubOrderHistory={setSelectedSubOrderHistory}
      />,
      { locale: Locale.CHINESE },
    );
    const buttonContentIfRejected = screen.queryByText('Refund 退款');
    expect(buttonContentIfRejected).not.toBeInTheDocument();

    customOrderHistory.state = SubOrderState.Refunded;
    customOrderHistory.refundState = 'REFUNDED';
    customI18nRender(
      <SubOrderHistoryDetails
        selectedSubOrderHistory={customOrderHistory}
        setSelectedSubOrderHistory={setSelectedSubOrderHistory}
      />,
      { locale: Locale.CHINESE },
    );
    const buttonContentIfFullyRefunded = screen.queryByText('Refund 退款');
    expect(buttonContentIfFullyRefunded).not.toBeInTheDocument();

    customOrderHistory.state = SubOrderState.Cancelled;
    customI18nRender(
      <SubOrderHistoryDetails
        selectedSubOrderHistory={customOrderHistory}
        setSelectedSubOrderHistory={setSelectedSubOrderHistory}
      />,
      { locale: Locale.CHINESE },
    );
    const buttonContentIfCancelled = screen.queryByText('Refund 退款');
    expect(buttonContentIfCancelled).not.toBeInTheDocument();
  });

  it('should render with refund button for other statuses', () => {
    customI18nRender(
      <SubOrderHistoryDetails
        selectedSubOrderHistory={subOrderHistory}
        setSelectedSubOrderHistory={setSelectedSubOrderHistory}
      />,
      { locale: Locale.CHINESE },
    );

    expect(screen.getByText('Refund 退款')).toBeInTheDocument();
  });
});
