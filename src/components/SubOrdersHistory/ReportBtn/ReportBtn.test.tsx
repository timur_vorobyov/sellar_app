import { cleanup } from '@testing-library/react';
import { screen } from '@testing-library/react';

import '@testing-library/jest-dom';
import ReportBtn from './ReportBtn';
import { customI18nRender } from 'utils/testing/customRender';
import { report } from '__mocks__/mocks';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<ReportBtn> component', () => {
  it('should render component with data ', () => {
    customI18nRender(<ReportBtn zReportData={report} />, { locale: Locale.CHINESE });

    expect(screen.getByText('Print Report 印刷报告')).toBeInTheDocument();
  });
});
