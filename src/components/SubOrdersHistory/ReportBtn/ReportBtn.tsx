import { useEuiI18n } from '@elastic/eui';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import { getStorageSerialNumber } from 'storage/adapters/serialNumber';
import { ZReportData } from 'types/ZReportData';
import { printZReportReceipt } from 'utils/receipt';

interface ReportBtnProps {
  zReportData: ZReportData | null;
}

const ReportBtn = ({ zReportData }: ReportBtnProps) => {
  const { isTablet } = useDeviceContext();

  const [printReportText] = useEuiI18n(['subOrdersHistory.reportButton.text'], []);

  return (
    <CustomButton
      onClick={() => printZReportReceipt(zReportData, getStorageSerialNumber(), isTablet)}
      text={printReportText}
    />
  );
};

export default ReportBtn;
