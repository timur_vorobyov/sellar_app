import '@testing-library/jest-dom';
import { cleanup, screen } from '@testing-library/react';
import SubOrderType from './SubOrderType';
import { SubOrderType as SubOrderTypeConst, SubOrderTypeTranslations } from 'constants/subOrder';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<SubOrderType> component', () => {
  it('should show deliverys order type', () => {
    customI18nRender(<SubOrderType type={SubOrderTypeConst.Delivery} />, { locale: Locale.CHINESE });

    expect(screen.getByText(SubOrderTypeTranslations(Locale.CHINESE)['delivery'])).toBeInTheDocument();
  });

  it('should show having here order type', () => {
    customI18nRender(<SubOrderType type={SubOrderTypeConst.HavingHere} />, { locale: Locale.CHINESE });

    expect(screen.getByText(SubOrderTypeTranslations(Locale.CHINESE)['having_here'])).toBeInTheDocument();
  });

  it('should show self collect order type', () => {
    customI18nRender(<SubOrderType type={SubOrderTypeConst.SelfCollect} />, { locale: Locale.CHINESE });

    expect(screen.getByText(SubOrderTypeTranslations(Locale.CHINESE)['self_collect'])).toBeInTheDocument();
  });
});
