import { BoxTypography } from 'components/ui-kit/Typography';
import { SubOrderType as SubOrderTypeConst } from 'constants/subOrder';
import useCommonTranslations from 'utils/hooks/user-common-translations';

interface SubOrderTypeProps {
  type: string;
}

const SubOrderType = ({ type }: SubOrderTypeProps) => {
  const { orderTypes: orderTypeTranslations } = useCommonTranslations();

  switch (type) {
    case SubOrderTypeConst.Delivery: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }}>
          {orderTypeTranslations[SubOrderTypeConst.Delivery]}
        </BoxTypography>
      );
    }
    case SubOrderTypeConst.HavingHere: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }}>
          {orderTypeTranslations[SubOrderTypeConst.HavingHere]}
        </BoxTypography>
      );
    }
    case SubOrderTypeConst.SelfCollect: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }}>
          {orderTypeTranslations[SubOrderTypeConst.SelfCollect]}
        </BoxTypography>
      );
    }
    default:
      return <BoxTypography typographyProps={{ variant: 'span' }}>{type}</BoxTypography>;
  }
};

export default SubOrderType;
