import '@testing-library/jest-dom';
import { screen, cleanup } from '@testing-library/react';
import { SubOrderState } from 'types/models/subOrder';
import SubOrderStatus from './SubOrderStatus';
import { customI18nRender } from 'utils/testing/customRender';

afterEach(cleanup);

describe('<SubOrderStatus> component', () => {
  it('should show ready for collection sub order status', () => {
    customI18nRender(<SubOrderStatus status={SubOrderState.ReadyForCollection} />);

    expect(screen.getByText('Ready for collection')).toBeInTheDocument();
  });

  it('should show collected sub order status', () => {
    customI18nRender(<SubOrderStatus status={SubOrderState.Collected} />);

    expect(screen.getByText('Collected')).toBeInTheDocument();
  });

  it('should show cancelled sub order status', () => {
    customI18nRender(<SubOrderStatus status={SubOrderState.Cancelled} />);

    expect(screen.getByText('Cancelled')).toBeInTheDocument();
  });

  it('should show rejected sub order status', () => {
    customI18nRender(<SubOrderStatus status={SubOrderState.Rejected} />);

    expect(screen.getByText('Rejected')).toBeInTheDocument();
  });
});
