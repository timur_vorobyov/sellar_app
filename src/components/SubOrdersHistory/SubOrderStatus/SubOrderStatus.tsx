import { useEuiI18n } from '@elastic/eui';
import { BoxTypography } from 'components/ui-kit/Typography';
import { SubOrderState } from 'types/models/subOrder';
import './SubOrderStatus.scss';

interface OrderStatusProps {
  status: string;
}

const SubOrderStatus = ({ status }: OrderStatusProps) => {
  const [
    statusRefundedText,
    statusPendinglRefundText,
    statusPartialRefundText,
    statusReadyForCollectionText,
    statusRejectedText,
    statusCancelledText,
    statusCollectedText,
  ] = useEuiI18n(
    [
      'subOrdersHistory.status.refunded',
      'subOrdersHistory.status.pendingRefund',
      'subOrdersHistory.status.partialRefund',
      'subOrdersHistory.status.readyForCollection',
      'subOrdersHistory.status.rejected',
      'subOrdersHistory.status.cancelled',
      'subOrdersHistory.status.collected',
    ],
    [],
  );

  switch (status) {
    case SubOrderState.Refunded: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatus__black' }}>
          {statusRefundedText}
        </BoxTypography>
      );
    }

    case SubOrderState.PartialRefund: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatus__black' }}>
          {statusPartialRefundText}
        </BoxTypography>
      );
    }

    case SubOrderState.PendingRefund: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatus__black' }}>
          {statusPendinglRefundText}
        </BoxTypography>
      );
    }

    case SubOrderState.ReadyForCollection: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatus__green' }}>
          {statusReadyForCollectionText}
        </BoxTypography>
      );
    }

    case SubOrderState.Rejected: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatus__red' }}>
          {statusRejectedText}
        </BoxTypography>
      );
    }

    case SubOrderState.Cancelled: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatus__red' }}>
          {statusCancelledText}
        </BoxTypography>
      );
    }

    case SubOrderState.Collected: {
      return (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'SubOrderStatus__green' }}>
          {statusCollectedText}
        </BoxTypography>
      );
    }

    default:
      return <BoxTypography typographyProps={{ variant: 'span' }}>{status}</BoxTypography>;
  }
};

export default SubOrderStatus;
