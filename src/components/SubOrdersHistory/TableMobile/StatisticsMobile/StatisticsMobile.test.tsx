import '@testing-library/jest-dom';
import { screen, cleanup } from '@testing-library/react';
import StatisticsMobile from './StatisticsMobile';
import { sliceTwoMoreDecimals, сonvertFromCentsToDollars } from 'utils/formater';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<StatisticsMobile> component', () => {
  it('should render component with data', () => {
    customI18nRender(<StatisticsMobile grossTotalSum={150} netTotalSum={360} refundSum={0} />, {
      locale: Locale.CHINESE,
    });

    expect(screen.getByText('Refunds Total 退款总额')).toBeInTheDocument();
    expect(screen.getByText('Net Sales 净销售额')).toBeInTheDocument();
    expect(screen.getByText('Excludes discounts & delivery fee 不含折扣及外卖费')).toBeInTheDocument();
    expect(screen.getByText('Gross Sales 销售总额')).toBeInTheDocument();
    expect(screen.getByText(`$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(360))}`)).toBeInTheDocument();
    expect(screen.getByText(`$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(150))}`)).toBeInTheDocument();
  });
});
