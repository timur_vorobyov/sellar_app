import { useEuiI18n } from '@elastic/eui';
import { useMemo } from 'react';
import './StatisticsMobile.scss';
import { sliceTwoMoreDecimals, сonvertFromCentsToDollars } from 'utils/formater';
interface StatisticsMobileProps {
  grossTotalSum: number;
  netTotalSum: number;
  refundSum: number;
}

const StatisticsMobile = ({ refundSum, grossTotalSum, netTotalSum }: StatisticsMobileProps) => {
  const [refundsTotalText, netSalesText, excludeDiscountsText, grossSalesText, allPlatformsText] = useEuiI18n(
    [
      'subOrdersHistory.statistics.refundsTotal',
      'subOrdersHistory.statistics.netSales',
      'subOrdersHistory.statistics.excludeDiscounts',
      'subOrdersHistory.statistics.grossSales',
      'subOrdersHistory.statistics.allPlatforms',
    ],
    [],
  );

  return useMemo(() => {
    return (
      <>
        <div className="SubOrdersHistoryStatisticsMobile__Caption">{allPlatformsText}</div>
        <div className="SubOrdersHistoryStatisticsMobile__Container">
          <div className="SubOrdersHistoryStatisticsMobile__table">
            <div className="SubOrdersHistoryStatisticsMobile__title">{refundsTotalText}</div>
            <div className="SubOrdersHistoryStatisticsMobile__value">
              {`$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(refundSum))}`}
            </div>
            <div className="SubOrdersHistoryStatisticsMobile__title">{netSalesText}</div>
            <div className="SubOrdersHistoryStatisticsMobile__value">{`$${sliceTwoMoreDecimals(
              сonvertFromCentsToDollars(netTotalSum),
            )}`}</div>
            <div className="SubOrdersHistoryStatisticsMobile__double">{excludeDiscountsText}</div>
            <div className="SubOrdersHistoryStatisticsMobile__title">{grossSalesText}</div>
            <div className="SubOrdersHistoryStatisticsMobile__value">{`$${sliceTwoMoreDecimals(
              сonvertFromCentsToDollars(grossTotalSum),
            )}`}</div>
          </div>
        </div>
      </>
    );
  }, [grossTotalSum, netTotalSum, refundsTotalText, netSalesText, excludeDiscountsText, grossSalesText]);
};

export default StatisticsMobile;
