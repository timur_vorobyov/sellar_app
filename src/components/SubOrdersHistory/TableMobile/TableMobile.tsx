import { useEuiI18n, EuiPagination } from '@elastic/eui';
import { useState } from 'react';
import { SubOrderState, SubOrderHistory } from 'types/models/subOrder';
import { printAcceptedReceipt, printCancelledReceipt } from 'utils/receipt';
import ItemBlockMobile from './ItemBlockMobile/ItemBlockMobile';
import StatisticsMobile from './StatisticsMobile/StatisticsMobile';
import { receiptTypes } from 'constants/receiptType';
import { useTreatments } from '@splitsoftware/splitio-react';
import { FeatureFlags } from 'constants/featureFlags';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import './TableMobile.scss';
import { BoxTypography } from 'components/ui-kit/Typography';
import { useLanguageContext } from 'contexts/languageContextProvider/context';

interface TableMobileProps {
  subOrdersHistory: SubOrderHistory[];
  setSelectedSubOrderHistory: (subOrderHistory: SubOrderHistory) => void;
  grossTotalSum: number;
  netTotalSum: number;
  refundSum: number;
}

const TableMobile = ({
  refundSum,
  subOrdersHistory,
  setSelectedSubOrderHistory,
  grossTotalSum,
  netTotalSum,
}: TableMobileProps) => {
  const [pageIndex, setPageIndex] = useState(0);
  const pageSize = 10;
  const { getLocale } = useLanguageContext();
  const feat = useTreatments([FeatureFlags.QR_CODE]);
  const [noItemsFoundText] = useEuiI18n(['menu.table.noItemsFound'], []);
  const { isTablet } = useDeviceContext();

  const goToPage = (pIndex: number) => {
    setPageIndex(pIndex);
  };

  const fromItem = pageIndex ? pageIndex * pageSize : 0;
  const toItem = pageSize * (pageIndex + 1);
  const pageOfItems = subOrdersHistory.slice(fromItem, toItem);
  const totalItemCount = subOrdersHistory.length;
  const pageCount = Math.ceil(totalItemCount / pageSize);

  return (
    <div className="SubOrdersHistoryTableMobile__container">
      <StatisticsMobile refundSum={refundSum} netTotalSum={netTotalSum} grossTotalSum={grossTotalSum} />
      <BoxTypography
        typographyProps={{
          variant: 'span',
          style: { fontSize: 16, color: '#333333' },
          fontWeight: '400',
        }}
        boxProps={{ mb: 10 }}
      >
        {`Showing ${fromItem + 1} - ${toItem > totalItemCount ? totalItemCount : toItem} out of ${totalItemCount}`}
      </BoxTypography>
      {totalItemCount ? (
        <div className="SubOrdersHistoryTableListMobile">
          {pageOfItems.map((item) => (
            <ItemBlockMobile
              onClick={setSelectedSubOrderHistory}
              key={item.id}
              subOrderHistory={item}
              onPrint={() => {
                item.state === SubOrderState.Cancelled
                  ? printCancelledReceipt(item, isTablet)
                  : printAcceptedReceipt(
                      item,
                      getLocale(),
                      receiptTypes.TENNANT,
                      feat[FeatureFlags.QR_CODE].treatment,
                      isTablet,
                    );
              }}
            />
          ))}
        </div>
      ) : (
        <BoxTypography
          typographyProps={{ variant: 'span' }}
          boxProps={{ p: 32, addClass: 'SubOrdersHistoryTable__noitems' }}
        >
          {noItemsFoundText}
        </BoxTypography>
      )}
      <div className="SubOrdersHistoryTable__pagination">
        <EuiPagination
          pageCount={pageCount}
          activePage={pageIndex}
          onPageClick={(activePage) => goToPage(activePage)}
        />
      </div>
    </div>
  );
};

export default TableMobile;
