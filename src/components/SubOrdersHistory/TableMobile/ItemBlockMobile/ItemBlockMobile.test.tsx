import { cleanup, fireEvent } from '@testing-library/react';
import { screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import ItemBlockMobile from './ItemBlockMobile';
import { subOrderHistory } from '__mocks__/mocks';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

const onClick = jest.fn();

describe('<ItemBlockMobile> component', () => {
  it('should render component', () => {
    customI18nRender(<ItemBlockMobile subOrderHistory={subOrderHistory} onPrint={() => null} onClick={onClick} />, {
      locale: Locale.ENGLISH,
    });

    expect(screen.getByText('No. of Items')).toBeTruthy();
    expect(screen.getByText('Total Amount')).toBeTruthy();
    fireEvent.click(screen.getByText('Total Amount'));
    expect(onClick).toBeCalled();
  });
});
