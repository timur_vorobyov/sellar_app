import { useEuiI18n } from '@elastic/eui';
import { sliceTwoMoreDecimals } from 'utils/formater';
import PrintIcon from 'components/assets/images/HistoryPage/printIcon.svg';
import { SubOrderHistory } from 'types/models/subOrder';
import './ItemBlockMobile.scss';
import SubOrderStatusMobile from 'components/SubOrdersHistory/SubOrderStatusMobile/SubOrderStatusMobile';

interface ItemMobileBlockProps {
  subOrderHistory: SubOrderHistory;
  onPrint: () => void;
  onClick: (subOrderHistory: SubOrderHistory) => void;
}

export default function ItemBlockMobile({ subOrderHistory, onPrint, onClick }: ItemMobileBlockProps) {
  const [numberOfItemsColumnText, totalAmountColumnText] = useEuiI18n(
    ['subOrdersHistory.table.numberOfItemsColumn', 'subOrdersHistory.table.totalAmountColumn'],
    [],
  );

  return (
    <div className="ItemBlockMobile__container" onClick={() => onClick(subOrderHistory)}>
      <div className="ItemBlockMobile__header">
        <div className="ItemBlockMobile__header__left">
          <img src={subOrderHistory.logo} />
          <div>{subOrderHistory.orderId}</div>
        </div>
        <div
          className="ItemBlockMobile__header__print"
          onClick={(event) => {
            event.stopPropagation();
            onPrint();
          }}
        >
          <img src={PrintIcon} />
        </div>
      </div>
      <div className="ItemBlockMobile__body">
        <div className="ItemBlockMobile__table">
          <div className="ItemBlockMobile__body__title">{numberOfItemsColumnText}</div>
          <div>{subOrderHistory.numberOfProducts}</div>
          <div className="ItemBlockMobile__body__title">{totalAmountColumnText}</div>
          <div>${sliceTwoMoreDecimals(subOrderHistory.originNetSales)}</div>
        </div>
        <div className="ItemBlockMobile__body__status">
          {subOrderHistory.order.refunds.length ? (
            <SubOrderStatusMobile status={subOrderHistory.refundState} />
          ) : (
            <SubOrderStatusMobile status={subOrderHistory.state} />
          )}
        </div>
      </div>
    </div>
  );
}
