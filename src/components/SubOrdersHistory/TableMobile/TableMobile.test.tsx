import '@testing-library/jest-dom';
import { screen, cleanup } from '@testing-library/react';
import { SubOrderHistory } from 'types/models/subOrder';
import TableMobile from './TableMobile';
import { sliceTwoMoreDecimals, сonvertFromCentsToDollars } from 'utils/formater';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';
import { subOrderHistoryList } from '__mocks__/mocks';

afterEach(cleanup);

const setSelectedSubOrderHistory = (subOrderHistory: null | SubOrderHistory) => {
  return subOrderHistory;
};

describe('<TableMobile> component', () => {
  it('should render component with data', () => {
    customI18nRender(
      <TableMobile
        refundSum={0}
        netTotalSum={150}
        grossTotalSum={360}
        subOrdersHistory={subOrderHistoryList}
        setSelectedSubOrderHistory={(subOrderHistory: SubOrderHistory) => setSelectedSubOrderHistory(subOrderHistory)}
      />,
      { locale: Locale.CHINESE },
    );

    expect(screen.getByText(`$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(360))}`)).toBeInTheDocument();
    expect(screen.getByText(`$${sliceTwoMoreDecimals(сonvertFromCentsToDollars(150))}`)).toBeInTheDocument();
    expect(screen.getAllByText('No. of Items 食品数量')[0]).toBeInTheDocument();
    expect(screen.getAllByText('Total Amount 总额')[0]).toBeInTheDocument();
    expect(screen.getByText(subOrderHistoryList[0].orderId)).toBeInTheDocument();
    expect(screen.getByText(subOrderHistoryList[1].orderId)).toBeInTheDocument();
  });
});
