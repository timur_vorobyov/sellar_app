import { cleanup, fireEvent } from '@testing-library/react';
import { screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { Filter } from './Filter';
import { customI18nRender } from 'utils/testing/customRender';

afterEach(cleanup);

const options = [
  {
    value: 1,
    text: 'Frist option',
  },
  {
    value: 2,
    text: 'Second option',
  },
  {
    value: 3,
    text: 'Third option',
  },
];

const label = 'Filter';

const selected = [1, 2, 3];

describe('<Filter> component', () => {
  it('should open popover after All 全部 button has beenpushed', () => {
    customI18nRender(<Filter label={label} options={options} selected={selected} onApply={() => null} />);

    const button = screen.getByTestId('filter');
    fireEvent.click(button);
    expect(screen.getByText('Apply')).toBeTruthy();
  });

  it('should deselect all items after Deselect button with Apply button has been pushed', () => {
    let localSelected = [1, 2, 3];

    const onApply = (selected: number[]) => {
      localSelected = selected;
    };

    customI18nRender(<Filter label={label} options={options} selected={localSelected} onApply={onApply} />);

    const button = screen.getByTestId('filter');
    fireEvent.click(button);
    const deselectButton = screen.getByText('Deselect all');
    fireEvent.click(deselectButton);
    const applyButton = screen.getByText('Apply');
    fireEvent.click(applyButton);

    expect(localSelected.length).toBe(0);
  });

  it('sould not to deselect all items after Deselect button has been pushed without Apply button', () => {
    let localSelected = [1, 2, 3];

    const onApply = (selected: number[]) => {
      localSelected = selected;
    };

    customI18nRender(<Filter label={label} options={options} selected={localSelected} onApply={onApply} />);

    const button = screen.getByTestId('filter');
    fireEvent.click(button);
    const deselectButton = screen.getByText('Deselect all');
    fireEvent.click(deselectButton);

    expect(localSelected.length).toBe(3);
  });

  it('should select a checkbox after the checkbox with Apply button has been pushed', () => {
    let localSelected = [1, 3];

    const onApply = (selected: number[]) => {
      localSelected = selected;
    };

    const { getByText, getByLabelText } = customI18nRender(
      <Filter label={label} options={options} selected={localSelected} onApply={onApply} />,
    );

    const button = screen.getByTestId('filter');
    fireEvent.click(button);
    const checkbox = getByLabelText('Second option');
    fireEvent.click(checkbox);
    const applyButton = getByText('Apply');
    fireEvent.click(applyButton);

    expect(localSelected.includes(2)).toBeTruthy();
  });

  it('should deselect a checkbox after the checkbox has been pushed whithout Apply button', () => {
    let localSelected = [1, 2, 3];

    const onApply = (selected: number[]) => {
      localSelected = selected;
    };

    customI18nRender(<Filter label={label} options={options} selected={localSelected} onApply={onApply} />);

    const button = screen.getByTestId('filter');
    fireEvent.click(button);
    const checkbox = screen.getByLabelText('Second option');
    fireEvent.click(checkbox);
    const applyButton = screen.getByText('Apply');
    fireEvent.click(applyButton);

    expect(localSelected.includes(2)).toBeFalsy();
  });

  it('should not to safe selected checkboxes if they were chosen without pushing Apply button', () => {
    let localSelected = [1, 3];

    const onApply = (selected: number[]) => {
      localSelected = selected;
    };

    customI18nRender(<Filter label={label} options={options} selected={localSelected} onApply={onApply} />);

    const button = screen.getByTestId('filter');
    fireEvent.click(button);
    const checkbox = screen.getByLabelText('Second option');
    fireEvent.click(checkbox);

    expect(localSelected.includes(2)).toBeFalsy();
  });
});
