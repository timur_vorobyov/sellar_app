import {
  EuiButton,
  EuiCheckbox,
  EuiFlexGrid,
  EuiFlexItem,
  EuiPopover,
  EuiPopoverFooter,
  EuiPopoverTitle,
  useEuiI18n,
} from '@elastic/eui';
import { useEffect, useState } from 'react';
import './Filter.scss';
import { Option } from 'types/generics';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import Typography from 'components/ui-kit/Typography';

interface FilterProps {
  options: Option<number>[] | null;
  selected: number[];
  label: string;
  onApply: (selected: number[]) => void;
}

export const Filter = ({ options, label, selected, onApply }: FilterProps): JSX.Element => {
  const [isPopoverOpen, setIsPopoverOpen] = useState(false);
  const [selectedCheckboxes, setSelectedCheckboxes] = useState<number[]>([]);
  const [isApplyBtnDisabled, setIsApplyBtnDisabled] = useState(true);

  const [
    selectedAllFiltersText,
    selectedNoFiltersText,
    selectedSingleFilterText,
    selectedMultipleFiltersText,
    deselectAllFiltersText,
    cancelFilterSelectionText,
    applyFilterSelectionText,
  ] = useEuiI18n(
    [
      'menu.filters.selectedNumber.all',
      'menu.filters.selectedNumber.none',
      'menu.filters.selectedNumber.single',
      'menu.filters.selectedNumber.multiple',
      'menu.filters.popover.deselectAll',
      'menu.filters.popover.cancel',
      'menu.filters.popover.apply',
    ],
    [],
  );

  useEffect(() => {
    if (selected && selected.length) {
      setSelectedCheckboxes(selected);
    }
  }, [selected]);

  useEffect(() => {
    if (selected.length == selectedCheckboxes.length && selected.every((el) => selectedCheckboxes.includes(el))) {
      setIsApplyBtnDisabled(true);
    }
  }, [selectedCheckboxes]);

  const handleCheckboxChange = (optionId: number, checked: boolean) => {
    setIsApplyBtnDisabled(false);
    if (checked) {
      if (!selectedCheckboxes.includes(optionId)) {
        setSelectedCheckboxes([...selectedCheckboxes, optionId]);
      }
    } else {
      if (selectedCheckboxes.includes(optionId)) {
        setSelectedCheckboxes(selectedCheckboxes.filter((id) => id !== optionId));
      }
    }
  };

  const getFilterItemsNumberText = () => {
    switch (selected.length) {
      case options?.length:
        return selectedAllFiltersText;
      case 1:
        return selectedSingleFilterText;
      case 0:
        return selectedNoFiltersText;
      default:
        return `${selected.length} ${selectedMultipleFiltersText}`;
    }
  };

  return (
    <div className="Filter__container">
      <Typography variant="span" fontWeight="bold">
        {label}
      </Typography>
      <EuiPopover
        panelClassName="Filter__popover-container"
        ownFocus={false}
        display="block"
        button={
          <EuiButton
            data-testid="filter"
            className="popover-button"
            iconType="arrowDown"
            iconSide="right"
            onClick={() => setIsPopoverOpen((isPopoverOpen) => !isPopoverOpen)}
          >
            <Typography variant="span">{getFilterItemsNumberText()}</Typography>
          </EuiButton>
        }
        isOpen={isPopoverOpen}
        closePopover={() => {
          setIsPopoverOpen(false);
          setSelectedCheckboxes(selected);
          setIsApplyBtnDisabled(true);
        }}
        anchorPosition="downLeft"
      >
        <EuiPopoverTitle className="Filter__popover-header">
          <Typography variant="span">{label}</Typography>
          <CustomButton
            size="sm"
            color="transparent"
            padding="no"
            onClick={() => {
              setSelectedCheckboxes([]);
              setIsApplyBtnDisabled(false);
            }}
            text={deselectAllFiltersText}
            typoColor="blue"
          />
        </EuiPopoverTitle>
        <EuiFlexGrid gutterSize="none" columns={2} className="Filter__popover-body">
          {options &&
            options.map((option, index: number) => {
              return (
                <EuiFlexItem key={index}>
                  <EuiCheckbox
                    id={option.value.toString()}
                    label={<Typography variant="span">{option.text}</Typography>}
                    checked={selectedCheckboxes.includes(option.value)}
                    onChange={(e) => handleCheckboxChange(option.value, e.target.checked)}
                  />
                </EuiFlexItem>
              );
            })}
        </EuiFlexGrid>
        <EuiPopoverFooter className="Filter__popover-footer">
          <CustomButton
            size="sm"
            color="transparent"
            padding="no"
            onClick={() => {
              setIsPopoverOpen(false);
              setIsApplyBtnDisabled(true);
              setSelectedCheckboxes(selected);
            }}
            text={cancelFilterSelectionText}
            typoColor="blue"
          />
          <CustomButton
            size="sm"
            color="light"
            padding="no"
            disabled={isApplyBtnDisabled}
            onClick={() => {
              onApply(selectedCheckboxes);
              setIsApplyBtnDisabled(true);
              setIsPopoverOpen(false);
            }}
            text={applyFilterSelectionText}
            typoColor="blue"
          />
        </EuiPopoverFooter>
      </EuiPopover>
    </div>
  );
};
