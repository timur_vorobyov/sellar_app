import { EuiPagination, useEuiI18n } from '@elastic/eui';
import { CustomisationOption, MenuItem } from 'types/models/menu';
import { useState } from 'react';
import './MenuTableMobile.scss';
import { updateCustomisationOption, updateMenuItem } from 'apis/rest/catalog';
import useExplicitAsyncCall from 'utils/hooks/use-explicit-api-call';
import ItemBlockMobile from './ItemBlockMobile/ItemBlockMobile';
import Typography from 'components/ui-kit/Typography';
import { useLanguageContext } from 'contexts/languageContextProvider/context';
import { BoxTypography } from '../../ui-kit/Typography';
import Box from '../../ui-kit/Box';

interface MenuTableMobileProps {
  items: MenuItem[] | CustomisationOption[];
  isMainTab: boolean;
  updateMenuItemState: (menuItem: MenuItem) => void;
  updateCustomisationOptionState: (customisationOption: CustomisationOption) => void;
}

export const MenuTableMobile = ({
  items,
  isMainTab,
  updateMenuItemState,
  updateCustomisationOptionState,
}: MenuTableMobileProps) => {
  const [pageIndex, setPageIndex] = useState(0);
  const pageSize = 10;
  const { getLocale } = useLanguageContext();

  const noItemsFoundText = useEuiI18n('menu.table.noItemsFound', '');

  const { execute: executeUpdateMenuItem } = useExplicitAsyncCall(updateMenuItem);
  const { execute: executeUpdateCustomisationOption } = useExplicitAsyncCall(updateCustomisationOption);

  const handleMenuItemChange = (menuItem: MenuItem) => {
    updateMenuItemState(menuItem);
    executeUpdateMenuItem(menuItem);
  };

  const handleCustomisationOptionChange = (customisationOption: CustomisationOption) => {
    updateCustomisationOptionState(customisationOption);
    executeUpdateCustomisationOption(customisationOption);
  };

  const goToPage = (pIndex: number) => {
    setPageIndex(pIndex);
  };

  const getItemName = (item: MenuItem): string => {
    let name = getLocale() === 'en' ? item.name : item.nameCn;

    if (!name) {
      name = item.name;
    }
    return name;
  };
  const fromItem = pageIndex ? pageIndex * pageSize : 0;
  const toItem = pageSize * (pageIndex + 1);
  const pageOfItems = items.slice(fromItem, toItem);
  const totalItemCount = items.length;
  const pageCount = Math.ceil(totalItemCount / pageSize);

  return (
    <div className="MenuTableMobile__container">
      <Box addClass="MenuTableMobile__itemsInfo" pr={16} pl={16} mb={16}>
        <Typography variant="span" style={{ fontSize: 16 }}>{`Showing ${fromItem + 1} - ${
          toItem > totalItemCount ? totalItemCount : toItem
        } out of ${totalItemCount}`}</Typography>
      </Box>
      {totalItemCount ? (
        <ul className="MenuTableListMobile">
          {isMainTab &&
            (pageOfItems as MenuItem[]).map((item) => (
              <li key={item.id}>
                <ItemBlockMobile
                  activeHours={item.activeHours}
                  name={getItemName(item)}
                  filterName={item.category}
                  available={item.available}
                  onStatusChange={(value: number) =>
                    handleMenuItemChange({
                      ...item,
                      available: value,
                    })
                  }
                />
              </li>
            ))}
          {!isMainTab &&
            (pageOfItems as CustomisationOption[]).map((item) => (
              <li key={item.id}>
                <ItemBlockMobile
                  activeHours={{}}
                  name={item.name}
                  filterName={item.customisation}
                  available={item.available}
                  onStatusChange={(value: number) =>
                    handleCustomisationOptionChange({
                      ...item,
                      available: value,
                    })
                  }
                />
              </li>
            ))}
        </ul>
      ) : (
        <BoxTypography typographyProps={{ variant: 'span' }} boxProps={{ addClass: 'MenuTableMobile__noitems', p: 32 }}>
          {noItemsFoundText}
        </BoxTypography>
      )}
      <div className="MenuTableMobile__pagination">
        <EuiPagination
          pageCount={pageCount}
          activePage={pageIndex}
          onPageClick={(activePage) => goToPage(activePage)}
        />
      </div>
    </div>
  );
};
