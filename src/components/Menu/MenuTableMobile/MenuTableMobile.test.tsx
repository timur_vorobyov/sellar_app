import { cleanup } from '@testing-library/react';
import { screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { customI18nRender } from 'utils/testing/customRender';
import { MenuTableMobile } from './MenuTableMobile';
import { Locale } from 'types/translation';
import { filteredMenuItems } from '../../../__mocks__/mocks';

afterEach(cleanup);

const updateMenuItem = jest.fn();
const updateCustomisationOption = jest.fn();

describe('<MenuTableMobile> component', () => {
  it('should show no items found', () => {
    customI18nRender(
      <MenuTableMobile
        isMainTab={true}
        items={[]}
        updateMenuItemState={updateMenuItem}
        updateCustomisationOptionState={updateCustomisationOption}
      />,
      { locale: Locale.ENGLISH },
    );

    expect(screen.getByText('No items found')).toBeTruthy();
  });

  it('should show items', () => {
    customI18nRender(
      <MenuTableMobile
        isMainTab={true}
        items={filteredMenuItems}
        updateMenuItemState={updateMenuItem}
        updateCustomisationOptionState={updateCustomisationOption}
      />,
      { locale: Locale.ENGLISH },
    );

    expect(screen.getByText('Bee hoon')).toBeInTheDocument();
    expect(screen.getByText('Kuey Chap')).toBeInTheDocument();
  });
});
