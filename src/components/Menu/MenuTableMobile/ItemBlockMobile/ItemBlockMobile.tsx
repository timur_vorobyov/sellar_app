import { useEuiI18n } from '@elastic/eui';
import classnames from 'clsx';
import MoreMobile from '../MoreMobile/MoreMobile';
import { MenuItemActiveHours } from '../../../../types/models/menu';
import { EmptyObject } from '../../../../types/generics';
import MenuAvailability from '../../MenuAvailablity/MenuAvailablity';
import './ItemBlockMobile.scss';
import { Option } from 'types/generics';
import { useLanguageContext } from 'contexts/languageContextProvider/context';
interface ItemMobileBlockProps {
  name: string;
  filterName: {
    name: string;
    nameCn: string;
  };
  available: number;
  activeHours: EmptyObject | MenuItemActiveHours;
  onStatusChange: (value: number) => void;
}

export default function ItemBlockMobile({
  filterName,
  name,
  available,
  activeHours,
  onStatusChange,
}: ItemMobileBlockProps) {
  const { getLocale } = useLanguageContext();
  const [availableText, unavailableText] = useEuiI18n(
    ['menu.table.availabilityOptions.available', 'menu.table.availabilityOptions.unavailable'],
    [],
  );

  const availabilityOptions = [
    { value: 1, text: availableText } as Option,
    { value: 0, text: unavailableText } as Option,
  ];
  return (
    <div className="ItemBlockMobile__container">
      <div className="ItemBlockMobile__body">
        <div className="ItemBlockMobile__category">{getLocale() === 'en' ? filterName.name : filterName.nameCn}</div>
        <div className="ItemBlockMobile__name">{name}</div>
        <MenuAvailability activeHours={activeHours} />
      </div>
      <div className="ItemBlockMobile__footer">
        <div
          className={classnames(
            'ItemBlockMobile__available',
            { 'light-green': available === 1 },
            { 'light-orange': available === 0 },
          )}
        >
          {availabilityOptions.find((option) => option.value === available)?.text}
        </div>
        <MoreMobile
          availabilityOptions={availabilityOptions}
          available={available}
          name={name}
          onStatusChange={onStatusChange}
        />
      </div>
    </div>
  );
}
