import { cleanup } from '@testing-library/react';
import { screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import ItemBlockMobile from './ItemBlockMobile';
import { filteredMenuItems } from '../../../../__mocks__/mocks';
import { customI18nRender } from '../../../../utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

const onStatusChange = jest.fn();

describe('<ItemBlockMobile> component', () => {
  it('should render component', () => {
    customI18nRender(
      <ItemBlockMobile
        activeHours={filteredMenuItems[0].activeHours}
        name="Name"
        filterName={{
          name: 'Category',
          nameCn: 'Category',
        }}
        available={1}
        onStatusChange={onStatusChange}
      />,
      { locale: Locale.ENGLISH },
    );

    expect(screen.getByText('Name')).toBeTruthy();
    expect(screen.getByText('Category')).toBeTruthy();
  });
});
