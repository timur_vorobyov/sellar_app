import { cleanup, fireEvent } from '@testing-library/react';
import { screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import MoreMobile from './MoreMobile';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

const name = 'MoreMobile';
const onStatusChange = jest.fn();
const availabilityOptions = [
  {
    value: 1,
    text: 'Available',
  },
  {
    value: 0,
    text: 'UnAvailable',
  },
];

describe('<MoreMobile> component', () => {
  it('should render component and change status', () => {
    const { container } = customI18nRender(
      <MoreMobile
        name={name}
        availabilityOptions={availabilityOptions}
        available={1}
        onStatusChange={onStatusChange}
      />,
      { locale: Locale.ENGLISH },
    );

    const moreButton = container.getElementsByClassName('MoreMobile__button')[0];
    fireEvent.click(moreButton);
    const availabilityCheckbox = screen.getByText('UnAvailable');
    expect(availabilityCheckbox).toBeTruthy();

    fireEvent.click(availabilityCheckbox);
    expect(onStatusChange).toHaveBeenCalled();
  });
});
