import './MoreMobile.scss';
import MoreIcon from 'components/assets/images/MenuPage/moreIcon.svg';
import CheckIcon from 'components/assets/images/MenuPage/checkIcon.svg';
import { useState } from 'react';
import classnames from 'clsx';

interface AvailabilityOptions {
  value: number;
  text: string;
}

interface MoreMobileProps {
  availabilityOptions: AvailabilityOptions[];
  name: string;
  available: number;
  onStatusChange: (value: number) => void;
}

export default function MoreMobile({ availabilityOptions, available, name, onStatusChange }: MoreMobileProps) {
  const [showMore, setShowMore] = useState(false);

  return (
    <>
      <div className="MoreMobile__button" onClick={() => setShowMore(!showMore)}>
        <img src={MoreIcon} width="24" />
      </div>
      {showMore && (
        <div className="MoreMobile__container" onClick={() => setShowMore(false)}>
          <div className="MoreMobile__content">
            <div className="MoreMobile__header">{name}</div>
            <div className="MoreMobile__items">
              {availabilityOptions.map((option) => {
                return (
                  <div
                    key={option.value}
                    className={classnames('MoreMobile__item', { checked: available === option.value })}
                    onClick={() => onStatusChange(option.value)}
                  >
                    <div className="MoreMobile__item__check">
                      {available === option.value && <img src={CheckIcon} width="24" />}
                    </div>
                    <div>{option.text}</div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      )}
    </>
  );
}
