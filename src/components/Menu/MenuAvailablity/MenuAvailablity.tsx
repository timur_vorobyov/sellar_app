import React from 'react';
import moment from 'moment-timezone';
import { EmptyObject } from '../../../types/generics';
import { MenuItemActiveHours } from '../../../types/models/menu';
import { getWeekdayWithActiveHours, transformJSON } from '../utils';
import Typography from '../../ui-kit/Typography';
import { EuiI18n, useEuiI18n } from '@elastic/eui';
import { useLanguageContext } from '../../../contexts/languageContextProvider/context';
import Box from '../../ui-kit/Box';
import './MenuAvailability.scss';

const MenuAvailability = (props: ItemMobileBlockProps) => {
  const { activeHours } = props;
  const { getLocale } = useLanguageContext();
  const [availableOn, availableOnAllDays] = useEuiI18n(['menu.table.availableOn', 'menu.table.availableOnAllDays'], []);

  const currentWeekday = moment().tz('Asia/Singapore').format('dddd').toLowerCase();
  const transformedActiveHours = transformJSON(activeHours);
  let activeHoursComponent = null;
  let activeWeekdays = '';
  const activeDays: string[] = [];
  for (const day in transformedActiveHours) {
    if (transformedActiveHours[day].includes(transformedActiveHours[currentWeekday])) {
      activeDays.push(day);
    }
  }

  const weekdayWithActiveHours = getWeekdayWithActiveHours(activeDays, getLocale());
  if (transformedActiveHours[currentWeekday]) {
    activeHoursComponent = (
      <Typography variant="span" className="activeHoursText">
        <EuiI18n
          token="menu.table.availableFrom"
          default={''}
          values={{ openCloseTime: transformedActiveHours[currentWeekday] }}
        />
      </Typography>
    );
  }
  if (weekdayWithActiveHours.length) {
    activeWeekdays =
      weekdayWithActiveHours?.length === 7
        ? `${availableOnAllDays}`
        : `${availableOn} ${weekdayWithActiveHours.join(', ')}`;
  }

  return (
    <Box addClass="MenuAvailability" mt={10}>
      <Box>{activeHoursComponent}</Box>
      <Box>
        {activeWeekdays && (
          <Typography variant="span" className="activeHoursText">
            {activeWeekdays}
          </Typography>
        )}
      </Box>
    </Box>
  );
};

interface ItemMobileBlockProps {
  activeHours: EmptyObject | MenuItemActiveHours;
}

export default MenuAvailability;
