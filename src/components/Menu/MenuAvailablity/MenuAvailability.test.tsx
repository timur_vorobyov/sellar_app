import '@testing-library/jest-dom';
import { cleanup, fireEvent, screen } from '@testing-library/react';
import { filteredMenuItems } from '../../../__mocks__/mocks';
import { customI18nRender } from '../../../utils/testing/customRender';
import { Locale } from '../../../types/translation';
import MenuAvailability from './MenuAvailablity';

afterEach(cleanup);

describe('MenuAvailability component', () => {
  it('should render without fail', () => {
    const { container } = customI18nRender(<MenuAvailability activeHours={filteredMenuItems[0].activeHours} />, {
      locale: Locale.ENGLISH,
    });
    const selectAll = container.getElementsByClassName('activeHoursText')[0];
    expect(selectAll).toBeInTheDocument();
  });
});
