import { MenuItemActiveHours } from 'types/models/menu';
import { EmptyObject } from 'types/generics';
import moment from 'moment-timezone';

/** wait for backend
export const getCurrentAvailabilityOption = (item: MenuItem) => {
  const currentDate = moment().tz('Asia/Singapore');
  const currentWeekday = currentDate.format('dddd').toLowerCase();
  const currentTime = currentDate.format('hh:mm:ss');
  const unavailableUntil = null;
  const isAvailable = true;
  const activeHours: MenuItemActiveHours = {
    friday: {
      timings: [
      ]
    },
    saturday: {
      timings: [
      ]
    },
    sunday: {
      timings: [
      ]
    },
    monday: {
      timings: [
      ]
    },
    tuesday: {
      timings: [
      ]
    },
    thursday: {
      timings: [
      ]
    },
    wednesday: {
      timings: [
        {
          openTime: "05:00:00",
          closeTime: "06:00:00"
        }
      ]
    }
  }

  if (!isAvailable && !unavailableUntil) {
    return 0
  }

  if (!isAvailable && unavailableUntil) {
    const difference = moment(unavailableUntil).diff(moment(currentDate, "DD/MM/YYYY HH:mm:ss"));
    if (difference >= 0) {
      if (
        unavailableUntil <= currentDate.add(1, 'day').format("DD/MM/YYYY 00:00:00") &&
        unavailableUntil >= currentDate.format("DD/MM/YYYY 00:00:00")
      ) {
        return 1
      }
    }

    return 0
  }

  if (Object.prototype.hasOwnProperty.call(item.activeHours, currentWeekday)) {
    const timings = activeHours[currentWeekday as keyof MenuItemActiveHours].timings
    if (timings?.length) {
      const isOpened = convertToSingaporeTime(`${currentDate.format("YYYY-MM-DD")}T${timings[0].openTime}Z`, 'HH:mm:ss') >= currentTime &&
        convertToSingaporeTime(`${currentDate.format("YYYY-MM-DD")}T${timings[0].closeTime}Z`, 'HH:mm:ss') <= currentTime;
      if (!isOpened) {
        return 3
      }
    }
  }

  return 2
}
 */

export const getWeekdayWithActiveHours = (days: string[], locale: string) => {
  const sortedDays = moment.weekdays().filter((e1) => days.find((item) => item?.includes(e1.toLowerCase())));
  return sortedDays.map((day) => {
    switch (day.toLowerCase()) {
      case 'monday':
        return getWeekday(day, locale, '星期一');
      case 'tuesday':
        return getWeekday(day, locale, '星期二');
      case 'wednesday':
        return getWeekday(day, locale, '星期三');
      case 'thursday':
        return getWeekday(day, locale, '星期四');
      case 'friday':
        return getWeekday(day, locale, '星期四');
      case 'saturday':
        return getWeekday(day, locale, '星期六');
      case 'sunday':
        return getWeekday(day, locale, '星期日');
    }
  });
};

const getWeekday = (weekday: string, locale: string, chaneseName: string) => {
  const nameEn = capitalizeFirstLetter(weekday);
  const nameCn = nameEn + ' ' + chaneseName;
  return locale === 'en' ? nameEn : nameCn;
};

const capitalizeFirstLetter = (string: string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export function transformJSON(data: MenuItemActiveHours | EmptyObject) {
  const currentDate = moment().tz('Asia/Singapore');
  const weekDays: string[] = Object.keys(data);
  // eslint-disable-next-line
  const transformedData: any = {};
  weekDays.forEach((day: string) => {
    if (data[day as keyof MenuItemActiveHours]?.timings?.length) {
      transformedData[day] = data[day as keyof MenuItemActiveHours]?.timings
        ?.map((openCloseTime: { openTime: string; closeTime: string }) => {
          const openTime = moment(`${currentDate.format('YYYY-MM-DD')}T${openCloseTime.openTime}`).format('ha');
          const closeTime = moment(`${currentDate.format('YYYY-MM-DD')}T${openCloseTime.closeTime}`).format('ha');
          return openTime.concat(' - ', closeTime);
        })
        .join(', ');
    }
  });

  return transformedData;
}
