import { cleanup, fireEvent, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { MenuTable } from './MenuTable';
import { filteredMenuItems } from '../../../__mocks__/mocks';
import { customI18nRender } from '../../../utils/testing/customRender';
import { Locale } from '../../../types/translation';

afterEach(cleanup);

const updateMenuItemState = jest.fn();
const updateCustomisationOptionState = jest.fn();

describe('<MenuTable> component', () => {
  it('should show all items', () => {
    customI18nRender(
      <MenuTable
        items={filteredMenuItems}
        isMainTab={true}
        updateMenuItemState={updateMenuItemState}
        updateCustomisationOptionState={updateCustomisationOptionState}
      />,
      {
        locale: Locale.ENGLISH,
      },
    );
    const selectAll = screen.getByText('Showing 1 - 5 out of 22');
    expect(selectAll).toBeInTheDocument();
    expect(screen.getByText('Bee hoon')).toBeInTheDocument();
    fireEvent.click(screen.getByText('2'));
    expect(screen.getByText('Showing 6 - 10 out of 22')).toBeInTheDocument();
    fireEvent.change(screen.getByTestId('menuitem_options_1589'), {
      target: { value: 0 },
    });
    expect(updateMenuItemState).toHaveBeenCalled();
  });
});
