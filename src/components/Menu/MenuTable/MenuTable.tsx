import { useState } from 'react';
import { CriteriaWithPagination, EuiBasicTable, EuiBasicTableColumn, EuiSelect, useEuiI18n } from '@elastic/eui';
import { CustomisationOption, MenuItem } from 'types/models/menu';
import { updateCustomisationOption, updateMenuItem } from 'apis/rest/catalog';
import useExplicitAsyncCall from 'utils/hooks/use-explicit-api-call';
import Typography from 'components/ui-kit/Typography';
import { useLanguageContext } from 'contexts/languageContextProvider/context';
import MenuAvailability from '../MenuAvailablity/MenuAvailablity';
import './MenuTable.scss';

interface MenuTableProps {
  items: MenuItem[] | CustomisationOption[];
  isMainTab: boolean;
  updateMenuItemState: (menuItem: MenuItem) => void;
  updateCustomisationOptionState: (customisationOption: CustomisationOption) => void;
}

export const MenuTable = ({
  items,
  isMainTab,
  updateMenuItemState,
  updateCustomisationOptionState,
}: MenuTableProps) => {
  const [pageIndex, setPageIndex] = useState(0);
  const [pageSize, setPageSize] = useState(5);
  const { getLocale } = useLanguageContext();

  const [availableText, unavailableText] = useEuiI18n(
    ['menu.table.availabilityOptions.available', 'menu.table.availabilityOptions.unavailable'],
    [],
  );

  const availabilityOptions = [
    { value: 1, text: availableText },
    { value: 0, text: unavailableText },
  ];

  const [
    serialNumberColumnText,
    categoryColumnText,
    itemNameColumnText,
    availabilityColumnText,
    customisationColumnText,
    customisationOptionColumnText,
    noItemsFoundText,
  ] = useEuiI18n(
    [
      'menu.table.columns.serialNumber',
      'menu.table.columns.category',
      'menu.table.columns.itemName',
      'menu.table.columns.availability',
      'menu.table.columns.customisation',
      'menu.table.columns.customisationOption',
      'menu.table.noItemsFound',
      'menu.table.availableOn',
      'menu.table.availableOnAllDays',
    ],
    [],
  );

  const { execute: executeUpdateMenuItem } = useExplicitAsyncCall(updateMenuItem);
  const { execute: executeUpdateCustomisationOption } = useExplicitAsyncCall(updateCustomisationOption);

  const handleMenuItemChange = (menuItem: MenuItem) => {
    updateMenuItemState(menuItem);
    executeUpdateMenuItem(menuItem);
  };

  const handleCustomisationOptionChange = (customisationOption: CustomisationOption) => {
    updateCustomisationOptionState(customisationOption);
    executeUpdateCustomisationOption(customisationOption);
  };

  const columns = isMainTab
    ? [
        {
          field: 'id',
          name: serialNumberColumnText,
          width: '10%',
          render: (id: number) => <Typography variant="span">{id}</Typography>,
        },
        {
          field: 'category',
          name: categoryColumnText,
          width: '35%',
          truncateText: true,
          render: (category: { name: string; nameCn: string }) => (
            <Typography variant="span">{getLocale() === 'en' ? category.name : category.nameCn}</Typography>
          ),
        },
        {
          name: itemNameColumnText,
          width: '35%',
          render: (record: MenuItem) => {
            let name = getLocale() === 'en' ? record.name : record.nameCn;
            if (!name) {
              name = record.name;
            }
            return (
              <div className="Table__itemNameContainer">
                <Typography variant="span">{name}</Typography>
                <MenuAvailability activeHours={record.activeHours} />
              </div>
            );
          },
        },
        {
          field: 'available',
          name: availabilityColumnText,
          width: '30%',
          render: (available: number, record: MenuItem) => (
            <EuiSelect
              aria-label={`Availability for ${record.id}`}
              data-testid={`menuitem_options_${record.id}`}
              className="Table__select"
              value={available}
              options={availabilityOptions}
              onChange={(event) =>
                handleMenuItemChange({
                  ...record,
                  available: Number(event.target.value),
                })
              }
            />
          ),
        },
      ]
    : [
        {
          field: 'id',
          name: serialNumberColumnText,
          width: '10%',
          render: (id: number) => <Typography variant="span">{id}</Typography>,
        },
        {
          name: customisationColumnText,
          width: '35%',
          truncateText: true,
          render: (record: CustomisationOption) => {
            let name = getLocale() === 'en' ? record.customisation.name : record.customisation.nameCn;

            if (!name) {
              name = record.customisation.name;
            }

            return <Typography variant="span">{name}</Typography>;
          },
        },
        {
          name: customisationOptionColumnText,
          width: '35%',
          render: (record: CustomisationOption) => {
            let name = getLocale() === 'en' ? record.name : record.nameCn;

            if (!name) {
              name = record.name;
            }

            return <Typography variant="span">{name}</Typography>;
          },
        },
        {
          field: 'available',
          name: availabilityColumnText,
          width: '30%',
          render: (available: number, record: CustomisationOption) => (
            <EuiSelect
              aria-label="Availability"
              className="Table__select"
              value={available}
              options={availabilityOptions}
              onChange={(event) =>
                handleCustomisationOptionChange({
                  ...record,
                  available: Number(event.target.value),
                })
              }
            />
          ),
        },
      ];

  const onTableChange = (criteria: CriteriaWithPagination<MenuItem | CustomisationOption>) => {
    const { index: pageIndex, size: pageSize } = criteria?.page || {};
    setPageIndex(pageIndex);
    setPageSize(pageSize);
  };

  const fromItem = pageIndex ? pageIndex * pageSize : 0;
  const toItem = pageSize * (pageIndex + 1);
  const pageOfItems = items.slice(fromItem, toItem);
  const totalItemCount = items.length;

  const pagination = {
    pageIndex,
    pageSize,
    totalItemCount,
    pageSizeOptions: [5, 10, 15],
  };

  return (
    <div className="MenuTable_container">
      <div className="MenuTable_itemsInfo">
        <Typography variant="span">{`Showing ${fromItem + 1} - ${
          toItem > totalItemCount ? totalItemCount : toItem
        } out of ${totalItemCount}`}</Typography>
      </div>
      <EuiBasicTable
        columns={columns as EuiBasicTableColumn<MenuItem | CustomisationOption>[]}
        items={pageOfItems as MenuItem[] | CustomisationOption[]}
        itemId="id"
        pagination={pagination}
        isSelectable={true}
        onChange={onTableChange}
        tableLayout="auto"
        noItemsMessage={noItemsFoundText}
      />
    </div>
  );
};
