import { cleanup, fireEvent, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { customI18nRender } from 'utils/testing/customRender';
import { StateProvider } from 'contexts/store';
import { Locale } from 'types/translation';
import MenuTabs from './MenuTabs';
import { MenuTableMobile } from '../MenuTableMobile/MenuTableMobile';
import { filteredCustomisationOptions, filteredMenuItems } from '__mocks__/mocks';
import { DeviceContextProvider } from 'contexts/deviceContextProvider/deviceContextProvider';

afterEach(cleanup);

const updateMenuItemState = jest.fn();
const updateCustomisationOptionState = jest.fn();
describe('<MenuTabs>', () => {
  it('should render component', () => {
    const { container } = customI18nRender(
      <StateProvider>
        <DeviceContextProvider width={500}>
          <MenuTabs
            firstTabContent={
              <div className="MenuAvailabilityMobile__container">
                <MenuTableMobile
                  items={filteredMenuItems}
                  isMainTab={true}
                  updateMenuItemState={updateMenuItemState}
                  updateCustomisationOptionState={updateCustomisationOptionState}
                />
              </div>
            }
            secondTabContent={
              <div className="MenuAvailabilityMobile__container">
                <MenuTableMobile
                  items={filteredCustomisationOptions}
                  isMainTab={false}
                  updateMenuItemState={updateMenuItemState}
                  updateCustomisationOptionState={updateCustomisationOptionState}
                />
              </div>
            }
            onMenuFiltersChange={jest.fn()}
            onCustomisationOptionFiltersChange={jest.fn()}
            onShowFilterMobile={jest.fn()}
            menuCategories={[]}
            customisations={[]}
          />
        </DeviceContextProvider>
      </StateProvider>,
      { locale: Locale.ENGLISH },
    );
    expect(screen.getByText('Main Menu')).toBeInTheDocument();
    expect(screen.getByText('Customisations')).toBeInTheDocument();
    expect(screen.getByText('Showing 1 - 10 out of 22')).toBeInTheDocument();
    expect(container.querySelectorAll('.ItemBlockMobile__container').length).toBe(10);
  });

  it('should change the tab on click', () => {
    customI18nRender(
      <StateProvider>
        <DeviceContextProvider width={500}>
          <MenuTabs
            firstTabContent={
              <div className="MenuAvailabilityMobile__container">
                <MenuTableMobile
                  items={filteredMenuItems}
                  isMainTab={true}
                  updateMenuItemState={updateMenuItemState}
                  updateCustomisationOptionState={updateCustomisationOptionState}
                />
              </div>
            }
            secondTabContent={
              <div className="MenuAvailabilityMobile__container">
                <MenuTableMobile
                  items={filteredCustomisationOptions}
                  isMainTab={false}
                  updateMenuItemState={updateMenuItemState}
                  updateCustomisationOptionState={updateCustomisationOptionState}
                />
              </div>
            }
            onMenuFiltersChange={jest.fn()}
            onCustomisationOptionFiltersChange={jest.fn()}
            onShowFilterMobile={jest.fn()}
            menuCategories={[]}
            customisations={[]}
          />
        </DeviceContextProvider>
      </StateProvider>,
      { locale: Locale.ENGLISH },
    );

    const CustomisationsTab = screen.getByText('Customisations');
    fireEvent.click(CustomisationsTab);
    expect(screen.getByText('Showing 1 - 3 out of 3')).toBeInTheDocument();
  });
});
