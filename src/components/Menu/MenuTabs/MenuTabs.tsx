import { useEffect, useState } from 'react';
import { EuiTab, EuiFieldSearch, EuiTabs, useEuiI18n } from '@elastic/eui';
import './MenuTabs.scss';
import { Filter } from '../Filter/Filter';
import { CustomisationOptionFilter, MenuFilter } from 'types/pages';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import FilterIcon from 'components/assets/images/MenuPage/filterIcon.svg';
import { FilterMobile } from 'components/PDA/FilterMobile/FilterMobile';
import HeaderMobile from 'components/PDA/HeaderMobile/HeaderMobile';
import { Option } from 'types/generics';

interface MenuTabsProps {
  menuCategories: Option[];
  customisations: Option[];
  firstTabContent: JSX.Element | null;
  secondTabContent: JSX.Element | null;
  onMenuFiltersChange: (filters: MenuFilter) => void;
  onCustomisationOptionFiltersChange: (filters: CustomisationOptionFilter) => void;
  onShowFilterMobile: (value: boolean) => void;
}

const MenuTabs = ({
  menuCategories,
  customisations,
  firstTabContent,
  secondTabContent,
  onMenuFiltersChange,
  onCustomisationOptionFiltersChange,
  onShowFilterMobile,
}: MenuTabsProps) => {
  const { isMobile, isTablet } = useDeviceContext();
  const [showFilterMobile, setShowFilterMobile] = useState(false);
  const [filterItemsNumber, setFilterItemsNumber] = useState(0);

  const [choosenTab, setChoosenTab] = useState('first');

  const [menuFilters, setMenuFilters] = useState<MenuFilter>({
    category: [],
    availability: [],
    searchTerm: '',
  });
  const [customisationFilters, setCustomisationFilters] = useState<CustomisationOptionFilter>({
    customisationIds: [],
    availability: [],
    searchTerm: '',
  });

  const [
    mainMenuTabText,
    customisationOptionsTabText,
    categoryFilterText,
    customisationsFilterText,
    availabilityFilterText,
    searchFieldPlaceholderText,
    availableText,
    unavailableText,
    filterText,
  ] = useEuiI18n(
    [
      'menu.tabs.mainMenu',
      'menu.tabs.customisationOptions',
      'menu.tabs.filters.category',
      'menu.tabs.filters.customisations',
      'menu.tabs.filters.availability',
      'menu.tabs.searchField.placeholder',
      'menu.table.availabilityOptions.available',
      'menu.table.availabilityOptions.unavailable',
      'menu.filters.filters',
    ],
    [],
  );

  const availabilityOptions = [
    { value: 1, text: availableText },
    { value: 0, text: unavailableText },
  ];

  useEffect(() => {
    if (choosenTab == 'first' && menuCategories) {
      setMenuFilters({
        ...menuFilters,
        category: menuCategories.map((option) => option.value),
        availability: availabilityOptions.map((option) => option.value),
      });
    }
    if (choosenTab == 'second' && customisations) {
      setCustomisationFilters({
        ...customisationFilters,
        customisationIds: customisations.map((option) => option.value),
        availability: availabilityOptions.map((option) => option.value),
      });
    }
  }, [choosenTab, menuCategories, customisations]);

  useEffect(() => {
    if (choosenTab == 'first') {
      onMenuFiltersChange(menuFilters);
      const selectedLength = menuFilters.category.length + menuFilters.availability.length;
      const OptionsLength = menuCategories.length + availabilityOptions.length;
      setFilterItemsNumber(selectedLength === OptionsLength ? 0 : selectedLength);
    } else {
      onCustomisationOptionFiltersChange(customisationFilters);
      setFilterItemsNumber(
        customisationFilters.customisationIds.length === customisations.length
          ? 0
          : customisationFilters.customisationIds.length,
      );
    }
  }, [choosenTab, menuFilters, customisationFilters]);

  useEffect(() => {
    onShowFilterMobile(showFilterMobile);
  }, [showFilterMobile]);

  return showFilterMobile ? (
    <>
      {choosenTab === 'first' && (
        <>
          <HeaderMobile text={filterText} onButtonClick={() => setShowFilterMobile(false)} />
          <FilterMobile
            label={`${categoryFilterText}:`}
            secondLabel={`${availabilityFilterText}:`}
            options={menuCategories}
            secondOptions={availabilityOptions}
            selected={menuFilters.category}
            secondSelected={menuFilters.availability}
            onApply={(selected: number[], secondSelected: number[]) =>
              setMenuFilters({ ...menuFilters, category: selected, availability: secondSelected })
            }
            backToMenu={() => setShowFilterMobile(false)}
          />
        </>
      )}
      {choosenTab === 'second' && (
        <>
          <HeaderMobile text={filterText} onButtonClick={() => setShowFilterMobile(false)} />
          <FilterMobile
            label={`${customisationsFilterText}:`}
            secondLabel={`${availabilityFilterText}:`}
            options={customisations}
            secondOptions={availabilityOptions}
            selected={customisationFilters.customisationIds}
            secondSelected={customisationFilters.availability}
            onApply={(selected: number[], secondSelected: number[]) =>
              setCustomisationFilters({
                ...customisationFilters,
                customisationIds: selected,
                availability: secondSelected,
              })
            }
            backToMenu={() => setShowFilterMobile(false)}
          />
        </>
      )}
    </>
  ) : (
    <div className="MenuTabs__container">
      <div className="MenuTabs__container__header">
        <EuiTabs>
          <EuiTab isSelected={choosenTab === 'first'} onClick={() => setChoosenTab('first')}>
            {mainMenuTabText}
          </EuiTab>
          <EuiTab isSelected={choosenTab === 'second'} onClick={() => setChoosenTab('second')}>
            {isMobile && customisationsFilterText}
            {isTablet && customisationOptionsTabText}
          </EuiTab>
        </EuiTabs>
        <div className="MenuTabs__container__search">
          <EuiFieldSearch
            placeholder={searchFieldPlaceholderText}
            value={choosenTab === 'first' ? menuFilters.searchTerm : customisationFilters.searchTerm}
            onChange={(event) => {
              choosenTab === 'first'
                ? setMenuFilters({ ...menuFilters, searchTerm: event?.target.value })
                : setCustomisationFilters({ ...customisationFilters, searchTerm: event?.target.value });
            }}
            isClearable={true}
          />
          {isMobile && (
            <div className="MenuTabs__mobile__filters__button" onClick={() => setShowFilterMobile(true)}>
              {!!filterItemsNumber && <div className="MenuTabs__mobile__filters__number">{filterItemsNumber}</div>}
              <img src={FilterIcon} width="24" alt="Filter Icon" />
            </div>
          )}
        </div>
      </div>
      <div className="MenuTabs__container__filters">
        {choosenTab === 'first' ? (
          <Filter
            label={`${categoryFilterText}:`}
            options={menuCategories}
            selected={menuFilters.category}
            onApply={(selected: number[]) => setMenuFilters({ ...menuFilters, category: selected })}
          />
        ) : (
          <Filter
            label={`${customisationsFilterText}:`}
            options={customisations}
            selected={customisationFilters.customisationIds}
            onApply={(selected: number[]) =>
              setCustomisationFilters({ ...customisationFilters, customisationIds: selected })
            }
          />
        )}
        <Filter
          label={`${availabilityFilterText}:`}
          options={availabilityOptions}
          selected={choosenTab === 'first' ? menuFilters.availability : customisationFilters.availability}
          onApply={(selected: number[]) =>
            choosenTab === 'first'
              ? setMenuFilters({ ...menuFilters, availability: selected })
              : setCustomisationFilters({ ...customisationFilters, availability: selected })
          }
        />
      </div>
      {choosenTab === 'first' ? firstTabContent : secondTabContent}
    </div>
  );
};

export default MenuTabs;
