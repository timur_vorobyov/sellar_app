import { PropsWithChildren } from 'react';
import './NavBarTablet.scss';
import { useLocation, useHistory } from 'react-router-dom';
import { routes } from 'config/routes';
import { EuiSuperSelect, useEuiI18n } from '@elastic/eui';

interface NavBarTabletProps {
  append?: JSX.Element;
}

const NavBarTablet = (props: PropsWithChildren<NavBarTabletProps>): JSX.Element => {
  const history = useHistory();
  const location = useLocation();

  const topNavBarOptions = Object.values(routes)
    .filter((el) => el.title && !el.mobile)
    .map((el) => {
      const routeName = useEuiI18n(el.title || '', el.title || '');

      return {
        value: el.path,
        inputDisplay: <span className="NavBarTablet__navItem">{routeName}</span>,
      };
    });

  return (
    <nav className="NavBarTablet__container">
      <EuiSuperSelect
        data-testid="nav_selector"
        fullWidth={true}
        className="NavBarTablet__selector"
        options={topNavBarOptions}
        valueOfSelected={location.pathname}
        itemClassName="NavBarTablet__selector--item"
        onChange={history.push}
      />

      {props.append && <div>{props.append}</div>}
    </nav>
  );
};
export default NavBarTablet;
