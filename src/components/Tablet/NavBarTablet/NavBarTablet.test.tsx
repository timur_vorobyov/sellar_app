import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { BrowserRouter } from 'react-router-dom';
import NavBarTablet from './NavBarTablet';
import { RouteId, routes } from 'config/routes';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<NavBarTablet> component', () => {
  it('should render component and change to Menu page', () => {
    customI18nRender(
      <BrowserRouter>
        <NavBarTablet />
      </BrowserRouter>,
      { locale: Locale.CHINESE },
    );

    const navSelector = screen.getByTestId('nav_selector');
    fireEvent.click(navSelector);
    const menuRoute = screen.getByText('Menu Availability 菜单销售');
    fireEvent.click(menuRoute);
    expect(window.location.href).toBe('http://localhost' + routes[RouteId.Menu].path);
  });

  it('should render component and change to Stall page', () => {
    customI18nRender(
      <BrowserRouter>
        <NavBarTablet />
      </BrowserRouter>,
      { locale: Locale.CHINESE },
    );

    const navSelector = screen.getByTestId('nav_selector');
    fireEvent.click(navSelector);
    const stallRoute = screen.getByText('Stall Settings 店铺设置');
    fireEvent.click(stallRoute);
    expect(window.location.href).toBe('http://localhost' + routes[RouteId.Stall].path);
  });

  it('should render component and change to Order History page', () => {
    customI18nRender(
      <BrowserRouter>
        <NavBarTablet />
      </BrowserRouter>,
      { locale: Locale.CHINESE },
    );

    const navSelector = screen.getByTestId('nav_selector');
    fireEvent.click(navSelector);
    const orderHistoryRoute = screen.getByText('Order History 订单历史');
    fireEvent.click(orderHistoryRoute);
    expect(window.location.href).toBe('http://localhost' + routes[RouteId.History].path);
  });

  it('should render append component', () => {
    render(
      <BrowserRouter>
        <NavBarTablet append={<div>Append element</div>} />
      </BrowserRouter>,
    );

    expect(screen.getByText('Append element')).toBeInTheDocument();
  });
});
