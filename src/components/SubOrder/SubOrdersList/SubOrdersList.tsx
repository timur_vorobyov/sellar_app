import { SubOrder } from 'types/models/subOrder';
import SubOrderBlock from './SubOrderBlock/SubOrderBlock';

import './SubOrdersList.scss';

interface SubOrdersListProps {
  subOrders: SubOrder[];
  checkedSubOrderId: number;
  isAccepted?: boolean;
  onSubOrderClick: (subOrder: SubOrder) => void;
  onSubOrderExpire?: (subOrder: SubOrder) => void;
  onSubOrderPickup?: (subOrder: SubOrder) => void;
}

export const SubOrdersList = ({
  subOrders,
  checkedSubOrderId,
  isAccepted = false,
  onSubOrderClick,
  onSubOrderExpire,
  onSubOrderPickup,
}: SubOrdersListProps) => {
  return (
    <ul className="SubOrderList">
      {subOrders.map((subOrder) => (
        <li key={subOrder.id} onClick={() => onSubOrderClick(subOrder)}>
          <SubOrderBlock
            subOrder={subOrder}
            isAccepted={isAccepted}
            isChecked={subOrder.id === checkedSubOrderId}
            onExpire={(subOrder) => {
              onSubOrderExpire && onSubOrderExpire(subOrder);
            }}
            onPickup={(subOrder) => {
              onSubOrderPickup && onSubOrderPickup(subOrder);
            }}
          />
        </li>
      ))}
    </ul>
  );
};
