import TimerIcon from 'components/assets/images/SubOrderPage/timer-orange.svg';
import { useEuiI18n } from '@elastic/eui';
import Timer from '../../Timer/Timer';
import { CancelledSubOrder, SubOrder, SubOrderState } from 'types/models/subOrder';
import classnames from 'clsx';
import './SubOrderBlock.scss';
import { printCancelledReceipt } from 'utils/receipt';
import useExplicitApiCall from 'utils/hooks/use-explicit-api-call';
import { markCancelSubOrderReceiptPrinted } from 'apis/rest/subOrders';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import useCommonTranslations from 'utils/hooks/user-common-translations';
import { useStateContext } from 'contexts/store';
import { useState } from 'react';
import { convertToSingaporeTime } from 'utils/datetime';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import Typography from 'components/ui-kit/Typography';
import Box from 'components/ui-kit/Box';

interface SubOrderBlockProps {
  subOrder: SubOrder;
  isAccepted: boolean;
  isChecked: boolean;
  onExpire?: (subOrder: SubOrder) => void;
  onPickup?: (subOrder: SubOrder) => void;
}

export default function SubOrderBlock({
  subOrder,
  isAccepted = false,
  isChecked,
  onExpire,
  onPickup,
}: SubOrderBlockProps) {
  const { execute: markOrderAsCancelPrinted } = useExplicitApiCall(markCancelSubOrderReceiptPrinted);
  const { isTablet } = useDeviceContext();

  const {
    state: { newIncomigSubOrders },
  } = useStateContext();
  const createTime = convertToSingaporeTime(subOrder.createdAt, 'hh:mm A');
  const pickedUpTime = subOrder.pickUpTime ? convertToSingaporeTime(subOrder.pickUpTime, 'hh:mm A') : '----';
  const [isClicked, setIsClicked] = useState(false);
  const handleCancel = (subOrder: CancelledSubOrder) => {
    setIsClicked(true);
    printCancelledReceipt(subOrder, isTablet);
    markOrderAsCancelPrinted(subOrder, true);
  };
  const [
    orderCancelledText,
    printReceiptButtonText,
    dineInTimeText,
    pickUpTimeText,
    readyToCollectText,
    acceptInText,
    newText,
  ] = useEuiI18n(
    [
      'subOrders.subOrderBlock.cancelled.state',
      'subOrders.subOrderBlock.cancelled.printReceipt',
      'subOrders.subOrderBlock.dineInTime',
      'subOrders.subOrderBlock.pickUpTime',
      'subOrders.subOrderBlock.readyToCollect',
      'subOrders.subOrderBlock.acceptIn',
      'subOrders.subOrderBlock.new',
    ],
    [],
  );

  const productsAmountText = useEuiI18n('subOrders.subOrderBlock.productsAmount', '', {
    count: subOrder.productsAmount || 0,
  });

  const { orderTypes: orderTypeTranslations } = useCommonTranslations();

  const getSubOrderBlockFooter = () => {
    if (subOrder.state === SubOrderState.Cancelled && !subOrder.isCancellationReceiptPrinted) {
      return (
        <>
          <Box addClass="SubOrderBlock__footer__time-section">
            <Typography fontWeight="bold" variant="span">
              {orderCancelledText}
            </Typography>
          </Box>
          <CustomButton
            size="md"
            padding="no"
            color="orange"
            disabled={isClicked}
            onClick={() => handleCancel(subOrder)}
            text={printReceiptButtonText}
          />
        </>
      );
    } else {
      return (
        <>
          <Box addClass="SubOrderBlock__footer__time-section">
            {subOrder.type === 'having_here' ? (
              <Typography variant="span">{dineInTimeText}</Typography>
            ) : (
              <Typography variant="span">{pickUpTimeText}</Typography>
            )}

            <span className="SubOrderBlock__footerTime-withButton">{pickedUpTime}</span>
          </Box>
          <CustomButton
            padding="no"
            size="md"
            onClick={() => {
              onPickup && onPickup(subOrder);
              setIsClicked(true);
            }}
            disabled={isClicked}
            text={readyToCollectText}
          />
        </>
      );
    }
  };

  return (
    <>
      {isAccepted ? (
        <div className={classnames('SubOrderBlock__container', { checked: isChecked })}>
          <div className={classnames({ SubOrderBlock__triangle: isChecked })}></div>
          <div className="SubOrderBlock__header">
            <div>
              <span>{createTime} | </span>
              <span className="SubOrderBlock__headerOrderType">{orderTypeTranslations[subOrder.type]}</span>
            </div>
          </div>
          <div className="SubOrderBlock__body">
            <div className="SubOrderBlock__body__orderDetails">
              <img src={subOrder.logo} alt="logo" />
              <span className="SubOrderBlock__orderNumber">{subOrder.orderId}</span>
            </div>
          </div>
          <div className="SubOrderBlock__footer withButton">{getSubOrderBlockFooter()}</div>
        </div>
      ) : (
        <div className={classnames('SubOrderBlock__container', { checked: isChecked })}>
          <div className={classnames({ SubOrderBlock__triangle: isChecked })}></div>
          <div className="SubOrderBlock__header">
            <div className="SubOrderBlock__header__left-column">
              <span>{createTime} | </span>
              <span className="SubOrderBlock__headerOrderType">{orderTypeTranslations[subOrder.type]}</span>
            </div>
            {newIncomigSubOrders[subOrder.id] ? (
              <div className="SubOrderBlock__header__right-column">
                <span>{newText}</span>
              </div>
            ) : null}
          </div>
          <div className="SubOrderBlock__body">
            <div className="SubOrderBlock__body__orderDetails">
              <img src={subOrder.logo} alt="logo" />
              <span className="SubOrderBlock__orderNumber">{subOrder.orderId}</span>
            </div>
            <div className="SubOrderBlock__body__text">{productsAmountText}</div>
          </div>

          <div className="SubOrderBlock__footer">
            {subOrder.isAutoAccepted ? (
              <>
                <Box addClass="SubOrderBlock__footer__auto-accepted">
                  {subOrder.type === 'having_here' ? (
                    <Typography variant="span">{dineInTimeText}</Typography>
                  ) : (
                    <Typography variant="span">{pickUpTimeText}</Typography>
                  )}

                  <span className="SubOrderBlock__auto-accepted__footerTime">{pickedUpTime}</span>
                </Box>
              </>
            ) : (
              <>
                <Typography variant="span" className="SubOrderBlock__footerText">
                  {acceptInText}
                </Typography>
                <img alt="Timer Icon" src={TimerIcon} />
                <span className="SubOrderBlock__footerTime">
                  <Timer id={subOrder.id} onExpire={() => onExpire && onExpire(subOrder)} />
                </span>
              </>
            )}
          </div>
        </div>
      )}
    </>
  );
}
