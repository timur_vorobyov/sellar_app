import '@testing-library/jest-dom';
import { render, screen, cleanup } from '@testing-library/react';
import SubOrderBlock from './SubOrderBlock';
import { customI18nRender } from 'utils/testing/customRender';
import { subOrder as submittedSubOrder } from '__mocks__/mocks';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<SubOrderBlock> component', () => {
  it('should render component with data as incoming', () => {
    customI18nRender(<SubOrderBlock subOrder={submittedSubOrder} isAccepted={false} isChecked={false} />, {
      locale: Locale.CHINESE,
    });

    expect(screen.getAllByRole('img')[0]).toHaveAttribute('src', submittedSubOrder.logo);
    expect(screen.getByText(submittedSubOrder.orderId)).toBeInTheDocument();
    expect(screen.getByText('Accept in 时间内接受')).toBeInTheDocument();
  });

  it('should render component with data as accepted', () => {
    customI18nRender(<SubOrderBlock subOrder={submittedSubOrder} isAccepted={true} isChecked={false} />, {
      locale: Locale.CHINESE,
    });

    expect(screen.getByText('Pick-up Time 接取时间')).toBeInTheDocument();
    expect(screen.getByText('Ready to Collect 准备接取')).toBeInTheDocument();
  });

  it('should add checked selector', () => {
    const { container } = render(<SubOrderBlock subOrder={submittedSubOrder} isAccepted={false} isChecked={true} />);

    const checked = container.getElementsByClassName('SubOrderBlock__triangle')[0];

    expect(checked).toBeInTheDocument();
  });
});
