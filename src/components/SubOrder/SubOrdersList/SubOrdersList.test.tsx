import '@testing-library/jest-dom';
import { screen, cleanup } from '@testing-library/react';
import { SubOrdersList } from './SubOrdersList';
import { subOrders } from '__mocks__/mocks';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<OrdersList> component', () => {
  it('should render list of sub orders as incoming', () => {
    customI18nRender(
      <SubOrdersList subOrders={subOrders} checkedSubOrderId={1} isAccepted={false} onSubOrderClick={() => null} />,
      {
        locale: Locale.CHINESE,
      },
    );

    const firstOrder = screen.getAllByText('Accept in 时间内接受')[0];
    const secondOrder = screen.getAllByText('Accept in 时间内接受')[1];
    const firstOrderLogo = screen.getAllByAltText('logo')[0];
    const secondOrderLogo = screen.getAllByAltText('logo')[1];

    expect(firstOrder).toBeInTheDocument();
    expect(secondOrder).toBeInTheDocument();
    expect(firstOrderLogo).toHaveAttribute('src', subOrders[0].logo);
    expect(secondOrderLogo).toHaveAttribute('src', subOrders[1].logo);
  });

  it('should render list of sub orders as accepted', () => {
    customI18nRender(
      <SubOrdersList subOrders={subOrders} checkedSubOrderId={1} isAccepted={true} onSubOrderClick={() => null} />,
      {
        locale: Locale.CHINESE,
      },
    );

    const firstOrder = screen.getByText('Dine in time 及时用餐');
    const secondOrder = screen.getByText('Pick-up Time 接取时间');

    expect(firstOrder).toBeInTheDocument();
    expect(secondOrder).toBeInTheDocument();
  });
});
