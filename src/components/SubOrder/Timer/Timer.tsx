import { useState, useEffect } from 'react';
import { useStateContext } from 'contexts/store';
import './Timer.scss';

interface TimerProps {
  id: number;
  onExpire: () => void;
}

export default function Timer({ id, onExpire }: TimerProps) {
  const [expireIn, setExpireIn] = useState<number>(1);

  const {
    state: { incomingSubOrdersTimers },
  } = useStateContext();

  useEffect(() => {
    if (expireIn === 0) {
      onExpire && onExpire();
    }
    const seconds = incomingSubOrdersTimers.find((el) => el.subOrderId === id)?.expireIn;
    setExpireIn(seconds ? seconds : 0);
  }, [incomingSubOrdersTimers]);

  const min = Math.floor(expireIn / 60);
  const sec = expireIn - min * 60;

  return (
    <span className="Timer__container">
      {min}m:{sec < 10 ? '0' + sec : sec}s
    </span>
  );
}
