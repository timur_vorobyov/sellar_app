import '@testing-library/jest-dom';
import { render, screen, cleanup, act } from '@testing-library/react';
import Timer from './Timer';
import { useEffect } from 'react';
import { useStateContext, StateProvider } from 'contexts/store';
import { ActionTypes } from 'types/store';
import { subOrders } from '__mocks__/mocks';

afterEach(cleanup);

const onExpire = jest.fn();
interface TimerWithContextProps {
  id: number;
}
const TimerWithContext = ({ id }: TimerWithContextProps) => {
  const { dispatch } = useStateContext();
  useEffect(
    () =>
      dispatch({
        type: ActionTypes.SET_INCOMING_SUB_ORDERS,
        payload: { subOrders },
      }),
    [],
  );
  return <Timer id={id} onExpire={onExpire} />;
};

describe('<Timer> component', () => {
  it('should render Timer with expiration time', () => {
    render(
      <StateProvider>
        <TimerWithContext id={1} />
      </StateProvider>,
    );
    const timer = screen.getByText('5m:00s');
    expect(timer).toBeInTheDocument();
  });

  it('should call onExpire when timer ends', () => {
    jest.useFakeTimers();
    render(
      <StateProvider>
        <TimerWithContext id={2} />
      </StateProvider>,
    );
    act(() => {
      jest.advanceTimersByTime(11000);
    });
    expect(onExpire).toHaveBeenCalled();
  });
});
