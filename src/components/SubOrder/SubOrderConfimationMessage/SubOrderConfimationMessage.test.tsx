import '@testing-library/jest-dom';
import { screen, cleanup } from '@testing-library/react';
import SubOrderConfimationMessage from './SubOrderConfimationMessage';
import ConfirmOrderIcon from 'components/assets/images/SubOrderPage/confirmOrderIcon.svg';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

const pickUpTime = '12:13AM';

describe('<SubOrderConfimationMessage> component', () => {
  it('should render component', () => {
    customI18nRender(<SubOrderConfimationMessage pickUpTime={pickUpTime} />, { locale: Locale.CHINESE });

    expect(screen.getByRole('img')).toHaveAttribute('src', ConfirmOrderIcon);
    expect(
      screen.getByText(`Order accepted. Pick-up at ${pickUpTime} 已接受订单。接取时间为 ${pickUpTime}.`),
    ).toBeInTheDocument();
    expect(
      screen.getByText('You may also view order details on the printed receipt. 您也能在已印刷的收据上查看订单详细。'),
    ).toBeInTheDocument();
  });
});
