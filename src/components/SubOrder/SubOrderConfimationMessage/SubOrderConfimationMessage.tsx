import { useEuiI18n } from '@elastic/eui';
import ConfirmOrderIcon from 'components/assets/images/SubOrderPage/confirmOrderIcon.svg';
import { DateString } from 'types/generics';
import './SubOrderConfimationMessage.scss';

interface SubOrderConfimationMessageProps {
  pickUpTime: DateString | null;
}

export default function SubOrderConfimationMessage({ pickUpTime }: SubOrderConfimationMessageProps) {
  const confirmationTitleText = useEuiI18n('subOrders.confirmationMessage.title', '', { pickUpTime });
  const confirmationMessageText = useEuiI18n('subOrders.confirmationMessage.text', '');

  return (
    <div className="SubOrderConfirmationMessage__container">
      <img src={ConfirmOrderIcon} />
      <p className="SubOrderConfirmationMessage__title">{confirmationTitleText}</p>
      <p className="SubOrderConfirmationMessage__text">{confirmationMessageText}</p>
    </div>
  );
}
