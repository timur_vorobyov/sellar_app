import { useState, useEffect } from 'react';
import { useStateContext } from 'contexts/store';
import { SubOrder } from 'types/models/subOrder';
import { ActionTypes } from 'types/store';
import './IncomingSubOrdersMobile.scss';
import { useEuiI18n } from '@elastic/eui';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import HeaderMobile from 'components/PDA/HeaderMobile/HeaderMobile';
import { SubOrderDetails } from 'components/SubOrder/SubOrderDetails/SubOrderDetails';
import { SubOrdersList } from 'components/SubOrder/SubOrdersList/SubOrdersList';
import SubOrderDetailsEmpty from 'components/SubOrder/SubOrderDetails/SubOrderDetailsEmpty';

interface IncomingSubOrdersMobileProps {
  previousSubOrder?: SubOrder;
  onSubOrderAccept: (subOrder: SubOrder) => void;
  onSubOrderExpire: (subOrder: SubOrder) => void;
  onSubOrderState?: (state: boolean) => void;
}

export const IncomingSubOrdersMobile = ({
  previousSubOrder,
  onSubOrderAccept,
  onSubOrderExpire,
  onSubOrderState,
}: IncomingSubOrdersMobileProps): JSX.Element => {
  const orderDetailsText = useEuiI18n('menu.header.subOrderDetails', '');

  const {
    state: { incomingSubOrders: subOrders, incomingSubOrdersTimers },
    dispatch,
  } = useStateContext();

  const [selectedSubOrder, setSelectedSubOrder] = useState<SubOrder | null>(null);

  const { isMobile } = useDeviceContext();

  useEffect(() => {
    if (onSubOrderState) {
      onSubOrderState(!!selectedSubOrder);
    }
  }, [selectedSubOrder]);

  useEffect(() => {
    if (subOrders?.length) {
      setSelectedSubOrder(previousSubOrder ? previousSubOrder : null);
    }
  }, [subOrders]);

  useEffect(() => {
    if (subOrders.length === 0) {
      return;
    }
    const timer = setTimeout(() => {
      dispatch({ type: ActionTypes.UPDATE_SUB_ORDERS_EXPIRATION_TIME });
    }, 1000);
    return () => clearTimeout(timer);
  }, [incomingSubOrdersTimers]);

  const handleExpire = (subOrder: SubOrder) => {
    setSelectedSubOrder(null);
    onSubOrderExpire(subOrder);
  };

  const onSubOrderClick = (subOrder: SubOrder) => {
    setSelectedSubOrder(subOrder);
    dispatch({
      type: ActionTypes.UPDATE_NEW_SUB_ORDER_STATUS,
      payload: {
        id: subOrder.id,
        isMobile,
      },
    });
  };

  return (
    <div className="IncomingSubOrderMobile__container">
      {!!subOrders?.length && selectedSubOrder && (
        <>
          <HeaderMobile text={orderDetailsText} onButtonClick={() => setSelectedSubOrder(null)} />
          <SubOrderDetails subOrder={selectedSubOrder} type="incoming" onAccept={onSubOrderAccept} />
        </>
      )}
      {!!subOrders?.length && !selectedSubOrder && (
        <SubOrdersList
          subOrders={subOrders}
          checkedSubOrderId={0}
          onSubOrderClick={onSubOrderClick}
          onSubOrderExpire={handleExpire}
        />
      )}
      {!subOrders?.length && <SubOrderDetailsEmpty />}
    </div>
  );
};
