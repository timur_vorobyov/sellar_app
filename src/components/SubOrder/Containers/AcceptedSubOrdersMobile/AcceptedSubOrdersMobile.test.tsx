import '@testing-library/jest-dom';
import { screen, cleanup } from '@testing-library/react';
import { AcceptedSubOrdersMobile } from './AcceptedSubOrdersMobile';
import { useStateContext, StateProvider } from 'contexts/store';
import { useEffect } from 'react';
import { ActionTypes } from 'types/store';
import { SubOrder } from 'types/models/subOrder';
import { customI18nRender } from '../../../../utils/testing/customRender';
import { Locale } from 'types/translation';
import { subOrders } from '../../../../__mocks__/mocks';

afterEach(cleanup);

interface AcceptedSubOrdersWithContextProps {
  subOrdersArr: SubOrder[];
}
const AcceptedSubOrdersWithContext = ({ subOrdersArr }: AcceptedSubOrdersWithContextProps) => {
  const { dispatch } = useStateContext();
  useEffect(() => {
    dispatch({
      type: ActionTypes.SET_ACCEPTED_SUB_ORDERS,
      payload: { subOrders: subOrdersArr },
    });
  }, [subOrdersArr]);

  return <AcceptedSubOrdersMobile onSubOrderPickup={() => null} onSubOrderState={() => null} />;
};

describe('<AcceptedOrdersMobile> component', () => {
  it('should render accepted sub orders list', () => {
    customI18nRender(
      <StateProvider>
        <AcceptedSubOrdersWithContext subOrdersArr={subOrders} />
      </StateProvider>,
      { locale: Locale.CHINESE },
    );

    const firstOrderId = screen.getAllByText(subOrders[0].orderId)[0];
    const firstOrder = screen.getAllByText('Pick-up Time 接取时间')[0];
    const secondOrder = screen.getByText('Dine in time 及时用餐');

    expect(firstOrderId).toBeInTheDocument();
    expect(firstOrder).toBeInTheDocument();
    expect(secondOrder).toBeInTheDocument();
  });

  it('should render empty sub order details when no accepted sub orders', () => {
    customI18nRender(
      <StateProvider>
        <AcceptedSubOrdersWithContext subOrdersArr={[]} />
      </StateProvider>,
      { locale: Locale.CHINESE },
    );

    expect(screen.getByText('There are currently no online orders. 暂时无网上订单。')).toBeInTheDocument();
    expect(screen.queryByText('Pick-up Time 接取时间')).not.toBeInTheDocument();
  });
});
