import { useEffect, useState } from 'react';
import './AcceptedSubOrdersMobile.scss';
import { useStateContext } from 'contexts/store';
import { SubOrder } from 'types/models/subOrder';
import { useEuiI18n } from '@elastic/eui';
import HeaderMobile from 'components/PDA/HeaderMobile/HeaderMobile';
import { SubOrderDetails } from 'components/SubOrder/SubOrderDetails/SubOrderDetails';
import { SubOrdersList } from 'components/SubOrder/SubOrdersList/SubOrdersList';
import SubOrderDetailsEmpty from 'components/SubOrder/SubOrderDetails/SubOrderDetailsEmpty';

interface AcceptedSubOrdersMobileProps {
  onSubOrderPickup: (subOrder: SubOrder) => void;
  onSubOrderState?: (state: boolean) => void;
}

export const AcceptedSubOrdersMobile = ({
  onSubOrderPickup,
  onSubOrderState,
}: AcceptedSubOrdersMobileProps): JSX.Element => {
  const [orderDetailsText] = useEuiI18n(['menu.header.subOrderDetails'], []);

  const {
    state: { acceptedSubOrders: subOrders },
  } = useStateContext();

  const [selectedOrder, setSelectedOrder] = useState<SubOrder | null>(null);

  useEffect(() => {
    if (onSubOrderState) {
      onSubOrderState(!!selectedOrder);
    }
  }, [selectedOrder]);

  useEffect(() => {
    if (subOrders?.length) {
      setSelectedOrder(null);
    }
  }, [subOrders]);

  return (
    <div className="AcceptedSubOrderMobile__container">
      {!!subOrders?.length && selectedOrder && (
        <>
          <HeaderMobile text={orderDetailsText} onButtonClick={() => setSelectedOrder(null)} />
          <SubOrderDetails
            type="accepted"
            subOrder={selectedOrder}
            onAccept={(_order: SubOrder) => null}
            onPickup={onSubOrderPickup}
          />
        </>
      )}
      {!!subOrders?.length && !selectedOrder && (
        <SubOrdersList
          subOrders={subOrders}
          checkedSubOrderId={0}
          isAccepted={true}
          onSubOrderClick={(subOrder: SubOrder) => setSelectedOrder(subOrder)}
          onSubOrderPickup={onSubOrderPickup}
        />
      )}
      {!subOrders?.length && <SubOrderDetailsEmpty />}
    </div>
  );
};
