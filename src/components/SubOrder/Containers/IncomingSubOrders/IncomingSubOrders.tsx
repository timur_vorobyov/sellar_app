import { useState, useEffect } from 'react';
import { useStateContext } from 'contexts/store';
import { SubOrder } from 'types/models/subOrder';
import { ActionTypes } from 'types/store';
import './IncomingSubOrders.scss';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import SubOrderDetailsEmpty from '../../SubOrderDetails/SubOrderDetailsEmpty';
import { SubOrderDetails } from '../../SubOrderDetails/SubOrderDetails';
import { SubOrdersList } from '../../SubOrdersList/SubOrdersList';

interface IncomingSubOrdersProps {
  onSubOrderAccept: (subOrder: SubOrder) => void;
  onSubOrderExpire: (subOrder: SubOrder) => void;
}

export const IncomingSubOrders = ({ onSubOrderAccept, onSubOrderExpire }: IncomingSubOrdersProps): JSX.Element => {
  const {
    state: { incomingSubOrders: subOrders, incomingSubOrdersTimers },
    dispatch,
  } = useStateContext();

  const [selectedSubOrder, setSelectedSubOrder] = useState<SubOrder | null>(null);
  const { isTablet } = useDeviceContext();

  useEffect(() => {
    if (!selectedSubOrder) {
      setSelectedSubOrder(subOrders && subOrders.length ? subOrders[0] : null);
      const subOrder = subOrders[0];
      if (subOrder) {
        dispatch({
          type: ActionTypes.UPDATE_NEW_SUB_ORDER_STATUS,
          payload: {
            id: subOrder.id,
          },
        });
      }
    }
    const timer = setTimeout(() => {
      setSelectedSubOrder(subOrders && subOrders.length ? subOrders[0] : null);
    }, 500);
    return () => clearTimeout(timer);
  }, [subOrders]);

  useEffect(() => {
    if (subOrders.length === 0) {
      return;
    }
    const timer = setTimeout(() => {
      dispatch({ type: ActionTypes.UPDATE_SUB_ORDERS_EXPIRATION_TIME });
    }, 1000);
    return () => clearTimeout(timer);
  }, [incomingSubOrdersTimers]);

  const handleExpire = (subOrder: SubOrder) => {
    setSelectedSubOrder(null);
    onSubOrderExpire(subOrder);
  };

  const onSubOrderClick = (subOrder: SubOrder | null) => {
    setSelectedSubOrder(subOrder);

    if (subOrder) {
      dispatch({
        type: ActionTypes.UPDATE_NEW_SUB_ORDER_STATUS,
        payload: {
          id: subOrder.id,
          isTablet,
        },
      });
    }
  };

  return (
    <div className="IncomingSubOrder__container">
      {selectedSubOrder ? (
        <SubOrderDetails subOrder={selectedSubOrder} type="incoming" onAccept={onSubOrderAccept} />
      ) : (
        <SubOrderDetailsEmpty />
      )}

      <SubOrdersList
        subOrders={subOrders}
        checkedSubOrderId={selectedSubOrder ? selectedSubOrder.id : 0}
        onSubOrderClick={onSubOrderClick}
        onSubOrderExpire={handleExpire}
      />
    </div>
  );
};
