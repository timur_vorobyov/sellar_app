import '@testing-library/jest-dom';
import { screen, cleanup } from '@testing-library/react';
import { IncomingSubOrders } from './IncomingSubOrders';
import { useStateContext, StateProvider } from 'contexts/store';
import { useEffect } from 'react';
import { ActionTypes } from 'types/store';
import { SubOrder } from 'types/models/subOrder';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';
import { subOrders } from '__mocks__/mocks';

afterEach(cleanup);

interface IncomingOrdersWithContextProps {
  subOrdersArr: SubOrder[];
}

const IncomingOrdersWithContext = ({ subOrdersArr }: IncomingOrdersWithContextProps) => {
  const { dispatch } = useStateContext();
  useEffect(() => {
    dispatch({
      type: ActionTypes.SET_INCOMING_SUB_ORDERS,
      payload: { subOrders: subOrdersArr },
    });
  }, [subOrdersArr]);

  return <IncomingSubOrders onSubOrderAccept={() => null} onSubOrderExpire={() => null} />;
};

describe('<IncomingSubOrders> component', () => {
  it('should render sub order details of first sub order and render sub order list as incoming', () => {
    customI18nRender(
      <StateProvider>
        <IncomingOrdersWithContext subOrdersArr={subOrders} />
      </StateProvider>,
      { locale: Locale.CHINESE },
    );

    const subOrderList = screen.getAllByText('Accept in 时间内接受');

    expect(screen.getByText(subOrders[0].products[0].name)).toBeInTheDocument();
    expect(subOrderList[0]).toBeInTheDocument();
    expect(subOrderList[1]).toBeInTheDocument();
  });

  it('should render empty sub order details when no incoming sub orders', () => {
    customI18nRender(
      <StateProvider>
        <IncomingOrdersWithContext subOrdersArr={[]} />
      </StateProvider>,
      { locale: Locale.CHINESE },
    );
    expect(screen.getByText('There are currently no online orders. 暂时无网上订单。')).toBeInTheDocument();
    expect(screen.queryByText('Accept in 时间内接受')).not.toBeInTheDocument();
  });
});
