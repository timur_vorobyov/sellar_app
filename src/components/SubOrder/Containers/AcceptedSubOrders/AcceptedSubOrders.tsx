import { useState, useEffect } from 'react';
import './AcceptedSubOrders.scss';
import { useStateContext } from 'contexts/store';
import { SubOrder } from 'types/models/subOrder';
import { ActionTypes } from 'types/store';
import { SubOrderDetails } from '../../SubOrderDetails/SubOrderDetails';
import { SubOrdersList } from '../../SubOrdersList/SubOrdersList';
import SubOrderDetailsEmpty from '../../SubOrderDetails/SubOrderDetailsEmpty';

interface AcceptedSubOrdersProps {
  onSubOrderPickup: (subOrder: SubOrder) => void;
}

export const AcceptedSubOrders = ({ onSubOrderPickup }: AcceptedSubOrdersProps): JSX.Element => {
  const {
    state: { acceptedSubOrders: subOrders, incomingSubOrders },
    dispatch,
  } = useStateContext();

  const [selectedSubOrder, setSelectedSubOrder] = useState<SubOrder | null>(null);

  useEffect(() => {
    if (incomingSubOrders[0]) {
      dispatch({
        type: ActionTypes.UPDATE_NEW_SUB_ORDER_STATUS,
        payload: {
          id: incomingSubOrders[0].id,
        },
      });
    }
  }, []);

  useEffect(() => {
    setSelectedSubOrder(subOrders && subOrders.length ? subOrders[0] : null);
  }, [subOrders]);

  return (
    <div className="AcceptedSubOrder__container">
      {selectedSubOrder ? (
        <SubOrderDetails type="accepted" subOrder={selectedSubOrder} onAccept={(_order: SubOrder) => null} />
      ) : (
        <SubOrderDetailsEmpty />
      )}
      <SubOrdersList
        subOrders={subOrders}
        checkedSubOrderId={selectedSubOrder ? selectedSubOrder.id : 0}
        isAccepted={true}
        onSubOrderClick={(subOrder: SubOrder) => setSelectedSubOrder(subOrder)}
        onSubOrderPickup={onSubOrderPickup}
      />
    </div>
  );
};
