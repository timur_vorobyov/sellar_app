import '@testing-library/jest-dom';
import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import IncomingButtons from './IncomingButtons';
import { subOrder } from '__mocks__/mocks';

afterEach(cleanup);

describe('<BottomButton> component', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  const onRejectSubOrderClickedMock = jest.fn();
  const onAcceptOrderMock = jest.fn();
  const onAcceptOrderClickedMock = jest.fn();

  const acceptOrderShownText = 'acceptOrderShownText';
  const rejectOrderShownText = 'rejectOrderShownText';
  const printReceiptShownText = 'printReceiptShownText';

  it('should render component without errors', () => {
    render(
      <IncomingButtons
        isAutoAccepted={false}
        subOrder={subOrder}
        isSubOrderRejected={false}
        onRejectSubOrderClicked={onRejectSubOrderClickedMock}
        onAccept={onAcceptOrderMock}
        onAcceptSubOrderClicked={onAcceptOrderClickedMock}
        acceptSubOrderText=""
        rejectSubOrderText=""
        printReceiptText=""
      />,
    );
  });

  it('should display print receipt text, if Sub order is auto accepted', () => {
    render(
      <IncomingButtons
        isAutoAccepted={true}
        subOrder={subOrder}
        isSubOrderRejected={false}
        onRejectSubOrderClicked={onRejectSubOrderClickedMock}
        onAccept={onAcceptOrderMock}
        onAcceptSubOrderClicked={onAcceptOrderClickedMock}
        acceptSubOrderText={acceptOrderShownText}
        rejectSubOrderText={rejectOrderShownText}
        printReceiptText={printReceiptShownText}
      />,
    );

    expect(screen.queryByText(acceptOrderShownText)).not.toBeInTheDocument();
    expect(screen.queryByText(rejectOrderShownText)).not.toBeInTheDocument();

    const button = screen.getByText(printReceiptShownText);
    expect(button).toBeInTheDocument();

    fireEvent.click(button);
    expect(onAcceptOrderClickedMock).toBeCalledTimes(1);
    expect(onAcceptOrderClickedMock).toBeCalledWith(true);
    expect(onAcceptOrderMock).toBeCalledTimes(1);
    expect(onAcceptOrderMock).toBeCalledWith(subOrder);
  });

  it('should display print reject sub order and accept sub order text, if order is not auto accepted', () => {
    render(
      <IncomingButtons
        isAutoAccepted={false}
        subOrder={subOrder}
        isSubOrderRejected={false}
        onRejectSubOrderClicked={onRejectSubOrderClickedMock}
        onAccept={onAcceptOrderMock}
        onAcceptSubOrderClicked={onAcceptOrderClickedMock}
        acceptSubOrderText={acceptOrderShownText}
        rejectSubOrderText={rejectOrderShownText}
        printReceiptText={printReceiptShownText}
      />,
    );

    const acceptOrderButton = screen.getByText(acceptOrderShownText);
    const rejectOrderButton = screen.getByText(rejectOrderShownText);

    expect(acceptOrderButton).toBeInTheDocument();
    expect(rejectOrderButton).toBeInTheDocument();

    fireEvent.click(acceptOrderButton);
    expect(onAcceptOrderMock).toBeCalledTimes(1);
    expect(onAcceptOrderMock).toBeCalledWith(subOrder);
    expect(onAcceptOrderClickedMock).toBeCalledTimes(1);
    expect(onAcceptOrderClickedMock).toBeCalledWith(true);

    fireEvent.click(rejectOrderButton);
    expect(onRejectSubOrderClickedMock).toBeCalledTimes(1);

    expect(screen.queryByText(printReceiptShownText)).not.toBeInTheDocument();
  });
});
