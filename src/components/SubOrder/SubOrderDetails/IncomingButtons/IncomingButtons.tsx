import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import { SubOrder } from 'types/models/subOrder';
import './IncomingButtons.scss';

interface IncomingButtonsProps {
  isAutoAccepted: boolean;
  subOrder: SubOrder;
  isSubOrderRejected: boolean;
  onRejectSubOrderClicked: () => void;
  onAccept: (subOrder: SubOrder) => void;
  onAcceptSubOrderClicked: (value: boolean) => void;
  acceptSubOrderText: string;
  rejectSubOrderText: string;
  printReceiptText: string;
}

const IncomingButtons = ({
  isAutoAccepted,
  subOrder,
  isSubOrderRejected,
  onRejectSubOrderClicked,
  onAccept,
  onAcceptSubOrderClicked,
  acceptSubOrderText,
  rejectSubOrderText,
  printReceiptText,
}: IncomingButtonsProps) => (
  <div className="OrderDetails__footer">
    <div className="OrderDetails__buttonsContainer">
      {isAutoAccepted ? (
        <CustomButton
          size="lg"
          onClick={() => {
            onAccept(subOrder);
            onAcceptSubOrderClicked(true);
          }}
          text={printReceiptText}
        />
      ) : (
        <>
          <CustomButton
            color="red"
            disabled={isSubOrderRejected}
            onClick={() => onRejectSubOrderClicked()}
            text={rejectSubOrderText}
          />
          <CustomButton
            color="green"
            onClick={() => {
              onAccept(subOrder);
              onAcceptSubOrderClicked(true);
            }}
            text={acceptSubOrderText}
          />
        </>
      )}
    </div>
  </div>
);
export default IncomingButtons;
