import '@testing-library/jest-dom';
import { cleanup, screen } from '@testing-library/react';
import AcceptedButtons from './AcceptedButtons';
import { customI18nRender } from 'utils/testing/customRender';
import { subOrder } from '__mocks__/mocks';
import { Locale } from 'types/translation';
import { SubOrderState } from 'types/models/subOrder';

afterEach(cleanup);

describe('<AcceptedButtons> component', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should display print receipt text, if order is Submitted', () => {
    customI18nRender(<AcceptedButtons item={subOrder} onPickup={() => null} />, {
      locale: Locale.CHINESE,
    });

    expect(screen.getByText('Ready to Collect 准备接取')).toBeInTheDocument();
  });

  it('should display print receipt text, if order is Cancelled', () => {
    customI18nRender(<AcceptedButtons item={{ ...subOrder, state: SubOrderState.Cancelled }} onPickup={() => null} />, {
      locale: Locale.CHINESE,
    });

    expect(screen.getByText('Print Receipt 印收据')).toBeInTheDocument();
  });
});
