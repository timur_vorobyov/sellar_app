import { useEuiI18n } from '@elastic/eui';
import { markCancelSubOrderReceiptPrinted } from 'apis/rest/subOrders';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import { useState } from 'react';
import { CancelledSubOrder, SubOrder, SubOrderState } from 'types/models/subOrder';
import useExplicitApiCall from 'utils/hooks/use-explicit-api-call';
import { printCancelledReceipt } from 'utils/receipt';
import './AcceptedButtons.scss';

interface AcceptedButtonsProps {
  item: SubOrder;
  onPickup?: (item: SubOrder) => void;
}

const AcceptedButtons = ({ item, onPickup }: AcceptedButtonsProps): JSX.Element => {
  const { execute: markOrderAsCancelPrinted } = useExplicitApiCall(markCancelSubOrderReceiptPrinted);
  const [printReceiptButtonText, readyToCollectText] = useEuiI18n(
    ['subOrders.subOrderBlock.cancelled.printReceipt', 'subOrders.subOrderBlock.readyToCollect'],
    [],
  );
  const { isTablet } = useDeviceContext();

  const [isClicked, setIsClicked] = useState(false);
  const handleCancel = (cancelledOrder: CancelledSubOrder) => {
    printCancelledReceipt(cancelledOrder, isTablet);
    markOrderAsCancelPrinted(cancelledOrder, true);
  };

  return (
    <div className="AcceptedButtons__footer">
      {item.state === SubOrderState.Cancelled && !item.isCancellationReceiptPrinted ? (
        <CustomButton
          size="max"
          padding="no"
          color="orange"
          onClick={() => handleCancel(item)}
          text={printReceiptButtonText}
        />
      ) : (
        <CustomButton
          size="max"
          padding="no"
          onClick={() => {
            onPickup && onPickup(item);
            setIsClicked(true);
          }}
          disabled={isClicked}
          text={readyToCollectText}
        />
      )}
    </div>
  );
};
export default AcceptedButtons;
