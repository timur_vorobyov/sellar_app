import { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';
import { EuiFlexGroup, EuiFlexItem, EuiLink, useEuiI18n } from '@elastic/eui';
import { SubOrder, SubOrderData, SubOrderState } from 'types/models/subOrder';
import { ProductCustomisation, SubOrderProduct } from 'types/models/product';
import TimerIcon from 'components/assets/images/SubOrderPage/timer-orange.svg';
import SubOrderConfimationMessage from '../SubOrderConfimationMessage/SubOrderConfimationMessage';
import { SubOrderType } from 'constants/subOrder';
import { printAcceptedReceipt, printCancelledReceipt } from 'utils/receipt';
import IncomingButtons from './IncomingButtons/IncomingButtons';
import useCommonTranslations from 'utils/hooks/user-common-translations';
import { sliceTwoMoreDecimals, сonvertFromCentsToDollars } from 'utils/formater';
import { receiptTypes } from 'constants/receiptType';
import { useTreatments } from '@splitsoftware/splitio-react';
import { FeatureFlags } from 'constants/featureFlags';
import { convertToSingaporeTime } from 'utils/datetime';
import useApiCall from 'utils/hooks/use-api-call';
import { getSubOrderById } from 'apis/rest/subOrders';
import './SubOrderDetails.scss';
import Typography, { BoxTypography } from 'components/ui-kit/Typography';
import Box from 'components/ui-kit/Box';
import { useLanguageContext } from 'contexts/languageContextProvider/context';
import Timer from '../Timer/Timer';
import AcceptedButtons from 'components/SubOrder/SubOrderDetails/AcceptedButtons/AcceptedButtons';

interface SubOrderDetailsProps {
  subOrder: SubOrder;
  type: string;
  onAccept: (subOrder: SubOrder) => void;
  onPickup?: (item: SubOrder) => void;
}

export const SubOrderDetails = ({ subOrder, type, onAccept, onPickup }: SubOrderDetailsProps) => {
  const [isSubOrderRejected, setIsSubOrderRejected] = useState(false);
  const [isSubOrderAccepted, setIsSubOrderAccepted] = useState(false);
  const [rejectSubOrderClicked, setRejectSubOrderClicked] = useState(false);
  const feat = useTreatments([FeatureFlags.QR_CODE]);
  const { getLocale } = useLanguageContext();
  const { data: subOrderData }: { data: SubOrderData | null; error: unknown; isLoading: boolean } = useApiCall(
    getSubOrderById,
    { args: [subOrder.id] },
  );

  useEffect(() => {
    setIsSubOrderRejected(false);
    setIsSubOrderAccepted(false);
    setRejectSubOrderClicked(false);
  }, [subOrder]);

  const [
    orderCancelledText,
    dineInTimeText,
    pickUpTimeText,
    acceptInText,
    reprintReceiptText,
    subtotalText,
    deliveryFeeText,
    totalPriceText,
    customerContactText,
    rejectOrderText,
    acceptOrderText,
    containerFeeText,
    appPaymentDiscountText,
    cutleryNeededText,
    noCutleryNeededText,
    printReceiptText,
  ] = useEuiI18n(
    [
      'subOrders.subOrderDetails.cancelled.state',
      'subOrders.subOrderDetails.dineInTime',
      'subOrders.subOrderDetails.pickUpTime',
      'subOrders.subOrderDetails.acceptIn',
      'subOrders.subOrderDetails.reprintReceipt',
      'subOrders.subOrderDetails.subtotal',
      'subOrders.subOrderDetails.deliveryFee',
      'subOrders.subOrderDetails.total',
      'subOrders.subOrderDetails.customerContract',
      'subOrders.subOrderDetails.rejectSubOrder',
      'subOrders.subOrderDetails.acceptSubOrder',
      'subOrders.subOrderDetails.containerFee',
      'subOrders.subOrderDetails.appPaymentDiscount',
      'subOrders.subOrderDetails.cutleryNeeded',
      'subOrders.subOrderDetails.noCutleryNeeded',
      'subOrders.subOrderDetails.printReceipt',
    ],
    [],
  );

  const { orderTypes: orderTypeTranslations } = useCommonTranslations();

  if (rejectSubOrderClicked) {
    return (
      <Redirect
        to={{
          pathname: '/reject',
          state: {
            subOrder: subOrder,
          },
        }}
      />
    );
  }

  const getSubOrderStateDesc = () => {
    if (subOrder.state === SubOrderState.Cancelled && !subOrder.isCancellationReceiptPrinted) {
      return <BoxTypography typographyProps={{ variant: 'span' }}>{orderCancelledText}</BoxTypography>;
    } else {
      return (
        <div>
          {subOrder.type === 'having_here' ? (
            <BoxTypography typographyProps={{ variant: 'span' }}>{dineInTimeText}</BoxTypography>
          ) : (
            <BoxTypography typographyProps={{ variant: 'span' }}>{pickUpTimeText}</BoxTypography>
          )}
          <BoxTypography typographyProps={{ variant: 'span', fontWeight: 'bold' }}>
            {subOrder.pickUpTime ? convertToSingaporeTime(subOrder.pickUpTime, 'hh:mm A') : '----'}
          </BoxTypography>
        </div>
      );
    }
  };

  const getCustomisations = (customisations: ProductCustomisation[]) => {
    return customisations.map((customisation) => {
      let customisatioName = getLocale() === 'en' ? customisation.name : customisation.nameCn;

      if (!customisatioName) {
        customisatioName = customisation.name;
      }

      return (
        <div className="SubOrderDetails__customisation" key={customisation.id}>
          <Typography variant="span">{customisatioName + ' ' + 'x' + customisation.actualQuantity}</Typography>
        </div>
      );
    });
  };

  const getProductsList = () => {
    return subOrder.productsGroupedByCategories.map((formattedProduct: SubOrderProduct) => {
      let categoryName = getLocale() === 'en' ? formattedProduct.category.name : formattedProduct.category.nameCn;

      if (!categoryName) {
        categoryName = formattedProduct.category.name;
      }

      return (
        <>
          <EuiFlexItem grow={1}>
            <BoxTypography
              typographyProps={{ variant: 'span', fontWeight: 'bold' }}
              boxProps={{ addClass: 'SubOrderDetails__category', mt: 16 }}
            >
              {categoryName}
            </BoxTypography>
          </EuiFlexItem>
          {formattedProduct.list.map((product) => {
            let productName = getLocale() === 'en' ? product.name : product.nameCn;

            if (!productName) {
              productName = product.name;
            }

            return (
              <EuiFlexGroup key={product.id} className="SubOrderDetails__listItem">
                <EuiFlexItem grow={1}>
                  <Typography variant="span" fontWeight="bold">
                    {'x' + product.quantity}
                  </Typography>
                </EuiFlexItem>
                <EuiFlexItem grow={10}>
                  <BoxTypography typographyProps={{ variant: 'span', fontWeight: 'bold' }} boxProps={{ ml: 10 }}>
                    {productName}
                  </BoxTypography>
                  {getCustomisations(product.customisations)}
                  <BoxTypography typographyProps={{ variant: 'span', style: { fontSize: 18 } }} boxProps={{ mt: 8 }}>
                    {product.customerComments}
                  </BoxTypography>
                </EuiFlexItem>
                <EuiFlexItem grow={1} className="SubOrderDetails__productPrice">
                  <Typography variant="span">{'$' + sliceTwoMoreDecimals(product.fullProductSum)}</Typography>
                </EuiFlexItem>
              </EuiFlexGroup>
            );
          })}
        </>
      );
    });
  };

  if (isSubOrderAccepted) {
    return (
      <SubOrderConfimationMessage
        pickUpTime={subOrder.pickUpTime ? convertToSingaporeTime(subOrder.pickUpTime, 'LT') : ''}
      />
    );
  }

  return (
    <div className="SubOrderDetails__container">
      <div className={`SubOrderDetails__header`}>
        <Box>
          <img src={subOrder.logo} alt="logo" />
          <Box>
            <Typography variant="span" fontWeight="bold">
              {subOrder.orderId}
            </Typography>
            <span className="SubOrderDetails__header__divider">|</span>
          </Box>
        </Box>
        {type == 'accepted' || subOrder.isAutoAccepted ? (
          getSubOrderStateDesc()
        ) : (
          <div>
            <BoxTypography typographyProps={{ variant: 'span' }}>{acceptInText}</BoxTypography>
            <div className="SubOrderDetails__header__pickUpTimer">
              <img src={TimerIcon} />
              <Timer
                id={subOrder.id}
                onExpire={() => {
                  setIsSubOrderRejected(true);
                }}
              />
            </div>
          </div>
        )}
      </div>
      <div className={`SubOrderDetails__body`}>
        <div className="SubOrderDetails__listItem OrderDetails__type">
          <Typography variant="span">{orderTypeTranslations[subOrder.type]}</Typography>
          {type == 'accepted' && (
            <>
              <EuiLink
                className="SubOrderDetails__body__reprintBtn"
                onClick={() =>
                  subOrder.state === SubOrderState.Cancelled
                    ? printCancelledReceipt(subOrder, true)
                    : printAcceptedReceipt(
                        subOrder,
                        getLocale(),
                        receiptTypes.TENNANT,
                        feat[FeatureFlags.QR_CODE].treatment,
                        true,
                      )
                }
              >
                {reprintReceiptText}
              </EuiLink>
            </>
          )}
        </div>
        <div className="SubOrderDetails__listItem">
          <Typography variant="span">{subOrder.isCutleryNeeded ? cutleryNeededText : noCutleryNeededText} </Typography>
        </div>
        <div>{getProductsList()}</div>
        <div className="SubOrderDetails__prices">
          <Box addClass="SubOrderDetails__price" mt={16} pb={16}>
            <Typography variant="span">{subtotalText}</Typography>
            <Typography variant="span">{'$' + sliceTwoMoreDecimals(subOrder.subTotalPrice)}</Typography>
          </Box>
          <Box addClass="SubOrderDetails__price" mt={16} pb={16}>
            <Typography variant="span">{containerFeeText}</Typography>
            <Typography variant="span">{'$' + sliceTwoMoreDecimals(subOrder.totalContainerFeesAmount)}</Typography>
          </Box>
          <Box addClass="SubOrderDetails__price" mt={16} pb={16}>
            <Typography variant="span">{appPaymentDiscountText} (10%)</Typography>
            <Typography variant="span">
              {subOrderData
                ? '-$' + sliceTwoMoreDecimals(сonvertFromCentsToDollars(subOrderData.appPaymentDiscount))
                : ''}
            </Typography>
          </Box>
          <Box addClass="SubOrderDetails__deliveryPrice SubOrderDetails__price" mt={16} pb={16}>
            <Typography variant="span">{deliveryFeeText}</Typography>
            <Typography variant="span">
              {subOrder.type == SubOrderType.Delivery ? '$' + sliceTwoMoreDecimals(subOrder.deliveryPrice) : '$' + 0}
            </Typography>
          </Box>
          <Box addClass="SubOrderDetails__totalPrice SubOrderDetails__price" mt={16} pb={16}>
            <Typography variant="span">{totalPriceText} (+7% GST)</Typography>
            <Typography variant="span">{'$' + sliceTwoMoreDecimals(subOrder.netSales)}</Typography>
          </Box>
        </div>
        <div className="SubOrderDetails__contacts">
          <div className="SubOrderDetails__customerContacts">
            <Typography variant="span" fontWeight="bold">
              {customerContactText}
            </Typography>
            <Typography variant="span">{subOrder.customer.name + ' | ' + subOrder.customer.phoneNumber}</Typography>
          </div>
        </div>
      </div>
      {type === 'incoming' && (
        <IncomingButtons
          isAutoAccepted={subOrder.isAutoAccepted}
          subOrder={subOrder}
          isSubOrderRejected={isSubOrderRejected}
          onRejectSubOrderClicked={() => {
            setIsSubOrderRejected(true);
            setRejectSubOrderClicked(true);
          }}
          onAcceptSubOrderClicked={setIsSubOrderAccepted}
          onAccept={onAccept}
          rejectSubOrderText={rejectOrderText}
          acceptSubOrderText={acceptOrderText}
          printReceiptText={printReceiptText}
        />
      )}
      {type === 'accepted' && <AcceptedButtons item={subOrder} onPickup={onPickup} />}
    </div>
  );
};
