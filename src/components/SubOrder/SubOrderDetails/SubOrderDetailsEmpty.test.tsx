import '@testing-library/jest-dom';
import { screen, cleanup } from '@testing-library/react';
import SubOrderDetailsEmpty from './SubOrderDetailsEmpty';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<SubOrderDetailsEmpty> component', () => {
  it('should render component', () => {
    customI18nRender(<SubOrderDetailsEmpty />, { locale: Locale.CHINESE });
    expect(screen.getByText('There are currently no online orders. 暂时无网上订单。')).toBeInTheDocument();
  });
});
