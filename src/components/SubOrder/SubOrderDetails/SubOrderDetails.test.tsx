import '@testing-library/jest-dom';
import { screen, cleanup } from '@testing-library/react';
import { SubOrderDetails } from './SubOrderDetails';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';
import { subOrder } from '__mocks__/mocks';

afterEach(cleanup);

describe('<SubOrderDetails> component', () => {
  it('should render component with data as incoming sub order', () => {
    customI18nRender(<SubOrderDetails subOrder={subOrder} type="incoming" onAccept={() => null} />, {
      locale: Locale.CHINESE,
    });

    expect(screen.getByText('Accept in 时间内接受')).toBeInTheDocument();
    expect(screen.getByText('Meatball Noodle')).toBeInTheDocument();
    expect(screen.getByText('No cutlery needed 不需要餐具')).toBeInTheDocument();

    // TODO: Rewrite test, to make sure that translated string is loaded immediately without undefined initial value
    // expect(screen.getByText('Reject Order 拒绝订单')).toBeInTheDocument();
    // expect(screen.getByText('Accept Order 接受订单')).toBeInTheDocument();
  });

  it('should render component with data as accepted sub order', () => {
    customI18nRender(<SubOrderDetails subOrder={subOrder} type="accepted" onAccept={() => null} />, {
      locale: Locale.CHINESE,
    });

    expect(screen.getByText('Pick-up Time 接取时间')).toBeInTheDocument();
    expect(screen.getByRole('img')).toHaveAttribute('src', subOrder.logo);
  });
});
