import { useEuiI18n } from '@elastic/eui';
import './SubOrderDetailsEmpty.scss';

export default function SubOrderDetailsEmpty() {
  const [noOrdersText] = useEuiI18n(['subOrders.subOrderDetails.empty'], []);

  return (
    <div className="SubOrderDetailsEmpty__container">
      <p className="SubOrderDetailsEmpty__content">{noOrdersText}</p>
    </div>
  );
}
