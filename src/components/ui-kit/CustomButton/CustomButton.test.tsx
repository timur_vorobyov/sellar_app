import { cleanup, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import CustomButton from './CustomButton';

afterEach(cleanup);

describe('<CustomButton> component', () => {
  it('should render with first tab', () => {
    render(<CustomButton text="test Text" />);
    expect(screen.getByText('test Text')).toBeInTheDocument();
  });
});
