import './CustomButton.scss';
import classnames from 'clsx';
import Typography from '../Typography';

type Size = 'sm' | 'md' | 'lg' | 'xl' | 'max' | 'no';

type CustomButtonProps = {
  size?: Size;
  color?: string;
  padding?: Size;
  className?: string;
  disabled?: boolean;
  typoColor?: string;
  text: string;
  onClick: () => void;
};

const CustomButton = ({ size, color, padding, disabled, typoColor, text, onClick }: CustomButtonProps) => (
  <button
    className={classnames('CustomButton', color, size, padding ? `padding-${padding}` : undefined)}
    disabled={disabled}
    onClick={(event) => {
      event.stopPropagation();
      onClick();
    }}
  >
    <span className="CustomButton__content">
      <span className="CustomButton__content__text">
        <Typography color={typoColor ? typoColor : 'white'} variant="span">
          {text}
        </Typography>
      </span>
    </span>
  </button>
);

export default CustomButton;
