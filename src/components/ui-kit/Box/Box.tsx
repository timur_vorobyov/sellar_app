import React from 'react';
import classnames from 'clsx';
import './Box.scss';

export type PADDING_MARGIN_VALUE_TYPE = 2 | 4 | 6 | 8 | 10 | 12 | 14 | 16 | 18 | 20 | 22 | 24 | 26 | 28 | 30 | 32;

export type BoxComponentProps = {
  p?: PADDING_MARGIN_VALUE_TYPE; // Add padding in all direction
  pt?: PADDING_MARGIN_VALUE_TYPE; // Add padding in Top
  pr?: PADDING_MARGIN_VALUE_TYPE; // Add padding in Right
  pb?: PADDING_MARGIN_VALUE_TYPE; // Add padding in Bottom
  pl?: PADDING_MARGIN_VALUE_TYPE; // Add padding in Left

  m?: PADDING_MARGIN_VALUE_TYPE; // Add margin in all direction
  mt?: PADDING_MARGIN_VALUE_TYPE; // Add margin in Top
  mr?: PADDING_MARGIN_VALUE_TYPE; // Add margin in Right
  mb?: PADDING_MARGIN_VALUE_TYPE; // Add margin in Bottom
  ml?: PADDING_MARGIN_VALUE_TYPE; // Add margin in Left
  addClass?: string;
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  children?: any;
};

const Box = (props: BoxComponentProps) => {
  const { children, p, pt, pr, pb, pl, m, mt, mr, mb, ml, addClass } = props;

  const classes = classnames('box-container', {
    [`${addClass}`]: !!addClass,
    [`p-${p}`]: !!p,
    [`pt-${pt}`]: !p && !!pt,
    [`pr-${pr}`]: !p && !!pr,
    [`pb-${pb}`]: !p && !!pb,
    [`pl-${pl}`]: !p && !!pl,
    [`m-${m}`]: !!m,
    [`mt-${mt}`]: !m && !!mt,
    [`mr-${mr}`]: !m && !!mr,
    [`mb-${mb}`]: !m && !!mb,
    [`ml-${ml}`]: !m && !!ml,
  });
  return <div className={classes}>{children}</div>;
};

export default Box;
