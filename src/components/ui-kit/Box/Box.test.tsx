import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import Box from './Box';

afterEach(cleanup);

describe('<Box> component', () => {
  it('should render without error', () => {
    const { getByText } = render(<Box>Test Text</Box>);
    expect(getByText('Test Text')).toBeInTheDocument();
    expect(getByText('Test Text').className.includes('box-container')).toBeTruthy();
  });

  // Test cases for the margin props
  it('should render with class m-2', () => {
    const { getByText } = render(<Box m={2}>Test Text</Box>);
    expect(getByText('Test Text').className.includes('box-container m-2')).toBeTruthy();
  });
  it('should render with class mt-2', () => {
    const { getByText } = render(<Box mt={2}>Test Text</Box>);
    expect(getByText('Test Text').className.includes('box-container mt-2')).toBeTruthy();
  });
  it('should render with class mr-2', () => {
    const { getByText } = render(<Box mr={2}>Test Text</Box>);
    expect(getByText('Test Text').className.includes('box-container mr-2')).toBeTruthy();
  });
  it('should render with class mb-2', () => {
    const { getByText } = render(<Box mb={2}>Test Text</Box>);
    expect(getByText('Test Text').className.includes('box-container mb-2')).toBeTruthy();
  });
  it('should render with class ml-2', () => {
    const { getByText } = render(<Box ml={2}>Test Text</Box>);
    expect(getByText('Test Text').className.includes('box-container ml-2')).toBeTruthy();
  });

  // Test cases for the padding props
  it('should render with class p-2', () => {
    const { getByText } = render(<Box p={2}>Test Text</Box>);
    expect(getByText('Test Text').className.includes('box-container p-2')).toBeTruthy();
  });
  it('should render with class pt-2', () => {
    const { getByText } = render(<Box pt={2}>Test Text</Box>);
    expect(getByText('Test Text').className.includes('box-container pt-2')).toBeTruthy();
  });
  it('should render with class pr-2', () => {
    const { getByText } = render(<Box pr={2}>Test Text</Box>);
    expect(getByText('Test Text').className.includes('box-container pr-2')).toBeTruthy();
  });
  it('should render with class pb-2', () => {
    const { getByText } = render(<Box pb={2}>Test Text</Box>);
    expect(getByText('Test Text').className.includes('box-container pb-2')).toBeTruthy();
  });
  it('should render with class pl-2', () => {
    const { getByText } = render(<Box pl={2}>Test Text</Box>);
    expect(getByText('Test Text').className.includes('box-container pl-2')).toBeTruthy();
  });
});
