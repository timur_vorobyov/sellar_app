import React from 'react';
import classes from './Typography.module.scss';
import cn from 'clsx';
import { DateString } from 'types/generics';

export type TypographyProps = {
  variant: 'p' | 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'li' | 'span' | 'label';
  children: React.ReactChild | string[] | DateString[];
  className?: string;
  style?: React.CSSProperties;
  color?: string;
  fontWeight?: string;
  lh?: string;
};

const Typography = (props: TypographyProps) => {
  const { variant, children, style, color } = props;
  const Component = variant;
  const compStyle = { ...style };
  if (color != null) {
    const fontColor = ['white', 'blue', 'gray', 'black'].includes(color);
    if (fontColor) {
      compStyle.color = color;
    }
  }
  const getClassName = (): string => {
    const { fontWeight, color, className, lh } = props;
    return cn({
      [classes.common]: true,
      [classes.p]: variant === 'p',
      [classes.h1]: variant === 'h1',
      [classes.h2]: variant === 'h2',
      [classes.h3]: variant === 'h3',
      [classes.h4]: variant === 'h4',
      [classes.h5]: variant === 'h5',
      [classes.li]: variant === 'li',
      [classes.label]: variant === 'label',
      [classes.span]: variant === 'span',
      [classes.white]: color == 'white',
      [classes.blue]: color == 'blue',
      [classes.gray]: color == 'gray',
      [classes.black]: color == 'black',
      [classes.orange]: color == 'orange',
      [classes.red]: color == 'red',
      [classes.bold]: fontWeight == 'bold',
      [classes.normal]: fontWeight == 'normal',
      [classes.SubOrderBlock__footerText]: className == 'SubOrderBlock__footerText',
      [`${className}`]: className && className !== 'SubOrderBlock__footerText',
      [`ln-${lh}`]: lh,
    });
  };

  return (
    <Component style={compStyle} className={getClassName()}>
      {children}
    </Component>
  );
};

export default Typography;
