import React from 'react';
import { DateString } from 'types/generics';
import Box, { BoxComponentProps } from '../Box/Box';
import Typography, { TypographyProps } from './Typography';

type BoxTypographyParams = {
  typographyProps: {
    variant: TypographyProps['variant'];
    className?: TypographyProps['className'];
    style?: TypographyProps['style'];
    color?: TypographyProps['color'];
    fontWeight?: TypographyProps['fontWeight'];
    lh?: TypographyProps['lh'];
  };
  boxProps?: {
    p?: BoxComponentProps['p']; // Add padding in all direction
    pt?: BoxComponentProps['pt']; // Add padding in Top
    pr?: BoxComponentProps['pr']; // Add padding in Right
    pb?: BoxComponentProps['pb']; // Add padding in Bottom
    pl?: BoxComponentProps['pl']; // Add padding in Left

    m?: BoxComponentProps['m']; // Add margin in all direction
    mt?: BoxComponentProps['mt']; // Add margin in Top
    mr?: BoxComponentProps['mr']; // Add margin in Right
    mb?: BoxComponentProps['mb']; // Add margin in Bottom
    ml?: BoxComponentProps['ml']; // Add margin in Left
    addClass?: BoxComponentProps['addClass']; // Add custom class
  };
  children: React.ReactChild | string[] | DateString[];
};

const BoxTypography = (props: BoxTypographyParams) => {
  const { typographyProps, boxProps, children } = props;
  return (
    <Box {...boxProps}>
      <Typography {...typographyProps}>{children}</Typography>
    </Box>
  );
};

export default BoxTypography;
