import '@testing-library/jest-dom';
import { render, screen, cleanup } from '@testing-library/react';
import Typography from './Typography';

afterEach(cleanup);

describe('<Typography> component', () => {
  it('should render p tag with choosen props', () => {
    render(
      <Typography variant="p" fontWeight="bold" color="black">
        Typography
      </Typography>,
    );

    expect(screen.getByText('Typography')).toHaveClass('p');
    expect(screen.getByText('Typography')).toHaveClass('bold');
    expect(screen.getByText('Typography')).toHaveClass('black');
  });

  it('should render span tag with choosen props', () => {
    render(
      <Typography variant="span" fontWeight="normal" color="white">
        Typography
      </Typography>,
    );

    expect(screen.getByText('Typography')).toHaveClass('span');
    expect(screen.getByText('Typography')).toHaveClass('normal');
    expect(screen.getByText('Typography')).toHaveClass('white');
  });

  it('should render h1 tag', () => {
    render(<Typography variant="h1">Typography</Typography>);

    expect(screen.getByText('Typography')).toHaveClass('h1');
  });

  it('should render h2 tag', () => {
    render(<Typography variant="h2">Typography</Typography>);

    expect(screen.getByText('Typography')).toHaveClass('h2');
  });

  it('should render h3 tag', () => {
    render(<Typography variant="h3">Typography</Typography>);

    expect(screen.getByText('Typography')).toHaveClass('h3');
  });

  it('should render h4 tag', () => {
    render(<Typography variant="h4">Typography</Typography>);

    expect(screen.getByText('Typography')).toHaveClass('h4');
  });

  it('should render h5 tag', () => {
    render(<Typography variant="h5">Typography</Typography>);

    expect(screen.getByText('Typography')).toHaveClass('h5');
  });

  it('should render label tag', () => {
    render(<Typography variant="label">Typography</Typography>);

    expect(screen.getByText('Typography')).toHaveClass('label');
  });

  it('should render li tag', () => {
    render(<Typography variant="li">Typography</Typography>);

    expect(screen.getByText('Typography')).toHaveClass('li');
  });
});
