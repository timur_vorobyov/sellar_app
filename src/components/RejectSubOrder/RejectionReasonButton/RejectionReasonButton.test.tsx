import '@testing-library/jest-dom';
import { cleanup, render, screen, fireEvent } from '@testing-library/react';
import RejectionReasonButton from './RejectionReasonButton';

afterEach(cleanup);

describe('<RejectionReasonButton> component', () => {
  it('should render unchecked component', () => {
    const { container } = render(<RejectionReasonButton label="Too busy" checked={false} onClick={() => null} />);
    expect(screen.getByText('Too busy')).toBeInTheDocument();
    expect(container.firstChild).not.toHaveClass('checked');
  });

  it('should render checked component', () => {
    const onClick = jest.fn();
    const { container } = render(<RejectionReasonButton label="Too busy" checked={true} onClick={onClick} />);

    fireEvent.click(screen.getByText('Too busy'));

    expect(container.firstChild).toHaveClass('checked');
    expect(onClick).toHaveBeenCalled();
  });
});
