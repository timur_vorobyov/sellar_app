import { EuiIcon } from '@elastic/eui';
import Typography from 'components/ui-kit/Typography';
import './RejectionReasonButton.scss';

interface RejectionReasonButtonProps {
  label: string;
  checked: boolean;
  onClick: () => void;
}

export default function RejectionReasonButton({ label, checked, onClick }: RejectionReasonButtonProps) {
  return (
    <button
      className={checked ? 'RejectionReasonButton__container checked' : 'RejectionReasonButton__container'}
      onClick={onClick}
    >
      <Typography variant="span" fontWeight="bold">
        {label}
      </Typography>
      <div className={checked ? 'RejectionReasonButton__checkbox checked' : 'RejectionReasonButton__checkbox'}>
        <EuiIcon color="#FFFFFF" type="check" size="l" />
      </div>
    </button>
  );
}
