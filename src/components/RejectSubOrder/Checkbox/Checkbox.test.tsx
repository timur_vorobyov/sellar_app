import '@testing-library/jest-dom';
import { cleanup } from '@testing-library/react';
import { render, screen } from '@testing-library/react';
import { product } from '__mocks__/mocks';
import Checkbox from './Checkbox';

afterEach(cleanup);

describe('<Checkbox> component', () => {
  it('should render Checkbox component with product data', () => {
    render(<Checkbox locale="en" product={product} checked={false} onChange={() => null} />);
    const productName = screen.getByText(`x${product.quantity} ${product.name}`);
    expect(productName).toHaveTextContent(product.name);
  });

  it('should show checkbox checked with prop false', () => {
    const { container } = render(<Checkbox locale="en" product={product} checked={false} onChange={() => null} />);
    const checkbox = container.getElementsByClassName('RejectSubOrder__checkbox__icon')[0];
    expect(checkbox).toHaveClass('RejectSubOrder__checkbox__icon');
  });

  it('should show checked checkbox with prop true', () => {
    const { container } = render(<Checkbox locale="en" product={product} checked={true} onChange={() => null} />);
    const checkbox = container.getElementsByClassName('RejectSubOrder__checkbox__icon-checked')[0];
    expect(checkbox).toHaveClass('RejectSubOrder__checkbox__icon-checked');
  });
});
