import { EuiIcon } from '@elastic/eui';
import { Product, ProductCustomisation } from 'types/models/product';
import './Checkbox.scss';

interface CheckboxProps {
  product?: Product;
  locale: string;
  customization?: ProductCustomisation;
  checked: boolean | null;
  onChange: (checked: boolean) => void;
  option?: string;
}

const Checkbox = ({ product, customization, checked, onChange, option, locale }: CheckboxProps) => {
  const formatLabelProduct = (product: Product) => {
    let productName = locale === 'en' ? product.name : product.nameCn;

    if (!productName) {
      productName = product.name;
    }

    return `x${product.quantity} ${productName} `;
  };

  const getCheckboxIconColor = (option?: string, checked?: boolean | null) => {
    if (option === 'freezed') {
      return checked ? '#FFFFFF' : '#CCCCCC';
    }
    return '#FFFFFF';
  };

  return (
    <div className="RejectSubOrder__checkbox__container">
      <div
        className={
          checked
            ? `RejectSubOrder__checkbox__icon-checked${option === 'freezed' ? '-freezed' : ''}`
            : `RejectSubOrder__checkbox__icon${option === 'freezed' ? '-freezed' : ''}`
        }
        onClick={() => option !== 'freezed' && onChange(!checked)}
      >
        <EuiIcon color={getCheckboxIconColor(option, checked)} type="check" size="l" />
      </div>

      <div className={product ? 'RejectSubOrder__checkbox__label__product' : 'RejectSubOrder__checkbox__label__item'}>
        <span>{product && formatLabelProduct(product)}</span>
        {customization &&
          (customization.name ? (locale === 'en' ? customization.name : customization.nameCn) : customization.nameCn)}
      </div>
    </div>
  );
};

export default Checkbox;
