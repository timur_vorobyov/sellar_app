import '@testing-library/jest-dom';
import { cleanup, screen } from '@testing-library/react';
import { TOO_BUSY_INTERVALS } from 'constants/subOrder';
import { Locale } from 'types/translation';
import { customI18nRender } from 'utils/testing/customRender';
import TooBusy from './TooBusy';

afterEach(cleanup);

describe('<TooBusy> component', () => {
  it('should render component', () => {
    customI18nRender(<TooBusy selected={`${TOO_BUSY_INTERVALS[0]}`} onChange={() => null} />, {
      locale: Locale.ENGLISH,
    });

    expect(screen.getAllByText(`${TOO_BUSY_INTERVALS[0]} mins`).pop()).toBeInTheDocument();
  });
});
