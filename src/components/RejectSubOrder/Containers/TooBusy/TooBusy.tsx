import { EuiSuperSelect, useEuiI18n } from '@elastic/eui';
import Typography from 'components/ui-kit/Typography';
import { TOO_BUSY_INTERVALS } from 'constants/subOrder';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import './TooBusy.scss';

interface TooBusyProps {
  onChange: (value: number) => void;
  selected: string;
}

export default function TooBusy({ onChange, selected }: TooBusyProps) {
  const { isMobile } = useDeviceContext();
  const setNewPrepTime = useEuiI18n('subOrderRejectPage.tooBusy.setNewPrepTime', '');

  const tooBusyOptions = TOO_BUSY_INTERVALS.map((interval) => {
    const text = useEuiI18n('timing.minutes', `${interval} mins`, { count: interval });

    return {
      value: `${interval}`,
      dropdownDisplay: <span className="StallTable__superSelect">{text}</span>,
      inputDisplay: <span className="StallTable__superSelect">{text}</span>,
    };
  });

  const tooBusyOptionsMobile = TOO_BUSY_INTERVALS.map((value) => {
    const text = useEuiI18n('timing.minutes', `${value} mins`, { count: value });
    return {
      value,
      text,
    };
  });

  return (
    <div>
      <div className="TooBusy__header_container">
        <Typography variant="h2" fontWeight="bold" className="TooBusy__header__title">
          {setNewPrepTime}
        </Typography>
      </div>
      <div className="TooBusy__selector__container">
        {isMobile ? (
          <>
            {tooBusyOptionsMobile.map((option) => (
              <div className="TooBusyMobile__radio" key={option.text}>
                <input
                  type="radio"
                  name="tooBusy"
                  id={option.text}
                  value={option.value}
                  checked={Number(selected) === option.value}
                  onChange={() => onChange(option.value)}
                />
                <label htmlFor={option.text}>
                  <Typography variant="span">{option.text}</Typography>
                </label>
              </div>
            ))}
          </>
        ) : (
          <EuiSuperSelect
            className="lg"
            options={tooBusyOptions}
            valueOfSelected={selected}
            onChange={(value: string) => onChange(Number(value))}
          />
        )}
      </div>
    </div>
  );
}
