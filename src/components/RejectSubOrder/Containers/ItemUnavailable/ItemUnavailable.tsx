import { useEuiI18n } from '@elastic/eui';
import Checkbox from '../../Checkbox/Checkbox';
import './ItemUnavailable.scss';
import { Product, ProductCustomisation } from 'types/models/product';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import { useLanguageContext } from 'contexts/languageContextProvider/context';
interface ItemUnavailableProps {
  products: Product[];
  selectedProducts: Product[];
  selectedCustomizations: ProductCustomisation[];
  onProductChange: (product: Product, checked: boolean) => void;
  onCustomizationChange: (product: ProductCustomisation, checked: boolean) => void;
  onSelectAll: () => void;
}

export default function ItemUnavailable({
  products,
  selectedProducts,
  selectedCustomizations,
  onProductChange,
  onCustomizationChange,
  onSelectAll,
}: ItemUnavailableProps) {
  const [titleText, selectAllText] = useEuiI18n(
    ['rejectSubOrder.itemUnavailable.title', 'rejectSubOrder.itemUnavailable.selectAll'],
    [],
  );
  const { getLocale } = useLanguageContext();

  return (
    <>
      <div className="ItemUnavailable__header">
        <h2 className="ItemUnavailable__header__title">{titleText}</h2>
      </div>
      <div className="ItemUnavailable__body">
        <CustomButton
          color="transparent"
          padding="no"
          size="max"
          typoColor="blue"
          onClick={() => onSelectAll()}
          text={selectAllText}
        />
        <ul>
          {products.map((product: Product) => (
            <li key={product.id} className="ItemUnavailable__product" data-testid={product.id}>
              <Checkbox
                locale={getLocale()}
                product={product}
                checked={selectedProducts.includes(product)}
                onChange={(checked) => {
                  onProductChange(product, checked);
                }}
              />

              {product.customisations && (
                <ul className="ItemUnavailable_customisationItems__list">
                  {product.customisations.map((item) => (
                    <li key={item.id}>
                      <Checkbox
                        locale={getLocale()}
                        customization={item}
                        checked={selectedCustomizations.includes(item)}
                        onChange={(checked) => {
                          onCustomizationChange(item, checked);
                          onProductChange(product, true);
                        }}
                      />
                    </li>
                  ))}
                </ul>
              )}
            </li>
          ))}
        </ul>
      </div>
    </>
  );
}
