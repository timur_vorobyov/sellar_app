import '@testing-library/jest-dom';
import { cleanup, fireEvent, screen } from '@testing-library/react';
import { Locale } from 'types/translation';
import { customI18nRender } from 'utils/testing/customRender';
import { products } from '__mocks__/mocks';
import ItemUnavailable from './ItemUnavailable';

afterEach(cleanup);

describe('<TooBusy> component', () => {
  it('should render component', () => {
    const onSelectAll = jest.fn();
    const onProductChange = jest.fn();
    const onCustomizationChange = jest.fn();
    const { container } = customI18nRender(
      <ItemUnavailable
        products={products}
        selectedProducts={[]}
        selectedCustomizations={[]}
        onProductChange={onProductChange}
        onCustomizationChange={onCustomizationChange}
        onSelectAll={onSelectAll}
      />,
      {
        locale: Locale.ENGLISH,
      },
    );

    const selectAll = screen.getByText('Select all');
    expect(selectAll).toBeInTheDocument();
    fireEvent.click(selectAll);
    expect(onSelectAll).toHaveBeenCalled();

    const checkBoxes = container.getElementsByClassName('RejectSubOrder__checkbox__icon');
    fireEvent.click(checkBoxes[0]);
    expect(onProductChange).toHaveBeenCalled();

    fireEvent.click(checkBoxes[checkBoxes.length - 1]);
    expect(onProductChange).toHaveBeenCalled();
  });
});
