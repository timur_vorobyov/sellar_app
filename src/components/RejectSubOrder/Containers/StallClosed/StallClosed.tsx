import React from 'react';
import './StallClosed.scss';
import Box from '../../../ui-kit/Box';
import Typography from '../../../ui-kit/Typography';

export default function StallClosed() {
  return (
    <Box pt={10} pb={10} pl={20} pr={20} addClass="StallClosed__container">
      <Typography variant="h2" fontWeight="bold" className="StallClosed__title">
        Your stall status will be set to ‘Close’. 您的店将转换到《营业结束》。
      </Typography>
    </Box>
  );
}
