import { useEuiI18n } from '@elastic/eui';
import ConfirmOrderIcon from 'components/assets/images/SubOrderPage/confirmOrderIcon.svg';
import { SubOrderRejectionReason } from 'constants/subOrder';
import { Product, ProductCustomisation } from 'types/models/product';
import './RejectConfirmationMessage.scss';

interface RejectConfirmationMessageProps {
  option: string;
  products: Product[];
  customizations: ProductCustomisation[];
  time: number;
}

export default function RejectConfirmationMessage({
  option,
  products,
  customizations,
  time,
}: RejectConfirmationMessageProps) {
  const [titleText, merchantClosedText] = useEuiI18n(
    ['rejectSubOrder.confirmation.title', 'rejectSubOrder.confirmation.merchantClosed'],
    [],
  );
  const merchantBusyText = useEuiI18n('rejectSubOrder.confirmation.merchantBusy', '', { time });

  const formatProducts = (products: Product[]) => {
    if (products.length === 0) {
      return;
    }
    const productsStr = products.map((product) => product.name).join(', ');
    const customisationItemsStr = customizations.map((item) => item.name).join(', ');
    return productsStr + ', ' + customisationItemsStr;
  };

  return (
    <div className="RejectConfirmationMessage__container">
      <img src={ConfirmOrderIcon} />
      <p className="RejectConfirmationMessage__title">{titleText}</p>
      {option === SubOrderRejectionReason.ItemUnavailable && (
        <p className="RejectConfirmationMessage__text">
          {products && formatProducts(products)} has been marked unavailable.
        </p>
      )}
      {option === SubOrderRejectionReason.MerchantBusy && (
        <p className="RejectConfirmationMessage__text">{merchantBusyText}</p>
      )}
      {option === SubOrderRejectionReason.MerchantClosed && (
        <p className="RejectConfirmationMessage__text">{merchantClosedText}</p>
      )}
    </div>
  );
}
