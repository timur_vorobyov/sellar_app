import '@testing-library/jest-dom';
import { render, screen, cleanup, waitFor } from '@testing-library/react';
import { useEffect } from 'react';
import Loader from './Loader';
import { useStateContext, StateProvider } from 'contexts/store';
import { ActionTypes } from 'types/store';

afterEach(cleanup);

interface LoaderWithContextProps {
  isLoaderShown: boolean;
}

const LoaderWithContext = ({ isLoaderShown }: LoaderWithContextProps) => {
  const { dispatch } = useStateContext();

  useEffect(
    () =>
      dispatch({
        type: ActionTypes.SET_LOADER_STATE,
        payload: { loading: isLoaderShown },
      }),
    [],
  );
  return <Loader />;
};

describe('<Loader> component', () => {
  it('should show Loader', async () => {
    render(
      <StateProvider>
        <LoaderWithContext isLoaderShown={true} />
      </StateProvider>,
    );

    const overlay = await screen.findByTestId('loader');

    expect(overlay).toBeInTheDocument();
  });

  it('should hide Loader', async () => {
    render(
      <StateProvider>
        <LoaderWithContext isLoaderShown={false} />
      </StateProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('loader')).not.toBeInTheDocument();
    });
  });
});
