import { EuiLoadingSpinner } from '@elastic/eui';
import { useStateContext } from 'contexts/store';

import './Loader.scss';

export default function Loader() {
  const {
    state: { loaders },
  } = useStateContext();

  if (!loaders) {
    return null;
  }

  return (
    <div className="Loader__overlay" data-testid="loader">
      <EuiLoadingSpinner size="xl" />
    </div>
  );
}
