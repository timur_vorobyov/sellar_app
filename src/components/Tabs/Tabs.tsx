import { useState } from 'react';
import { EuiTabs, EuiTab, EuiBetaBadge } from '@elastic/eui';
import './Tabs.scss';

interface TabsProps {
  firstTabText: string;
  firstTabContent: JSX.Element;
  firstTabAppend?: number | string | null;
  secondTabText: string;
  secondTabContent: JSX.Element;
  secondTabAppend?: number | string | null;
}

export default function Tabs({
  firstTabText,
  firstTabContent,
  firstTabAppend,
  secondTabText,
  secondTabContent,
  secondTabAppend,
}: TabsProps) {
  const [choosenTab, setChoosenTab] = useState('first');

  const getTabBadge = (tabAppend: number | string | null) => {
    if (!tabAppend) {
      return null;
    }
    return (
      <EuiBetaBadge color="accent" label={`${tabAppend || 0}`} className="euiBetaBadge--singleLetter"></EuiBetaBadge>
    );
  };

  const getFormattedTabBadge = (append: number | string | null | undefined): string => {
    if (append) {
      return `(${append})`;
    }

    return '';
  };

  return (
    <div className="Tabs__container">
      <div className="Tabs__container__header">
        <EuiTabs>
          <EuiTab
            isSelected={choosenTab === 'first'}
            onClick={() => setChoosenTab('first')}
            append={firstTabAppend && getTabBadge(firstTabAppend)}
          >
            {firstTabText}
          </EuiTab>
          <EuiTab
            isSelected={choosenTab === 'second'}
            onClick={() => setChoosenTab('second')}
            append={getFormattedTabBadge(secondTabAppend)}
          >
            {secondTabText}
          </EuiTab>
        </EuiTabs>
      </div>

      {choosenTab === 'first' ? firstTabContent : secondTabContent}
    </div>
  );
}
