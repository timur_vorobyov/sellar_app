import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import Tabs from './Tabs';

afterEach(cleanup);

const firstTabContent = () => {
  return <div>first tab content</div>;
};

const secondTabContent = () => {
  return <div>second tab content</div>;
};

describe('<Tabs> component', () => {
  it('should render with first tab', () => {
    render(
      <Tabs
        firstTabText="Incoming"
        firstTabContent={firstTabContent()}
        firstTabAppend={4}
        secondTabText="Accepted"
        secondTabContent={secondTabContent()}
        secondTabAppend={2}
      />,
    );
    expect(screen.getByText('4')).toBeInTheDocument();
    expect(screen.getByText('(2)')).toBeInTheDocument();
    expect(screen.getByText('first tab content')).toBeInTheDocument();
    expect(screen.queryByText('second tab content')).not.toBeInTheDocument();
  });

  it('should switch to second tab content when clicked', () => {
    render(
      <Tabs
        firstTabText="Incoming"
        firstTabContent={firstTabContent()}
        firstTabAppend={4}
        secondTabText="Accepted"
        secondTabContent={secondTabContent()}
        secondTabAppend={2}
      />,
    );
    const secondTab = screen.getByText('Accepted');
    fireEvent.click(secondTab);

    expect(screen.getByText('second tab content')).toBeInTheDocument();
    expect(screen.queryByText('first tab content')).not.toBeInTheDocument();
  });

  it('should switch to first tab when second active', () => {
    render(
      <Tabs
        firstTabText="Incoming"
        firstTabContent={firstTabContent()}
        firstTabAppend={4}
        secondTabText="Accepted"
        secondTabContent={secondTabContent()}
        secondTabAppend={2}
      />,
    );
    const firstTab = screen.getByText('Incoming');
    const secondTab = screen.getByText('Accepted');
    fireEvent.click(secondTab);
    fireEvent.click(firstTab);

    expect(screen.getByText('first tab content')).toBeInTheDocument();
    expect(screen.queryByText('second tab content')).not.toBeInTheDocument();
  });

  it('should render without TabAppend', () => {
    render(
      <Tabs
        firstTabText="Incoming"
        firstTabContent={firstTabContent()}
        firstTabAppend={4}
        secondTabText="Accepted"
        secondTabContent={secondTabContent()}
        secondTabAppend={2}
      />,
    );

    expect(screen.queryByText('(4)')).not.toBeInTheDocument();
    expect(screen.queryByText('2')).not.toBeInTheDocument();
  });
});
