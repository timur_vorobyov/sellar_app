import { useCallback, useEffect } from 'react';
import alertAudioFile from 'components/assets/audio/chime.ogg';
import jpAlertAudioFile from 'components/assets/audio/cling-loud.mp3';
import { useFoodSupplierContext } from 'contexts/foodSupplierProvider/context';

export default function useAlertAudio() {
  const { getFoodSupplierId } = useFoodSupplierContext();
  const foodSupplierId = Number(getFoodSupplierId());

  const alertAudio =
    foodSupplierId >= 1 && foodSupplierId <= 23 ? new Audio(jpAlertAudioFile) : new Audio(alertAudioFile);

  alertAudio.loop = true;
  alertAudio.preload = 'auto';

  const playAlert = useCallback(() => {
    alertAudio.play();
  }, []);

  const stopAlert = useCallback(() => {
    if (alertAudio.currentTime || !alertAudio.paused) {
      alertAudio.pause();
    }
  }, []);

  useEffect(() => {
    window.addEventListener('online', stopAlert);
    return () => {
      window.removeEventListener('online', stopAlert);
    };
  }, [stopAlert]);

  return {
    playAlert,
    stopAlert,
  };
}
