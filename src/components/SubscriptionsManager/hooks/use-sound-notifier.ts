import { useEffect } from 'react';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import { IntercomCommand } from 'contexts/intecomContextProvider/types';
import { useIntercomContext } from 'contexts/intecomContextProvider/context';
import { useTreatments } from '@splitsoftware/splitio-react';
import { FeatureFlags } from 'constants/featureFlags';
import { useStateContext } from 'contexts/store';
import useAlertAudio from './use-alert-audio';

const useSoundNotifier = () => {
  const {
    state: { amountOfNewSubOrders, incomingSubOrders },
  } = useStateContext();
  const feat = useTreatments([FeatureFlags.NOTIFICATION_SOUND]);
  const { isTablet } = useDeviceContext();
  const { postMessage } = useIntercomContext();

  const { playAlert, stopAlert } = useAlertAudio();

  useEffect(() => {
    if (feat[FeatureFlags.NOTIFICATION_SOUND].treatment === 'on') {
      amountOfNewSubOrders > 0 ? playAlert() : stopAlert();
    }

    if (isTablet) {
      if (incomingSubOrders) {
        postMessage(IntercomCommand.incomingOrdersCount, { value: amountOfNewSubOrders });
      }
    }
  }, [amountOfNewSubOrders, incomingSubOrders]);
};

export default useSoundNotifier;
