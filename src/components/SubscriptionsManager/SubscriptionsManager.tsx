import {
  subscribeToAcceptedSubOrdersList,
  subscribeToFoodSupplierPlatformsList,
  subscribeToIncomingSubOrdersList,
} from 'apis/graphql/hasura/subscriptions';
import { FoodSupplierDbType, SubOrderDbType } from 'apis/graphql/hasura/types';
import { formattedFoodSupplierPlatformData, formattedSubOrdersList } from 'apis/graphql/hasura/utils';
import { useEffect } from 'react';
import { ActionTypes } from 'types/store';
import { datadogRum } from '@datadog/browser-rum';
import axiosApiInstance from 'apis/rest/auth/axiosInstance';
import { getCategoriesListForSubOrder, getFoodSupplier } from 'apis/graphql/hasura/queries';
import { SubOrder } from 'types/models/subOrder';
import { getStorageSerialNumber } from 'storage/adapters/serialNumber';
import { useFoodSupplierContext } from 'contexts/foodSupplierProvider/context';
import { useStateContext } from 'contexts/store';
import useSoundNotifier from './hooks/use-sound-notifier';
import useExplicitApiCall from 'utils/hooks/use-explicit-api-call';
import { MenuCategory } from 'types/models/menuCaregory';

const SubscriptionsManager = () => {
  useSoundNotifier();
  const { getFoodSupplierId } = useFoodSupplierContext();
  const foodSupplierId = getFoodSupplierId();
  const { dispatch } = useStateContext();
  const { execute, data } = useExplicitApiCall(getCategoriesListForSubOrder);

  const getSubOrdersWithCustomers = async (
    subOrders: SubOrderDbType[],
    foodSupplier: FoodSupplierDbType,
    categories: MenuCategory[],
  ): Promise<SubOrder[]> => {
    const ordersIds = subOrders.map((subOrder: SubOrderDbType) => subOrder.order.id);
    const ordersIdsParams = ordersIds.join(',');
    let customers = [];

    if (ordersIdsParams) {
      const url = `${process.env.REACT_APP_ORDER_SERVICE_API_URI}/customers?orderId=${ordersIdsParams}`;
      const response = await axiosApiInstance.get(url);
      customers = response.data.data.items;
    }

    subOrders.forEach((subOrder: SubOrderDbType) => {
      subOrder.sub_order_items.forEach((subOrderItem) => {
        if (!categories[subOrderItem.menu_item_id]) {
          execute(foodSupplier.id);
          return;
        }
      });
    });
    return formattedSubOrdersList(subOrders as SubOrderDbType[], foodSupplier, customers, categories);
  };

  const subscribeToIncomingSubOrders = async (foodSupplier: FoodSupplierDbType, categories: MenuCategory[]) => {
    const subscription = await subscribeToIncomingSubOrdersList(foodSupplier.id);
    const subscribeConnection = subscription.subscribe({
      async next(incomingSubOrdersList) {
        const incomingSubOrders = await getSubOrdersWithCustomers(
          incomingSubOrdersList.data.sub_orders,
          foodSupplier,
          categories,
        );

        dispatch({
          type: ActionTypes.SET_INCOMING_SUB_ORDERS,
          payload: {
            subOrders: incomingSubOrders,
          },
        });
      },
    });

    return subscribeConnection;
  };

  const subscribeToAcceptedSubOrders = async (foodSupplier: FoodSupplierDbType, categories: MenuCategory[]) => {
    const subscription = await subscribeToAcceptedSubOrdersList(foodSupplier.id);
    const subscribeConnection = subscription.subscribe({
      async next(acceptedSubOrdersList) {
        const acceptedSubOrders = await getSubOrdersWithCustomers(
          acceptedSubOrdersList.data.sub_orders,
          foodSupplier,
          categories,
        );
        dispatch({
          type: ActionTypes.SET_ACCEPTED_SUB_ORDERS,
          payload: {
            subOrders: acceptedSubOrders,
          },
        });
      },
    });

    return subscribeConnection;
  };

  const subscribeToPlatforms = async (foodSupplierId: number) => {
    const subscription = await subscribeToFoodSupplierPlatformsList(foodSupplierId);
    const subscribeConnection = subscription.subscribe({
      next(foodSupplierPlatformsList) {
        dispatch({
          type: ActionTypes.SET_FOOD_SUPPLIER_PLATFORMS,
          payload: {
            platforms: formattedFoodSupplierPlatformData(foodSupplierPlatformsList.data.food_supplier_platforms),
          },
        });
      },
    });

    return subscribeConnection;
  };

  useEffect(() => {
    if (foodSupplierId) {
      execute(foodSupplierId);
    }
  }, [foodSupplierId]);

  useEffect(() => {
    (async () => {
      if (foodSupplierId && data) {
        const foodSupplier: FoodSupplierDbType = await getFoodSupplier(foodSupplierId);

        datadogRum.addRumGlobalContext('food_supplier_id', foodSupplierId);
        datadogRum.addRumGlobalContext('food_supplier_name', foodSupplier.name);
        datadogRum.addRumGlobalContext('food_supplier_stall_number', foodSupplier.stall_number);
        datadogRum.addRumGlobalContext('food_supplier_location', foodSupplier.location.id);
        datadogRum.addRumGlobalContext('food_supplier_location_name', foodSupplier.location.name);
        datadogRum.addRumGlobalContext('food_supplier_location_is_outlet', foodSupplier.location.is_outlet);

        const device = foodSupplier.food_supplier_devices.find(
          (supplierDevice) => supplierDevice.serial_number === getStorageSerialNumber(),
        );
        if (device) {
          datadogRum.addRumGlobalContext('food_supplier_device_serial_number', device.serial_number);
          datadogRum.addRumGlobalContext('food_supplier_device_name', device.name);
        }

        const subscribeToIncoming = await subscribeToIncomingSubOrders(foodSupplier, data as MenuCategory[]);
        const subscribeToAccepted = await subscribeToAcceptedSubOrders(foodSupplier, data as MenuCategory[]);
        const subscribeToPlatformsList = await subscribeToPlatforms(foodSupplier.id);

        return () => {
          subscribeToIncoming.unsubscribe();
          subscribeToAccepted.unsubscribe();
          subscribeToPlatformsList.unsubscribe();
        };
      }
    })();
  }, [foodSupplierId, data]);

  return <></>;
};

export default SubscriptionsManager;
