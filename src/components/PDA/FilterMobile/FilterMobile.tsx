import { EuiCheckbox, useEuiI18n } from '@elastic/eui';
import { useEffect, useState } from 'react';
import './FilterMobile.scss';
import { Option } from 'types/generics';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import Typography from 'components/ui-kit/Typography';

interface FilterProps {
  options: Option<number>[] | null;
  secondOptions: Option<number>[] | null;
  selected: number[];
  secondSelected: number[];
  label: string;
  secondLabel: string;
  onApply: (selected: number[], secondSelected: number[]) => void;
  backToMenu: () => void;
}

export const FilterMobile = ({
  options,
  secondOptions,
  label,
  secondLabel,
  selected,
  secondSelected,
  onApply,
  backToMenu,
}: FilterProps): JSX.Element => {
  const [selectedCheckboxes, setSelectedCheckboxes] = useState<number[]>([]);
  const [selectedAvailabilityCheckboxes, setSelectedAvailabilityCheckboxes] = useState<number[]>([]);
  const [isApplyBtnDisabled, setIsApplyBtnDisabled] = useState(true);

  const [clearFilterSelectionText, applyFilterSelectionText] = useEuiI18n(
    ['menu.filters.buttons.clear', 'menu.filters.popover.apply'],
    [],
  );

  useEffect(() => {
    if (selected && selected.length) {
      setSelectedCheckboxes(selected);
    }
  }, [selected]);

  useEffect(() => {
    if (secondSelected && secondSelected.length) {
      setSelectedAvailabilityCheckboxes(secondSelected);
    }
  }, [secondSelected]);

  useEffect(() => {
    if (
      selected.length == selectedCheckboxes.length &&
      selected.every((el) => selectedCheckboxes.includes(el)) &&
      secondSelected.length == selectedAvailabilityCheckboxes.length &&
      secondSelected.every((el) => selectedAvailabilityCheckboxes.includes(el))
    ) {
      setIsApplyBtnDisabled(true);
    }
  }, [selectedCheckboxes, selectedAvailabilityCheckboxes]);

  const handleCheckboxChange = (optionId: number, checked: boolean) => {
    setIsApplyBtnDisabled(false);
    if (checked) {
      if (!selectedCheckboxes.includes(optionId)) {
        setSelectedCheckboxes([...selectedCheckboxes, optionId]);
      }
    } else {
      if (selectedCheckboxes.includes(optionId)) {
        setSelectedCheckboxes(selectedCheckboxes.filter((id) => id !== optionId));
      }
    }
  };

  const handleAvailabilityCheckboxChange = (optionId: number, checked: boolean) => {
    setIsApplyBtnDisabled(false);
    if (checked) {
      if (!selectedAvailabilityCheckboxes.includes(optionId)) {
        setSelectedAvailabilityCheckboxes([...selectedAvailabilityCheckboxes, optionId]);
      }
    } else {
      if (selectedAvailabilityCheckboxes.includes(optionId)) {
        setSelectedAvailabilityCheckboxes(selectedAvailabilityCheckboxes.filter((id) => id !== optionId));
      }
    }
  };

  return (
    <div className="FilterMobile__container">
      <div className="FilterMobile__content">
        <div className="FilterMobile__header">{label}</div>
        <div className="FilterMobile__items">
          {options &&
            options.map((option, index) => {
              return (
                <EuiCheckbox
                  className="FilterMobile__checkbox"
                  key={index}
                  id={option.value.toString()}
                  label={<Typography variant="span">{option.text}</Typography>}
                  checked={selectedCheckboxes.includes(option.value)}
                  onChange={(e) => handleCheckboxChange(option.value, e.target.checked)}
                />
              );
            })}
        </div>
        <div className="FilterMobile__header availability">{secondLabel}</div>
        <div className="FilterMobile__items">
          {secondOptions &&
            secondOptions.map((option, index) => {
              return (
                <EuiCheckbox
                  className="FilterMobile__checkbox"
                  key={index}
                  id={`second-${option.value.toString()}`}
                  label={<Typography variant="span">{option.text}</Typography>}
                  checked={selectedAvailabilityCheckboxes.includes(option.value)}
                  onChange={(e) => handleAvailabilityCheckboxChange(option.value, e.target.checked)}
                />
              );
            })}
        </div>
      </div>
      <div className="FilterMobile__footer">
        <CustomButton
          size="max"
          color="red"
          padding="no"
          disabled={!selectedCheckboxes.length && !selectedAvailabilityCheckboxes.length}
          onClick={() => {
            setSelectedCheckboxes([]);
            setSelectedAvailabilityCheckboxes([]);
            setIsApplyBtnDisabled(false);
          }}
          text={clearFilterSelectionText}
        />
        <CustomButton
          size="max"
          color="green"
          padding="no"
          disabled={isApplyBtnDisabled}
          onClick={() => {
            onApply(selectedCheckboxes, selectedAvailabilityCheckboxes);
            setIsApplyBtnDisabled(true);
            backToMenu();
          }}
          text={applyFilterSelectionText}
        />
      </div>
    </div>
  );
};
