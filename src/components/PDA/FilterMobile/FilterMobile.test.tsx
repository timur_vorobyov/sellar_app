import { cleanup, fireEvent } from '@testing-library/react';
import { screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { FilterMobile } from './FilterMobile';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

const options = [
  {
    value: 1,
    text: 'Frist option',
  },
  {
    value: 2,
    text: 'Second option',
  },
  {
    value: 3,
    text: 'Third option',
  },
];

const availabilityOptions = [
  {
    value: 4,
    text: 'Available',
  },
  {
    value: 5,
    text: 'UnAvailable',
  },
];

const onApply = jest.fn();

const label = 'FilterMobile';

const selected = [1, 2, 3];
const availabilitSelected = [4, 5];

describe('<FilterMobile> component', () => {
  it('should be able to apply changes', () => {
    customI18nRender(
      <FilterMobile
        label={label}
        secondLabel={label}
        options={options}
        secondOptions={availabilityOptions}
        selected={selected}
        secondSelected={availabilitSelected}
        onApply={onApply}
        backToMenu={() => null}
      />,
      { locale: Locale.ENGLISH },
    );

    const checkbox = screen.getByLabelText('Second option');
    fireEvent.click(checkbox);
    const availabilityCheckbox = screen.getByLabelText('Available');
    fireEvent.click(availabilityCheckbox);

    const clearButton = screen.getByText('Clear');
    expect(clearButton).toBeTruthy();
    fireEvent.click(clearButton);

    const applyButton = screen.getByText('Apply');
    expect(applyButton).toBeTruthy();
    fireEvent.click(applyButton);
    expect(onApply).toHaveBeenCalled();
  });
});
