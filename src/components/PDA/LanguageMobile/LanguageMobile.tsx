import { useState } from 'react';
import { useEuiI18n } from '@elastic/eui';
import CustomButton from 'components/ui-kit/CustomButton/CustomButton';
import { Locale } from 'types/translation';
import './LanguageMobile.scss';
import Box from 'components/ui-kit/Box';
import Typography from 'components/ui-kit/Typography';
import { useLanguageContext } from 'contexts/languageContextProvider/context';

interface LanguageMobileProps {
  afterSave?: () => void;
  afterCancel?: () => void;
}
export const LanguageMobile = ({ afterSave, afterCancel }: LanguageMobileProps) => {
  const [cancelButtonText, saveButtonText] = useEuiI18n(['language.cancelButton', 'language.saveButton'], []);
  const { getLocale, setLocale } = useLanguageContext();

  const [selectedLanguageState, setSelectedLanguageState] = useState(getLocale());

  const setLanguage = () => {
    if (getLocale() !== selectedLanguageState) {
      setLocale(selectedLanguageState);
    }
  };
  const onSave = () => {
    setLanguage();
    if (afterSave) {
      afterSave();
    }
  };

  const onCancel = () => {
    if (afterCancel) {
      afterCancel();
    }
  };
  return (
    <>
      <div className="LanguageMobile__container MoreMobile__container">
        <div className="LanguageMobile__content MoreMobile__content">
          <Box mb={30}>
            <div className="LanguageMobile__radio" onClick={() => setSelectedLanguageState(Locale.ENGLISH)}>
              <input
                type="radio"
                name="locale"
                id="en"
                value={Locale.ENGLISH}
                checked={selectedLanguageState === Locale.ENGLISH}
              />
              <label htmlFor="en">
                <Typography variant="span" fontWeight="bold">
                  EN only
                </Typography>
              </label>
            </div>
            <div className="LanguageMobile__radio" onClick={() => setSelectedLanguageState(Locale.CHINESE)}>
              <input
                type="radio"
                name="locale"
                id="cn"
                value={Locale.CHINESE}
                checked={selectedLanguageState === Locale.CHINESE}
              />
              <label htmlFor="cn">
                <Typography variant="span" fontWeight="bold">
                  EN & 中文
                </Typography>
              </label>
            </div>
          </Box>
          <div className="LanguageMobile__footer">
            <CustomButton size="max" padding="no" color="red" onClick={onCancel} text={cancelButtonText} />
            <CustomButton
              size="max"
              padding="no"
              disabled={getLocale() === selectedLanguageState}
              onClick={onSave}
              text={saveButtonText}
            />
          </div>
        </div>
      </div>
    </>
  );
};
