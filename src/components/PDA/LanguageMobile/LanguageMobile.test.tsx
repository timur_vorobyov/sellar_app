import '@testing-library/jest-dom';
import { cleanup, fireEvent, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { Locale } from 'types/translation';
import { customI18nRender } from 'utils/testing/customRender';
import { LanguageMobile } from './LanguageMobile';

afterEach(cleanup);

describe('<LanguageMobile> component', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should display languages and change locale', () => {
    customI18nRender(
      <BrowserRouter>
        <LanguageMobile />
      </BrowserRouter>,
      {
        locale: Locale.ENGLISH,
      },
    );

    const radioButton = screen.getByText('EN & 中文');
    expect(radioButton).toBeInTheDocument();

    fireEvent.click(radioButton);

    const button = screen.getByText('Save');
    expect(button).toBeInTheDocument();

    fireEvent.click(button);
    expect(window.location.href).toBe('http://localhost/');
  });
});
