import './HeaderMobile.scss';
import BackIcon from 'components/assets/images/HeaderMobile/arrow-back.svg';
import Typography from 'components/ui-kit/Typography';

interface HeaderMobileProps {
  text: string;
  onButtonClick: () => void;
}

const HeaderMobile = ({ text, onButtonClick }: HeaderMobileProps): JSX.Element => {
  return (
    <div className="HeaderMobile__container" onClick={onButtonClick}>
      <img src={BackIcon} alt="back navigation" />
      <Typography color="black" variant="span" fontWeight="bold">
        {text}
      </Typography>
    </div>
  );
};

export default HeaderMobile;
