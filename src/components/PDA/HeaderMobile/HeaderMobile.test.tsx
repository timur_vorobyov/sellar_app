import { cleanup, fireEvent, render } from '@testing-library/react';
import { screen } from '@testing-library/react';

import '@testing-library/jest-dom';
import HeaderMobile from './HeaderMobile';

afterEach(cleanup);
const onClick = jest.fn();

describe('<HeaderMobile> component', () => {
  it('should render component without errors', () => {
    const testText = 'test Text';
    render(<HeaderMobile text={testText} onButtonClick={onClick} />);

    const header = screen.getByText(testText);
    expect(header).toBeInTheDocument();

    fireEvent.click(header);
    expect(onClick).toHaveBeenCalled();
  });
});
