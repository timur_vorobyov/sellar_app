import './NavBarMobile.scss';
import { useHistory, useLocation } from 'react-router-dom';
import MenuIcon from 'components/assets/images/NavBarMobile/menuIcon.svg';
import { routes } from 'config/routes';
import { useState } from 'react';
import MenuMobile from './MenuMobile/MenuMobile';
import { useEuiI18n } from '@elastic/eui';
import { ReactComponent as BellIcon } from 'components/assets/images/HeaderMobile/bell.svg';
import { ReactComponent as ActiveBellIcon } from 'components/assets/images/HeaderMobile/active-bell.svg';
import { ReactComponent as PrintIcon } from 'components/assets/images/HistoryPage/printIcon.svg';
import { printZReportReceipt } from 'utils/receipt';
import { ZReportData } from 'types/ZReportData';
import { getStorageSerialNumber } from 'storage/adapters/serialNumber';
import { useStateContext } from 'contexts/store';

interface NavBarMobileProps {
  zReportData?: ZReportData | null;
}

const NavBarMobile = ({ zReportData }: NavBarMobileProps): JSX.Element => {
  const location = useLocation();
  const [showMenu, setShowMenu] = useState(false);
  const history = useHistory();
  const route = Object.values(routes).find((route) => route.path === location.pathname);
  const topNavBarTitle = useEuiI18n(route?.title || '', '');
  const isHistoryPage = route?.path === '/history';
  const {
    state: { amountOfNewSubOrders },
  } = useStateContext();

  return (
    <>
      <nav className="NavBarMobile__container">
        <div className="NavBarMobile__left">
          <div className="MenuMobile__button">
            <img src={MenuIcon} width="18" alt="MenuIcon" onClick={() => setShowMenu(true)} />
          </div>
          {topNavBarTitle}
        </div>
        <div className="NavBarMobile__icons">
          {isHistoryPage ? (
            <PrintIcon
              className="NavBarMobile__printIcon "
              onClick={() => printZReportReceipt(zReportData ?? null, getStorageSerialNumber(), false)}
            />
          ) : null}
          {amountOfNewSubOrders ? (
            <div className="NavBarMobile__bellIcon active" onClick={() => history.push('/')}>
              <ActiveBellIcon />
            </div>
          ) : (
            <div className="NavBarMobile__bellIcon " onClick={() => history.push('/')}>
              <BellIcon />
            </div>
          )}
        </div>
      </nav>
      {showMenu && <MenuMobile onClose={() => setShowMenu(false)} routes={routes} />}
    </>
  );
};
export default NavBarMobile;
