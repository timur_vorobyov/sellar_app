import { cleanup } from '@testing-library/react';
import { screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import '@testing-library/jest-dom';
import MenuMobile from './MenuMobile';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';
import { StateProvider, useStateContext } from 'contexts/store';
import { ActionTypes } from 'types/store';
import { useEffect } from 'react';
import { foodSupplierPlatforms } from '__mocks__/mocks';
import { FoodSupplierPlatform } from 'types/models/stall';
import { routes } from 'config/routes';

afterEach(cleanup);

const onClose = jest.fn();

interface MenuMobileWithContextProps {
  platforms: FoodSupplierPlatform[];
}

const MenuMobileWithContext = ({ platforms }: MenuMobileWithContextProps) => {
  const { dispatch } = useStateContext();

  useEffect(() => {
    dispatch({
      type: ActionTypes.SET_FOOD_SUPPLIER_PLATFORMS,
      payload: { platforms },
    });
  }, [platforms]);
  return <MenuMobile onClose={onClose} routes={routes} />;
};

describe('<MenuMobile> component', () => {
  it('should render component', () => {
    customI18nRender(
      <StateProvider>
        <BrowserRouter>
          <MenuMobileWithContext platforms={foodSupplierPlatforms} />
        </BrowserRouter>
      </StateProvider>,
      { locale: Locale.ENGLISH },
    );
    expect(screen.queryAllByAltText('back')[0]).toBeInTheDocument();
  });
});
