import { EuiButton, EuiIcon, useEuiI18n } from '@elastic/eui';
import config from 'config';
import { RouteId as SappRouteId } from 'config/routes';
import { useStateContext } from 'contexts/store';
import { PropsWithChildren, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import './MenuMobile.scss';
import BackIcon from 'components/assets/images/HeaderMobile/arrow-back.svg';
import LanguageIcon from 'components/PDA/NavBarMobile/MenuMobile/images/Menu/languageIcon.svg';
import { Route } from 'types/router';
import { LanguageMobile } from 'components/PDA/LanguageMobile/LanguageMobile';

interface TopNavBarOptions {
  path: string;
  title: string;
  description: string;
  icon?: string;
  onClick: () => void;
}

interface MenuMobileProps {
  onClose: () => void;
  routes: Record<SappRouteId, Route>;
}

const getLocalString = (value: string) => {
  return useEuiI18n(value, value);
};

const MenuMobile = (props: PropsWithChildren<MenuMobileProps>) => {
  const history = useHistory();
  const location = useLocation();
  const {
    state: { foodSupplierPlatforms },
  } = useStateContext();

  const logo = foodSupplierPlatforms[0]?.logo;
  const [languageOption, showLanguageOptions] = useState(false);
  const topNavBarOptions: TopNavBarOptions[] = Object.values(props.routes)
    .filter((el) => el.title)
    .map((el) => {
      const routeName = getLocalString(el.title || '');
      const routeDescription = getLocalString(el.description || '');

      return {
        path: el.path,
        title: routeName,
        description: routeDescription,
        icon: el.icon,
        onClick: () => {
          location.pathname === el.path ? props.onClose() : history.push(el.path);
        },
      };
    });

  const closeLanguageOptions = () => {
    showLanguageOptions(false);
  };
  const [languageTitleText] = useEuiI18n(['navbar.routeLanguage'], []);
  return (
    <div className="MenuMobile__container">
      <div className="MenuMobile__header">
        <div className="MenuMobile__header--logo" onClick={() => props.onClose()}>
          <img alt="back" src={BackIcon} />
          {!!logo && <img src={logo} alt="logo" />}
        </div>
        <div className="MenuMobile__header__text">
          Food Seller App version:
          {config.buildVersion}
          <EuiButton size="s" fill onClick={() => window.location.reload()} className="MenuMobile__refreshButton">
            <EuiIcon type="refresh" size="xl" />
          </EuiButton>
        </div>
      </div>
      <div className="MenuMobile__items">
        {topNavBarOptions.map((option) => (
          <div key={option.title} className="MenuMobile__item" onClick={() => option.onClick()}>
            <div className="MenuMobile__item__icon">
              {!!option.icon && <img src={option.icon} width="28" alt="moreIcon" />}
            </div>
            <div>
              <div className="MenuMobile__item__title">{option.title}</div>
              <div className="MenuMobile__item__description">{option.description}</div>
            </div>
          </div>
        ))}
        <div
          key="LANGUAGE_OPTION"
          className="MenuMobile__item"
          onClick={() => {
            showLanguageOptions(true);
          }}
        >
          <div className="MenuMobile__item__icon">
            <img src={LanguageIcon} width="28" alt="Language Option" />
          </div>
          <div>
            <div className="MenuMobile__item__title">{languageTitleText}</div>
          </div>
          {languageOption && <LanguageMobile afterSave={closeLanguageOptions} afterCancel={closeLanguageOptions} />}
        </div>
      </div>
    </div>
  );
};

export default MenuMobile;
