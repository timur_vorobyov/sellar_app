import { cleanup, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { BrowserRouter } from 'react-router-dom';
import NavBarMobile from './NavBarMobile';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<NavBarMobile> component', () => {
  it('should render component and change to Menu page', () => {
    customI18nRender(
      <BrowserRouter>
        <NavBarMobile />
      </BrowserRouter>,
      { locale: Locale.CHINESE },
    );

    expect(screen.getByText('Online Orders 网上订单')).toBeTruthy();
  });
});
