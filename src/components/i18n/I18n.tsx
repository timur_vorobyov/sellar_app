import { EuiContext } from '@elastic/eui';
import { Locale, Translation } from 'types/translation';
import { useState, useEffect } from 'react';
import { useLanguageContext } from 'contexts/languageContextProvider/context';
import translations from 'i18n/messages/translations.json';

export const I18n = (props: { children: JSX.Element }): JSX.Element => {
  const [translation, setTranslation] = useState<Translation>({});
  const { getLocale } = useLanguageContext();
  const locale = getLocale() as Locale;

  useEffect(() => {
    setTranslation(translations[locale]);
  }, [locale]);

  const eui18n = {
    mapping: translation,
  };

  return <EuiContext i18n={eui18n}>{props.children}</EuiContext>;
};
