import './MoreMobile.scss';
import MoreIcon from 'components/assets/images/MenuPage/moreIcon.svg';
import CheckIcon from 'components/assets/images/MenuPage/checkIcon.svg';
import { useState } from 'react';
import classnames from 'clsx';

interface Options {
  value: number | string;
  disabled?: boolean;
  text?: string;
  element?: JSX.Element;
}

interface MoreMobileProps {
  options: Options[];
  header: JSX.Element;
  selectedValue?: number | string;
  onChange: (value: number | string) => void;
}

export default function MoreMobile({ options, selectedValue, header, onChange }: MoreMobileProps) {
  const [showMore, setShowMore] = useState(false);

  return (
    <>
      <div className="MoreMobile__button" onClick={() => setShowMore(!showMore)}>
        <img src={MoreIcon} width="24" alt="moreIcon" />
      </div>
      {showMore && (
        <div className="MoreMobile__container" onClick={() => setShowMore(false)}>
          <div className="MoreMobile__content">
            <div className="MoreMobile__header">{header}</div>
            <div className="MoreMobile__items">
              {options.map((option) => {
                return (
                  <div
                    key={option.value}
                    className={classnames(
                      'MoreMobile__item',
                      { checked: selectedValue === option.value },
                      { disabled: option.disabled },
                    )}
                    onClick={(event) => {
                      option.disabled ? event.stopPropagation() : onChange(option.value);
                    }}
                  >
                    <div className="MoreMobile__item__check">
                      {selectedValue === option.value && <img src={CheckIcon} width="24" />}
                    </div>
                    <div>{option.text || option.element}</div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      )}
    </>
  );
}
