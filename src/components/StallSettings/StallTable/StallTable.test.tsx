import '@testing-library/jest-dom';
import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import StallTable from './StallTable';
import { customI18nRender } from 'utils/testing/customRender';
import { foodSupplierPlatforms } from '__mocks__/mocks';
import { Locale } from 'types/translation';
import { Dictionary } from 'types/generics';

afterEach(cleanup);

describe('<StallTable> component', () => {
  it('should render StallTable with data', () => {
    render(<StallTable foodSuppliers={foodSupplierPlatforms} onChangeAll={() => null} onChange={() => null} />);

    const logo = screen.getByAltText(`${foodSupplierPlatforms[0].name}`);
    expect(logo).toHaveAttribute('src', foodSupplierPlatforms[0].logo);
  });

  it('should change stallStatus', async () => {
    const mockedFoodSupplier = foodSupplierPlatforms[0];
    const onChangeMock = jest.fn((foodSupplierPlatform: Dictionary) => {
      mockedFoodSupplier.status = foodSupplierPlatform.status;
    });

    customI18nRender(
      <StallTable foodSuppliers={foodSupplierPlatforms} onChangeAll={() => null} onChange={onChangeMock} />,
      {
        locale: Locale.CHINESE,
      },
    );

    const statusSelector = screen.getAllByTestId('stall_status_selector')[0];
    fireEvent.click(statusSelector);

    const newStatusOption = screen.getByText('Closed 已关闭');
    fireEvent.click(newStatusOption);

    expect(onChangeMock).toBeCalled();
    expect(mockedFoodSupplier.status).toBe('CLOSED');
  });

  /* TODO: Uncomment this testing block, when auto accept functionality will be implemented
  it('should change autoAcceptSubOrders', () => {
    const mockedFoodSupplier = foodSupplierPlatforms[1];
    const onChangeMock = jest.fn((foodSupplierPlatform: Dictionary) => {
      mockedFoodSupplier.autoAccept = foodSupplierPlatform.autoAccept;
    });
    customI18nRender(
      <StallTable foodSuppliers={foodSupplierPlatforms} onChangeAll={() => null} onChange={onChangeMock} />,
      { locale: Locale.CHINESE },
    );

    const switcher = screen.getAllByTestId('switcher')[1];
    const autoPrintText = screen.getByText('Auto accept orders 自动接受订单');
    expect(autoPrintText).toBeInTheDocument();

    fireEvent.click(switcher);
    expect(mockedFoodSupplier.autoAccept).toBeTruthy();
  });
  */

  it('should change prepTime', () => {
    const mockedFoodSupplier = foodSupplierPlatforms[0];
    const onChangeMock = jest.fn((foodSupplierPlatform: Dictionary) => {
      mockedFoodSupplier.prepTime = foodSupplierPlatform.prepTime;
    });

    customI18nRender(
      <StallTable foodSuppliers={foodSupplierPlatforms} onChangeAll={() => null} onChange={onChangeMock} />,
      {
        locale: Locale.CHINESE,
      },
    );

    const prepTimeSelector = screen.getAllByTestId('prep_time_selector')[0];
    fireEvent.click(prepTimeSelector);

    const newPrepTimeOption = screen.getByText('45 mins 45 分钟');
    fireEvent.click(newPrepTimeOption);

    expect(mockedFoodSupplier.prepTime).toBe(45);
  });
});
