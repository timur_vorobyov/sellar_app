import { EuiHealth, EuiSuperSelect, EuiSwitch, useEuiI18n } from '@elastic/eui';
import { STALL_STATUS } from 'constants/stall';
import { TOO_BUSY_INTERVALS } from 'constants/subOrder';
import './StallTable.scss';
import { FoodSupplierPlatform } from 'types/models/stall';
import { Dictionary } from 'types/generics';
import { useTreatments } from '@splitsoftware/splitio-react';
import { FeatureFlags } from 'constants/featureFlags';
import Typography from '../../ui-kit/Typography';

interface StallTableProps {
  onChange: (foodSuppliers: Dictionary) => void;
  onChangeAll: (foodSuppliers: Dictionary[]) => void;
  foodSuppliers: FoodSupplierPlatform[];
}

export default function StallTable({ onChange, foodSuppliers }: StallTableProps) {
  const feat = useTreatments([FeatureFlags.AUTO_ACCEPT]);

  const onItemChange = (id: string, property: string, value: string | boolean | number) => {
    const supplier = foodSuppliers.find((s) => s.id === id);

    if (supplier) {
      onChange({
        id: supplier.id,
        [property]: value,
      });
    }
  };

  const [platformText, statusText, prepTimeText, autoAcceptOnlyText, manualAcceptOnlyText, autoAcceptSubOrdersText] =
    useEuiI18n(
      [
        'stallSettings.table.platform',
        'stallSettings.table.status',
        'stallSettings.table.acceptedSubOrderPrepTimeTitle',
        'stallSettings.table.autoAcceptOnly',
        'stallSettings.table.manualAcceptOnly',
        'stallSettings.table.autoAcceptSubOrders',
      ],
      [],
    );

  const acceptTimingText = useEuiI18n('timing.minutes', '5 mins', { count: 5 });

  /* hidden until BE implements this features*/

  // const onAllItemsChange = (property: string, value: string | boolean) => {
  //     const newFoodSuppliers: any = foodSuppliers.map((el) => {
  //         if (property === 'autoAccept' && typeof value === 'boolean') {
  //             if (el.autoAcceptOnly || el.manualAcceptOnly) {
  //                 return;
  //             }
  //             if (!value) {
  //                 return { id: el.id, [property]: value, autoPrint: false };
  //             }
  //             return { id: el.id, [property]: value };
  //         }

  //         if (!getSwitcherForAllState(foodSuppliers).autoAccept && property === 'autoPrint') {
  //             return;
  //         }

  //         if (property === 'status' && typeof value === 'string' && value !== 'CLOSED' && value !== 'OPEN') {
  //             return {
  //                 id: el.id,
  //                 status: 'CLOSED',
  //                 closeFor: value,
  //                 closeAt: dayjs(Date.now()).format('YYYY-MM-DDTHH:mm:SSS[Z]'),
  //             };
  //         }

  //         if ((property === 'status' && typeof value === 'string' && value === 'CLOSED') || value === 'OPEN') {
  //             return {
  //                 id: el.id,
  //                 status: value,
  //                 closeFor: null,
  //                 closeAt: null,
  //             };
  //         }

  //         return { id: el.id, [property]: value };
  //     });

  //     onChangeAll(newFoodSuppliers);
  // };

  // const getSwitcherForAllState = (foodSuppliers: FoodSupplierPlatform[]) => {
  //     const newFoodSuppliers = foodSuppliers.filter((el) => !el.manualAcceptOnly && !el.autoAcceptOnly);

  //     return {
  //         autoAccept: newFoodSuppliers.every((el) => el.autoAccept),
  //         autoPrint: newFoodSuppliers.every((el) => el.autoPrint),
  //     };
  // };

  // const handleAutoPrintSwitcherText = (autoAccept: boolean, manualAcceptOnly: boolean | undefined) => {
  //     if (manualAcceptOnly) {
  //         return <span className="SwitchCheckbox__text">Not applicable不适用</span>;
  //     }
  //     if (!autoAccept) {
  //         return <span className="SwitchCheckbox__text">Turn on auto accept first 先打开自动接收</span>;
  //     }

  //     return null;
  // };

  const handleAutoAcceptSwitcherText = (autoAcceptOnly?: boolean) => {
    if (autoAcceptOnly) {
      return (
        <Typography variant="span" className="SwitchCheckbox__text">
          {autoAcceptOnlyText}
        </Typography>
      );
    }
    return (
      <Typography variant="span" className="SwitchCheckbox__text">
        {manualAcceptOnlyText}
      </Typography>
    );
  };

  const tooBusyOptions = TOO_BUSY_INTERVALS.map((interval) => {
    const text = useEuiI18n('timing.minutes', `${interval} mins`, { count: interval });

    return {
      value: `${interval}`,
      dropdownDisplay: (
        <Typography variant="span" className="StallTable__superSelect">
          {text}
        </Typography>
      ),
      inputDisplay: (
        <Typography variant="span" className="StallTable__superSelect">
          {text}
        </Typography>
      ),
    };
  });

  const stallStatusOptions = STALL_STATUS.map((option) => {
    const text = useEuiI18n(option.text, option.text);

    return {
      value: option.value,
      checked: option.checked,
      dropdownDisplay: option.indicatorInDropdown ? (
        <EuiHealth color={option.indicatorColor}>
          <Typography variant="span" className={option.classes}>
            {text}
          </Typography>
        </EuiHealth>
      ) : (
        <Typography variant="span" className={option.classes}>
          {text}
        </Typography>
      ),
      inputDisplay: (
        <EuiHealth color={option.indicatorColor}>
          <Typography variant="span" className="StallTable__superSelect">
            {text}
          </Typography>
        </EuiHealth>
      ),
    };
  });

  const onStatusChange = (elId: string, value: string) => {
    if (value !== 'CLOSE_FOR') {
      onItemChange(elId, 'status', value);
    }
  };

  return (
    <div className="StallTable__container">
      <table className="StallTable">
        <thead>
          <tr className="StallTable__header">
            <th className="StallTable__header__item-first">
              <Typography variant="span">{platformText}</Typography>
            </th>
            <th className="StallTable__header__item-second">
              <Typography variant="p">{statusText}</Typography>
              {/* <StallStatusModal onApply={(value: string) => onAllItemsChange('status', value)} /> */}
            </th>

            {feat[FeatureFlags.AUTO_ACCEPT].treatment === 'on' ? (
              <th className="StallTable__header__item-third">
                <Typography variant="p" className="StallTable__header__item-margin-bottom">
                  {autoAcceptSubOrdersText}
                </Typography>
                {/*<EuiSwitch
                                data-testid="switcher"
                                showLabel={false}
                                label="Enable"
                                disabled={foodSuppliers.every((el) => el.autoAcceptOnly || el.manualAcceptOnly)}
                                checked={getSwitcherForAllState(foodSuppliers).autoAccept}
                                onChange={(e: { target: { checked: boolean } }) =>
                                    onAllItemsChange('autoAccept', e.target.checked)
                                }
                            />
                              </th>
             <th className="StallTable__header__item-fourth">
                            <p>Auto print receipts</p>
                            <p className="StallTable__header__item-margin-bottom">自动印刷收据</p>
                            <EuiSwitch
                                data-testid="switcher"
                                showLabel={false}
                                label="Enable"
                                disabled={foodSuppliers.every((el) => el.autoAcceptOnly || el.manualAcceptOnly)}
                                checked={getSwitcherForAllState(foodSuppliers).autoPrint}
                                onChange={(e: { target: { checked: boolean } }) =>
                                    onAllItemsChange('autoPrint', e.target.checked)
                                }
                            />
                        </th> */}
              </th>
            ) : null}
            <th className="StallTable__header__item">{prepTimeText}</th>
          </tr>
        </thead>
        <tbody>
          {foodSuppliers.map((el) => (
            <tr className="StallTable__body" key={el.id}>
              <td className="StallTable__body__item">
                <img src={el.logo} alt={el.name} />
              </td>
              <td className="StallTable__body__item">
                <EuiSuperSelect
                  data-testid="stall_status_selector"
                  options={stallStatusOptions}
                  valueOfSelected={el.status}
                  onChange={(value) => onStatusChange(el.id, value)}
                />
              </td>
              {feat[FeatureFlags.AUTO_ACCEPT].treatment === 'on' ? (
                <td className="StallTable__body__item">
                  {el.autoAcceptOnly || el.manualAcceptOnly ? (
                    el.autoAcceptOnly ? (
                      handleAutoAcceptSwitcherText(el.autoAcceptOnly)
                    ) : (
                      handleAutoAcceptSwitcherText()
                    )
                  ) : (
                    <EuiSwitch
                      data-testid="switcher"
                      showLabel={false}
                      label="Enable"
                      checked={el.autoAccept}
                      onChange={(e: { target: { checked: boolean } }) =>
                        onItemChange(el.id, 'autoAccept', e.target.checked)
                      }
                    />
                  )}
                </td>
              ) : null}
              {/* hidden until BE implements this features*/}
              {/* <td className="StallTable__body__item">
                                {el.autoAccept || el.autoAcceptOnly ? (
                                    <>
                                        <EuiSwitch
                                            data-testid="switcher"
                                            showLabel={false}
                                            label="Enable"
                                            checked={el.autoPrint}
                                            onChange={(e: { target: { checked: boolean } }) =>
                                                onItemChange(el.id, 'autoPrint', e.target.checked)
                                            }
                                        />
                                        {el.autoPrint && (
                                            <div>
                                                <span className="SwitchCheckbox__text">
                                                    Orders appear in ‘Accepted’
                                                </span>
                                                <span className="SwitchCheckbox__text"> 订单显示在《已接受》</span>
                                            </div>
                                        )}
                                    </>
                                ) : (
                                    handleAutoPrintSwitcherText(el.autoAccept, el.manualAcceptOnly)
                                )}
                            </td> */}
              <td className="StallTable__body__item">
                <div className="StallTable__selector__container">
                  {el.autoAcceptOnly || el.manualAcceptOnly ? (
                    <span className="Selector__text-disabled">{acceptTimingText}</span>
                  ) : (
                    <EuiSuperSelect
                      data-testid="prep_time_selector"
                      options={tooBusyOptions}
                      valueOfSelected={el.prepTime ? `${el.prepTime}` : tooBusyOptions[0].value}
                      onChange={(value: string) => onItemChange(el.id, 'prepTime', Number(value))}
                    />
                  )}
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
