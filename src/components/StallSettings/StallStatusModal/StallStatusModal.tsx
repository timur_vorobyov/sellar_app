import { useEffect, useState } from 'react';
import classnames from 'clsx';
import {
  EuiButton,
  EuiHealth,
  EuiLink,
  EuiModal,
  EuiModalBody,
  EuiModalFooter,
  EuiModalHeader,
  EuiModalHeaderTitle,
  EuiIcon,
  useEuiI18n,
} from '@elastic/eui';
import './StallStatusModal.scss';
import { STALL_STATUS } from 'constants/stall';
import Typography from '../../ui-kit/Typography';
import Box from '../../ui-kit/Box';

interface StallStatusModalProps {
  onApply: (status: string) => void;
}

export default function StallStatusModal({ onApply }: StallStatusModalProps) {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [currentStatus, setCurrentStatus] = useState(STALL_STATUS[0].value);

  const [setStatusLinkText, headerText, statusText, cancelButtonText, applyButtonText] = useEuiI18n(
    [
      'stallSettings.statusModal.header.setStatusLink',
      'stallSettings.statusModal.header.text',
      'stallSettings.statusModal.header.status',
      'stallSettings.statusModal.cancel',
      'stallSettings.statusModal.apply',
    ],
    [],
  );

  useEffect(() => {
    if (isModalVisible) {
      setCurrentStatus(STALL_STATUS[0].value);
    }
  }, [isModalVisible]);

  const handleApply = () => {
    onApply(currentStatus);
    setIsModalVisible(false);
  };

  const optionsContent = STALL_STATUS.map((option) => {
    const text = useEuiI18n(option.text, option.text);

    return (
      <div
        className={classnames('StallStatusModal__body__options-list-item', {
          'Selectable__item--no-select': option.value === 'CLOSE_FOR',
          checked: currentStatus === option.value,
        })}
        key={option.value}
        onClick={() => option.value !== 'CLOSE_FOR' && setCurrentStatus(option.value)}
      >
        {currentStatus === option.value ? (
          <EuiIcon type="check" />
        ) : (
          <div className="StallStatusModal__item_spacer"></div>
        )}

        {option.indicatorInDropdown ? (
          <EuiHealth textSize="inherit" color={option.indicatorColor} className={option.classes}>
            {text}
          </EuiHealth>
        ) : (
          <Typography variant="span" className={option.classes}>
            {text}
          </Typography>
        )}
      </div>
    );
  });

  return (
    <>
      <EuiLink
        onClick={() => setIsModalVisible(true)}
        className="StallStatusModal__link"
        data-testid="StallStatusModal__link"
      >
        <p>{setStatusLinkText}</p>
      </EuiLink>
      {isModalVisible && (
        <EuiModal onClose={() => setIsModalVisible(false)} className="StallStatusModal__Modal" maxWidth={false}>
          <EuiModalHeader>
            <EuiModalHeaderTitle>
              <h1 className="StallStatusModal__header__title">{headerText}</h1>
              <Typography variant="p" className="StallStatusModal__header__text">
                {statusText}
              </Typography>
            </EuiModalHeaderTitle>
          </EuiModalHeader>
          <EuiModalBody>
            <Box ml={16}>{optionsContent}</Box>
          </EuiModalBody>
          <EuiModalFooter className="StallStatusModal__footer">
            <EuiButton
              color="danger"
              onClick={() => setIsModalVisible(false)}
              fill
              data-testid="StallStatusModal__cancelBtn"
            >
              {cancelButtonText}
            </EuiButton>
            <EuiButton
              className="StallStatusModal__footer__applyBtn"
              onClick={handleApply}
              fill
              disabled={(currentStatus as string) === ''}
              data-testid="StallStatusModal__applyBtn"
            >
              {applyButtonText}
            </EuiButton>
          </EuiModalFooter>
        </EuiModal>
      )}
    </>
  );
}
