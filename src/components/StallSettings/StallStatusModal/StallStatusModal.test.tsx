import '@testing-library/jest-dom';
import { screen, cleanup, fireEvent } from '@testing-library/react';
import StallStatusModal from './StallStatusModal';
import { customI18nRender } from 'utils/testing/customRender';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<StallStatusModal> component', () => {
  it('should open Modal', () => {
    customI18nRender(<StallStatusModal onApply={() => null} />, { locale: Locale.CHINESE });

    const openModalLink = screen.getByTestId('StallStatusModal__link');
    fireEvent.click(openModalLink);

    const modalHeader = screen.getByText('Stall Status 营业状态');

    expect(modalHeader).toBeInTheDocument();
  });

  it('should close Modal', () => {
    customI18nRender(<StallStatusModal onApply={() => null} />, { locale: Locale.CHINESE });

    const openModalLink = screen.getByTestId('StallStatusModal__link');
    fireEvent.click(openModalLink);

    const modalHeader = screen.getByText('Stall Status 营业状态');
    const cancelBtn = screen.getByTestId('StallStatusModal__cancelBtn');

    fireEvent.click(cancelBtn);

    expect(modalHeader).not.toBeInTheDocument();
  });
});
