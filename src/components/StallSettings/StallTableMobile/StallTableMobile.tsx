import { EuiHealth, EuiLoadingSpinner, EuiSwitch, useEuiI18n } from '@elastic/eui';
import { STALL_STATUS } from 'constants/stall';
import { TOO_BUSY_INTERVALS } from 'constants/subOrder';
import './StallTableMobile.scss';
import { FoodSupplierPlatform } from 'types/models/stall';
import { Dictionary } from 'types/generics';
import MoreMobile from '../MoreMobile/MoreMobile';

interface StallTableProps {
  onChange: (foodSuppliers: Dictionary) => void;
  onChangeAll: (foodSuppliers: Dictionary[]) => void;
  foodSuppliers: FoodSupplierPlatform[];
}

export default function StallTableMobile({ onChange, onChangeAll, foodSuppliers }: StallTableProps) {
  const autoAccept = false;
  const allStatus = false;
  const onItemChange = (id: string, property: string, value: string | boolean | number) => {
    const supplier = foodSuppliers.find((s) => s.id === id);

    if (supplier) {
      onChange({
        id: supplier.id,
        [property]: value,
      });
    }
  };

  const [statusText, prepTimeText, autoAcceptOnlyText, manualAcceptOnlyText, statusTextAll, autoAcceptOrdersText] =
    useEuiI18n(
      [
        'stallSettings.table.status',
        'stallSettings.table.acceptedSubOrderPrepTimeTitle',
        'stallSettings.table.autoAcceptOnly',
        'stallSettings.table.manualAcceptOnly',
        'stallSettings.table.status.all',
        'stallSettings.table.autoAcceptSubOrders',
      ],
      [],
    );

  const acceptTimingText = useEuiI18n('timing.minutes', '5 mins', { count: 5 });
  const stallStatusI18n = STALL_STATUS.map(({ indicatorColor, value, text }) => {
    return {
      indicatorColor,
      value,
      i18nText: useEuiI18n(text, text),
    };
  });

  const onAllItemsChange = (property: string, value: string | boolean) => {
    const newFoodSuppliers: Dictionary[] = foodSuppliers
      .filter((el) => {
        if (el.autoAcceptOnly || el.manualAcceptOnly) {
          return false;
        }
        if (!getSwitcherForAllState(foodSuppliers).autoAccept && property === 'autoPrint') {
          return false;
        }
        return true;
      })
      .map((el) => {
        if (property === 'autoAccept' && typeof value === 'boolean') {
          if (!value) {
            return { id: el.id, [property]: value, autoPrint: false };
          }
          return { id: el.id, [property]: value };
        }
        return { id: el.id, [property]: value };
      });

    onChangeAll(newFoodSuppliers);
  };

  const getSwitcherForAllState = (fSuppliers: FoodSupplierPlatform[]) => {
    const newFoodSuppliers = fSuppliers.filter((el) => !el.manualAcceptOnly && !el.autoAcceptOnly);

    return {
      autoAccept: newFoodSuppliers.every((el) => el.autoAccept),
      autoPrint: newFoodSuppliers.every((el) => el.autoPrint),
    };
  };

  const handleAutoAcceptSwitcherText = (autoAcceptOnly?: boolean) => {
    if (autoAcceptOnly) {
      return <span className="SwitchAutoAccept__text">{autoAcceptOnlyText}</span>;
    }
    return <span className="SwitchAutoAccept__text manual">{manualAcceptOnlyText}</span>;
  };

  const tooBusyOptions = TOO_BUSY_INTERVALS.map((interval) => {
    return {
      value: interval,
      text: useEuiI18n('timing.minutes', `${interval} mins`, { count: interval }),
    };
  });

  const stallStatusOptions = STALL_STATUS.map((option) => {
    const text = useEuiI18n(option.text, option.text);

    return {
      value: option.value,
      disabled: option.disabled,
      element: option.indicatorInDropdown ? (
        <EuiHealth color={option.indicatorColor}>
          <span className="StallTableMobile__status">{text}</span>
        </EuiHealth>
      ) : (
        <span className="StallTableMobile__subStatus">{text}</span>
      ),
    };
  });

  const getStallStatus = (status: string) => {
    const stall = stallStatusI18n.find((s) => s.value === status);

    if (stall) {
      const { i18nText, indicatorColor } = stall;

      return (
        <EuiHealth color={indicatorColor}>
          <span className="StallTableMobile__status">{i18nText}</span>
        </EuiHealth>
      );
    }
    return null;
  };

  const tooBusyOptionText = (interval: string | null) => {
    return interval ? tooBusyOptions.find((b) => b.value === Number(interval))?.text : tooBusyOptions[0].text;
  };

  const getStallStatusHeader = (logo: string, name: string) => {
    return (
      <>
        <img src={logo} alt={name} />
        <div>{statusText}</div>
      </>
    );
  };

  const getTooBusyOptionMenuHeader = (logo: string, name: string) => {
    return (
      <>
        <img src={logo} alt={name} />
        <div>{prepTimeText}</div>
      </>
    );
  };

  const getAllStallStatusHeader = () => {
    return (
      <>
        <div>{statusText}</div>
        <div className="StallTableMobile__section__header__description">{statusTextAll}</div>
      </>
    );
  };

  const getSelectedValue = (value: string | null): number => (value ? Number(value) : TOO_BUSY_INTERVALS[0]);

  if (!foodSuppliers.length) {
    return (
      <div className="Loader__overlay" data-testid="loader">
        <EuiLoadingSpinner size="xl" />
      </div>
    );
  }

  return (
    <div className="StallTableMobile__container">
      <div className="StallTableMobile__section">
        <div className="StallTableMobile__section__header">
          {statusText}
          {allStatus && (
            <MoreMobile
              header={getAllStallStatusHeader()}
              options={stallStatusOptions}
              selectedValue={
                foodSuppliers?.length && foodSuppliers.every(({ status }) => status === foodSuppliers[0].status)
                  ? foodSuppliers[0].status
                  : undefined
              }
              onChange={(value) => onAllItemsChange('status', value.toString())}
            />
          )}
        </div>
        {foodSuppliers.map((el) => (
          <div key={el.id} className="StallTableMobile__section__item">
            <img src={el.logo} alt={el.name} />
            <div className="StallTableMobile__section__item__content">
              {getStallStatus(el.status)}
              <MoreMobile
                header={getStallStatusHeader(el.logo, el.name)}
                options={stallStatusOptions}
                selectedValue={el.status}
                onChange={(value) => onItemChange(el.id, 'status', value)}
              />
            </div>
          </div>
        ))}
      </div>
      {autoAccept && (
        <div className="StallTableMobile__section">
          <div className="StallTableMobile__section__header">
            {autoAcceptOrdersText}
            <EuiSwitch
              data-testid="switcher"
              showLabel={false}
              label="Enable"
              disabled={foodSuppliers.every((el) => el.autoAcceptOnly || el.manualAcceptOnly)}
              checked={getSwitcherForAllState(foodSuppliers).autoAccept}
              onChange={(e: { target: { checked: boolean } }) => onAllItemsChange('autoAccept', e.target.checked)}
            />
          </div>
          {foodSuppliers.map((el) => (
            <div key={el.id} className="StallTableMobile__section__item flex">
              <img src={el.logo} alt={el.name} />
              <div className="StallTableMobile__section__item__content">
                {el.autoAcceptOnly || el.manualAcceptOnly ? (
                  <>
                    {el.autoAcceptOnly && handleAutoAcceptSwitcherText(true)}
                    {!el.autoAcceptOnly && handleAutoAcceptSwitcherText()}
                  </>
                ) : (
                  <EuiSwitch
                    data-testid="switcher"
                    showLabel={false}
                    label="Enable"
                    checked={el.autoAccept}
                    onChange={(e: { target: { checked: boolean } }) =>
                      onItemChange(el.id, 'autoAccept', e.target.checked)
                    }
                  />
                )}
              </div>
            </div>
          ))}
        </div>
      )}
      <div className="StallTableMobile__section">
        <div className="StallTableMobile__section__header">{prepTimeText}</div>
        {foodSuppliers.map((el) => (
          <div key={el.id} className="StallTableMobile__section__item flex">
            <img src={el.logo} alt={el.name} />
            <div className="StallTableMobile__section__item__content">
              {el.autoAcceptOnly || el.manualAcceptOnly ? (
                <span className="StallTableMobile__time disabled">{acceptTimingText}</span>
              ) : (
                <>
                  <span className="StallTableMobile__time">{tooBusyOptionText(el.prepTime)}</span>
                  <MoreMobile
                    header={getTooBusyOptionMenuHeader(el.logo, el.name)}
                    options={tooBusyOptions}
                    selectedValue={getSelectedValue(el.prepTime)}
                    onChange={(value) => onItemChange(el.id, 'prepTime', value)}
                  />
                </>
              )}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
