import '@testing-library/jest-dom';
import { cleanup, render, screen } from '@testing-library/react';
import StallTableMobile from './StallTableMobile';

import { foodSupplierPlatforms } from '__mocks__/mocks';

afterEach(cleanup);

describe('<StallTableMobile> component', () => {
  it('should render StallTableMobile with data', () => {
    render(<StallTableMobile foodSuppliers={foodSupplierPlatforms} onChangeAll={() => null} onChange={() => null} />);

    const logo = screen.getAllByAltText(`${foodSupplierPlatforms[0].name}`)[0];
    expect(logo).toHaveAttribute('src', foodSupplierPlatforms[0].logo);
  });
});
