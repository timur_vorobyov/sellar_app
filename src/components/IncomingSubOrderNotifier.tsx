import { useIntercomContext } from 'contexts/intecomContextProvider/context';
import { IntercomCommand } from 'contexts/intecomContextProvider/types';
import { useStateContext } from 'contexts/store';
import { useEffect } from 'react';

function IncomingSubOrderNotifier() {
  const {
    state: { incomingSubOrders, foodSupplierPlatforms },
  } = useStateContext();

  const { postMessage } = useIntercomContext();

  useEffect(() => {
    if (incomingSubOrders) {
      postMessage(IntercomCommand.incomingOrdersCount, {
        value: incomingSubOrders.length,
        isLong: !!foodSupplierPlatforms[0]?.isLongRingtone,
      });
    }
  }, [incomingSubOrders]);

  return <></>;
}

export default IncomingSubOrderNotifier;
