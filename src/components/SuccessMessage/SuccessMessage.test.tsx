import '@testing-library/jest-dom';
import { cleanup, screen } from '@testing-library/react';
import SuccessMessage from './SuccessMessage';
import ConfirmOrderIcon from 'components/assets/images/SubOrderPage/confirmOrderIcon.svg';
import { customI18nRender } from 'utils/testing/customRender';
import { SubOrderRejectionReason } from 'constants/subOrder';
import { products } from '__mocks__/mocks';
import { Locale } from 'types/translation';

afterEach(cleanup);

describe('<SuccessMessage> component', () => {
  it('should render component with Item unavailable reason', () => {
    customI18nRender(<SuccessMessage option={SubOrderRejectionReason.ItemUnavailable} products={products} />, {
      locale: Locale.CHINESE,
    });

    expect(screen.getByRole('img')).toHaveAttribute('src', ConfirmOrderIcon);
    expect(screen.getByText('Order rejected 已拒绝订单')).toBeInTheDocument();
    expect(
      screen.getByText('Fishball Soup Noodle has been marked unavailable. Fishball Soup Noodle 已标记为不可用。'),
    ).toBeInTheDocument();
  });

  it('should render component with Too busy ', () => {
    customI18nRender(<SuccessMessage option={SubOrderRejectionReason.MerchantBusy} time={15} />, {
      locale: Locale.CHINESE,
    });

    expect(screen.getByRole('img')).toHaveAttribute('src', ConfirmOrderIcon);
    expect(screen.getByText('Order rejected 已拒绝订单')).toBeInTheDocument();
    expect(
      screen.getByText(
        'Prep time for upcoming orders has been updated to 15 mins. 即将到来的订单的准备时间已更新为 15 分钟',
      ),
    ).toBeInTheDocument();
  });

  it('should render component with Stall closed', () => {
    customI18nRender(<SuccessMessage option={SubOrderRejectionReason.MerchantClosed} />, { locale: Locale.CHINESE });

    expect(screen.getByRole('img')).toHaveAttribute('src', ConfirmOrderIcon);
    expect(screen.getByText('Order rejected 已拒绝订单')).toBeInTheDocument();
    expect(screen.getByText('Stall has been set to closed. 营业已转换为结束。')).toBeInTheDocument();
  });

  it('should render component for refund', () => {
    customI18nRender(<SuccessMessage refund={true} refundPrice={20} />, { locale: Locale.CHINESE });

    expect(screen.getByRole('img')).toHaveAttribute('src', ConfirmOrderIcon);
    expect(screen.getByText('Refund initiated 已发起退款')).toBeInTheDocument();
    expect(screen.getByTestId('SuccessMessageText')).toBeInTheDocument();
  });
});
