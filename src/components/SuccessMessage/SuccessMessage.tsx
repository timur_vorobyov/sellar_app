import { useEuiI18n } from '@elastic/eui';
import ConfirmOrderIcon from 'components/assets/images/SubOrderPage/confirmOrderIcon.svg';
import { SubOrderRejectionReason } from 'constants/subOrder';
import { Product, ProductCustomisation } from 'types/models/product';
import './SuccessMessage.scss';
import { sliceTwoMoreDecimals } from 'utils/formater';
import { useLanguageContext } from 'contexts/languageContextProvider/context';

interface SuccessMessageProps {
  option?: string;
  products?: Product[];
  customisations?: ProductCustomisation[];
  time?: number;
  refund?: boolean;
  refundPrice?: number;
}

export default function SuccessMessage({
  option,
  products,
  customisations,
  time,
  refund,
  refundPrice,
}: SuccessMessageProps) {
  const { getLocale } = useLanguageContext();
  const prepareCnName = (items: (Product | ProductCustomisation)[]) => {
    return items
      ? items.map((item) => {
          if (item.nameCn && item.nameCn.includes(item.name)) {
            item.nameCn = item.nameCn.replace(item.name, '');
          }
          if (!item.nameCn) {
            item.nameCn = item.name;
          }
          return { ...item };
        })
      : items;
  };

  const formatProducts = (products: Product[]) => {
    if (products.length === 0) {
      return;
    }
    let prepItemsUnavailableText;
    const updatedProducts = prepareCnName([...products]);
    const updatedCust = customisations && prepareCnName([...customisations]);
    const productsStr = updatedProducts.map((product) => product.name).join(', ');
    const customisationItemsStr = updatedCust && updatedCust.map((item) => item.name).join(', ');
    const fullEnText = customisationItemsStr ? productsStr + ', ' + customisationItemsStr : productsStr;
    if (getLocale() === 'cn') {
      const productsStrCn = updatedProducts.map((product) => product.nameCn).join(', ');
      const customisationItemsStrCn = updatedCust && updatedCust.map((item) => item.nameCn).join(', ');
      const fullCnText = customisationItemsStrCn ? productsStrCn + ', ' + customisationItemsStrCn : productsStrCn;
      prepItemsUnavailableText = useEuiI18n('successMessage.subOrderRejected.itemsUnavailable', '', {
        items: fullEnText,
        itemsCn: fullCnText,
      });
    } else {
      prepItemsUnavailableText = useEuiI18n('successMessage.subOrderRejected.itemsUnavailable', '', {
        items: fullEnText,
      });
    }

    return prepItemsUnavailableText;
  };
  const formattedRefundPrice = refundPrice ? <b>${sliceTwoMoreDecimals(refundPrice)}</b> : '';
  const [orderRejectedText, stallHasBeenClosedText, refundInitiatedText] = useEuiI18n(
    [
      'successMessage.subOrderRejected.subOrderRejected',
      'successMessage.subOrderRejected.stallHasBeenClosed',
      'successMessage.subOrderRejected.refundInitiated',
    ],
    [],
  );

  const inProcessOfRefundText = useEuiI18n('successMessage.subOrderRejected.inProcessOfRefund', '', {
    formattedRefundPrice,
  });
  const prepTimeHasBeenUpdatedText = useEuiI18n('successMessage.subOrderRejected.prepTimeHasBeenUpdated', '', { time });

  return (
    <div className="SuccessMessage__container">
      <img src={ConfirmOrderIcon} />

      {option === SubOrderRejectionReason.ItemUnavailable && (
        <>
          <p className="SuccessMessage__title">{orderRejectedText}</p>
          <p className="SuccessMessage__text">{products && formatProducts(products)}</p>
        </>
      )}
      {option === SubOrderRejectionReason.MerchantBusy && (
        <>
          <p className="SuccessMessage__title">{orderRejectedText}</p>
          <p className="SuccessMessage__text">{prepTimeHasBeenUpdatedText}</p>
        </>
      )}
      {option === SubOrderRejectionReason.MerchantClosed && (
        <>
          <p className="SuccessMessage__title">{orderRejectedText}</p>
          <p className="SuccessMessage__text">{stallHasBeenClosedText}</p>
        </>
      )}
      {refund && (
        <>
          <p className="SuccessMessage__title">{refundInitiatedText}</p>
          <p className="SuccessMessage__text" data-testid="SuccessMessageText">
            {inProcessOfRefundText}
          </p>
        </>
      )}
    </div>
  );
}
