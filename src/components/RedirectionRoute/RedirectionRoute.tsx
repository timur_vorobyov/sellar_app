import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import { useTennantDataContext } from 'contexts/tenant/context';
import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';

interface RedirectionRouteProps {
  children: JSX.Element[];
}

const RedirectionRoute = ({ children }: RedirectionRouteProps) => {
  const { isTablet } = useDeviceContext();
  const history = useHistory();
  const { redirectionRoute } = useTennantDataContext();

  useEffect(() => {
    if (isTablet) {
      if (redirectionRoute.route) {
        history.push(redirectionRoute.route);
      }
    }
  }, [redirectionRoute]);

  return <>{children}</>;
};

export default RedirectionRoute;
