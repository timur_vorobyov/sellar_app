export enum RefundReasonKey {
  KCardDiscount = 'KCARD_DISCOUNT',
  QualityOfFood = 'QUALITY_OF_FOOD',
  FoodPoisoning = 'FOOD_POISONING',
  MissingItems = 'MISSING_ITEMS',
  DamagedItems = 'DAMAGED_ITEMS',
  WrongItems = 'WRONG_ITEMS',
  NoShow = 'NO_SHOW',
  LateDelivery = 'LATE_DELIVERY',
  AppPaymentDiscount = 'APP_PAYMENT_DISCOUNT',
}

export const refundReasons = {
  [RefundReasonKey.KCardDiscount]: 'refundSubOrder.reason.KCARD_DISCOUNT',
  [RefundReasonKey.QualityOfFood]: 'refundSubOrder.reason.QUALITY_OF_FOOD',
  [RefundReasonKey.FoodPoisoning]: 'refundSubOrder.reason.FOOD_POISONING',
  [RefundReasonKey.MissingItems]: 'refundSubOrder.reason.MISSING_ITEMS',
  [RefundReasonKey.DamagedItems]: 'refundSubOrder.reason.DAMAGED_ITEMS',
  [RefundReasonKey.WrongItems]: 'refundSubOrder.reason.WRONG_ITEMS',
  [RefundReasonKey.NoShow]: 'refundSubOrder.reason.NO_SHOW',
  [RefundReasonKey.LateDelivery]: 'refundSubOrder.reason.LATE_DELIVERY',
  [RefundReasonKey.AppPaymentDiscount]: 'refundSubOrder.reason.APP_PAYMENT_DISCOUNT',
};
