import { Option } from 'types/generics';

export const AVAILABILITY_FILTER_OPTIONS: Option[] = [
  { value: 1, text: 'Available' },
  { value: 0, text: 'Unavailable' },
];
