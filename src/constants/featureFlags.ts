export enum FeatureFlags {
  AUTO_ACCEPT = 'dbtech_sellerapp_auto_accept',
  QR_CODE = 'dbtech_sellerapp_receipt_qr',
  NOTIFICATION_SOUND = 'dbtech_sellerapp_notification_sound',
}
