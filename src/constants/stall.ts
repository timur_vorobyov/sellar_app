import { Dictionary } from 'types/generics';
import { StallStatusOption } from 'types/models/stall';

export const STALL_STATUS: StallStatusOption[] = [
  {
    value: 'OPEN',
    checked: false,
    indicatorColor: '#3FC078',
    indicatorInDropdown: true,
    text: 'stallSettings.status.open',
    disabled: false,
    classes: 'stallStatus-option',
  },
  {
    value: 'CLOSE_FOR',
    checked: false,
    indicatorColor: '#EA6100',
    indicatorInDropdown: true,
    text: 'stallSettings.status.closeFor',
    disabled: true,
    classes: 'stallStatus-option',
  },
  {
    value: 'CLOSED_FOR_15',
    checked: false,
    disabled: false,
    indicatorColor: '#EA6100',
    indicatorInDropdown: false,
    text: 'stallSettings.status.closedFor15',
    classes: 'stallStatus-option marginR20',
  },
  {
    value: 'CLOSED_FOR_30',
    checked: false,
    disabled: false,
    indicatorColor: '#EA6100',
    indicatorInDropdown: false,
    text: 'stallSettings.status.closedFor30',
    classes: 'stallStatus-option marginR20',
  },
  {
    value: 'CLOSED_FOR_45',
    checked: false,
    disabled: false,
    indicatorColor: '#EA6100',
    indicatorInDropdown: false,
    text: 'stallSettings.status.closedFor45',
    classes: 'stallStatus-option marginR20',
  },
  {
    value: 'CLOSED_FOR_60',
    checked: false,
    disabled: false,
    indicatorColor: '#EA6100',
    indicatorInDropdown: false,
    text: 'stallSettings.status.closedFor60',
    classes: 'stallStatus-option marginR20',
  },
  {
    value: 'CLOSED_FOR_120',
    checked: false,
    disabled: false,
    indicatorColor: '#EA6100',
    indicatorInDropdown: false,
    text: 'stallSettings.status.closedFor120',
    classes: 'stallStatus-option marginR20',
  },
  {
    value: 'CLOSED_FOR_180',
    checked: false,
    disabled: false,
    indicatorColor: '#EA6100',
    indicatorInDropdown: false,
    text: 'stallSettings.status.closedFor180',
    classes: 'stallStatus-option marginR20',
  },
  {
    value: 'CLOSED',
    checked: false,
    disabled: false,
    indicatorColor: '#C1083A',
    indicatorInDropdown: true,
    text: 'stallSettings.status.closed',
    classes: 'stallStatus-option',
  },
];

export const CLOSED_TO_MINUTES_MAPPER: Dictionary = {
  CLOSED_FOR_15: 15,
  CLOSED_FOR_30: 30,
  CLOSED_FOR_45: 45,
  CLOSED_FOR_60: 60,
  CLOSED_FOR_120: 120,
  CLOSED_FOR_180: 180,
};
