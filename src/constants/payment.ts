export enum PaymentMethod {
  Card = 'CARD',
  LinkPoints = 'LINKPOINTS',
}
export const PaymentMethodTranslations = {
  [PaymentMethod.Card]: 'Card 塑料卡',
  [PaymentMethod.LinkPoints]: 'LINKPOINTS',
};
