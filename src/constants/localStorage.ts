export enum LOCALSTORAGE {
  AUTH = 'auth',
  ACCESS_TOKEN = 'access_token',
  REFRESH_TOKEN = 'refresh_token',
  SERIAL_NUMBER = 'serial_number',
  EXPIRES_IN = 'expires_in',
  LANGUAGE = 'lng',
}
