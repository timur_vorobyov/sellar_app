import { Option } from 'types/generics';

export const DEFAULT_ORDER_ACCEPT_GRACE_TIME = 5 * 60;

export enum SubOrderType {
  HavingHere = 'having_here',
  SelfCollect = 'self_collect',
  Takeaway = 'takeaway',
  Delivery = 'delivery',
}

export const SubOrderTypeTranslations = (locale: string) => {
  return {
    [SubOrderType.HavingHere]: locale === 'en' ? 'Having here' : 'Having here 这边吃',
    [SubOrderType.SelfCollect]: locale === 'en' ? 'Takeaway' : 'Takeaway 打包',
    [SubOrderType.Takeaway]: locale === 'en' ? 'Takeaway' : 'Takeaway 打包',
    [SubOrderType.Delivery]: locale === 'en' ? 'Delivery' : 'Delivery 外卖',
  };
};

export enum SubOrderRejectionReason {
  MerchantBusy = 'MERCHANT_BUSY',
  MerchantClosed = 'MERCHANT_CLOSED',
  PaymentFailed = 'PAYMENT_FAILED',
  ItemUnavailable = 'ITEM_UNAVAILABLE',
  CustomerRequest = 'CUSTOMER_REQUEST',
  AutoRejected = 'AUTO_REJECTED',
  NoShow = 'NO_SHOW',
}

export const TOO_BUSY_INTERVALS = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60];

export const REASONS_TOO_BUSY: Option[] = [
  {
    value: 5,
    text: '5 mins 5分钟',
  },
  {
    value: 10,
    text: '10 mins 10分钟',
  },
  {
    value: 15,
    text: '15 mins 15分钟',
  },
  {
    value: 20,
    text: '20 mins 20分钟',
  },
  {
    value: 25,
    text: '25 mins 25分钟',
  },
  {
    value: 30,
    text: '30 mins 30分钟',
  },
  {
    value: 35,
    text: '35 mins 35分钟',
  },
  {
    value: 40,
    text: '40 mins 40分钟',
  },
  {
    value: 45,
    text: '45 mins 45分钟',
  },
  {
    value: 50,
    text: '50 mins 50分钟',
  },
  {
    value: 55,
    text: '55 mins 55分钟',
  },
  {
    value: 60,
    text: '60 mins 60分钟',
  },
];
