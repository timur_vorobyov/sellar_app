import { Context, createContext, useContext } from 'react';

type getValue = () => string | null;
type setValue = (value: string | null) => void;

type FoodSupplierContextType = {
  getFoodSupplierId: getValue;
  setFoodSupplierId: setValue;
};

export const FoodSupplierContext: Context<FoodSupplierContextType> = createContext({} as FoodSupplierContextType);

export const useFoodSupplierContext = () => useContext(FoodSupplierContext);
