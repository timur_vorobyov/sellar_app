import config from 'config';
import { useCallback, useState } from 'react';
import { FoodSupplierContext } from './context';

export const FoodSupplierProvider = (props: { children: JSX.Element }) => {
  const [foodSupplierIdState, setFoodSupplierIdState] = useState(config.devFoodSupplierId || null);
  const setFoodSupplierId = useCallback(
    (foodSupplierId: string | null) => {
      setFoodSupplierIdState(foodSupplierId);
    },
    [foodSupplierIdState],
  );
  const getFoodSupplierId = useCallback(() => foodSupplierIdState, [foodSupplierIdState]);

  return (
    <FoodSupplierContext.Provider
      value={{
        getFoodSupplierId,
        setFoodSupplierId,
      }}
    >
      {props.children}
    </FoodSupplierContext.Provider>
  );
};
