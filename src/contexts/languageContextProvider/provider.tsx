import { useCallback, useState } from 'react';
import { getStorageLanguage, setStorageLanguage as setLang } from 'storage/adapters/language';
import { LanguageContext } from './context';

export const LanguageProvider = (props: { children: JSX.Element }) => {
  const [locale, setLocale] = useState(getStorageLanguage() || 'en');
  const setLanguage = useCallback(
    (locale: string) => {
      setLang(locale);
      setLocale(locale);
    },
    [locale],
  );
  const getLanguage = useCallback(() => locale, [locale]);

  return (
    <LanguageContext.Provider
      value={{
        getLocale: getLanguage,
        setLocale: setLanguage,
      }}
    >
      {props.children}
    </LanguageContext.Provider>
  );
};
