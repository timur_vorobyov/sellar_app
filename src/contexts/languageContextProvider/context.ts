import { Context, createContext, useContext } from 'react';

type getValue = () => string;
type setValue = (value: string) => void;

type LanguageContextContextType = {
  getLocale: getValue;
  setLocale: setValue;
};

export const LanguageContext: Context<LanguageContextContextType> = createContext({} as LanguageContextContextType);

export const useLanguageContext = () => useContext(LanguageContext);
