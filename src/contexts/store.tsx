import { AcceptedSubOrder, SubOrder, SubOrderState } from 'types/models/subOrder';
import { createContext, PropsWithChildren, useContext, useReducer } from 'react';
import { getSubOrdersTimers, isSubOrderExpired } from 'utils/datetime';
import { State, AppContext, ActionTypes, Action } from 'types/store';

export const initialState: State = {
  loaders: 0,
  incomingSubOrders: [],
  acceptedSubOrders: [],
  foodSupplierPlatforms: [],
  incomingSubOrdersTimers: [],
  newIncomigSubOrders: {},
  isNewIncomingSubOrderUpdateAllowed: false,
  amountOfNewSubOrders: 0,
};

export const initialStoreContext: AppContext = {
  state: initialState,
  dispatch: (_a) => {
    // noop
  },
};

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case ActionTypes.SET_LOADER_STATE: {
      let { loaders } = state;
      const value = action.payload?.loading ? 1 : -1;

      loaders = loaders + value;

      if (loaders < 0) {
        loaders = 0;
      }

      return { ...state, loaders };
    }
    case ActionTypes.SET_INCOMING_SUB_ORDERS: {
      const incomingSubOrders: SubOrder[] = action.payload?.subOrders || [];
      const newIncomigSubOrders: { [key: number]: boolean } = incomingSubOrders.reduce(
        (updatedIncomingSubOrders, incomingSubOrder) => {
          if (incomingSubOrders[0] === incomingSubOrder && state.isNewIncomingSubOrderUpdateAllowed) {
            return {
              ...updatedIncomingSubOrders,
              [incomingSubOrder.id]: false,
            };
          } else {
            const newIncomigSubOrderId = state.newIncomigSubOrders[incomingSubOrder.id];
            const isIncomigSubOrderNew =
              newIncomigSubOrderId !== undefined && newIncomigSubOrderId === false ? false : true;
            return {
              ...updatedIncomingSubOrders,
              [incomingSubOrder.id]: !!isIncomigSubOrderNew,
            };
          }
        },
        {},
      );

      let amountOfNewSubOrders = 0;
      for (const key in newIncomigSubOrders) {
        if (newIncomigSubOrders[key]) {
          amountOfNewSubOrders++;
        }
      }

      return {
        ...state,
        incomingSubOrders: incomingSubOrders.filter((subOrder) => isSubOrderExpired(subOrder)),
        incomingSubOrdersTimers: getSubOrdersTimers(incomingSubOrders),
        newIncomigSubOrders: newIncomigSubOrders,
        isNewIncomingSubOrderUpdateAllowed: incomingSubOrders.length ? state.isNewIncomingSubOrderUpdateAllowed : false,
        amountOfNewSubOrders,
      };
    }
    case ActionTypes.UPDATE_SUB_ORDERS_EXPIRATION_TIME: {
      return {
        ...state,
        incomingSubOrdersTimers: getSubOrdersTimers(state.incomingSubOrders),
      };
    }
    case ActionTypes.SET_ACCEPTED_SUB_ORDERS: {
      const subOrders = action.payload?.subOrders || [];
      const acceptedSubOrders = subOrders.filter((subOrder: AcceptedSubOrder) => {
        if (subOrder.state === SubOrderState.Accepted) {
          return subOrder.isReceiptPrinted;
        }

        return true;
      });

      return {
        ...state,
        acceptedSubOrders: acceptedSubOrders,
      };
    }
    case ActionTypes.SET_FOOD_SUPPLIER_PLATFORMS: {
      return { ...state, foodSupplierPlatforms: action.payload?.platforms || [] };
    }
    case ActionTypes.UPDATE_FOOD_SUPPLIER_PLATFORMS: {
      const platforms = action.payload?.platforms;
      const newPlatforms = [...state.foodSupplierPlatforms];

      for (const platform of platforms) {
        const index = newPlatforms.findIndex((supplierPlatform) => supplierPlatform.id === platform.id);
        newPlatforms.splice(index, 1, platform);
      }

      return { ...state, foodSupplierPlatforms: newPlatforms };
    }
    case ActionTypes.EXPIRE_SUB_ORDER: {
      const subOrder = action.payload?.subOrder;
      const incoming = state.incomingSubOrders.filter((incomingSubOrder) => incomingSubOrder.id !== subOrder.id);

      return { ...state, incomingSubOrders: incoming };
    }
    case ActionTypes.UPDATE_NEW_SUB_ORDER_STATUS: {
      const subOrderId = action.payload?.id;

      if (subOrderId) {
        const isMobile = action.payload?.isMobile;
        const newIncomigSubOrders = { ...state.newIncomigSubOrders };
        newIncomigSubOrders[subOrderId] = false;

        if (!state.isNewIncomingSubOrderUpdateAllowed && !isMobile) {
          const firstincomingSubOrderKey = Number(Object.keys(newIncomigSubOrders)[0]);
          newIncomigSubOrders[firstincomingSubOrderKey] = false;
        }

        let amountOfNewSubOrders = 0;
        for (const key in newIncomigSubOrders) {
          if (newIncomigSubOrders[key]) {
            amountOfNewSubOrders++;
          }
        }
        return {
          ...state,
          newIncomigSubOrders,
          isNewIncomingSubOrderUpdateAllowed: !!state.incomingSubOrders.length,
          amountOfNewSubOrders,
        };
      }

      return { ...state };
    }
    default:
      throw new Error(`Invalid action ${action.type}`);
  }
};

export const store = createContext<AppContext>(initialStoreContext);
export const useStateContext = () => useContext(store);

const { Provider } = store;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const StateProvider = ({ children }: PropsWithChildren<any>) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export type DispatchFn = (action: Action) => void;
