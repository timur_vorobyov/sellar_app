import config from 'config';
import { createContext, useContext } from 'react';
import { DeviceType } from 'types/device';

const defaultDeviceContextProps = {
  isMobile: false,
  isTablet: true,
} as DeviceType;

export const DeviceContext = createContext(defaultDeviceContextProps);
export const useDeviceContext = () => useContext(DeviceContext);

export const DeviceContextProvider = (props: { children: JSX.Element; width: number }): JSX.Element => {
  const width = props.width ? props.width : Number(config.devicePDAWidth) + 1;
  const device = {
    isMobile: width <= config.devicePDAWidth,
    isTablet: width > config.devicePDAWidth,
  };

  const { isMobile, isTablet } = device;

  return <DeviceContext.Provider value={{ isMobile, isTablet }}>{props.children}</DeviceContext.Provider>;
};
