import config from 'config';
import { WindowsMessagePayload } from './types';

export const sendMessage = (payload: WindowsMessagePayload) => {
  if (window.top && window !== window.top) {
    window.top.postMessage(JSON.stringify(payload), config.kposOrigin);
  }
};
