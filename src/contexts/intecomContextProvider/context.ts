import { createContext, useContext } from 'react';
import { Dictionary } from 'types/generics';
import { IntercomCommand } from './types';

type IntercomContextType = { postMessage: (command: IntercomCommand, payload: Dictionary) => void };

export const IntercomContext = createContext({} as IntercomContextType);

export const useIntercomContext = () => useContext(IntercomContext);
