/* eslint-disable @typescript-eslint/no-explicit-any */

import { Dictionary } from 'types/generics';

export enum IntercomCommand {
  setToken = 'setToken',
  incomingOrdersCount = 'incomingOrdersCount',
  setSerialNumber = 'setSerialNumber',
  setOnlineStatus = 'setOnlineStatus',
  setLanguage = 'setLanguage',
  setRedirectionRoute = 'setRedirectionRoute',

  getToken = 'getToken',
  getSerialNumber = 'getSerialNumber',
  getOnlineStatus = 'getOnlineStatus',
  getLanguage = 'getLanguage',
}
export interface WindowsMessagePayload extends Dictionary {
  command: IntercomCommand;
  value?: any;
}
