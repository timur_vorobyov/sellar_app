import config from 'config';
import { useAuth } from 'contexts/authContextProvider/authContextProvider';
import { useLanguageContext } from 'contexts/languageContextProvider/context';
import { useTennantDataContext } from 'contexts/tenant/context';
import { useEffect } from 'react';
import { getStorageAccessToken, setStorageAccessToken } from 'storage/adapters/accessToken';
import { getStorageSerialNumber, setStorageSerialNumber } from 'storage/adapters/serialNumber';
import useInterval from 'utils/hooks/use-interval';
import { IntercomCommand, WindowsMessagePayload } from './types';
import { sendMessage } from './utils';

const IframeListener = (): JSX.Element => {
  const { setLocale } = useLanguageContext();
  const { setIsAuthenticated } = useAuth();
  const { setIsOnline, setRedirectionRoute } = useTennantDataContext();

  const messageEventListener = (event: MessageEvent<string>) => {
    if (config.kposOrigin !== event.origin) {
      return;
    }

    let data: WindowsMessagePayload = { command: '' as IntercomCommand };

    try {
      data = JSON.parse(event.data || '{}');
    } catch (e) {
      return;
    }

    const { command }: { command: IntercomCommand } = data;
    switch (command) {
      case IntercomCommand.setLanguage:
        setLocale(data.value || 'en');
        break;
      case IntercomCommand.setOnlineStatus:
        setIsOnline(data.value);
        break;
      case IntercomCommand.setSerialNumber:
        setStorageSerialNumber(data.value);
        if (getStorageAccessToken()) {
          setIsAuthenticated(true);
        }
        break;
      case IntercomCommand.setToken:
        setStorageAccessToken(data.value);
        if (getStorageSerialNumber()) {
          setIsAuthenticated(true);
        }
        break;
      case IntercomCommand.setRedirectionRoute:
        setRedirectionRoute(data.value);
        break;
      default:
        return '';
    }
  };

  const handler = (event: MessageEvent<string>) => messageEventListener(event);

  useInterval(() => {
    sendMessage({ command: IntercomCommand.getToken });
    sendMessage({ command: IntercomCommand.getOnlineStatus });
    sendMessage({ command: IntercomCommand.getSerialNumber });
  }, 500);

  useEffect(() => {
    sendMessage({ command: IntercomCommand.getLanguage });
  }, []);

  useEffect(() => {
    window.addEventListener('message', handler, false);
    return () => window.removeEventListener('message', handler, false);
  }, [handler]);

  return <></>;
};

export default IframeListener;
