import { Dictionary } from 'types/generics';
import { IntercomCommand } from './types';
import { IntercomContext } from './context';
import { sendMessage } from './utils';
import IframeListener from './IframeListener';

export const IntercomProvider = (props: { children: JSX.Element }) => {
  const postMessageLocal = (command: IntercomCommand, payload: Dictionary) => {
    console.log(`SAPP: postMessage: command=${command}`, payload);
    sendMessage({ command, ...payload });
  };

  return (
    <>
      <IframeListener />
      <IntercomContext.Provider value={{ postMessage: postMessageLocal }}>{props.children}</IntercomContext.Provider>
    </>
  );
};
