import React, { Context, createContext, useState } from 'react';
import { getStorageAccessToken } from 'storage/adapters/accessToken';
import { getStorageSerialNumber } from 'storage/adapters/serialNumber';

export type AuthContextType = {
  isAuthenticated: boolean;
  setIsAuthenticated: (isAuthenticated: boolean) => void;
};

const AuthContext: Context<AuthContextType> = createContext({} as AuthContextType);

export const AuthProvider = (props: { children: JSX.Element }): JSX.Element => {
  const access_token: string = getStorageAccessToken() || '';

  const serialNumber: string = getStorageSerialNumber() || '';

  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(access_token && serialNumber ? true : false);

  return (
    <AuthContext.Provider
      value={{
        isAuthenticated,
        setIsAuthenticated,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export const useAuth = (): AuthContextType => React.useContext(AuthContext);
