import { Context, createContext, useContext } from 'react';

type TennantDataContextType = {
  isOnline: boolean;
  setIsOnline: (value: boolean) => void;
  redirectionRoute: {
    route: string;
    amount: number;
  };
  setRedirectionRoute: (redirectionRoute: { route: string; amount: number }) => void;
};

export const TennantDataContext: Context<TennantDataContextType> = createContext({} as TennantDataContextType);

export const useTennantDataContext = () => useContext(TennantDataContext);
