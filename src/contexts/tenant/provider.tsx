import { useState } from 'react';
import { TennantDataContext } from './context';

export const TenantDataProvider = (props: { children: JSX.Element }) => {
  const [isOnline, setIsOnline] = useState(false);
  const [redirectionRoute, setRedirectionRoute] = useState({
    route: '',
    amount: 0,
  });

  return (
    <TennantDataContext.Provider
      value={{
        isOnline,
        setIsOnline,
        redirectionRoute,
        setRedirectionRoute,
      }}
    >
      {props.children}
    </TennantDataContext.Provider>
  );
};
