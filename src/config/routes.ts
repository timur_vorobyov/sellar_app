import { SubOrdersHistoryPage } from 'views/SubOrdersHistoryPage/SubOrdersHistoryPage';
import { StallSettingsPage } from 'views/StallSettingsPage/StallSettingsPage';
import { RejectSubOrderPage } from 'views/RejectSubOrderPage/RejectSubOrderPage';
import { MenuPage } from 'views/MenuPage/MenuPage';
import { RefundSubOrderPage } from 'views/RefundSubOrderPage/RefundSubOrderPage';
import OrdersIcon from 'components/PDA/NavBarMobile/MenuMobile/images/Menu/ordersIcon.svg';
import AvailabilityIcon from 'components/PDA/NavBarMobile/MenuMobile/images/Menu/availabilityIcon.svg';
import StallIcon from 'components/PDA/NavBarMobile/MenuMobile/images/Menu/stallIcon.svg';
import HistoryIcon from 'components/PDA/NavBarMobile/MenuMobile/images/Menu/historyIcon.svg';
import { Route } from 'types/router';
import SubOrdersPageWrapper from 'wrappers/SubOrdersPageWrapper';

export enum RouteId {
  Orders = 'orders',
  Menu = 'menu',
  Stall = 'stall',
  History = 'history',
  Reject = 'reject',
  Refund = 'refund',
}

export const routes: Record<RouteId, Route> = {
  [RouteId.Orders]: {
    path: '/',
    title: 'navbar.routeOnlineOrders',
    description: 'navbar.routeOnlineOrders.description',
    icon: OrdersIcon,
    props: { exact: true },
    element: SubOrdersPageWrapper,
  },
  [RouteId.Menu]: {
    path: '/menu',
    title: 'navbar.routeMenuAvailability',
    description: 'navbar.routeMenuAvailability.description',
    icon: AvailabilityIcon,
    props: { exact: true },
    element: MenuPage,
  },
  [RouteId.Stall]: {
    path: '/stall',
    title: 'navbar.routeStallSettings',
    description: 'navbar.routeStallSettings.description',
    icon: StallIcon,
    props: { exact: true },
    element: StallSettingsPage,
  },
  [RouteId.History]: {
    path: '/history',
    title: 'navbar.routeOrderHistory',
    description: 'navbar.routeOrderHistory.description',
    icon: HistoryIcon,
    props: { exact: true },
    element: SubOrdersHistoryPage,
  },
  [RouteId.Reject]: {
    path: '/reject',
    props: { exact: true },
    element: RejectSubOrderPage,
  },
  [RouteId.Refund]: {
    path: '/refund',
    props: { exact: true },
    element: RefundSubOrderPage,
  },
};
