const config = {
  isDevelopment: process.env.NODE_ENV === 'development',
  devFoodSupplierId: process.env.REACT_APP_DEV_FOOD_SUPPLIER_ID as string,
  datadogApplicationId: process.env.REACT_APP_DATADOG_APPLICATION_ID as string,
  datadogClientToken: process.env.REACT_APP_DATADOG_CLIENT_TOKEN as string,
  splitApiKey: process.env.REACT_APP_SPLIT_API_KEY as string,

  orderServiceHasuraUri: process.env.REACT_APP_ORDER_SERVICE_HASURA_URI as string,
  orderServiceHasuraSubscriptionUri: process.env.REACT_APP_ORDER_SERVICE_HASURA_SUBSCRIPTION_URI as string,
  orderServiceHasuraApiKey: process.env.REACT_APP_ORDER_SERVICE_HASURA_API_KEY as string,
  orderServiceApiUri: process.env.REACT_APP_ORDER_SERVICE_API_URI as string,

  catalogServiceHasuraApiKey: process.env.REACT_APP_CATALOG_SERVICE_HASURA_API_KEY as string,
  catalogServiceHasuraUri: process.env.REACT_APP_CATALOG_SERVICE_HASURA_URI as string,
  catalogServiceHasuraSubscriptionUri: process.env.REACT_APP_CATALOG_SERVICE_HASURA_SUBSCRIPTION_URI as string,
  catalogServiceApiUri: process.env.REACT_APP_CATALOG_SERVICE_API_URI as string,

  kposOrigin: process.env.REACT_APP_KPOS_ORIGIN as string,

  printServerUrl: process.env.REACT_APP_PRINT_SERVER_URL as string,

  // DEV section, all these fields must be empty in production
  devEnableAutorejectApi: process.env.REACT_APP_DEV_ENABLE_AUTOREJECT_API === 'true',

  buildVersion: process.env.REACT_APP_BUILD_VERSION || '',

  devicePDAWidth: (process.env.REACT_APP_DEVICE_PDA_WIDTH as string) || 768,

  corsService: process.env.REACT_APP_CORS_SERVICE as string,
  auth0Domain: process.env.REACT_APP_AUTH0_DOMAIN as string,
  auth0ClientId: process.env.REACT_APP_AUTH0_CLIENT_ID as string,
  auth0Audience: process.env.REACT_APP_AUTH0_AUDIENCE as string,
};

export default config;
