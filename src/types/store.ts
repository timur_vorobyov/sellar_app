import { Dispatch } from 'react';
import { FoodSupplierPlatform } from './models/stall';
import { SubOrder, IncomingSubOrderTimer } from './models/subOrder';
import { Dictionary } from 'types/generics';

export enum ActionTypes {
  SET_LOADER_STATE = 'SET_LOADER_STATE',
  SET_INCOMING_SUB_ORDERS = 'SET_INCOMING_SUB_ORDERS',
  SET_ACCEPTED_SUB_ORDERS = 'SET_ACCEPTED_SUB_ORDERS',
  EXPIRE_SUB_ORDER = 'EXPIRE__SUB_ORDER',
  SET_FOOD_SUPPLIER_PLATFORMS = 'SET_FOOD_SUPPLIER_PLATFORMS',
  UPDATE_FOOD_SUPPLIER_PLATFORMS = 'UPDATE_FOOD_SUPPLIER_PLATFORMS',
  UPDATE_SUB_ORDERS_EXPIRATION_TIME = 'UPDATE_SUB_ORDERS_EXPIRATION_TIME',
  UPDATE_NEW_SUB_ORDER_STATUS = 'UPDATE_NEW_SUB_ORDER_STATUS',
}
export interface Action {
  type: ActionTypes;
  payload?: Dictionary;
}
export interface State {
  loaders: number;
  incomingSubOrders: SubOrder[];
  acceptedSubOrders: SubOrder[];
  foodSupplierPlatforms: FoodSupplierPlatform[];
  incomingSubOrdersTimers: IncomingSubOrderTimer[];
  newIncomigSubOrders: { [key: number]: boolean };
  amountOfNewSubOrders: number;
  isNewIncomingSubOrderUpdateAllowed: boolean;
}

export type AppContext = { state: State; dispatch: Dispatch<Action> };
