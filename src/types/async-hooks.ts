import { Anything } from './generics';

export interface ApiCallOptions {
  args?: Anything[];
  notifyOnSuccess?: string | false;
  notifyOnFail?: string | false;
  onSuccess?: (response: Anything, args: Anything[]) => void;
  onFail?: (error: unknown, args: Anything[]) => void;
  useLoader?: boolean;
}

export const defaultApiCallOptions = {
  useLoader: true,
};
