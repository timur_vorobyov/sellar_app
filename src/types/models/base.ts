import { DateString } from 'types/generics';

export interface Timestamp {
  createdAt: DateString;
  updatedAt: DateString;
}
