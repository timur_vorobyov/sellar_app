export interface Location {
  id: number;
  address: string;
  available: boolean;
  coordinates: string;
  disabled: boolean;
  isOutlet: boolean;
  name: string;
}
