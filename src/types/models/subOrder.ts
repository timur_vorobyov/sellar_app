import { Customer } from './customer';
import { Driver } from './driver';
import { Payment } from './payment';
import { Product, SubOrderHistoryProduct, SubOrderProduct } from './product';
import { Location } from './location';
import { SubOrderType } from 'constants/subOrder';
import { PaymentMethod } from 'constants/payment';

export enum Platform {
  Deliveroo = 'DELIVEROO',
  Fairprice = 'FAIRPRICE',
  Foodpanda = 'FOODPANDA',
  Grab = 'GRAB',
}

export enum SubOrderState {
  Incoming = 'SUBMITTED',
  Accepted = 'PREPARING',
  ReadyForCollection = 'READY_FOR_COLLECTION',
  Cancelled = 'CANCELLED',
  Collected = 'COLLECTED',
  Rejected = 'REJECTED',
  Refunded = 'REFUNDED',
  PartialRefund = 'PARTIAL_REFUND',
  PendingRefund = 'REFUND_PENDING',
}

export const SubOrderStateTranslations = {
  [SubOrderState.Incoming]: 'Submitted',
  [SubOrderState.Accepted]: 'Preparing',
  [SubOrderState.ReadyForCollection]: 'Ready for collection',
  [SubOrderState.Cancelled]: 'Cancelled',
  [SubOrderState.Collected]: 'Collected',
  [SubOrderState.Rejected]: 'Rejected',
  [SubOrderState.Refunded]: 'Fully Refunded',
  [SubOrderState.PartialRefund]: 'Partially Refunded',
};

export interface SubOrder {
  id: number;
  isCancellationReceiptPrinted: boolean;
  fees: number;
  isCutleryNeeded: boolean;
  logo: string;
  state: string;
  type: SubOrderType;
  createdAt: Date;
  graceTime: number;
  subTotalPrice: number;
  totalPrice: number;
  productsAmount?: number;
  productsGroupedByCategories: SubOrderProduct[];
  products: Product[];
  deliveryPrice: number;
  orderId: string;
  customer: Customer;
  driver: Driver;
  payments: Payment[];
  foodSupplierId: number;
  location: Location;
  pickUpTime: Date | null;
  platform: Platform;
  isAutoAccepted: boolean;
  isReceiptPrinted: boolean;
  netSales: number;
  order: {
    id: number;
    discounts: number;
  };
  totalContainerFeesAmount: number;
}

export interface CustomerData {
  orderId: number;
  customer: { name: string; phone: string };
}

export type IncomingSubOrder = SubOrder;
export type AcceptedSubOrder = SubOrder;
export type CancelledSubOrder = SubOrder;

export interface IncomingSubOrderTimer {
  subOrderId: number;
  expireIn: number;
}

/** extends subOrderHistory by Order type */
export interface SubOrderHistory {
  id: number;
  isCancellationReceiptPrinted: boolean;
  isCutleryNeeded: boolean;
  logo: string;
  state: string;
  type: SubOrderType;
  subTotalPrice: number;
  totalPrice: number;
  products: SubOrderHistoryProduct[];
  orderId: string;
  paymentsMethod: PaymentMethod;
  pickUpTime: Date | null;
  completedTime: Date | null;
  collectionTime: Date | null;
  cancelledTime: Date | null;
  readyForCollectionTime: Date | null;
  platform: Platform;
  deliveryPrice: number;
  customer: Customer;
  driver: Driver;
  createdAt: Date;
  location: Location;
  refundState: string;
  numberOfProducts: number;
  order: {
    id: number;
    refunds: SubOrderRefund[];
    discounts: number;
  };
  netSales: number;
  originNetSales: number;
  grossSales: number;
  refundedPrice: number;
  totalReturnedContainerFeesAmountWithDiscount: number;
  totalContainerFeesAmount: number;
}

export interface SubOrderRefund {
  totalRefunded: number;
  state: string;
  id: number;
}

export interface SubOrderData {
  appPaymentDiscount: number;
}
