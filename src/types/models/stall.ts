import { DateString } from 'types/generics';
import { Platform } from 'types/models/subOrder';

export interface FoodSupplierPlatform {
  id: string;
  name: Platform;
  logo: string;
  status: string;
  autoAccept: boolean;
  autoPrint: boolean;
  prepTime: DateString | null;
  isLongRingtone: boolean;
  autoAcceptOnly?: boolean;
  manualAcceptOnly?: boolean;
}

export interface StallStatusOption {
  value:
    | 'OPEN'
    | 'CLOSE_FOR'
    | 'CLOSED_FOR_15'
    | 'CLOSED_FOR_30'
    | 'CLOSED_FOR_45'
    | 'CLOSED_FOR_60'
    | 'CLOSED_FOR_120'
    | 'CLOSED_FOR_180'
    | 'CLOSED';
  checked: boolean;
  disabled: boolean;
  indicatorColor?: string;
  indicatorInDropdown?: boolean;
  text: string;
  classes?: string;
}
