import { EmptyObject } from 'types/generics';

export interface Menu {
  id: number;
  name: string;
  nameCn?: string;
}

export interface MenuItemActiveHours {
  friday: {
    timings:
      | []
      | [
          {
            openTime: string;
            closeTime: string;
          },
        ];
  };
  monday: {
    timings:
      | []
      | [
          {
            openTime: string;
            closeTime: string;
          },
        ];
  };
  sunday: {
    timings:
      | []
      | [
          {
            openTime: string;
            closeTime: string;
          },
        ];
  };
  tuesday: {
    timings:
      | []
      | [
          {
            openTime: string;
            closeTime: string;
          },
        ];
  };
  saturday: {
    timings:
      | []
      | [
          {
            openTime: string;
            closeTime: string;
          },
        ];
  };
  thursday: {
    timings:
      | []
      | [
          {
            openTime: string;
            closeTime: string;
          },
        ];
  };
  wednesday: {
    timings:
      | []
      | [
          {
            openTime: string;
            closeTime: string;
          },
        ];
  };
}

export interface MenuItem {
  id: number;
  category: {
    name: string;
    nameCn: string;
  };
  name: string;
  nameCn: string;
  available: number;
  unavailableUntil: Date | null;
  activeHours: EmptyObject | MenuItemActiveHours;
}

export type ProductItem = MenuItem;

export interface CustomisationOption {
  id: number;
  name: string;
  nameCn: string;
  available: number;
  customisationId: number;
  customisation: {
    name: string;
    nameCn: string;
  };
}

export interface Customisation {
  id: number;
  name: string;
  nameCn: string;
}
