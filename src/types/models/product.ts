export interface Product {
  id: number;
  fullProductSum: number;
  menuItemId: number;
  quantity: number;
  price: number;
  name: string;
  nameCn: string;
  customerComments: string;
  containerFee: number;
  customisations: ProductCustomisation[];
}

export interface CustomisationBase {
  name: string;
  nameCn: string;
  quantity: number;
  id: number;
  optionId: number;
  price: number;
  actualQuantity: number;
}

export type ProductCustomisation = CustomisationBase;

export interface SubOrderProduct {
  category: {
    id: number;
    name: string;
    nameCn: string;
  };
  list: Product[];
}
export interface SubOrderHistoryProductCustomisation extends ProductCustomisation {
  refundedQuantity: number;
}
export interface SubOrderHistoryProduct extends Product {
  discountedPrice: number;
  refundedQuantity: number;
  customisations: SubOrderHistoryProductCustomisation[];
  returnedContainerFeeWithDiscount: number;
}

export interface RefundPageProduct extends SubOrderHistoryProduct {
  actualQuantity: number;
}

export type RefundPageProductCustomisation = SubOrderHistoryProductCustomisation;

export interface ProductRefund {
  id: number;
  items: ProductRefundItems[];
}

export interface ProductRefundItems {
  id: number;
  quantity: number;
  customisationOptions: CustomisationOptionsRefund[];
}

export interface CustomisationOptionsRefund {
  id: number;
  quantity: number;
}
