export interface Driver {
  id: number;
  name: string;
  phoneNumber: string;
  note: string;
}
