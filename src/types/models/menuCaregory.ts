export type MenuCategory = {
  id: number;
  name: string;
  nameCn: string;
};
