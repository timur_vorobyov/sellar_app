export type PaymentState =
  | 'PENDING'
  | 'AUTHORIZATION_PENDING'
  | 'AUTHORIZED'
  | 'AUTHORIZATION_FAILED'
  | 'AUTHORIZATION_VOID'
  | 'CAPTURE_FAILED'
  | 'CAPTURED'
  | 'REFUNDED'
  | 'PARTIAL_REFUND';

export type PaymentMethodType = 'CARD' | 'LINKPOINTS';

export interface Payment {
  id: number;
  method: PaymentMethodType;
  paymentId: string;
  state: PaymentState;
  total: number;
}
