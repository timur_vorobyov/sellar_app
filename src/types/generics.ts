/* eslint-disable @typescript-eslint/no-explicit-any */
export interface Dictionary<T = any> {
  [key: string]: T;
}

export type Anything = any;

export type DateString = string;

export type EmptyObject = Record<string, never>;

export type GenericFunction = (...args: any[]) => any;
export type GenericAsyncFunction = (...args: any[]) => Promise<any>;

export interface Option<V = number, T = string> {
  value: V;
  text: T;
}

export const NoOp = (..._args: any[]): any => {
  /* do nothing */
};
