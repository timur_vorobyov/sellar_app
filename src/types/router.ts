import { PropsWithChildren } from 'react';
import { Dictionary } from 'types/generics';

export interface Route {
  path: string;
  props?: Dictionary;
  title?: string;
  description?: string;
  mobile?: boolean;
  icon?: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  element: (props: PropsWithChildren<any>) => JSX.Element;
}
