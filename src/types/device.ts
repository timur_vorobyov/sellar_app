export interface DeviceType {
  isMobile: boolean;
  isTablet: boolean;
}
