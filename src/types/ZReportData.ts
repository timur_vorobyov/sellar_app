import { DateString } from 'types/generics';

export interface ZReportData {
  businessDate: DateString;
  discountsSum: number;
  grossTotalSum: number;
  gst: number;
  qc: { [key: string]: number };
  netTotalSum: number;
  foodSupplierId: number;
  refundSum: number;
  qcFunded: number;
  amountReceived: number;
  tenantFunded: number;
  containerFees: number;
}
