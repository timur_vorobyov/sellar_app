import * as moment from 'moment';
export interface MenuFilter {
  category: number[];
  availability: number[];
  searchTerm: string;
}
export interface CustomisationOptionFilter {
  customisationIds: number[];
  availability: number[];
  searchTerm: string;
}
export interface SubOrdersHistoryFilter {
  subOrderStatuses: number[];
  date: moment.Moment;
  platformsIds: number[];
  searchTerm: string;
}
