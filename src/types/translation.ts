export enum Locale {
  ENGLISH = 'en',
  CHINESE = 'cn',
}

export interface Translation {
  [key: string]: string;
}
