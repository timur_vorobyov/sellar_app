import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import { datadogRum } from '@datadog/browser-rum';
import '@fontsource/lato';
import 'styles/global.scss';
import config from 'config';
import TenantIntegration from 'wrappers/SappWrapper/TenantIntegration';
import SappWrapper from './wrappers/SappWrapper/SappWrapper';
import { TenantDataProvider } from 'contexts/tenant/provider';
import { BrowserRouter } from 'react-router-dom';
import { LanguageProvider } from 'contexts/languageContextProvider/provider';
import { SplitFactory } from '@splitsoftware/splitio-react';
import { EuiProvider } from '@elastic/eui';
import { AuthProvider } from 'contexts/authContextProvider/authContextProvider';
import { FoodSupplierProvider } from 'contexts/foodSupplierProvider/provider';
import PDAIntegration from 'wrappers/SappWrapper/PDAIntegration';

if (config.isDevelopment) {
  const envVarsInUse = Object.keys(process.env)
    .filter((envKey) => envKey.startsWith('REACT_APP_'))
    .map((envKey) => `${envKey} = "${process.env[envKey]}"`);

  console.log(`Using env variables:\n${envVarsInUse.join('\n')}`);
}

datadogRum.init({
  applicationId: config.datadogApplicationId,
  clientToken: config.datadogClientToken,
  site: 'datadoghq.com',
  service: 'seller-app',
  env: process.env.REACT_APP_ENVIRONMENT,
  version: process.env.REACT_APP_BUILD_VERSION,
  sampleRate: 100,
  trackInteractions: true,
  defaultPrivacyLevel: 'mask-user-input',
  useCrossSiteSessionCookie: true,
  trackSessionAcrossSubdomains: true,
});

datadogRum.startSessionReplayRecording();

const sdkConfig: SplitIO.IBrowserSettings = {
  core: {
    authorizationKey: config.splitApiKey,
    key: config.splitApiKey,
  },
};

const isKPOSIntegrationEnabled = window.location !== window.parent.location;

ReactDOM.render(
  <React.StrictMode>
    <SplitFactory config={sdkConfig}>
      <AuthProvider>
        <LanguageProvider>
          <FoodSupplierProvider>
            <EuiProvider>
              <BrowserRouter>
                {isKPOSIntegrationEnabled ? (
                  <TenantDataProvider>
                    <TenantIntegration>
                      <SappWrapper />
                    </TenantIntegration>
                  </TenantDataProvider>
                ) : (
                  <PDAIntegration>
                    <SappWrapper />
                  </PDAIntegration>
                )}
              </BrowserRouter>
            </EuiProvider>
          </FoodSupplierProvider>
        </LanguageProvider>
      </AuthProvider>
    </SplitFactory>
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
