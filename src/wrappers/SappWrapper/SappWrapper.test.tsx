import { cleanup } from '@testing-library/react';
import '@testing-library/jest-dom';
import SappWrapper from './SappWrapper';
import { FoodSupplierProvider } from '../../contexts/foodSupplierProvider/provider';
import { BrowserRouter } from 'react-router-dom';
import { customI18nRender } from '../../utils/testing/customRender';

afterEach(cleanup);
jest.mock('components/SubscriptionsManager/SubscriptionsManager', () => () => <div>Grapqhql subscriptions</div>);
jest.mock('components/Loader/Loader', () => () => <div>Loader</div>);

describe('<SappWrapper> wrapper', () => {
  it('should render without crashing', () => {
    const { container } = customI18nRender(
      <FoodSupplierProvider>
        <BrowserRouter>
          <SappWrapper />
        </BrowserRouter>
      </FoodSupplierProvider>,
    );
    expect(container.textContent).toMatch('Grapqhql subscriptions');
    expect(container.textContent).toMatch('Loader');
  });
});
