import { cleanup } from '@testing-library/react';
import '@testing-library/jest-dom';
import { knownRoutes } from './utils';

afterEach(cleanup);

describe('<knownRoutes> component', () => {
  it('should render with routs', () => {
    expect(knownRoutes.length).toBe(6);
  });
});
