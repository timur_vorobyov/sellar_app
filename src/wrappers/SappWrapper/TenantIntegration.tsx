import { mapSerialNumberToFoodSupplierId } from 'apis/graphql/hasura/queries';
import { pingKpos } from 'apis/rest/catalog';
import { Splash } from 'components/Splash/Splash';
import IncomingSubOrderNotifier from 'components/IncomingSubOrderNotifier';
import { IntercomProvider } from 'contexts/intecomContextProvider/provider';
import { useTennantDataContext } from 'contexts/tenant/context';
import { useEffect } from 'react';
import { getStorageAccessToken } from 'storage/adapters/accessToken';
import { getStorageSerialNumber } from 'storage/adapters/serialNumber';
import { useFoodSupplierContext } from 'contexts/foodSupplierProvider/context';

const TenantIntegration = (props: { children: JSX.Element }) => {
  const { isOnline } = useTennantDataContext();
  const { getFoodSupplierId, setFoodSupplierId } = useFoodSupplierContext();
  const foodSupplierId = getFoodSupplierId();
  const accessToken = getStorageAccessToken();
  const serialNumber = getStorageSerialNumber();

  useEffect(() => {
    if (serialNumber && !foodSupplierId) {
      (async () => {
        const mappedFoodSupplierId = await mapSerialNumberToFoodSupplierId(serialNumber);
        setFoodSupplierId(mappedFoodSupplierId);
        setInterval(() => {
          pingKpos(serialNumber);
        }, 60000);
      })();
    }
  }, []);

  const shouldRenderSapp = !!(accessToken && isOnline && foodSupplierId);

  let splashComponent: JSX.Element = <></>;

  if (!accessToken) {
    splashComponent = <Splash message="Awaiting authorization from KPOS" />;
  } else if (!isOnline) {
    splashComponent = (
      <Splash message="Unable to connect app. Make sure that Wi-Fi or mobile data is turned on, then try again" />
    );
  } else if (!foodSupplierId) {
    splashComponent = <Splash message="Awaiting for KPOS information" />;
  }

  return (
    <IntercomProvider>
      <>
        {shouldRenderSapp ? props.children : splashComponent}
        <IncomingSubOrderNotifier />
      </>
    </IntercomProvider>
  );
};

export default TenantIntegration;
