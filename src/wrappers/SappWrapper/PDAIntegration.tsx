import { mapSerialNumberToFoodSupplierId } from 'apis/graphql/hasura/queries';
import { useAuth } from 'contexts/authContextProvider/authContextProvider';
import { useEffect } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { getStorageSerialNumber } from 'storage/adapters/serialNumber';
import LoginPage from 'views/LoginPage/LoginPage';
import { useFoodSupplierContext } from 'contexts/foodSupplierProvider/context';

const PDAIntegration = (props: { children: JSX.Element }) => {
  const { isAuthenticated } = useAuth();
  const { getFoodSupplierId, setFoodSupplierId } = useFoodSupplierContext();
  const foodSupplierId = getFoodSupplierId();
  const serialNumber = getStorageSerialNumber();

  if (!isAuthenticated) {
    return (
      <Switch>
        <Route path={'/login'} exact component={LoginPage} />
        <Redirect to={'/login'} />
      </Switch>
    );
  }

  useEffect(() => {
    if (serialNumber && !foodSupplierId) {
      (async () => {
        const mappedFoodSupplierId = await mapSerialNumberToFoodSupplierId(serialNumber);
        setFoodSupplierId(mappedFoodSupplierId);
      })();
    }
  }, []);

  return props.children;
};

export default PDAIntegration;
