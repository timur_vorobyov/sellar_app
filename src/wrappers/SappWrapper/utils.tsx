import { routes } from 'config/routes';
import { Route } from 'react-router-dom';

export const knownRoutes = Object.values(routes).map((route, index) => (
  <Route key={index} path={route.path} {...(route.props || {})}>
    <route.element />
  </Route>
));
