import { Switch } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import '@elastic/eui/dist/eui_theme_light.css';
import Loader from 'components/Loader/Loader';
import RedirectionRoute from 'components/RedirectionRoute/RedirectionRoute';
import { knownRoutes } from './utils';
import { I18n } from 'components/i18n/I18n';
import { DeviceContextProvider } from 'contexts/deviceContextProvider/deviceContextProvider';
import { StateProvider } from 'contexts/store';
import SubscriptionsManager from 'components/SubscriptionsManager/SubscriptionsManager';

const SappWrapper = () => {
  const { innerWidth } = window;

  return (
    <DeviceContextProvider width={innerWidth}>
      <StateProvider>
        <SubscriptionsManager />
        <Loader />
        <ToastContainer autoClose={3000} closeOnClick pauseOnFocusLoss={false} pauseOnHover={false} />
        <I18n>
          <Switch>
            <RedirectionRoute>{knownRoutes}</RedirectionRoute>
          </Switch>
        </I18n>
      </StateProvider>
    </DeviceContextProvider>
  );
};

export default SappWrapper;
