import { acceptSubOrder, pickupSubOrder, rejectSubOrder, updateReceiptFlag } from 'apis/rest/subOrders';
import { useStateContext } from 'contexts/store';
import { SubOrder } from 'types/models/subOrder';
import { printAcceptedReceipt } from 'utils/receipt';
import useExplicitApiCall from 'utils/hooks/use-explicit-api-call';
import { ActionTypes } from 'types/store';
import { toast } from 'react-toastify';
import { SubOrderRejectionReason } from 'constants/subOrder';
import config from 'config';
import { useDeviceContext } from 'contexts/deviceContextProvider/deviceContextProvider';
import { receiptTypes } from 'constants/receiptType';
import { useTreatments } from '@splitsoftware/splitio-react';
import { FeatureFlags } from 'constants/featureFlags';
import { IntercomCommand } from 'contexts/intecomContextProvider/types';
import { useIntercomContext } from 'contexts/intecomContextProvider/context';
import { useLanguageContext } from 'contexts/languageContextProvider/context';
import SubOrdersPage from 'views/SubOrdersPage/SubOrdersPage';

const SubOrdersPageWrapper = () => {
  const { getLocale } = useLanguageContext();

  const feat = useTreatments([FeatureFlags.QR_CODE]);

  const { execute: executeAcceptSubOrder } = useExplicitApiCall(acceptSubOrder, {
    onSuccess: (_response, [subOrder]) => toast.success(`Order "${subOrder.orderId}" accepted`),
  });
  const { execute: executePickupSubOrder } = useExplicitApiCall(pickupSubOrder, {
    onSuccess: (_response, [subOrder]) => toast.success(`Order "${subOrder.orderId}" is ready for pickup`),
  });
  const { execute: executeSubOrderReject } = useExplicitApiCall(rejectSubOrder, {
    onSuccess: (_response, [subOrder]) => toast.warn(`Order ${subOrder.orderId} rejected by timeout`),
  });
  const { execute: executePrintReceipt } = useExplicitApiCall(updateReceiptFlag);

  const {
    state: { amountOfNewSubOrders, incomingSubOrders, acceptedSubOrders },
    dispatch,
  } = useStateContext();

  const { isTablet } = useDeviceContext();
  const { postMessage } = useIntercomContext();

  const getSubOrdersQueueNumber = (arr: SubOrder[]) => {
    if (arr.length === 0) {
      return null;
    }
    if (arr.length > 20) {
      return '20+';
    }
    return arr.length.toString();
  };

  const handleAccept = async (subOrder: SubOrder) => {
    if (subOrder.isAutoAccepted) {
      executePrintReceipt(subOrder.id, true);
    } else {
      executeAcceptSubOrder(subOrder);
      executePrintReceipt(subOrder.id, true);
    }

    // NOTE: Intentionally print twice
    const responseCode = await printAcceptedReceipt(
      subOrder,
      getLocale(),
      receiptTypes.TENNANT,
      feat[FeatureFlags.QR_CODE].treatment,
      isTablet,
    );
    if (responseCode === 200 || responseCode === 201) {
      await printAcceptedReceipt(
        subOrder,
        getLocale(),
        receiptTypes.CUSTOMER,
        feat[FeatureFlags.QR_CODE].treatment,
        isTablet,
      );
    }

    if (isTablet) {
      postMessage(IntercomCommand.incomingOrdersCount, { value: amountOfNewSubOrders });
    }
  };

  const handleExpire = (subOrder: SubOrder) => {
    if (config.devEnableAutorejectApi) {
      const stillIncoming = incomingSubOrders.find((incoming) => incoming.id === subOrder.id);

      if (stillIncoming) {
        console.log('subOrder for autoexpire found in incoming:', stillIncoming);
        executeSubOrderReject(subOrder, SubOrderRejectionReason.AutoRejected);
      } else {
        console.log(`Order ${subOrder.id} not found in incoming. Either accepted or rejected`);
      }
    }

    dispatch({
      type: ActionTypes.EXPIRE_SUB_ORDER,
      payload: { subOrder },
    });

    if (isTablet) {
      postMessage(IntercomCommand.incomingOrdersCount, { value: incomingSubOrders.length });
    }
  };

  const handlePickup = (subOrder: SubOrder) => {
    executePickupSubOrder(subOrder);

    if (isTablet) {
      postMessage(IntercomCommand.incomingOrdersCount, { value: incomingSubOrders.length });
    }
  };

  return (
    <SubOrdersPage
      handleExpire={handleExpire}
      handlePickup={handlePickup}
      handleAccept={handleAccept}
      incomingSubOrdersNumber={getSubOrdersQueueNumber(incomingSubOrders)}
      acceptedSubOrdersNumber={getSubOrdersQueueNumber(acceptedSubOrders)}
    />
  );
};

export default SubOrdersPageWrapper;
