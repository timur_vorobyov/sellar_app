import { cleanup, fireEvent, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { BrowserRouter } from 'react-router-dom';
import SubOrdersPageWrapper from './SubOrdersPageWrapper';
import { customI18nRender } from '../utils/testing/customRender';
import { FoodSupplierProvider } from '../contexts/foodSupplierProvider/provider';
import { DeviceContextProvider } from '../contexts/deviceContextProvider/deviceContextProvider';
import { StateProvider } from '../contexts/store';

afterEach(cleanup);

describe('<SubOrdersPageWrapper> wrapper', () => {
  it('should render without crashing', () => {
    const { container } = customI18nRender(
      <FoodSupplierProvider>
        <BrowserRouter>
          <DeviceContextProvider width={500}>
            <StateProvider>
              <SubOrdersPageWrapper />
            </StateProvider>
          </DeviceContextProvider>
        </BrowserRouter>
      </FoodSupplierProvider>,
    );
    expect(screen.getByText('There are currently no online orders.')).toBeInTheDocument();
    fireEvent.click(screen.getByText('Accepted'));
    expect(container.getElementsByClassName('AcceptedSubOrderMobile__container')[0]).toBeTruthy();
  });
});
