import { CLOSED_TO_MINUTES_MAPPER } from 'constants/stall';
import { Dictionary } from 'types/generics';
import { FoodSupplierPlatform } from 'types/models/stall';
import { toDate } from 'utils/datetime';
import { DeviceDbType } from './types';

export const filterObject = (obj: Dictionary, allowedKeys: string[]): Dictionary =>
  Object.keys(obj).reduce((acc: Dictionary, key: string) => {
    if (allowedKeys.includes(key)) {
      acc[key] = obj[key];
    }

    return acc;
  }, {});

export const prepareStallUpdatePayload = (foodSuppliersPlatform: Partial<FoodSupplierPlatform>): Dictionary => {
  const result: Dictionary = {
    platformId: foodSuppliersPlatform.id,
  };

  result.autoAccept = foodSuppliersPlatform.autoAccept;
  if (foodSuppliersPlatform.autoPrint) {
    result.autoPrint = foodSuppliersPlatform.autoPrint && foodSuppliersPlatform.autoAccept;
  }

  if (foodSuppliersPlatform.prepTime) {
    result.preparationTime = foodSuppliersPlatform.prepTime;
  }

  if (foodSuppliersPlatform.status) {
    if (foodSuppliersPlatform.status === 'CLOSED') {
      result.status = 'CLOSED';
      result.closedAt = new Date();
      result.closedFor = null;
    } else if (foodSuppliersPlatform.status === 'OPEN') {
      result.status = 'OPEN';
      result.closedAt = null;
      result.closedFor = null;
    } else if (CLOSED_TO_MINUTES_MAPPER[foodSuppliersPlatform.status]) {
      result.status = 'CLOSED';
      result.closedAt = new Date();
      result.closedFor = CLOSED_TO_MINUTES_MAPPER[foodSuppliersPlatform.status];
    }
  }

  return result;
};

export const formattedFoddSupplierDevices = (foodSupplierDevices: DeviceDbType) => {
  return {
    createdAt: toDate(foodSupplierDevices.createdAt),
    foodSupplierId: foodSupplierDevices.foodSupplierId,
    name: foodSupplierDevices.name,
    serialNumber: foodSupplierDevices.serialNumber,
    updatedAt: foodSupplierDevices.updatedAt,
  };
};
