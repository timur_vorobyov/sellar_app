import { ApolloClient, DefaultOptions, InMemoryCache } from '@apollo/client';
import { WebSocketLink } from '@apollo/client/link/ws';
import { FOOD_SUPPLIER_PLATFORMS_SUBSCRIPTION } from 'apis/graphql/hasura/schemas/subscription/catalogServiceSubscriptions/foodSupplierPlatformSubscriptions';
import {
  ACCEPTED_SUB_ORDERS_SUBSCRIPTION,
  INCOMING_SUB_ORDERS_SUBSCRIPTION,
} from 'apis/graphql/hasura/schemas/subscription/orderServiceSubscriptions/subOrder';
import config from 'config';
import { SubscriptionClient } from 'subscriptions-transport-ws';

const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
  },
  query: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all',
  },
};

const orderServiceSubscriptionClient = new SubscriptionClient(config.orderServiceHasuraSubscriptionUri, {
  reconnect: true,
  connectionParams: {
    headers: {
      'x-hasura-admin-secret': config.orderServiceHasuraApiKey,
    },
  },
});
const orderServiceSubscriptionLink = new WebSocketLink(orderServiceSubscriptionClient);
const orderServiceSubscriptionApolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  link: orderServiceSubscriptionLink,
  defaultOptions: defaultOptions,
});

const catalogServiceSubscriptionClient = new SubscriptionClient(config.catalogServiceHasuraSubscriptionUri, {
  reconnect: true,
  connectionParams: {
    headers: {
      'x-hasura-admin-secret': config.catalogServiceHasuraApiKey,
    },
  },
});
const catalogSerrviceSubscriptionLink = new WebSocketLink(catalogServiceSubscriptionClient);
const catalogServiceSubscriptionApolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  link: catalogSerrviceSubscriptionLink,
  defaultOptions: defaultOptions,
});

export const subscribeToIncomingSubOrdersList = async (foodSupplierId: number) => {
  return orderServiceSubscriptionApolloClient.subscribe({
    query: INCOMING_SUB_ORDERS_SUBSCRIPTION,
    variables: { foodSupplierId },
  });
};

export const subscribeToAcceptedSubOrdersList = async (foodSupplierId: number) => {
  return orderServiceSubscriptionApolloClient.subscribe({
    query: ACCEPTED_SUB_ORDERS_SUBSCRIPTION,
    variables: { foodSupplierId },
  });
};

export const subscribeToFoodSupplierPlatformsList = async (foodSupplierId: number) => {
  return catalogServiceSubscriptionApolloClient.subscribe({
    query: FOOD_SUPPLIER_PLATFORMS_SUBSCRIPTION,
    variables: { foodSupplierId },
  });
};
