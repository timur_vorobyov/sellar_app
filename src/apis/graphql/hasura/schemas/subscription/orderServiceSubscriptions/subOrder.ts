import { gql } from '@apollo/client';
import { CORE_SUB_ORDER_FIELDS } from '../../fragments/subOrder';

export const INCOMING_SUB_ORDERS_SUBSCRIPTION = gql`
  subscription ($foodSupplierId: Int) {
    sub_orders(
      order_by: { created_at: asc }
      where: { sub_order_food_supplier: { food_supplier_id: { _eq: $foodSupplierId } }, state: { _in: ["SUBMITTED"] } }
    ) {
      ...CoreSubOrderFields
    }
  }
  ${CORE_SUB_ORDER_FIELDS}
`;

export const ACCEPTED_SUB_ORDERS_SUBSCRIPTION = gql`
  subscription ($foodSupplierId: Int) {
    sub_orders(
      order_by: { created_at: asc }
      where: {
        sub_order_food_supplier: { food_supplier_id: { _eq: $foodSupplierId } }
        state: { _in: ["PREPARING", "CANCELLED"] }
        cancellation_receipt_printed: { _eq: false }
        cancelled_by: { _neq: "CUSTOMER" }
      }
    ) {
      ...CoreSubOrderFields
    }
  }
  ${CORE_SUB_ORDER_FIELDS}
`;
