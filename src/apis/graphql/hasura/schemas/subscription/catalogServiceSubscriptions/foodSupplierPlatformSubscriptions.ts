import { gql } from '@apollo/client';

export const FOOD_SUPPLIER_PLATFORMS_SUBSCRIPTION = gql`
  subscription ($foodSupplierId: Int!) {
    food_supplier_platforms(where: { deleted_at: { _is_null: true }, food_supplier_id: { _eq: $foodSupplierId } }) {
      preparation_time
      long_ringtone
      platform {
        id
        name
        auto_accept_only
        manual_accept_only
        image {
          url
        }
      }
      status
      auto_accept
      auto_print
      closed_at
      closed_for
    }
  }
`;
