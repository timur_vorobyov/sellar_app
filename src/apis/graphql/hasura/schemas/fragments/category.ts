import { gql } from '@apollo/client';

export const CORE_CATEGORY_FIELDS = gql`
  fragment CoreCategoryFields on food_supplier_categories {
    id
    name
    name_cn
  }
`;
