import { gql } from '@apollo/client';

export const CORE_SUB_ORDER_FIELDS = gql`
  fragment CoreSubOrderFields on sub_orders {
    id
    fees
    pickup_at
    state
    created_at
    receipt_printed
    total
    net_sales
    subtotal
    order {
      id
      cutlery
      fulfillment_method
      discounts
      order_payments {
        id
        method
        payment_id
        state
      }
      platform
      short_reference
    }
    sub_order_items {
      name
      name_cn
      quantity
      price
      discounts
      total
      subtotal
      id
      menu_item_id
      special_instructions
      sub_order_item_customisations {
        name
        name_cn
        id
        option_id
        price
        quantity
        actual_quantity
      }
      item_fees {
        amount
        type
      }
    }
    sub_order_food_supplier {
      brand_id
      brand_name
      food_supplier_id
      name
    }
  }
`;
