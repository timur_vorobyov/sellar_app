import { gql } from '@apollo/client';
import { CORE_CATEGORY_FIELDS } from '../../fragments/category';

export const GET_CATEGORIES = gql`
  query ($foodSupplierId: Int!) {
    food_supplier_categories(
      where: { deleted_at: { _is_null: true }, food_supplier_menu: { food_supplier_id: { _eq: $foodSupplierId } } }
    ) {
      ...CoreCategoryFields
    }
  }
  ${CORE_CATEGORY_FIELDS}
`;

export const GET_CATEGORIES_FOR_SUBORDER = gql`
  query ($foodSupplierId: Int!) {
    food_supplier_categories(
      where: { deleted_at: { _is_null: true }, food_supplier_menu: { food_supplier_id: { _eq: $foodSupplierId } } }
    ) {
      ...CoreCategoryFields
      food_supplier_menu_items {
        id
      }
    }
  }
  ${CORE_CATEGORY_FIELDS}
`;
