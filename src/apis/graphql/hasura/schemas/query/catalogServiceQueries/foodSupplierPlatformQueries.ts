import { gql } from '@apollo/client';

export const GET_FOOD_SUPPLIER_PLATFORMS = gql`
  query ($foodSupplierId: Int!) {
    food_supplier_platforms(where: { deleted_at: { _is_null: true }, food_supplier_id: { _eq: $foodSupplierId } }) {
      platform {
        id
        name
        auto_accept_only
        manual_accept_only
        image {
          url
        }
      }
    }
  }
`;
