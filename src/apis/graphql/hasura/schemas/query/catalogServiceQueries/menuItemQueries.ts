import { gql } from '@apollo/client';

export const GET_MENU_ITEMS = gql`
  query ($foodSupplierId: Int!) {
    food_supplier_menu_items(
      order_by: { id: asc }
      where: {
        deleted_at: { _is_null: true }
        hidden: { _eq: false }
        food_supplier_category: { food_supplier_menu: { food_supplier_id: { _eq: $foodSupplierId } } }
      }
    ) {
      id
      available
      unavailable_until
      active_hours
      name
      name_cn
      food_supplier_category {
        name_cn
        name
      }
      food_supplier_menu_item_customisations {
        food_supplier_customisation {
          id
          name
          name_cn
          food_supplier_customisation_options {
            id
            name
            name_cn
            available
          }
        }
      }
    }
  }
`;
