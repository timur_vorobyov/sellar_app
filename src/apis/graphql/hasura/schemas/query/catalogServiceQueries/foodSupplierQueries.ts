import { gql } from '@apollo/client';

export const GET_FOOD_SUPPLLIER = gql`
  query ($foodSupplierId: Int) {
    food_suppliers(where: { deleted_at: { _is_null: true }, id: { _eq: $foodSupplierId } }) {
      id
      name
      grace_period
      location_id
      stall_number
      location {
        address
        available
        coordinates
        disabled
        id
        is_outlet
        name
      }
      food_supplier_devices {
        serial_number
        name
      }
      food_supplier_platforms {
        auto_accept
      }
    }
  }
`;

export const MAP_SERIAL_NUMBER_TO_FOOD_SUPPLIER_ID = gql`
  query ($serialNumber: String) {
    food_supplier_devices(where: { serial_number: { _eq: $serialNumber } }) {
      food_supplier_id
      name
    }
  }
`;

export const GET_FOOD_SUPPLLIER_FOR_ZREPORT = gql`
  query ($foodSupplierId: Int) {
    food_suppliers(where: { deleted_at: { _is_null: true }, id: { _eq: $foodSupplierId } }) {
      stall_number
    }
  }
`;
