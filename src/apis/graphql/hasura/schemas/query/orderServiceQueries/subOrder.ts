import { gql } from '@apollo/client';

export const GET_SUB_ORDERS_HISTORY = gql`
  query (
    $foodSupplierId: Int
    $searchTerm: String
    $dateFrom: timestamp
    $dateTo: timestamp
    $platformsIds: [String!]
    $ordersStatuses: [String!]
  ) {
    sub_orders(
      order_by: { created_at: desc }
      where: {
        sub_order_food_supplier: { food_supplier_id: { _eq: $foodSupplierId } }
        order: { platform: { _in: $platformsIds }, short_reference: { _regex: $searchTerm } }
        state: { _in: $ordersStatuses }
        _or: [
          { order: { order_refunds: { refunded_at: { _gte: $dateFrom, _lte: $dateTo } } } }
          { created_at: { _gte: $dateFrom, _lte: $dateTo } }
        ]
      }
    ) {
      id
      cancellation_receipt_printed
      fees
      pickup_at
      created_at
      completed_at
      collection_at
      cancelled_at
      ready_for_collection_at
      state
      net_sales
      origin_net_sales
      gross_sales
      discounts
      total
      subtotal
      refund_state
      order {
        id
        cutlery
        fulfillment_method
        platform
        discounts
        short_reference
        order_payments {
          method
        }
        order_refunds {
          id
          method
          order_id
          total_refunded
          state
          refunded_at
          refund_id
          payment_id
          group_id
          created_at
        }
      }
      sub_order_items {
        name
        name_cn
        quantity
        total
        id
        special_instructions
        subtotal
        discounted_price
        refunded_quantity
        price
        discounts
        sub_order_item_customisations {
          name
          name_cn
          id
          option_id
          quantity
          price
          discounted_price
          refunded_quantity
          actual_quantity
        }
        item_fees {
          amount
          returned_amount
          type
        }
      }
      sub_order_food_supplier {
        brand_id
        brand_name
        food_supplier_id
        name
      }
    }
  }
`;
