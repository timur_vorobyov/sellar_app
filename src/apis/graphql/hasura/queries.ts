import { ApolloClient, createHttpLink, DefaultOptions, InMemoryCache } from '@apollo/client';
import { SubOrderState, Platform } from 'types/models/subOrder';
import { GET_SUB_ORDERS_HISTORY } from 'apis/graphql/hasura/schemas/query/orderServiceQueries/subOrder';
import {
  GET_FOOD_SUPPLLIER,
  GET_FOOD_SUPPLLIER_FOR_ZREPORT,
  MAP_SERIAL_NUMBER_TO_FOOD_SUPPLIER_ID,
} from 'apis/graphql/hasura/schemas/query/catalogServiceQueries/foodSupplierQueries';
import {
  formattedFoodSupplierPlatformData,
  formattedSubOrdersHistory,
  formatFoodSupplierForZReport,
  formattedCategoryData,
  formattedMenuItemData,
  formattedCustomisationsData,
  formattedCustomisationOptionData,
  formattedCategoryDataForSubOrder,
} from './utils';
import { FoodSupplierDbType, SubOrderHistoryDbType } from './types';
import config from 'config';
import { GET_FOOD_SUPPLIER_PLATFORMS } from 'apis/graphql/hasura/schemas/query/catalogServiceQueries/foodSupplierPlatformQueries';
import { SubOrdersHistoryFilter } from 'types/pages';
import { FoodSupplierForZReport } from 'types/models/foodSupplier';
import axiosApiInstance from 'apis/rest/auth/axiosInstance';
import { convertToSingaporeTime } from 'utils/datetime';
import { GET_CATEGORIES, GET_CATEGORIES_FOR_SUBORDER } from './schemas/query/catalogServiceQueries/categoryQueries';
import { GET_MENU_ITEMS } from './schemas/query/catalogServiceQueries/menuItemQueries';

const orderServiceLink = createHttpLink({
  uri: config.orderServiceHasuraUri,
  headers: {
    'x-hasura-admin-secret': config.orderServiceHasuraApiKey,
  },
});

const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
  },
  query: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all',
  },
};

const orderServiceClient = new ApolloClient({
  cache: new InMemoryCache(),
  link: orderServiceLink,
  defaultOptions: defaultOptions,
});

const catalogServiceLink = createHttpLink({
  uri: config.catalogServiceHasuraUri,
  headers: {
    'x-hasura-admin-secret': config.catalogServiceHasuraApiKey,
  },
});

const catalogServiceClient = new ApolloClient({
  cache: new InMemoryCache(),
  link: catalogServiceLink,
  defaultOptions: defaultOptions,
});

export const getFoodSupplier = async (foodSupplierId: string): Promise<FoodSupplierDbType> => {
  const catalogServiceResponse = await catalogServiceClient.query({
    query: GET_FOOD_SUPPLLIER,
    variables: { foodSupplierId },
  });

  return catalogServiceResponse.data.food_suppliers['0'];
};

export const getFoodSupplierForZReport = async (foodSupplierId: string): Promise<FoodSupplierForZReport> => {
  const catalogServiceResponse = await catalogServiceClient.query({
    query: GET_FOOD_SUPPLLIER_FOR_ZREPORT,
    variables: { foodSupplierId },
  });

  return formatFoodSupplierForZReport(catalogServiceResponse.data.food_suppliers['0']);
};

export const getSubOrdersHistory = async (foodSupplierId: string, args: [SubOrdersHistoryFilter]) => {
  const filters = args[0];
  const date = new Date(filters.date.toDate());
  const dateFrom = `${convertToSingaporeTime(date, 'YYYY-MM-DD')}T00:00:00`;
  date.setDate(date.getDate() + 1);
  const dateTo = `${convertToSingaporeTime(date, 'YYYY-MM-DD')}T00:00:00`;

  //temporarly while statuses are saving on frontend side I transform orderStatusesIds to
  //orderStatuses as on the backend side we have only their names

  const modifiedSubOrdersStatuses = filters.subOrderStatuses.map((subOrderStatus) => {
    switch (subOrderStatus) {
      case 1: {
        return SubOrderState.ReadyForCollection;
      }
      case 2: {
        return SubOrderState.Rejected;
      }
      case 3: {
        return SubOrderState.Cancelled;
      }
      case 4: {
        return SubOrderState.Collected;
      }
      default: {
        return '';
      }
    }
  });

  const modifiedPlatformsIds = filters.platformsIds.map((platformId) => {
    switch (platformId) {
      case 1: {
        return Platform.Fairprice;
      }
      case 2: {
        return Platform.Deliveroo;
      }
      default: {
        return '';
      }
    }
  });

  const subOrdersHistory = await orderServiceClient.query({
    query: GET_SUB_ORDERS_HISTORY,
    variables: {
      foodSupplierId,
      dateTo: dateTo,
      dateFrom: dateFrom,
      platformsIds: modifiedPlatformsIds,
      ordersStatuses: modifiedSubOrdersStatuses,
      searchTerm: filters.searchTerm,
    },
  });

  const subOrdersHistoryIds = subOrdersHistory.data.sub_orders.map(
    (subOrderHistory: SubOrderHistoryDbType) => subOrderHistory.order.id,
  );
  const ordersHistoryIdsParams = subOrdersHistoryIds.join(',');
  let customers = [];

  if (ordersHistoryIdsParams) {
    const url = `${process.env.REACT_APP_ORDER_SERVICE_API_URI}/customers?orderId=${ordersHistoryIdsParams}`;
    const response = await axiosApiInstance.get(url);
    customers = response.data.data.items;
  }
  const foodSupplier = await getFoodSupplier(foodSupplierId);

  return formattedSubOrdersHistory(subOrdersHistory.data.sub_orders, customers, foodSupplier);
};

export const getFoodSupplierPlatforms = async (foodSupplierId: string) => {
  const foodSuplierPlatforms = await catalogServiceClient.query({
    query: GET_FOOD_SUPPLIER_PLATFORMS,
    variables: {
      foodSupplierId,
    },
  });

  return formattedFoodSupplierPlatformData(foodSuplierPlatforms.data.food_supplier_platforms);
};

export const mapSerialNumberToFoodSupplierId = async (serialNumber: string): Promise<string> => {
  const catalogServiceResponse = await catalogServiceClient.query({
    query: MAP_SERIAL_NUMBER_TO_FOOD_SUPPLIER_ID,
    variables: { serialNumber },
  });

  return catalogServiceResponse.data?.food_supplier_devices[0]?.food_supplier_id;
};

export const getCategoriesListForSubOrder = async (foodSupplierId: number) => {
  const catalogServiceResponse = await catalogServiceClient.query({
    query: GET_CATEGORIES_FOR_SUBORDER,
    variables: { foodSupplierId },
  });

  return formattedCategoryDataForSubOrder(catalogServiceResponse.data.food_supplier_categories);
};

export const getCategoriesList = async (foodSupplierId: number) => {
  const catalogServiceResponse = await catalogServiceClient.query({
    query: GET_CATEGORIES,
    variables: { foodSupplierId },
  });

  return formattedCategoryData(catalogServiceResponse.data.food_supplier_categories);
};

export const getMenuDataList = async (foodSupplierId: number) => {
  const catalogServiceResponse = await catalogServiceClient.query({
    query: GET_MENU_ITEMS,
    variables: { foodSupplierId },
  });

  const menuItemData = catalogServiceResponse.data.food_supplier_menu_items;

  return {
    menuItemsResponse: formattedMenuItemData(menuItemData),
    customisationsResponse: formattedCustomisationsData(menuItemData),
    customisationOptionsResponse: formattedCustomisationOptionData(menuItemData),
  };
};
