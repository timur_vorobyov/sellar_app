import { CustomerData, SubOrder, SubOrderHistory, Platform } from 'types/models/subOrder';
import { PaymentMethodType, PaymentState } from 'types/models/payment';
import {
  SubOrderHistoryProduct,
  SubOrderHistoryProductCustomisation,
  ProductCustomisation,
  SubOrderProduct,
  Product,
} from 'types/models/product';
import {
  CustomisationOptionDbType,
  CategoryDbType,
  FoodSupplierPlatformDbType,
  FoodSupplierDbType,
  MenuItemDbType,
  SubOrderDbType,
  ProductDbType,
  SubOrderHistoryDbType,
  ProductCustomisationDbType,
  CustomisationsDbType,
  FoodSupplierZReportDbType,
  SubOrderHistoryProductDbType,
  OrderHistoryProductCustomisationDbType,
  ProductFeeDbType,
} from './types';
import logoDeliveroo from 'components/assets/images/SubOrderPage/deliveroo-logo.png';
import logoFairprice from 'components/assets/images/SubOrderPage/fairprice-logo.png';
import { MenuCategory } from 'types/models/menuCaregory';
import { Customisation, CustomisationOption, MenuItem } from 'types/models/menu';
import { DEFAULT_ORDER_ACCEPT_GRACE_TIME } from 'constants/subOrder';
import { toDate } from 'utils/datetime';
import { FoodSupplierPlatform } from 'types/models/stall';
import { сonvertFromCentsToDollars } from 'utils/formater';
import { ProductFeeType } from 'constants/product';

export const getOrderLogo = (platform: Platform): string => {
  switch (platform) {
    case Platform.Deliveroo:
      return logoDeliveroo;
    case Platform.Fairprice:
    default:
      return logoFairprice;
  }
};

export const formattedCustomisations = (customisations?: ProductCustomisationDbType[]): ProductCustomisation[] => {
  return (customisations || []).map(
    (cust): ProductCustomisation => ({
      id: cust.id,
      optionId: cust.option_id,
      name: cust.name,
      nameCn: cust.name_cn,
      quantity: cust.quantity || 0,
      actualQuantity: cust.actual_quantity,
      price: сonvertFromCentsToDollars(cust.price),
    }),
  );
};

export const formattedSubOrderHistoryCustomisations = (
  customisations?: OrderHistoryProductCustomisationDbType[],
): SubOrderHistoryProductCustomisation[] => {
  return (customisations || []).map(
    (cust): SubOrderHistoryProductCustomisation => ({
      id: cust.id,
      optionId: cust.option_id,
      name: cust.name,
      nameCn: cust.name_cn,
      quantity: cust.quantity || 0,
      refundedQuantity: cust.refunded_quantity,
      actualQuantity: cust.actual_quantity,
      price: сonvertFromCentsToDollars(cust.price),
    }),
  );
};

const getFullProductPrice = (product: ProductDbType) => {
  const price = product.price * product.quantity - product.discounts;
  return сonvertFromCentsToDollars(
    product.sub_order_item_customisations.reduce(
      (acc: number, customisation: ProductCustomisationDbType) =>
        (acc += customisation.actual_quantity * customisation.price),
      price,
    ),
  );
};

export const calculateTotalProductQuantity = (products: ProductDbType[]) => {
  return products
    .map((product) => product.quantity)
    .reduce((previousValue, currentValue) => previousValue + currentValue, 0);
};

export const formattedSubOrder = (
  subOrder: SubOrderDbType,
  foodSupplier: FoodSupplierDbType,
  customersData: CustomerData[],
  categories: MenuCategory[],
): SubOrder => {
  const customerData: CustomerData = customersData.find((customer) => customer.orderId === subOrder.order.id) || {
    orderId: 0,
    customer: { name: '', phone: '' },
  };

  const products = subOrder.sub_order_items.map((product: ProductDbType): Product => {
    return {
      id: product.id,
      menuItemId: product.menu_item_id,
      quantity: product.quantity,
      price: сonvertFromCentsToDollars(product.subtotal),
      name: product.name,
      nameCn: product.name_cn,
      customerComments: product.special_instructions,
      containerFee: getContainerFee(product),
      customisations: formattedCustomisations(product.sub_order_item_customisations),
      fullProductSum: getFullProductPrice(product),
    };
  });

  const productsGroupedByCategories: SubOrderProduct[] = [];

  products.forEach((product: Product): void => {
    const formattedProduct = product;
    if (productsGroupedByCategories[product.menuItemId]) {
      productsGroupedByCategories[product.menuItemId].list.push(formattedProduct);
    } else {
      productsGroupedByCategories[product.menuItemId] = {
        list: [formattedProduct],
        category: categories[product.menuItemId],
      };
    }
  });

  return {
    id: subOrder.id,
    isCancellationReceiptPrinted: subOrder.cancellation_receipt_printed,
    fees: subOrder.fees,
    foodSupplierId: subOrder.sub_order_food_supplier.food_supplier_id,
    logo: getOrderLogo(subOrder.order.platform),
    orderId: subOrder.order.short_reference || '##MISSING##',
    isCutleryNeeded: subOrder.order.cutlery,
    type: subOrder.order.fulfillment_method,
    state: subOrder.state,
    order: {
      id: subOrder.order.id,
      discounts: сonvertFromCentsToDollars(subOrder.order.discounts),
    },
    createdAt: toDate(subOrder.created_at),
    graceTime: getGraceTime(foodSupplier),
    subTotalPrice: сonvertFromCentsToDollars(subOrder.subtotal),
    totalPrice: сonvertFromCentsToDollars(subOrder.total),
    netSales: сonvertFromCentsToDollars(subOrder.net_sales),
    productsAmount: calculateTotalProductQuantity(subOrder.sub_order_items),
    platform: subOrder.order.platform,
    isAutoAccepted: false,
    isReceiptPrinted: subOrder.receipt_printed,
    totalContainerFeesAmount: getTotalContainerFeesAmount(subOrder.sub_order_items),
    productsGroupedByCategories: productsGroupedByCategories,
    products: products,
    pickUpTime: subOrder.pickup_at ? toDate(subOrder.pickup_at) : null,
    customer: {
      id: 1,
      name: customerData.customer.name,
      phoneNumber: customerData.customer.phone,
    },
    deliveryPrice: 0,
    driver: {
      id: 1,
      name: "DOESN'T_HAVE_FIELD",
      phoneNumber: "DOESN'T_HAVE_FIELD",
      note: "DOESN'T_HAVE_FIELD",
    },
    payments: (subOrder.order.order_payments || []).map((payment) => ({
      id: payment.id,
      method: payment.method as PaymentMethodType,
      paymentId: payment.payment_id,
      state: payment.state as PaymentState,
      total: payment.total,
    })),
    location: {
      address: foodSupplier?.location?.address || '',
      available: foodSupplier?.location?.available || true,
      coordinates: foodSupplier?.location?.coordinates || '',
      disabled: foodSupplier?.location?.disabled || false,
      id: foodSupplier?.location?.id || -1,
      isOutlet: foodSupplier?.location?.is_outlet || false,
      name: foodSupplier?.location?.name || '',
    },
  };
};

export const formattedSubOrdersList = (
  subOrdersList: SubOrderDbType[],
  foodSupplier: FoodSupplierDbType,
  customersData: CustomerData[],
  categories: MenuCategory[],
): SubOrder[] => {
  return subOrdersList.map((subOrder: SubOrderDbType): SubOrder => {
    return formattedSubOrder(subOrder, foodSupplier, customersData, categories);
  });
};

const getGraceTime = (foodSupplier: FoodSupplierDbType) => {
  if (foodSupplier) {
    return foodSupplier.grace_period ? foodSupplier.grace_period * 60 : DEFAULT_ORDER_ACCEPT_GRACE_TIME;
  }

  return DEFAULT_ORDER_ACCEPT_GRACE_TIME;
};

const getRefundedPrice = (products: SubOrderHistoryProductDbType[]) => {
  return сonvertFromCentsToDollars(
    products.reduce((acc: number, product: SubOrderHistoryProductDbType) => {
      return (acc +=
        product.discounted_price * product.refunded_quantity +
        product.sub_order_item_customisations.reduce(
          (custAcc: number, customisation: OrderHistoryProductCustomisationDbType) => {
            return (custAcc += customisation.refunded_quantity * customisation.discounted_price);
          },
          0,
        ));
    }, 0),
  );
};

const getTotalReturnedContainerFeesAmountWithDiscount = (products: SubOrderHistoryProductDbType[]): number =>
  products.reduce(
    (acc: number, product: SubOrderHistoryProductDbType) => (acc += getReturnedContainerFeeWithDiscount(product)),
    0,
  );

const getReturnedContainerFeeWithDiscount = (product: ProductDbType | SubOrderHistoryProductDbType): number => {
  const fee =
    product.item_fees.find((itemFee: ProductFeeDbType) => itemFee.type === ProductFeeType.ContainerFee)
      ?.returned_amount || 0;

  return сonvertFromCentsToDollars(fee);
};

const getTotalContainerFeesAmount = (products: ProductDbType[]) =>
  products.reduce((acc: number, product: ProductDbType) => (acc += getContainerFee(product)), 0);

const getContainerFee = (product: ProductDbType | SubOrderHistoryProductDbType): number => {
  const fee =
    product.item_fees.find((itemFee: ProductFeeDbType) => itemFee.type === ProductFeeType.ContainerFee)?.amount || 0;

  return сonvertFromCentsToDollars(fee);
};

const getNumberOfProducts = (products: SubOrderHistoryProductDbType[]): number =>
  products.reduce((acc: number, product: SubOrderHistoryProductDbType) => (acc = acc + product.quantity), 0);

export const formattedMenuItemData = (menuItemsList: MenuItemDbType[]) => {
  return menuItemsList.map((menuItem: MenuItemDbType): MenuItem => {
    return {
      id: menuItem.id,
      category: {
        name: menuItem.food_supplier_category.name,
        nameCn: menuItem.food_supplier_category.name_cn,
      },
      name: menuItem.name,
      nameCn: menuItem.name_cn,
      available: menuItem.available ? 1 : 0,
      unavailableUntil: menuItem.unavailable_until ? toDate(menuItem.unavailable_until) : null,
      activeHours: menuItem.active_hours,
    };
  });
};

export const formattedCustomisationsData = (menuItemsList: MenuItemDbType[]) => {
  const formattedCustomisations = [] as Customisation[];
  menuItemsList.forEach((menuItem: MenuItemDbType) => {
    menuItem.food_supplier_menu_item_customisations.forEach((customisation: CustomisationsDbType) => {
      const customisationIsExist = formattedCustomisations.find(
        (formattedCustomisation) => formattedCustomisation.id === customisation.food_supplier_customisation.id,
      );
      if (!customisationIsExist) {
        formattedCustomisations.push({
          id: customisation.food_supplier_customisation.id,
          name: customisation.food_supplier_customisation.name,
          nameCn: customisation.food_supplier_customisation.name_cn,
        });
      }
    });
  });

  return formattedCustomisations;
};

export const formattedCustomisationOptionData = (menuItemsList: MenuItemDbType[]) => {
  const formattedCustomisationOptions = [] as CustomisationOption[];
  menuItemsList.forEach((menuItem: MenuItemDbType) => {
    menuItem.food_supplier_menu_item_customisations.forEach((customisation: CustomisationsDbType) => {
      customisation.food_supplier_customisation.food_supplier_customisation_options.forEach(
        (customisationOption: CustomisationOptionDbType) => {
          const customisationOptionIsExist = formattedCustomisationOptions.find(
            (formattedCustomisationOption) => formattedCustomisationOption.id === customisationOption.id,
          );
          if (!customisationOptionIsExist) {
            formattedCustomisationOptions.push({
              id: customisationOption.id,
              name: customisationOption.name,
              nameCn: customisationOption.name_cn,
              available: customisationOption.available ? 1 : 0,
              customisationId: customisation.food_supplier_customisation.id,
              customisation: {
                name: customisation.food_supplier_customisation.name,
                nameCn: customisation.food_supplier_customisation.name_cn,
              },
            });
          }
        },
      );
    });
  });

  return formattedCustomisationOptions;
};

export const formattedCategoryDataForSubOrder = (categoriesList: CategoryDbType[]) => {
  const categories: MenuCategory[] = [];
  categoriesList.forEach((category: CategoryDbType) => {
    category.food_supplier_menu_items.forEach((food_supplier_menu_item) => {
      categories[food_supplier_menu_item.id] = {
        id: category.id,
        name: category.name,
        nameCn: category.name_cn,
      };
    });
  });

  return categories;
};

export const formattedCategoryData = (categoriesList: CategoryDbType[]) => {
  return categoriesList.map((category: CategoryDbType): MenuCategory => {
    return {
      id: category.id,
      name: category.name,
      nameCn: category.name_cn,
    };
  });
};

export const formattedFoodSupplierPlatformData = (foodSupplierPlatforms: FoodSupplierPlatformDbType[]) =>
  foodSupplierPlatforms.map((foodSupplierPlatform: FoodSupplierPlatformDbType): FoodSupplierPlatform => {
    const result = {
      id: foodSupplierPlatform.platform.id,
      name: foodSupplierPlatform.platform.name,
      logo: foodSupplierPlatform.platform.image.url,
      status: foodSupplierPlatform.status,
      autoAccept: foodSupplierPlatform.auto_accept,
      autoPrint: foodSupplierPlatform.auto_print,
      prepTime: foodSupplierPlatform.preparation_time,
      isLongRingtone: !!foodSupplierPlatform.long_ringtone,
      autoAcceptOnly: foodSupplierPlatform.platform.auto_accept_only,
      manualAcceptOnly: foodSupplierPlatform.platform.manual_accept_only,
      closeFor: foodSupplierPlatform.closed_for,
      closeAt: foodSupplierPlatform.closed_at,
    };

    if (foodSupplierPlatform.status === 'CLOSED' && foodSupplierPlatform.closed_for) {
      result.status = `CLOSED_FOR_${foodSupplierPlatform.closed_for}`;
    }

    return result;
  });

const formattedSubOrderHistory = (
  subOrderHistory: SubOrderHistoryDbType,
  customersData: CustomerData[],
  foodSupplier: FoodSupplierDbType,
): SubOrderHistory => {
  const customerData: CustomerData = customersData.find(
    (customer) => customer.orderId === subOrderHistory.order.id,
  ) || {
    orderId: 0,
    customer: { name: '', phone: '' },
  };

  return {
    id: subOrderHistory.id,
    isCancellationReceiptPrinted: subOrderHistory.cancellation_receipt_printed,
    logo: getOrderLogo(subOrderHistory.order.platform),
    orderId: subOrderHistory.order.short_reference || '##MISSING##',
    isCutleryNeeded: subOrderHistory.order.cutlery,
    type: subOrderHistory.order.fulfillment_method,
    state: subOrderHistory.state,
    subTotalPrice: сonvertFromCentsToDollars(subOrderHistory.subtotal),
    totalPrice: сonvertFromCentsToDollars(subOrderHistory.total),
    platform: subOrderHistory.order.platform,
    netSales: сonvertFromCentsToDollars(subOrderHistory.net_sales),
    originNetSales: сonvertFromCentsToDollars(subOrderHistory.origin_net_sales),
    grossSales: сonvertFromCentsToDollars(subOrderHistory.gross_sales),
    refundedPrice: getRefundedPrice(subOrderHistory.sub_order_items),
    totalReturnedContainerFeesAmountWithDiscount: getTotalReturnedContainerFeesAmountWithDiscount(
      subOrderHistory.sub_order_items,
    ),
    totalContainerFeesAmount: getTotalContainerFeesAmount(subOrderHistory.sub_order_items),
    refundState: subOrderHistory.refund_state,
    numberOfProducts: getNumberOfProducts(subOrderHistory.sub_order_items),
    products: subOrderHistory.sub_order_items.map((product: SubOrderHistoryProductDbType): SubOrderHistoryProduct => {
      return {
        id: product.id,
        menuItemId: product.menu_item_id,
        quantity: product.quantity,
        price: сonvertFromCentsToDollars(product.price),
        name: product.name,
        nameCn: product.name_cn,
        customerComments: product.special_instructions,
        customisations: formattedSubOrderHistoryCustomisations(product.sub_order_item_customisations),
        discountedPrice: сonvertFromCentsToDollars(product.discounted_price),
        containerFee: getContainerFee(product),
        returnedContainerFeeWithDiscount: getReturnedContainerFeeWithDiscount(product),
        refundedQuantity: product.refunded_quantity,
        fullProductSum: getFullProductPrice(product),
      };
    }),
    pickUpTime: subOrderHistory.pickup_at ? toDate(subOrderHistory.pickup_at) : null,
    order: {
      id: subOrderHistory.order.id,
      discounts: сonvertFromCentsToDollars(subOrderHistory.order.discounts),
      refunds: subOrderHistory.order.order_refunds.map((subOrderHistoryRefunded) => {
        return {
          totalRefunded: subOrderHistoryRefunded.total_refunded,
          state: subOrderHistoryRefunded.state,
          id: subOrderHistoryRefunded.id,
        };
      }),
    },
    completedTime: subOrderHistory.completed_at ? toDate(subOrderHistory.completed_at) : null,
    collectionTime: subOrderHistory.collection_at ? toDate(subOrderHistory.collection_at) : null,
    cancelledTime: subOrderHistory.cancelled_at ? toDate(subOrderHistory.cancelled_at) : null,
    readyForCollectionTime: subOrderHistory.ready_for_collection_at
      ? toDate(subOrderHistory.ready_for_collection_at)
      : null,
    paymentsMethod: subOrderHistory.order.order_payments[0].method,
    deliveryPrice: 0,
    customer: {
      id: 1,
      name: customerData.customer.name,
      phoneNumber: customerData.customer.phone,
    },
    driver: {
      id: 1,
      name: "DOESN'T_HAVE_FIELD",
      phoneNumber: "DOESN'T_HAVE_FIELD",
      note: "DOESN'T_HAVE_FIELD",
    },
    location: {
      address: foodSupplier?.location?.address || '',
      available: foodSupplier?.location?.available || true,
      coordinates: foodSupplier?.location?.coordinates || '',
      disabled: foodSupplier?.location?.disabled || false,
      id: foodSupplier?.location?.id || -1,
      isOutlet: foodSupplier?.location?.is_outlet || false,
      name: foodSupplier?.location?.name || '',
    },
    createdAt: toDate(subOrderHistory.created_at),
  };
};

export const formattedSubOrdersHistory = (
  subOrdersHistory: SubOrderHistoryDbType[],
  customersData: CustomerData[],
  foodSupplier: FoodSupplierDbType,
): SubOrderHistory[] => {
  return subOrdersHistory.map((subOrderHistory: SubOrderHistoryDbType): SubOrderHistory => {
    return formattedSubOrderHistory(subOrderHistory, customersData, foodSupplier);
  });
};

export const formatFoodSupplierForZReport = (foodSupplier: FoodSupplierZReportDbType) => {
  return {
    stallNumber: foodSupplier.stall_number,
  };
};
