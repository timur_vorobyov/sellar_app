import { Platform } from 'types/models/subOrder';
import { DateString, EmptyObject } from 'types/generics';
import { SubOrderType } from 'constants/subOrder';
import { PaymentMethod } from 'constants/payment';
import { MenuItemActiveHours } from 'types/models/menu';

export interface SubOrderDbType {
  id: number;
  cancellation_receipt_printed: boolean;
  fees: number;
  created_at: DateString;
  pickup_at: DateString | null;
  state: string;
  receipt_printed: boolean;
  total: number;
  subtotal: number;
  net_sales: number;
  order: {
    id: number;
    cutlery: boolean;
    fulfillment_method: SubOrderType;
    platform: Platform;
    short_reference: string;
    discounts: number;
    order_payments: {
      id: number;
      method: PaymentMethod;
      payment_id: string;
      state: string;
      total: number;
    }[];
  };
  sub_order_items: ProductDbType[];
  sub_order_food_supplier: {
    brand_id: number;
    brand_name: string;
    food_supplier_id: number;
    name: string;
  };
}

export interface ProductCustomisationDbType {
  name: string;
  name_cn: string;
  id: number;
  option_id: number;
  price: number;
  quantity: number;
  actual_quantity: number;
}

export interface OrderHistoryProductCustomisationDbType extends ProductCustomisationDbType {
  refunded_quantity: number;
  discounted_price: number;
}
export interface ProductDbType {
  name: string;
  name_cn: string;
  quantity: number;
  total: number;
  subtotal: number;
  id: number;
  menu_item_id: number;
  special_instructions: string;
  sub_order_item_customisations: ProductCustomisationDbType[];
  item_fees: ProductFeeDbType[];
  price: number;
  discounts: number;
}

export interface ProductFeeDbType {
  amount: number;
  returned_amount: number;
  type: string;
}

export interface SubOrderHistoryProductDbType extends ProductDbType {
  discounted_price: number;
  refunded_quantity: number;
  sub_order_item_customisations: OrderHistoryProductCustomisationDbType[];
}

export interface FoodSupplierDeviceDbType {
  name: string;
  serial_number: string;
}

export interface FoodSupplierPlatformsDbType {
  auto_accept: boolean;
}

export interface FoodSupplierDbType {
  id: number;
  name: string;
  grace_period: number | null;
  location_id: number;
  stall_number: string;
  location: {
    address: string;
    available: boolean;
    coordinates: string;
    disabled: boolean;
    id: number;
    is_outlet: boolean;
    name: string;
  };
  food_supplier_devices: FoodSupplierDeviceDbType[];
  food_supplier_platforms: FoodSupplierPlatformsDbType[];
}

export interface FoodSupplierZReportDbType {
  stall_number: string;
}

export type MenuItemDbType = {
  id: number;
  food_supplier_category: {
    name: string;
    name_cn: string;
  };
  name: string;
  name_cn: string;
  available: boolean;
  unavailable_until: DateString | null;
  active_hours: EmptyObject | MenuItemActiveHours;
  food_supplier_menu_item_customisations: CustomisationsDbType[];
};

export type CustomisationOptionDbType = {
  id: number;
  name: string;
  name_cn: string;
  available: boolean;
};

export type CustomisationsDbType = {
  food_supplier_customisation: {
    id: number;
    name: string;
    name_cn: string;
    food_supplier_customisation_options: CustomisationOptionDbType[];
  };
};

export type CategoryDbType = {
  id: number;
  name: string;
  name_cn: string;
  food_supplier_menu_items: {
    id: number;
  }[];
};

export type FoodSupplierPlatformDbType = {
  preparation_time: DateString | null;
  long_ringtone: boolean;
  platform: {
    id: string;
    name: Platform;
    auto_accept_only: boolean;
    manual_accept_only: boolean;
    image: {
      url: string;
    };
  };
  status: string;
  auto_accept: boolean;
  auto_print: boolean;
  closed_for: string;
  closed_at: string;
};

export interface SubOrderRefundDbType {
  total_refunded: number;
  state: string;
  id: number;
}

export interface SubOrderHistoryDbType extends SubOrderDbType {
  completed_at: string | null;
  collection_at: string | null;
  cancelled_at: string | null;
  ready_for_collection_at: string | null;
  net_sales: number;
  origin_net_sales: number;
  gross_sales: number;
  subtotal: number;
  refund_state: string;
  order: {
    id: number;
    cutlery: boolean;
    fulfillment_method: SubOrderType;
    platform: Platform;
    short_reference: string;
    discounts: number;
    order_payments: {
      id: number;
      method: PaymentMethod;
      payment_id: string;
      state: string;
      total: number;
    }[];
    order_refunds: SubOrderRefundDbType[];
  };
  sub_order_items: SubOrderHistoryProductDbType[];
}
