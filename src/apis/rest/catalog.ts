import axios, { AxiosResponse } from 'axios';
import config from 'config';
import { Anything } from 'types/generics';
import { Customisation, CustomisationOption, Menu, MenuItem } from 'types/models/menu';
import { FoodSupplierPlatform } from 'types/models/stall';
import { ParseResponseOptions, parseAxiosResponse } from '../base';
import { filterObject, formattedFoddSupplierDevices, prepareStallUpdatePayload } from '../utils';

const catalogService = axios.create({
  baseURL: config.catalogServiceApiUri,
});

const parseAxiosCatalogResponse = (response: AxiosResponse, opts?: Partial<ParseResponseOptions>) => {
  return parseAxiosResponse(response, {
    serviceName: 'catalogService',
    requireBody: false,
    ...(opts || {}),
  });
};

export const setCatalogServiceAuthToken = (token: string | null): void => {
  if (token) {
    catalogService.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  } else {
    delete catalogService.defaults.headers.common['Authorization'];
  }
};

export const updateMenu = async (menu: Partial<Menu>): Promise<Anything> => {
  const response = await catalogService.patch(`/menus/${menu.id}`, menu);

  return parseAxiosCatalogResponse(response);
};

export const updateCustomization = async (customisation: Partial<Customisation>): Promise<Anything> => {
  const response = await catalogService.patch(`/customisations/${customisation.id}`, customisation);

  return parseAxiosCatalogResponse(response);
};

export const updateMenuItem = async (menuItem: Partial<MenuItem>): Promise<Anything> => {
  const knownKeys = ['available', 'id'];
  const payload = filterObject(menuItem, knownKeys);

  if (payload.available !== undefined) {
    payload.available = !!payload.available;
  }

  const response = await catalogService.patch(`/menu-items/${menuItem.id}`, payload);

  return parseAxiosCatalogResponse(response);
};

export const updateCustomisationOption = async (
  customisationOption: Partial<CustomisationOption>,
): Promise<Anything> => {
  const knownKeys = ['available', 'id'];
  const payload = filterObject(customisationOption, knownKeys);

  if (payload.available !== undefined) {
    payload.available = !!payload.available;
  }

  const response = await catalogService.patch(`/customisation-options/${customisationOption.id}`, payload);

  return parseAxiosCatalogResponse(response);
};

export const updateStallSettings = async (platform: Partial<FoodSupplierPlatform>, foodSupplierId: string) => {
  const payload = prepareStallUpdatePayload(platform);

  const response = await catalogService.patch(`/foodsuppliers-platforms/${foodSupplierId}/platforms/${platform.id} `, {
    fsPlatform: payload,
  });

  return parseAxiosCatalogResponse(response);
};

export const pingKpos = async (serialNumber: string) => {
  await catalogService.post(`foodsupplier-devices/${serialNumber}/heartbeat`);
};

export const getDeviceDataForZReport = async (serialNumber: string) => {
  const response = await catalogService.get(`foodsupplier-devices/${serialNumber}`);

  return formattedFoddSupplierDevices(response.data.data);
};
