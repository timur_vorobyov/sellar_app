import axios from 'axios';
import { getStorageAccessToken, setStorageAccessToken } from 'storage/adapters/accessToken';
import { setStorageAuth } from 'storage/adapters/auth';
import { getStorageRefreshToken, setStorageRefreshToken } from 'storage/adapters/refreshToken';
import { requestAuthToken } from './auth';

async function refreshAccessToken() {
  try {
    const refresh_token: string = getStorageRefreshToken() || '';
    const { data } = await requestAuthToken('', refresh_token);
    if (data?.access_token) {
      setStorageAuth(JSON.stringify(data));
      setStorageAccessToken(data.access_token);
      setStorageRefreshToken(data.refresh_token);
    }
    return data?.access_token || '';
  } catch (error) {
    console.log(error);
  }
}

const axiosApiInstance = axios.create();

axiosApiInstance.interceptors.request.use(
  async (config) => {
    const access_token: string = getStorageAccessToken() || '';
    config.headers = {
      Authorization: `Bearer ${access_token}`,
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  },
);

// Response interceptor for API calls
axiosApiInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  async function (error) {
    const originalRequest = error.config;
    if ([401, 403].includes(error.response.status) && !originalRequest._retry) {
      originalRequest._retry = true;
      const access_token = await refreshAccessToken();
      axiosApiInstance.defaults.headers.common.Authorization = 'Bearer ' + access_token;
      return axiosApiInstance(originalRequest);
    }
    return Promise.reject(error);
  },
);

export default axiosApiInstance;
