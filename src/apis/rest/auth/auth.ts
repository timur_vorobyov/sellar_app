import axios from 'axios';
import config from 'config';

export const getSerialNumber = async () => {
  const peripheralSerialNumberUrl = config.printServerUrl + '/device/serialnumber';
  return axios.get(peripheralSerialNumberUrl);
};

const authDomain = config.corsService + config.auth0Domain;
const clientId = config.auth0ClientId;

export const requestDeviceCodeFlow = async () => {
  return axios.post(
    authDomain + '/oauth/device/code',
    {
      client_id: clientId,
      scope: 'offline_access openid profile',
      audience: config.auth0Audience,
    },
    { headers: { 'Content-Type': 'application/json' } },
  );
};

export const requestAuthToken = async (device_code: string, refresh_token: string) => {
  if (refresh_token === '') {
    return axios.post(
      authDomain + '/oauth/token',
      {
        grant_type: 'urn:ietf:params:oauth:grant-type:device_code',
        device_code: device_code,
        client_id: clientId,
      },
      { headers: { 'Content-Type': 'application/json' } },
    );
  } else {
    return axios.post(
      authDomain + '/oauth/token',
      {
        grant_type: 'refresh_token',
        client_id: clientId,
        refresh_token: refresh_token,
      },
      { headers: { 'Content-Type': 'application/json' } },
    );
  }
};
