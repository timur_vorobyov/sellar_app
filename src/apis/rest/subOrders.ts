import { ProductRefund } from 'types/models/product';
import { CancelledSubOrder, SubOrder, SubOrderData, SubOrderHistory } from 'types/models/subOrder';
import axios, { AxiosResponse } from 'axios';
import { SubOrderRejectionReason } from 'constants/subOrder';
import { Anything } from 'types/generics';
import { parseAxiosResponse, ParseResponseOptions } from '../base';
import config from 'config';
import moment from 'moment-timezone';
import { formattedSubOrder } from './utils';

const orderService = axios.create({
  baseURL: config.orderServiceApiUri,
});

const parseAxiosOrderResponse = (response: AxiosResponse, opts?: Partial<ParseResponseOptions>) => {
  return parseAxiosResponse(response, {
    serviceName: 'orderService',
    requireBody: false,
    ...(opts || {}),
  });
};

export const setOrderServiceAuthToken = (token: string | null): void => {
  if (token) {
    orderService.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  } else {
    delete orderService.defaults.headers.common['Authorization'];
  }
};

export const rejectSubOrder = async (
  subOrder: SubOrder,
  reason: SubOrderRejectionReason = SubOrderRejectionReason.AutoRejected,
): Promise<Anything> => {
  const response = await orderService.post(`/suborders/${subOrder.id}/reject`, {
    rejectBy: 'MERCHANT',
    reason,
  });

  return parseAxiosOrderResponse(response);
};

export const acceptSubOrder = async (subOrder: SubOrder): Promise<Anything> => {
  const response = await orderService.post(`/suborders/${subOrder.id}/accept`);
  return parseAxiosOrderResponse(response);
};

export const updateReceiptFlag = async (subOrderId: number, isPrinted: boolean): Promise<Anything> => {
  const response = await orderService.post(`/suborders/${subOrderId}/mark-receipt-printed`, {
    printed: isPrinted,
  });
  return parseAxiosOrderResponse(response);
};

export const pickupSubOrder = async (subOrder: SubOrder): Promise<Anything> => {
  const response = await orderService.post(`/suborders/${subOrder.id}/ready`);
  return parseAxiosOrderResponse(response);
};

export const refundOrder = async (orderId: number, reason: string, products: [ProductRefund]): Promise<Anything> => {
  const response = await orderService.post(`/orders/${orderId}/refund`, {
    orderId,
    reason,
    subOrders: products,
    email: 'aaa@aa.ua',
    refundBy: 'MERCHANT',
  });

  return parseAxiosOrderResponse(response);
};
export const markCancelSubOrderReceiptPrinted = async (
  subOrder: CancelledSubOrder | SubOrderHistory,
  printed = true,
): Promise<Anything> => {
  const response = await orderService.post(`/suborders/${subOrder.id}/mark-cancellation-printed`, { printed });
  return parseAxiosOrderResponse(response);
};

export const getZReportData = async (foodSupplierId: number, args: [{ date: moment.Moment }]): Promise<Anything> => {
  const date = args[0].date.format('YYYY-MM-DD');
  const response = await axios.get(
    `${process.env.REACT_APP_ORDER_SERVICE_API_URI}/orders/z-receipt?businessDate=${date}&foodSupplierID=${foodSupplierId}`,
  );
  response.data.data.foodSupplierId = foodSupplierId;

  return parseAxiosOrderResponse(response.data);
};

export const getSubOrderById = async (id: number): Promise<SubOrderData> => {
  const response = await orderService.get(`/suborders/${id}`);
  return formattedSubOrder(response.data.data[0]);
};
