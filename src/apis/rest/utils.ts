import { SubOrderData } from 'types/models/subOrder';
import { SubOrderDbType } from './types';

export const formattedSubOrder = (subOrder: SubOrderDbType): SubOrderData => {
  return {
    appPaymentDiscount: subOrder.appPaymentDiscount,
  };
};
