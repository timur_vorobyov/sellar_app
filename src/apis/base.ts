import { AxiosResponse } from 'axios';
import { Anything } from 'types/generics';

export interface ParseResponseOptions {
  serviceName: string;
  requireBody: boolean;
}

const defaultParseResponseOptions: ParseResponseOptions = {
  serviceName: 'not set',
  requireBody: false,
};

export const parseAxiosResponse = (
  response: AxiosResponse,
  opts: ParseResponseOptions = defaultParseResponseOptions,
): Anything => {
  if (response.data || !opts.requireBody) {
    return response.data || null;
  }

  if (!response.data && opts.requireBody) {
    // Log error as body expected by didn't receive
    return null;
  }

  // Log error as body expected by didn't receive
  return null;
};
