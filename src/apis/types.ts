export interface DeviceDbType {
  createdAt: string;
  foodSupplierId: number;
  name: string;
  serialNumber: string;
  updatedAt: string;
}
