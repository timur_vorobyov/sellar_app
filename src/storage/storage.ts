const setItem = (name: string, value: string) => {
  localStorage.setItem(name, value);
};

const getItem = (name: string): string | null => localStorage.getItem(name);

export default { setItem, getItem };
