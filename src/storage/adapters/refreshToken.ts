import { LOCALSTORAGE } from 'constants/localStorage';
import storage from 'storage/storage';

export const setStorageRefreshToken = (value: string) => {
  storage.setItem(LOCALSTORAGE.REFRESH_TOKEN, value);
};

export const getStorageRefreshToken = (): string | null => storage.getItem(LOCALSTORAGE.REFRESH_TOKEN);
