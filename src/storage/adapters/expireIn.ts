import { LOCALSTORAGE } from 'constants/localStorage';
import storage from 'storage/storage';

export const setStorageExpireIn = (value: string) => {
  storage.setItem(LOCALSTORAGE.EXPIRES_IN, value);
};

export const getStorageExpireIn = (): string | null => storage.getItem(LOCALSTORAGE.EXPIRES_IN);
