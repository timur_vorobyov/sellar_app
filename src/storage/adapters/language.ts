import { LOCALSTORAGE } from 'constants/localStorage';
import storage from 'storage/storage';

export const setStorageLanguage = (value: string) => {
  storage.setItem(LOCALSTORAGE.LANGUAGE, value);
};

export const getStorageLanguage = (): string | null => storage.getItem(LOCALSTORAGE.LANGUAGE);
