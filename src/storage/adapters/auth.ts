import { LOCALSTORAGE } from 'constants/localStorage';
import storage from 'storage/storage';

export const setStorageAuth = (value: string) => {
  storage.setItem(LOCALSTORAGE.AUTH, value);
};

export const getStorageAuth = (): string | null => storage.getItem(LOCALSTORAGE.AUTH);
