import { LOCALSTORAGE } from 'constants/localStorage';
import storage from 'storage/storage';

export const setStorageAccessToken = (value: string) => {
  storage.setItem(LOCALSTORAGE.ACCESS_TOKEN, value);
};

export const getStorageAccessToken = (): string | null => storage.getItem(LOCALSTORAGE.ACCESS_TOKEN);
