import { LOCALSTORAGE } from 'constants/localStorage';
import storage from 'storage/storage';

export const setStorageSerialNumber = (value: string) => {
  storage.setItem(LOCALSTORAGE.SERIAL_NUMBER, value);
};

export const getStorageSerialNumber = (): string | null => storage.getItem(LOCALSTORAGE.SERIAL_NUMBER);
