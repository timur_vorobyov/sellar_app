# Seller app code guidelines

To keep app maintainable some code style rules must be applied permanently.

## Code

-   no unused variables or imports allowed
-   now linter warnings or errors allowed

## Non-lintable code rules

-   Name convention: by default we are using "common sense" to define names with a "typescript-way". Nevertheless some
    rules must be enforced to use:
    -   Interfaces must not use `I` prefix. As well as types must not use `T` prefix.
    -   All `type` and `interface` are defined in `PascalCase`.
    -   All module-level (but defined out of FC) variables that builds based on some contants must be in `camelCase`.
        For example check component `TooBusy`
    -   All constants (and only constants) in files under `constants` folder must be in CONSTANT_CASE
        -   But! All native TypeScript features like `enum` defined there must be in `PascalCase`
    -   All local variables are names **meaningfully** (i.e. not `result1, result2, ...`) and in `camelCase`
    -   All components names **must** match filename
    -   All component props must follow naming convention `ComponentName` + `Props`. I.e. for `IncomingOrderList`
        properties interface will be named like `IncomingOrderListPorps`.
    -   Type vs Interface. Unless you are absolutely sure that for specific case must be used `type` - use `interface`.
        -   Exception allowed when you defined type alias like `interface Order { ... }; type IncomingOrder = A`.
    -   Postfixes for arrays and lists. Shot: **NO**. It is wrong to introduce meaningless aliases like
        `IncomingOrdersList` instead of using `IncomingOrder[]`.
-   Constants. All contants are defined in `ts` files under `constants` folder. These files contains only data and
    markup-unaware. I.e. it is totally wrong to put there `tsx` files with `JSX` markup inside. Think twice if you
    want to do it.
-   All components must be named meaningfully.
    -   Bad: `Selector`
    -   Good: `RejectionTimeDropdown`
-   API and data querying methods should never be called directly. Hooks like must be used to perform data
    fetching and changing.
    -   `useApiCall<T>(fn, { args: [arg1, arg2,...] })` - used to perform immediate data fetching. Exposes data type
        ```
        { data: T, error: Error, isLoading: boolean }
        ```
        Expect api function as input and corresponding spreaded parameters to pass to the fetching function. Example:
        ```
        const { data, error, isLoading } = useAsyncFetch<Product>(getProductFromHasura, product_1)
        ```
    -   `useExplicitApiCall<T>` -
        ```
        { data: T, error: Error, isLoading: boolean, execute: (...args) => void }
        ```
        Expect api function as input and corresponding spreaded parameters to pass to the fetching function.
        Also exposes function `execute` which allows to run api call. Also this function allows parameters to pass
        to execution function - it overrides arguments passed to the hook before. Example:
        ```
        const { data, error, isLoading, execute } = useAsyncFetch<Product>(updateProduct, product_1)
        <Button onClick={() => execute(product)}>
        ```
-   stateless. Each component should maintain its own state in **quite rare cases**. Usually it is enough to bind values from
    store via `useStateContext` hook. If something should be updated immediately it make sense to update it in store.
    If something should be updated only by some trigger - it can be implemented as inner component state.
-   handlers properties. Each components should have as least input handlers as possible. Usually it is better
    to perform data handlig on upper level - in parent control which promote habdlers for standard `onClick/onChange`.
    -   handler properties naming. It is recommended to use standadized handlers properties names
        like `onClick` and `onChange`.
-   isloation/decoupling. Each component must be of reasonable size and expect not more then **5-6** properties. Otherwise
    it becomes bloated and hard to understand. Also it shouldn't relies on parent too much. Short: eachcomponent
    expose interface and doesn't make any assumption how it would be used.
-   store/global state. Application uses global state build with Context and hook. It is accessible for all components
    so use it smart - if its data can e used on lower levels then do not import it on upper levels and promote
    as properties. Store doesn't use Redux so there is not asynchronous actions (like in Redux reducers are sync,
    action are async). If you need something async - rethink your approach. Application works with only one kind of
    async operations: CRUD on remoe data.
-   in-component handlers. Recommended notation is:
    ```
    const handleOrderChange = (order: Order) => { ... }
    ```
    i.e prefixed by `handle` then object that manipulated and then what kind of event handling
-   parameters passing. Unless we work with C or C++ if doesn't make sense to pass `id` or specific object from event
    handlers. Prefert to pass entire object. If you have component that show list of orders via sub-coponents list like
    `SingleOrderRow` then `SingleOrderRow` should expose property `onClick: (order: Order) => void`.
    Expose properties like `handleOrderRowClick: (orderId: number) => void` is a bad practice and would be rejected
-   destruction. Prefer use `const { dispatch } = useStateContext(); dispatch(...)` rather then
    `const context = useStateContext(); context.dispatch(...)`
-   reusability. If some controls looks very similar - create generic one and use it in many places. Code duplication
    is our second enemy after preliminary optimization.
    -   if component _looks_ like potentially reusable but it is hard to implement using `children` - do not do it
        general. Name it meaningfully according to specific usage and store under appropriate folder (not `Common`).
        Pass subcomponent in `props` generally bad idea unless parent component receive declaration to instantiate
        under the hood.
