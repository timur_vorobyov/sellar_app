# SEller app

## Commands you can use

Please use YARN as the primary build tools. Commands available:

- start: start dev server
- build: build prod-ready code
- test: run unti tests
- lint: lint and fix code issues (includes eslint, prettier and stylelint)

To run this locally, you have to copy `.env.example` to `.env` and fill with appropriate values (refer to any team member).

This app is also generated by create-react-app. Refer to [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started) for more information.

## SellerApp Spirit of contribution

### Typescript

Typescript over Javascript to leverage the safety of type system, preventing minor mistakes (wrong data type for example).

### Commit Message

Start with a corresponding task or story, after that - noun.

```
SA-17
SA-41

Add ...
Update ...
Test ...
```

### Making a PR

PR Must always start with this format:

```
[SA-xx] As a user, ...
```

Please, keep the PR size small. For the sake of the reviewer and for the sake of getting approved faster for you.

Commit early and often.

Do not lump many stories into 1 as it will defeat the purpose of splitting the task in the first place.
Such PR will be REJECTED.

### Approving a PR

Only approve PR with Successful build, and merge can only be done by default reviewer.

### Test, test, test

Do not send a PR without adequate test. You can leverage the pipeline to give you an idea of the coverage and what
you missed (check on Sonarcloud), but a PR without enough test will be rejected.

When you write your test, think from customer or the user's perspective. Since we use Jest, leverage the DSL to write BDD style tests.

### Folder Structure

Please follow this for folder structure:

```
src/
  components/
    your-component/
      your-component.tsx
      your-component.test.tsx
```

### Deployment

Deployment is done via Bitbucket pipeline, triggered from a merge to master branch (deploy to develop).

To deploy to Production, you will need to create a release/ branch with a proper version branching out from Master. Production deployment will be manually triggered from bitbucket pipeline.

# Create React App

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
